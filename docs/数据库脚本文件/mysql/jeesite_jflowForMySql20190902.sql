-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 192.168.0.100    Database: jeesite_jflow
-- ------------------------------------------------------
-- Server version	5.5.62-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cn_city`
--

DROP TABLE IF EXISTS `cn_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cn_city` (
  `No` varchar(50) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名',
  `Grade` int(11) DEFAULT '0' COMMENT 'Grade',
  `FK_SF` varchar(100) DEFAULT NULL COMMENT '省份',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区',
  `PinYin` varchar(200) DEFAULT NULL COMMENT '搜索拼音',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_city`
--

LOCK TABLES `cn_city` WRITE;
/*!40000 ALTER TABLE `cn_city` DISABLE KEYS */;
INSERT INTO `cn_city` VALUES ('1101','北京市东城区','东城',2,'11','AA',NULL),('1102','北京市西城区','西城',2,'11','AA',NULL),('1103','北京市崇文区','崇文',2,'11','AA',NULL),('1104','北京市宣武区','宣武',2,'11','AA',NULL),('1105','北京市朝阳区','朝阳',2,'11','AA',NULL),('1106','北京市丰台区','丰台',2,'11','AA',NULL),('1107','北京市石景山区','石景山',2,'11','AA',NULL),('1108','北京市海淀区','海淀',2,'11','AA',NULL),('1109','北京市门头沟区','门头沟',2,'11','AA',NULL),('1111','北京市房山区','房山',2,'11','AA',NULL),('1112','北京市通州区','通州',2,'11','AA',NULL),('1113','北京市顺义区','顺义',2,'11','AA',NULL),('1114','北京市昌平区','昌平',2,'11','AA',NULL),('1115','北京市大兴区','大兴',2,'11','AA',NULL),('1116','北京市怀柔区','怀柔',2,'11','AA',NULL),('1117','北京市平谷区','平谷',2,'11','AA',NULL),('1128','北京市密云县','密云',2,'11','AA',NULL),('1129','北京市延庆县','延庆',2,'11','AA',NULL),('1201','天津市和平区','和平区',2,'12','AA',NULL),('1202','天津市河东区','河东区',2,'12','AA',NULL),('1203','天津市河西区','河西区',2,'12','AA',NULL),('1204','天津市南开区','南开区',2,'12','AA',NULL),('1205','天津市河北区','河北区',2,'12','AA',NULL),('1206','天津市红桥区','红桥区',2,'12','AA',NULL),('1207','天津市塘沽区','塘沽区',2,'12','AA',NULL),('1208','天津市汉沽区','汉沽区',2,'12','AA',NULL),('1209','天津市大港区','大港区',2,'12','AA',NULL),('1210','天津市东丽区','东丽区',2,'12','AA',NULL),('1211','天津市西青区','西青区',2,'12','AA',NULL),('1212','天津市津南区','津南区',2,'12','AA',NULL),('1213','天津市北辰区','北辰区',2,'12','AA',NULL),('1214','天津市武清区','武清区',2,'12','AA',NULL),('1215','天津市宝坻区','宝坻区',2,'12','AA',NULL),('1221','天津市宁河县','宁河区',2,'12','AA',NULL),('1223','天津市静海县','静海区',2,'12','AA',NULL),('1225','天津市蓟县','蓟县',2,'12','AA',NULL),('1301','河北省石家庄市','石家庄',2,'13','HB',NULL),('1302','河北省唐山市','唐山',2,'13','HB',NULL),('1303','河北省秦皇岛市','秦皇岛',2,'13','HB',NULL),('1304','河北省邯郸市','邯郸',2,'13','HB',NULL),('1305','河北省邢台市','邢台',2,'13','HB',NULL),('1306','河北省保定市','保定',2,'13','HB',NULL),('1307','河北省张家口市','张家口',2,'13','HB',NULL),('1308','河北省承德市','承德',2,'13','HB',NULL),('1309','河北省沧州市','沧州',2,'13','HB',NULL),('1310','河北省廊坊市','廊坊',2,'13','HB',NULL),('1311','河北省衡水市','衡水',2,'13','HB',NULL),('1401','山西省太原市','太原',2,'14','HB',NULL),('1402','山西省大同市','大同',2,'14','HB',NULL),('1403','山西省阳泉市','阳泉',2,'14','HB',NULL),('1404','山西省长治市','长治',2,'14','HB',NULL),('1405','山西省晋城市','晋城',2,'14','HB',NULL),('1406','山西省朔州市','朔州',2,'14','HB',NULL),('1407','山西省晋中市','晋中',2,'14','HB',NULL),('1408','山西省运城市','运城',2,'14','HB',NULL),('1409','山西省忻州市','忻州',2,'14','HB',NULL),('1410','山西省临汾市','临汾',2,'14','HB',NULL),('1411','山西省吕梁市','吕梁',2,'14','HB',NULL),('1412','山西省临汾市尧都区','临汾尧都区',2,'14','HB',NULL),('1501','内蒙古呼和浩特市','呼和浩特',2,'15','HB',NULL),('1502','内蒙古包头市','包头',2,'15','HB',NULL),('1503','内蒙古乌海市','乌海',2,'15','HB',NULL),('1504','内蒙古赤峰市','赤峰',2,'15','HB',NULL),('1505','内蒙古通辽市','通辽',2,'15','HB',NULL),('1506','内蒙古鄂尔多斯市','鄂尔多斯',2,'15','HB',NULL),('1507','内蒙古呼伦贝尔市','呼伦贝尔',2,'15','HB',NULL),('1508','内蒙古巴彦淖尔市','巴彦淖尔',2,'15','HB',NULL),('1509','内蒙古乌兰察布市','乌兰察布',2,'15','HB',NULL),('1522','内蒙古兴安盟','兴安盟',2,'15','HB',NULL),('1525','内蒙古锡林郭勒盟','锡林郭勒盟',2,'15','HB',NULL),('1526','内蒙古乌兰察布盟','乌兰察布盟',2,'15','HB',NULL),('1529','内蒙古阿拉善盟','阿拉善盟',2,'15','HB',NULL),('2101','辽宁省沈阳市','沈阳',2,'21','DB',NULL),('2102','辽宁省大连市','大连',2,'21','DB',NULL),('2103','辽宁省鞍山市','鞍山',2,'21','DB',NULL),('2104','辽宁省抚顺市','抚顺',2,'21','DB',NULL),('2105','辽宁省本溪市','本溪',2,'21','DB',NULL),('2106','辽宁省丹东市','丹东',2,'21','DB',NULL),('2107','辽宁省锦州市','锦州',2,'21','DB',NULL),('2108','辽宁省营口市','营口',2,'21','DB',NULL),('2109','辽宁省阜新市','阜新',2,'21','DB',NULL),('2110','辽宁省辽阳市','辽阳',2,'21','DB',NULL),('2111','辽宁省盘锦市','盘锦市',2,'21','DB',NULL),('2112','辽宁省铁岭市','铁岭',2,'21','DB',NULL),('2113','辽宁省朝阳市','朝阳',2,'21','DB',NULL),('2114','辽宁省葫芦岛市','葫芦岛市',2,'21','DB',NULL),('2201','吉林省长春市','长春',2,'22','DB',NULL),('2202','吉林省吉林市','吉林',2,'22','DB',NULL),('2203','吉林省四平市','四平',2,'22','DB',NULL),('2204','吉林省辽源市','辽源',2,'22','DB',NULL),('2205','吉林省通化市','通化',2,'22','DB',NULL),('2206','吉林省白山市','白山',2,'22','DB',NULL),('2207','吉林省松原市','松原',2,'22','DB',NULL),('2208','吉林省白城市','白城',2,'22','DB',NULL),('2224','吉林省延边朝鲜族自治州','延边',2,'22','DB',NULL),('2301','黑龙江省哈尔滨市','哈尔滨',2,'23','DB',NULL),('2302','黑龙江省齐齐哈尔市','齐齐哈尔',2,'23','DB',NULL),('2303','黑龙江省鸡西市','鸡西',2,'23','DB',NULL),('2304','黑龙江省鹤岗市','鹤岗',2,'23','DB',NULL),('2305','黑龙江省双鸭山市','双鸭山',2,'23','DB',NULL),('2306','黑龙江省大庆市','大庆',2,'23','DB',NULL),('2307','黑龙江省伊春市','伊春',2,'23','DB',NULL),('2308','黑龙江省佳木斯市','佳木斯',2,'23','DB',NULL),('2309','黑龙江省七台河市','七台河',2,'23','DB',NULL),('2310','黑龙江省牡丹江市','牡丹江',2,'23','DB',NULL),('2311','黑龙江省黑河市','黑河',2,'23','DB',NULL),('2312','黑龙江省绥化市','绥化',2,'23','DB',NULL),('2327','黑龙江省大兴安岭地区','大兴安岭地区',2,'23','DB',NULL),('3101','上海市黄浦区','黄浦区',2,'31','AA',NULL),('3103','上海市卢湾区','卢湾区',2,'31','AA',NULL),('3104','上海市徐汇区','徐汇区',2,'31','AA',NULL),('3105','上海市长宁区','长宁区',2,'31','AA',NULL),('3106','上海市静安区','静安区',2,'31','AA',NULL),('3107','上海市普陀区','普陀区',2,'31','AA',NULL),('3108','上海市闸北区','闸北区',2,'31','AA',NULL),('3109','上海市虹口区','虹口区',2,'31','AA',NULL),('3110','上海市杨浦区','杨浦区',2,'31','AA',NULL),('3112','上海市闵行区','闵行区',2,'31','AA',NULL),('3113','上海市宝山区','宝山区',2,'31','AA',NULL),('3114','上海市嘉定区','嘉定区',2,'31','AA',NULL),('3115','上海市浦东新区','浦东新区',2,'31','AA',NULL),('3116','上海市金山区','金山区',2,'31','AA',NULL),('3117','上海市松江区','松江区',2,'31','AA',NULL),('3118','上海市青浦区','青浦区',2,'31','AA',NULL),('3119','上海市南汇区','南汇区',2,'31','AA',NULL),('3120','上海市奉贤区','奉贤区',2,'31','AA',NULL),('3130','上海市崇明县','崇明县',2,'31','AA',NULL),('3201','江苏省南京市','南京',2,'32','HD',NULL),('3202','江苏省无锡市','无锡',2,'32','HD',NULL),('3203','江苏省徐州市','徐州',2,'32','HD',NULL),('3204','江苏省常州市','常州',2,'32','HD',NULL),('3205','江苏省苏州市','苏州',2,'32','HD',NULL),('3206','江苏省南通市','南通',2,'32','HD',NULL),('3207','江苏省连云港市','连云港',2,'32','HD',NULL),('3208','江苏省淮安市','淮安',2,'32','HD',NULL),('3209','江苏省盐城市','盐城',2,'32','HD',NULL),('3210','江苏省扬州市','扬州',2,'32','HD',NULL),('3211','江苏省镇江市','镇江',2,'32','HD',NULL),('3212','江苏省泰州市','泰州',2,'32','HD',NULL),('3213','江苏省宿迁市','宿迁',2,'32','HD',NULL),('3301','浙江省杭州市','杭州',2,'33','HD',NULL),('3302','浙江省宁波市','宁波',2,'33','HD',NULL),('3303','浙江省温州市','温州',2,'33','HD',NULL),('3304','浙江省嘉兴市','嘉兴',2,'33','HD',NULL),('3305','浙江省湖州市','湖州',2,'33','HD',NULL),('3306','浙江省绍兴市','绍兴',2,'33','HD',NULL),('3307','浙江省金华市','金华',2,'33','HD',NULL),('3308','浙江省衢州市','衢州',2,'33','HD',NULL),('3309','浙江省舟山市','舟山',2,'33','HD',NULL),('3310','浙江省台州市','台州',2,'33','HD',NULL),('3311','浙江省丽水市','丽水',2,'33','HD',NULL),('3401','安徽省合肥市','合肥',2,'34','HD',NULL),('3402','安徽省芜湖市','芜湖',2,'34','HD',NULL),('3403','安徽省蚌埠市','蚌埠',2,'34','HD',NULL),('3404','安徽省淮南市','淮南',2,'34','HD',NULL),('3405','安徽省马鞍山市','马鞍山',2,'34','HD',NULL),('3406','安徽省淮北市','淮北',2,'34','HD',NULL),('3407','安徽省铜陵市','铜陵',2,'34','HD',NULL),('3408','安徽省安庆市','安庆',2,'34','HD',NULL),('3410','安徽省黄山市','黄山',2,'34','HD',NULL),('3411','安徽省滁州市','滁州',2,'34','HD',NULL),('3412','安徽省阜阳市','阜阳',2,'34','HD',NULL),('3413','安徽省宿州市','宿州',2,'34','HD',NULL),('3414','安徽省巢湖市','巢湖',2,'34','HD',NULL),('3415','安徽省六安市','六安',2,'34','HD',NULL),('3416','安徽省亳州市','亳州',2,'34','HD',NULL),('3417','安徽省池州市','池州',2,'34','HD',NULL),('3418','安徽省宣城市','宣城',2,'34','HD',NULL),('3501','福建省福州市','福州',2,'35','HD',NULL),('3502','福建省厦门市','厦门',2,'35','HD',NULL),('3503','福建省莆田市','莆田',2,'35','HD',NULL),('3504','福建省三明市','三明',2,'35','HD',NULL),('3505','福建省泉州市','泉州',2,'35','HD',NULL),('3506','福建省漳州市','漳州',2,'35','HD',NULL),('3507','福建省南平市','南平',2,'35','HD',NULL),('3508','福建省龙岩市','龙岩',2,'35','HD',NULL),('3509','福建省宁德市','宁德',2,'35','HD',NULL),('3601','江西省南昌市','南昌',2,'36','HD',NULL),('3602','江西省景德镇市','景德镇',2,'36','HD',NULL),('3603','江西省萍乡市','萍乡',2,'36','HD',NULL),('3604','江西省九江市','九江',2,'36','HD',NULL),('3605','江西省新余市','新余',2,'36','HD',NULL),('3606','江西省鹰潭市','鹰潭',2,'36','HD',NULL),('3607','江西省赣州市','赣州',2,'36','HD',NULL),('3608','江西省吉安市','吉安',2,'36','HD',NULL),('3609','江西省宜春市','宜春',2,'36','HD',NULL),('3610','江西省抚州市','抚州',2,'36','HD',NULL),('3611','江西省上饶市','上饶',2,'36','HD',NULL),('3701','山东省济南市','济南',2,'37','HD',NULL),('3702','山东省青岛市','青岛',2,'37','HD',NULL),('3703','山东省淄博市','淄博',2,'37','HD',NULL),('3704','山东省枣庄市','枣庄',2,'37','HD',NULL),('3705','山东省东营市','东营',2,'37','HD',NULL),('3706','山东省烟台市','烟台',2,'37','HD',NULL),('3707','山东省潍坊市','潍坊',2,'37','HD',NULL),('3708','山东省济宁市','济宁',2,'37','HD',NULL),('3709','山东省泰安市','泰安',2,'37','HD',NULL),('3710','山东省威海市','威海',2,'37','HD',NULL),('3711','山东省日照市','日照',2,'37','HD',NULL),('3712','山东省莱芜市','莱芜',2,'37','HD',NULL),('3713','山东省临沂市','临沂',2,'37','HD',NULL),('3714','山东省德州市','德州',2,'37','HD',NULL),('3715','山东省聊城市','聊城',2,'37','HD',NULL),('3716','山东省滨州市','滨州',2,'37','HD',NULL),('3717','山东省荷泽市','荷泽',2,'37','HD',NULL),('4101','河南省郑州市','郑州',2,'41','ZN',NULL),('4102','河南省开封市','开封',2,'41','ZN',NULL),('4103','河南省洛阳市','洛阳',2,'41','ZN',NULL),('4104','河南省平顶山市','平顶山',2,'41','ZN',NULL),('4105','河南省安阳市','安阳',2,'41','ZN',NULL),('4106','河南省鹤壁市','鹤壁',2,'41','ZN',NULL),('4107','河南省新乡市','新乡',2,'41','ZN',NULL),('4108','河南省焦作市','焦作',2,'41','ZN',NULL),('4109','河南省濮阳市','濮阳',2,'41','ZN',NULL),('4110','河南省许昌市','许昌',2,'41','ZN',NULL),('4111','河南省漯河市','漯河',2,'41','ZN',NULL),('4112','河南省三门峡市','三门峡',2,'41','ZN',NULL),('4113','河南省南阳市','南阳',2,'41','ZN',NULL),('4114','河南省商丘市','商丘',2,'41','ZN',NULL),('4115','河南省信阳市','信阳',2,'41','ZN',NULL),('4116','河南省周口市','周口',2,'41','ZN',NULL),('4117','河南省驻马店市','驻马店',2,'41','ZN',NULL),('4201','湖北省武汉市','武汉',2,'42','',NULL),('4202','湖北省黄石市','黄石',2,'42','',NULL),('4203','湖北省十堰市','十堰',2,'42','',NULL),('4205','湖北省宜昌市','宜昌',2,'42','',NULL),('4206','湖北省襄樊市','襄樊',2,'42','',NULL),('4207','湖北省鄂州市','鄂州',2,'42','',NULL),('4208','湖北省荆门市','荆门',2,'42','',NULL),('4209','湖北省孝感市','孝感',2,'42','',NULL),('4210','湖北省荆州市','荆州',2,'42','',NULL),('4211','湖北省荆州市市辖区','荆州辖区',2,'42','',NULL),('4212','湖北省咸宁市','咸宁',2,'42','',NULL),('4213','湖北省随州市','随州',2,'42','',NULL),('4228','湖北省恩施土家族苗族自治州','恩施',2,'42','',NULL),('4290','湖北省省直辖行政单位','直辖行政单位',2,'42','',NULL),('4294','湖北省仙桃市','仙桃',2,'42','',NULL),('4295','湖北省潜江市','潜江',2,'42','',NULL),('4296','湖北省天门市','天门',2,'42','',NULL),('4301','湖南省长沙市','长沙',2,'43','ZN',NULL),('4302','湖南省株洲市','株洲',2,'43','ZN',NULL),('4303','湖南省湘潭市','湘潭',2,'43','ZN',NULL),('4304','湖南省衡阳市','衡阳',2,'43','ZN',NULL),('4305','湖南省邵阳市','邵阳',2,'43','ZN',NULL),('4306','湖南省岳阳市','岳阳',2,'43','ZN',NULL),('4307','湖南省常德市','常德',2,'43','ZN',NULL),('4308','湖南省张家界市','张家界',2,'43','ZN',NULL),('4309','湖南省益阳市','益阳',2,'43','ZN',NULL),('4310','湖南省郴州市','郴州',2,'43','ZN',NULL),('4311','湖南省永州市','永州',2,'43','ZN',NULL),('4312','湖南省郴州市北湖区','郴州北湖区',2,'43','ZN',NULL),('4313','湖南省郴州市苏仙区','郴州苏仙区',2,'43','ZN',NULL),('4331','湖南省湘西土家族苗族自治州','湘西',2,'43','ZN',NULL),('4401','广东省广州市','广州',2,'44','ZN',NULL),('4402','广东省韶关市','韶关',2,'44','ZN',NULL),('4403','广东省深圳市','深圳',2,'44','ZN',NULL),('4404','广东省珠海市','珠海',2,'44','ZN',NULL),('4405','广东省汕头市','汕头',2,'44','ZN',NULL),('4406','广东省佛山市','佛山',2,'44','ZN',NULL),('4407','广东省江门市','江门',2,'44','ZN',NULL),('4408','广东省湛江市','湛江',2,'44','ZN',NULL),('4409','广东省茂名市','茂名',2,'44','ZN',NULL),('4412','广东省肇庆市','肇庆',2,'44','ZN',NULL),('4413','广东省惠州市','惠州',2,'44','ZN',NULL),('4414','广东省梅州市','梅州',2,'44','ZN',NULL),('4415','广东省汕尾市','汕尾',2,'44','ZN',NULL),('4416','广东省河源市','河源',2,'44','ZN',NULL),('4417','广东省阳江市','阳江',2,'44','ZN',NULL),('4418','广东省清远市','清远',2,'44','ZN',NULL),('4419','广东省东莞市','东莞',2,'44','ZN',NULL),('4420','广东省中山市','中山',2,'44','ZN',NULL),('4451','广东省潮州市','潮州',2,'44','ZN',NULL),('4452','广东省揭阳市','揭阳',2,'44','ZN',NULL),('4453','广东省云浮市','云浮',2,'44','ZN',NULL),('4501','广西南宁市','南宁市',2,'45','ZN',NULL),('4502','广西柳州市','柳州市',2,'45','ZN',NULL),('4503','广西桂林市','桂林市',2,'45','ZN',NULL),('4504','广西梧州市','梧州市',2,'45','ZN',NULL),('4505','广西北海市','北海市',2,'45','ZN',NULL),('4506','广西防城港市','城港',2,'45','ZN',NULL),('4507','广西钦州市','钦州市',2,'45','ZN',NULL),('4508','广西贵港市','贵港市',2,'45','ZN',NULL),('4509','广西玉林市','玉林市',2,'45','ZN',NULL),('4510','广西百色市','百色市',2,'45','ZN',NULL),('4511','广西贺州市','贺州市',2,'45','ZN',NULL),('4512','广西河池市','河池市',2,'45','ZN',NULL),('4513','广西来宾市','来宾市',2,'45','ZN',NULL),('4514','广西崇左市','崇左市',2,'45','ZN',NULL),('4601','海南省海口市','海口',2,'46','ZN',NULL),('4602','海南省三亚市','三亚',2,'46','ZN',NULL),('4691','海南省五指山市','五指山',2,'46','ZN',NULL),('4692','海南省琼海市','琼海',2,'46','ZN',NULL),('4693','海南省儋州市','儋州',2,'46','ZN',NULL),('4695','海南省文昌市','文昌',2,'46','ZN',NULL),('4696','海南省万宁市','万宁',2,'46','ZN',NULL),('4697','海南省东方市','东方',2,'46','ZN',NULL),('5001','重庆市万州区','万州区',2,'50','AA',NULL),('5002','重庆市涪陵区','涪陵区',2,'50','AA',NULL),('5003','重庆市渝中区','渝中区',2,'50','AA',NULL),('5004','重庆市大渡口区','大渡口区',2,'50','AA',NULL),('5005','重庆市江北区','江北区',2,'50','AA',NULL),('5006','重庆市沙坪坝区','沙坪坝区',2,'50','AA',NULL),('5007','重庆市九龙坡区','九龙坡区',2,'50','AA',NULL),('5008','重庆市南岸区','南岸区',2,'50','AA',NULL),('5009','重庆市北碚区','北碚区',2,'50','AA',NULL),('5010','重庆市万盛区','万盛区',2,'50','AA',NULL),('5011','重庆市双桥区','双桥区',2,'50','AA',NULL),('5012','重庆市渝北区','渝北区',2,'50','AA',NULL),('5013','重庆市巴南区','巴南区',2,'50','AA',NULL),('5014','重庆市黔江区','黔江区',2,'50','AA',NULL),('5015','重庆市长寿区','长寿区',2,'50','AA',NULL),('5022','重庆市綦江县','綦江县',2,'50','AA',NULL),('5023','重庆市潼南县','潼南县',2,'50','AA',NULL),('5024','重庆市铜梁县','铜梁县',2,'50','AA',NULL),('5025','重庆市大足县','大足县',2,'50','AA',NULL),('5026','重庆市荣昌县','荣昌县',2,'50','AA',NULL),('5027','重庆市璧山县','璧山县',2,'50','AA',NULL),('5028','重庆市梁平县','梁平县',2,'50','AA',NULL),('5029','重庆市城口县','城口县',2,'50','AA',NULL),('5030','重庆市丰都县','丰都县',2,'50','AA',NULL),('5031','重庆市垫江县','垫江县',2,'50','AA',NULL),('5032','重庆市武隆县','武隆县',2,'50','AA',NULL),('5033','重庆市忠县','市忠县',2,'50','AA',NULL),('5034','重庆市开县','市开县',2,'50','AA',NULL),('5035','重庆市云阳县','云阳县',2,'50','AA',NULL),('5036','重庆市奉节县','奉节县',2,'50','AA',NULL),('5037','重庆市巫山县','巫山县',2,'50','AA',NULL),('5038','重庆市巫溪县','巫溪县',2,'50','AA',NULL),('5040','重庆市石柱土家族自治县','石柱',2,'50','AA',NULL),('5041','重庆市秀山土家族苗族自治县','秀山',2,'50','AA',NULL),('5042','重庆市酉阳土家族苗族自治县','酉阳',2,'50','AA',NULL),('5043','重庆市彭水苗族土家族自治县','彭水',2,'50','AA',NULL),('5081','重庆市江津市','江津市',2,'50','AA',NULL),('5082','重庆市合川市','合川市',2,'50','AA',NULL),('5083','重庆市永川市','永川市',2,'50','AA',NULL),('5084','重庆市南川市','南川市',2,'50','AA',NULL),('5101','四川省成都市','成都',2,'51','XN',NULL),('5103','四川省自贡市','自贡',2,'51','XN',NULL),('5104','四川省攀枝花市','攀枝花',2,'51','XN',NULL),('5105','四川省泸州市','泸州',2,'51','XN',NULL),('5106','四川省德阳市','德阳',2,'51','XN',NULL),('5107','四川省绵阳市','绵阳',2,'51','XN',NULL),('5108','四川省广元市','广元',2,'51','XN',NULL),('5109','四川省遂宁市','遂宁',2,'51','XN',NULL),('5110','四川省内江市','内江',2,'51','XN',NULL),('5111','四川省内江市市辖区','内江辖区',2,'51','XN',NULL),('5112','四川省内江市市中区','内江中区',2,'51','XN',NULL),('5113','四川省南充市','南充',2,'51','XN',NULL),('5114','四川省眉山市','眉山',2,'51','XN',NULL),('5115','四川省宜宾市','宜宾',2,'51','XN',NULL),('5116','四川省广安市','广安',2,'51','XN',NULL),('5117','四川省达州市','达州',2,'51','XN',NULL),('5118','四川省雅安市','雅安',2,'51','XN',NULL),('5119','四川省巴中市','巴中',2,'51','XN',NULL),('5120','四川省资阳市','资阳',2,'51','XN',NULL),('5121','四川省资阳市市辖区','资阳辖区',2,'51','XN',NULL),('5122','四川省资阳市雁江区','资阳雁江区',2,'51','XN',NULL),('5132','四川省阿坝藏族羌族自治州','阿坝',2,'51','XN',NULL),('5133','四川省甘孜藏族自治州','甘孜',2,'51','XN',NULL),('5134','四川省凉山彝族自治州','凉山',2,'51','XN',NULL),('5201','贵州省贵阳市','贵阳',2,'52','XN',NULL),('5202','贵州省六盘水市','六盘水',2,'52','XN',NULL),('5203','贵州省遵义市','遵义',2,'52','XN',NULL),('5204','贵州省安顺市','安顺',2,'52','XN',NULL),('5222','贵州省铜仁地区','铜仁地区',2,'52','XN',NULL),('5223','贵州省黔西南布依族苗族自治州','黔西南',2,'52','XN',NULL),('5224','贵州省毕节地区','毕节地区',2,'52','XN',NULL),('5226','贵州省黔东南苗族侗族自治州','黔东南',2,'52','XN',NULL),('5227','贵州省黔南布依族苗族自治州','黔南',2,'52','XN',NULL),('5301','云南省昆明市','昆明',2,'53','XN',NULL),('5303','云南省曲靖市','曲靖',2,'53','XN',NULL),('5304','云南省玉溪市','玉溪',2,'53','XN',NULL),('5305','云南省保山市','保山',2,'53','XN',NULL),('5306','云南省昭通市','昭通',2,'53','XN',NULL),('5307','云南省丽江市','丽江',2,'53','XN',NULL),('5308','云南省思茅市','思茅',2,'53','XN',NULL),('5309','云南省临沧市','临沧',2,'53','XN',NULL),('5323','云南省楚雄彝族自治州','楚雄',2,'53','XN',NULL),('5325','云南省红河哈尼族彝族自治州','红河',2,'53','XN',NULL),('5326','云南省文山壮族苗族自治州','文山',2,'53','XN',NULL),('5328','云南省西双版纳傣族自治州','西双版纳',2,'53','XN',NULL),('5329','云南省大理白族自治州','大理',2,'53','XN',NULL),('5331','云南省德宏傣族景颇族自治州','德宏',2,'53','XN',NULL),('5333','云南省怒江僳僳族自治州','怒江僳',2,'53','XN',NULL),('5334','云南省迪庆藏族自治州','迪庆',2,'53','XN',NULL),('5401','西藏拉萨市','拉萨',2,'54','XN',NULL),('5421','西藏昌都地区','昌都',2,'54','XN',NULL),('5422','西藏山南地区','山南',2,'54','XN',NULL),('5423','西藏日喀则地区','日喀则',2,'54','XN',NULL),('5424','西藏那曲地区','那曲',2,'54','XN',NULL),('5425','西藏阿里地区','阿里',2,'54','XN',NULL),('5426','西藏林芝地区','林芝',2,'54','XN',NULL),('6101','陕西省西安市','西安',2,'61','XB',NULL),('6102','陕西省铜川市','铜川',2,'61','XB',NULL),('6103','陕西省宝鸡市','宝鸡',2,'61','XB',NULL),('6104','陕西省咸阳市','咸阳',2,'61','XB',NULL),('6105','陕西省渭南市','渭南',2,'61','XB',NULL),('6106','陕西省延安市','延安',2,'61','XB',NULL),('6107','陕西省汉中市','汉中',2,'61','XB',NULL),('6108','陕西省榆林市','榆林',2,'61','XB',NULL),('6109','陕西省安康市','安康',2,'61','XB',NULL),('6110','陕西省商洛市','商洛',2,'61','XB',NULL),('6201','甘肃省兰州市','兰州',2,'62','XB',NULL),('6202','甘肃省嘉峪关市','嘉峪关',2,'62','XB',NULL),('6203','甘肃省金昌市','金昌',2,'62','XB',NULL),('6204','甘肃省白银市','白银',2,'62','XB',NULL),('6205','甘肃省天水市','天水',2,'62','XB',NULL),('6206','甘肃省武威市','武威',2,'62','XB',NULL),('6207','甘肃省张掖市','张掖',2,'62','XB',NULL),('6208','甘肃省平凉市','平凉',2,'62','XB',NULL),('6209','甘肃省酒泉市','酒泉',2,'62','XB',NULL),('6210','甘肃省庆阳市','庆阳',2,'62','XB',NULL),('6211','甘肃省庆阳市市辖区','庆阳辖区',2,'62','XB',NULL),('6212','甘肃省陇南市','陇南',2,'62','XB',NULL),('6229','甘肃省临夏回族自治州','临夏',2,'62','XB',NULL),('6230','甘肃省甘南藏族自治州','甘南',2,'62','XB',NULL),('6231','甘肃省合作市','合作',2,'62','XB',NULL),('6301','青海省西宁市','西宁',2,'63','XB',NULL),('6321','青海省海东地区','海东',2,'63','XB',NULL),('6322','青海省海北藏族自治州','海北州',2,'63','XB',NULL),('6323','青海省黄南藏族自治州','黄南州',2,'63','XB',NULL),('6325','青海省海南藏族自治州','海南州',2,'63','XB',NULL),('6326','青海省果洛藏族自治州','果洛州',2,'63','XB',NULL),('6327','青海省玉树藏族自治州','玉树州',2,'63','XB',NULL),('6328','青海省海西蒙古族藏族自治州','海西州',2,'63','XB',NULL),('6401','宁夏银川市','银川市',2,'64','XB',NULL),('6402','宁夏石嘴山市','嘴山',2,'64','XB',NULL),('6403','宁夏吴忠市','吴忠市',2,'64','XB',NULL),('6404','宁夏固原市','固原市',2,'64','XB',NULL),('6405','宁夏中卫市','中卫市',2,'64','XB',NULL),('6501','新疆乌鲁木齐市','乌鲁木齐',2,'65','XB',NULL),('6502','新疆克拉玛依市','克拉玛依',2,'65','XB',NULL),('6521','新疆克拉玛依市吐鲁番地区','吐鲁番',2,'65','XB',NULL),('6522','新疆哈密地区','哈密',2,'65','XB',NULL),('6523','新疆昌吉回族自治州','昌吉',2,'65','XB',NULL),('6527','新疆博尔塔拉蒙古自治州','博尔塔拉州',2,'65','XB',NULL),('6528','新疆巴音郭楞蒙古自治州','巴音郭楞州',2,'65','XB',NULL),('6529','新疆阿克苏地区','阿克苏',2,'65','XB',NULL),('6530','新疆克孜勒苏柯尔克孜自治州','克孜勒苏柯尔克孜州',2,'65','XB',NULL),('6531','新疆喀什地区','喀什',2,'65','XB',NULL),('6532','新疆和田地区','和田',2,'65','XB',NULL),('6540','新疆伊宁市','伊宁',2,'65','XB',NULL),('6542','新疆塔城地区','塔城',2,'65','XB',NULL),('6543','新疆奎屯市','奎屯市',2,'65','XB',NULL),('6591','新疆石河子市','石河子',2,'65','XB',NULL),('6592','新疆阿拉尔市','阿拉尔',2,'65','XB',NULL),('6593','新疆图木舒克市','图木舒克',2,'65','XB',NULL),('6594','新疆五家渠市','五家渠',2,'65','XB',NULL);
/*!40000 ALTER TABLE `cn_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cn_pq`
--

DROP TABLE IF EXISTS `cn_pq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cn_pq` (
  `No` varchar(50) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_pq`
--

LOCK TABLES `cn_pq` WRITE;
/*!40000 ALTER TABLE `cn_pq` DISABLE KEYS */;
INSERT INTO `cn_pq` VALUES ('AA','城市'),('DB','东北'),('HB','华北'),('HD','华东'),('XB','西北'),('XN','西南'),('ZN','中南'),('ZZ','香澳台');
/*!40000 ALTER TABLE `cn_pq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cn_sf`
--

DROP TABLE IF EXISTS `cn_sf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cn_sf` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名称',
  `JC` varchar(50) DEFAULT NULL COMMENT '简称',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_sf`
--

LOCK TABLES `cn_sf` WRITE;
/*!40000 ALTER TABLE `cn_sf` DISABLE KEYS */;
INSERT INTO `cn_sf` VALUES ('11','北京','北京市','京','AA'),('12','天津','天津市','津','AA'),('13','河北','河北省','冀','HB'),('14','山西','山西省','晋','HB'),('15','内蒙','内蒙古自治区','蒙','HB'),('21','辽宁','辽宁省','辽','DB'),('22','吉林','吉林省','吉','DB'),('23','黑龙江','黑龙江省','黑','DB'),('31','上海','上海市','沪','AA'),('32','江苏','江苏省','苏','HD'),('33','浙江','浙江省','浙','HD'),('34','安徽','安徽省','皖','HD'),('35','福建','福建省','闽','HD'),('36','江西','江西省','赣','HD'),('37','山东','山东省','鲁','HD'),('41','河南','河南省','豫','ZN'),('42','湖北','湖北省','鄂','ZN'),('43','湖南','湖南省','湘','ZN'),('44','广东','广东省','粤','ZN'),('45','广西','广西壮族自治区','桂','ZN'),('46','海南','海南省','琼','ZN'),('50','重庆','重庆市','渝','AA'),('51','四川','四川省','川','XN'),('52','贵州','贵州省','贵','XN'),('53','云南','云南省','云','XN'),('54','西藏','西藏自治区','藏','XN'),('61','陕西','陕西省','陕','XB'),('62','甘肃','甘肃省','甘','XB'),('63','青海','青海省','青','XB'),('64','宁夏','宁夏回族自治区','宁','XB'),('65','新疆','新疆维吾尔自治区','新','XB'),('71','台湾','台湾省','台','ZZ'),('81','香港','香港特别行政区','港','ZZ'),('82','澳门','澳门特别行政区','澳','ZZ');
/*!40000 ALTER TABLE `cn_sf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_app`
--

DROP TABLE IF EXISTS `gpm_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_app` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `AppModel` int(11) DEFAULT '0' COMMENT '应用类型',
  `Name` text COMMENT '名称',
  `FK_AppSort` varchar(100) DEFAULT NULL COMMENT '类别',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `Url` text COMMENT '默认连接',
  `SubUrl` text COMMENT '第二连接',
  `UidControl` varchar(100) DEFAULT NULL COMMENT '用户名控件',
  `PwdControl` varchar(100) DEFAULT NULL COMMENT '密码控件',
  `ActionType` int(11) DEFAULT '0' COMMENT '提交类型',
  `SSOType` int(11) DEFAULT '0' COMMENT '登录方式',
  `OpenWay` int(11) DEFAULT '0' COMMENT '打开方式',
  `RefMenuNo` text COMMENT '关联菜单编号',
  `AppRemark` varchar(500) DEFAULT NULL COMMENT '备注',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT 'ICON',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_app`
--

LOCK TABLES `gpm_app` WRITE;
/*!40000 ALTER TABLE `gpm_app` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_appsort`
--

DROP TABLE IF EXISTS `gpm_appsort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_appsort` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  `RefMenuNo` varchar(300) DEFAULT NULL COMMENT '关联的菜单编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_appsort`
--

LOCK TABLES `gpm_appsort` WRITE;
/*!40000 ALTER TABLE `gpm_appsort` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_appsort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_bar`
--

DROP TABLE IF EXISTS `gpm_bar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_bar` (
  `No` varchar(200) NOT NULL COMMENT '编号',
  `Name` text COMMENT '名称',
  `Title` text COMMENT '标题',
  `OpenWay` int(11) DEFAULT '0' COMMENT '打开方式',
  `IsLine` int(11) DEFAULT '0' COMMENT '是否独占一行',
  `MoreUrl` text COMMENT '更多标签Url',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_bar`
--

LOCK TABLES `gpm_bar` WRITE;
/*!40000 ALTER TABLE `gpm_bar` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_bar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_baremp`
--

DROP TABLE IF EXISTS `gpm_baremp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_baremp` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Bar` varchar(90) DEFAULT NULL COMMENT '信息块编号',
  `FK_Emp` varchar(90) DEFAULT NULL COMMENT '人员编号',
  `Title` text COMMENT '标题',
  `IsShow` int(11) DEFAULT '1' COMMENT '是否显示',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_baremp`
--

LOCK TABLES `gpm_baremp` WRITE;
/*!40000 ALTER TABLE `gpm_baremp` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_baremp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_empapp`
--

DROP TABLE IF EXISTS `gpm_empapp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_empapp` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_App` varchar(50) DEFAULT NULL COMMENT '系统',
  `Name` text COMMENT '系统-名称',
  `Url` text COMMENT '连接',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_empapp`
--

LOCK TABLES `gpm_empapp` WRITE;
/*!40000 ALTER TABLE `gpm_empapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_empapp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_empmenu`
--

DROP TABLE IF EXISTS `gpm_empmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_empmenu` (
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单',
  `FK_Emp` varchar(100) NOT NULL COMMENT '菜单功能',
  `IsChecked` int(11) DEFAULT '1' COMMENT '是否选中',
  PRIMARY KEY (`FK_Menu`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_empmenu`
--

LOCK TABLES `gpm_empmenu` WRITE;
/*!40000 ALTER TABLE `gpm_empmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_empmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_group`
--

DROP TABLE IF EXISTS `gpm_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_group` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_group`
--

LOCK TABLES `gpm_group` WRITE;
/*!40000 ALTER TABLE `gpm_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_groupemp`
--

DROP TABLE IF EXISTS `gpm_groupemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_groupemp` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员',
  PRIMARY KEY (`FK_Group`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_groupemp`
--

LOCK TABLES `gpm_groupemp` WRITE;
/*!40000 ALTER TABLE `gpm_groupemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_groupemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_groupmenu`
--

DROP TABLE IF EXISTS `gpm_groupmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_groupmenu` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组',
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单',
  `IsChecked` int(11) DEFAULT '1' COMMENT '是否选中',
  PRIMARY KEY (`FK_Group`,`FK_Menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_groupmenu`
--

LOCK TABLES `gpm_groupmenu` WRITE;
/*!40000 ALTER TABLE `gpm_groupmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_groupmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_groupstation`
--

DROP TABLE IF EXISTS `gpm_groupstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_groupstation` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位',
  PRIMARY KEY (`FK_Group`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_groupstation`
--

LOCK TABLES `gpm_groupstation` WRITE;
/*!40000 ALTER TABLE `gpm_groupstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_groupstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_menu`
--

DROP TABLE IF EXISTS `gpm_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_menu` (
  `No` varchar(4) NOT NULL COMMENT '功能编号',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `MenuType` int(11) DEFAULT '0' COMMENT '菜单类型',
  `FK_App` varchar(100) DEFAULT NULL COMMENT '系统',
  `OpenWay` int(11) DEFAULT '1' COMMENT '打开方式',
  `Url` text COMMENT '连接',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否启用?',
  `Icon` varchar(500) DEFAULT NULL COMMENT 'Icon',
  `MenuCtrlWay` int(11) DEFAULT '0' COMMENT '控制方式',
  `Flag` varchar(500) DEFAULT NULL COMMENT '标记',
  `Tag1` varchar(500) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(500) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(500) DEFAULT NULL COMMENT 'Tag3',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_menu`
--

LOCK TABLES `gpm_menu` WRITE;
/*!40000 ALTER TABLE `gpm_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_persetting`
--

DROP TABLE IF EXISTS `gpm_persetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_persetting` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(200) DEFAULT NULL COMMENT '人员',
  `FK_App` varchar(200) DEFAULT NULL COMMENT '系统',
  `UserNo` varchar(200) DEFAULT NULL COMMENT 'UserNo',
  `UserPass` varchar(200) DEFAULT NULL COMMENT 'UserPass',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_persetting`
--

LOCK TABLES `gpm_persetting` WRITE;
/*!40000 ALTER TABLE `gpm_persetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_persetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_stationmenu`
--

DROP TABLE IF EXISTS `gpm_stationmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gpm_stationmenu` (
  `FK_Station` varchar(50) NOT NULL COMMENT '岗位',
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单',
  `IsChecked` int(11) DEFAULT '1' COMMENT '是否选中',
  PRIMARY KEY (`FK_Station`,`FK_Menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_stationmenu`
--

LOCK TABLES `gpm_stationmenu` WRITE;
/*!40000 ALTER TABLE `gpm_stationmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_stationmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hy_dataincomeway`
--

DROP TABLE IF EXISTS `hy_dataincomeway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hy_dataincomeway` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `BeiZhui` text COMMENT '适应对象&优缺点',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hy_dataincomeway`
--

LOCK TABLES `hy_dataincomeway` WRITE;
/*!40000 ALTER TABLE `hy_dataincomeway` DISABLE KEYS */;
/*!40000 ALTER TABLE `hy_dataincomeway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hy_datasort`
--

DROP TABLE IF EXISTS `hy_datasort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hy_datasort` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hy_datasort`
--

LOCK TABLES `hy_datasort` WRITE;
/*!40000 ALTER TABLE `hy_datasort` DISABLE KEYS */;
/*!40000 ALTER TABLE `hy_datasort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hy_deptsort`
--

DROP TABLE IF EXISTS `hy_deptsort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hy_deptsort` (
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  `FK_Sort` varchar(100) NOT NULL COMMENT '类别',
  PRIMARY KEY (`FK_Dept`,`FK_Sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hy_deptsort`
--

LOCK TABLES `hy_deptsort` WRITE;
/*!40000 ALTER TABLE `hy_deptsort` DISABLE KEYS */;
/*!40000 ALTER TABLE `hy_deptsort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_gen_table`
--

DROP TABLE IF EXISTS `js_gen_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_gen_table` (
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `class_name` varchar(100) NOT NULL COMMENT '实体类名称',
  `comments` varchar(500) NOT NULL COMMENT '表说明',
  `parent_table_name` varchar(64) DEFAULT NULL COMMENT '关联父表的表名',
  `parent_table_fk_name` varchar(64) DEFAULT NULL COMMENT '本表关联父表的外键名',
  `data_source_name` varchar(64) DEFAULT NULL COMMENT '数据源名称',
  `tpl_category` varchar(200) DEFAULT NULL COMMENT '使用的模板',
  `package_name` varchar(500) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(200) DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(50) DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_base_dir` varchar(1000) DEFAULT NULL COMMENT '生成基础路径',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`table_name`),
  KEY `idx_gen_table_ptn` (`parent_table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_gen_table`
--

LOCK TABLES `js_gen_table` WRITE;
/*!40000 ALTER TABLE `js_gen_table` DISABLE KEYS */;
INSERT INTO `js_gen_table` VALUES ('test_data','TestData','测试数据',NULL,NULL,NULL,'crud','com.jeesite.modules','test','','测试数据','数据','ThinkGem',NULL,'{\"isHaveDelete\":\"1\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL),('test_data_child','TestDataChild','测试数据子表','test_data','test_data_id',NULL,'crud','com.jeesite.modules','test','','测试子表','数据','ThinkGem',NULL,NULL,'system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL),('test_tree','TestTree','测试树表',NULL,NULL,NULL,'treeGrid','com.jeesite.modules','test','','测试树表','数据','ThinkGem',NULL,'{\"treeViewName\":\"tree_name\",\"isHaveDelete\":\"1\",\"treeViewCode\":\"tree_code\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}','system','2018-12-03 11:21:30','system','2018-12-03 11:21:30',NULL);
/*!40000 ALTER TABLE `js_gen_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_gen_table_column`
--

DROP TABLE IF EXISTS `js_gen_table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_gen_table_column` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `column_name` varchar(64) NOT NULL COMMENT '列名',
  `column_sort` decimal(10,0) DEFAULT NULL COMMENT '列排序（升序）',
  `column_type` varchar(100) NOT NULL COMMENT '类型',
  `column_label` varchar(50) DEFAULT NULL COMMENT '列标签名',
  `comments` varchar(500) NOT NULL COMMENT '列备注说明',
  `attr_name` varchar(200) NOT NULL COMMENT '类的属性名',
  `attr_type` varchar(200) NOT NULL COMMENT '类的属性类型',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键',
  `is_null` char(1) DEFAULT NULL COMMENT '是否可为空',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否插入字段',
  `is_update` char(1) DEFAULT NULL COMMENT '是否更新字段',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段',
  `query_type` varchar(200) DEFAULT NULL COMMENT '查询方式',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段',
  `show_type` varchar(200) DEFAULT NULL COMMENT '表单类型',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  PRIMARY KEY (`id`),
  KEY `idx_gen_table_column_tn` (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成表列';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_gen_table_column`
--

LOCK TABLES `js_gen_table_column` WRITE;
/*!40000 ALTER TABLE `js_gen_table_column` DISABLE KEYS */;
INSERT INTO `js_gen_table_column` VALUES ('1069431398897889280','test_data','id',10,'varchar(64)','编号','编号','id','String','1','0','1',NULL,NULL,NULL,NULL,'1','hidden','{\"fieldValid\":\"abc\"}'),('1069431398923055104','test_data','test_input',20,'varchar(200)','单行文本','单行文本','testInput','String',NULL,'1','1','1','1','1','LIKE','1','input',NULL),('1069431398935638016','test_data','test_textarea',30,'varchar(200)','多行文本','多行文本','testTextarea','String',NULL,'1','1','1','1','1','LIKE','1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}'),('1069431398948220928','test_data','test_select',40,'varchar(10)','下拉框','下拉框','testSelect','String',NULL,'1','1','1','1','1',NULL,'1','select','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431398960803840','test_data','test_select_multiple',50,'varchar(200)','下拉多选','下拉多选','testSelectMultiple','String',NULL,'1','1','1','1','1',NULL,'1','select_multiple','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431398973386752','test_data','test_radio',60,'varchar(10)','单选框','单选框','testRadio','String',NULL,'1','1','1','1','1',NULL,'1','radio','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431398985969664','test_data','test_checkbox',70,'varchar(200)','复选框','复选框','testCheckbox','String',NULL,'1','1','1','1','1',NULL,'1','checkbox','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431398998552576','test_data','test_date',80,'datetime','日期选择','日期选择','testDate','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','date',NULL),('1069431399019524096','test_data','test_datetime',90,'datetime','日期时间','日期时间','testDatetime','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','datetime',NULL),('1069431399036301312','test_data','test_user_code',100,'varchar(64)','用户选择','用户选择','testUser','com.jeesite.modules.sys.entity.User',NULL,'1','1','1','1','1',NULL,'1','userselect',NULL),('1069431399053078528','test_data','test_office_code',110,'varchar(64)','机构选择','机构选择','testOffice','com.jeesite.modules.sys.entity.Office',NULL,'1','1','1','1','1',NULL,'1','officeselect',NULL),('1069431399069855744','test_data','test_area_code',120,'varchar(64)','区域选择','区域选择','testAreaCode|testAreaName','String',NULL,'1','1','1','1','1',NULL,'1','areaselect',NULL),('1069431399082438656','test_data','test_area_name',130,'varchar(100)','区域名称','区域名称','testAreaName','String',NULL,'1','1','1','1','0','LIKE','0','input',NULL),('1069431399099215872','test_data','status',140,'char(1)','状态','状态（0正常 1删除 2停用）','status','String',NULL,'0','1',NULL,'1','1',NULL,NULL,'select','{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}'),('1069431399115993088','test_data','create_by',150,'varchar(64)','创建者','创建者','createBy','String',NULL,'0','1',NULL,NULL,NULL,NULL,NULL,'input',NULL),('1069431399124381696','test_data','create_date',160,'datetime','创建时间','创建时间','createDate','java.util.Date',NULL,'0','1',NULL,'1',NULL,NULL,NULL,'dateselect',NULL),('1069431399136964608','test_data','update_by',170,'varchar(64)','更新者','更新者','updateBy','String',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input',NULL),('1069431399149547520','test_data','update_date',180,'datetime','更新时间','更新时间','updateDate','java.util.Date',NULL,'0','1','1',NULL,NULL,NULL,NULL,'dateselect',NULL),('1069431399162130432','test_data','remarks',190,'varchar(500)','备注信息','备注信息','remarks','String',NULL,'1','1','1','1','1',NULL,'1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}'),('1069431399237627904','test_data_child','id',10,'varchar(64)','编号','编号','id','String','1','0','1',NULL,NULL,NULL,NULL,'1','hidden','{\"fieldValid\":\"abc\"}'),('1069431399355068416','test_data_child','test_sort',20,'int(11)','排序号','排序号','testSort','Long',NULL,'1','1','1','1','1',NULL,'1','input','{\"fieldValid\":\"digits\"}'),('1069431399522840576','test_data_child','test_data_id',30,'varchar(64)','父表主键','父表主键','testData','String',NULL,'1','1','1','1','1',NULL,'1','input',NULL),('1069431401032790016','test_data_child','test_input',40,'varchar(200)','单行文本','单行文本','testInput','String',NULL,'1','1','1','1','1','LIKE','1','input',NULL),('1069431401049567232','test_data_child','test_textarea',50,'varchar(200)','多行文本','多行文本','testTextarea','String',NULL,'1','1','1','1','1','LIKE','1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}'),('1069431401125064704','test_data_child','test_select',60,'varchar(10)','下拉框','下拉框','testSelect','String',NULL,'1','1','1','1','1',NULL,'1','select','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431401141841920','test_data_child','test_select_multiple',70,'varchar(200)','下拉多选','下拉多选','testSelectMultiple','String',NULL,'1','1','1','1','1',NULL,'1','select_multiple','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431401158619136','test_data_child','test_radio',80,'varchar(10)','单选框','单选框','testRadio','String',NULL,'1','1','1','1','1',NULL,'1','radio','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431401175396352','test_data_child','test_checkbox',90,'varchar(200)','复选框','复选框','testCheckbox','String',NULL,'1','1','1','1','1',NULL,'1','checkbox','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069431401192173568','test_data_child','test_date',100,'datetime','日期选择','日期选择','testDate','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','date',NULL),('1069431401208950784','test_data_child','test_datetime',110,'datetime','日期时间','日期时间','testDatetime','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','datetime',NULL),('1069431401225728000','test_data_child','test_user_code',120,'varchar(64)','用户选择','用户选择','testUser','com.jeesite.modules.sys.entity.User',NULL,'1','1','1','1','1',NULL,'1','userselect',NULL),('1069431401242505216','test_data_child','test_office_code',130,'varchar(64)','机构选择','机构选择','testOffice','com.jeesite.modules.sys.entity.Office',NULL,'1','1','1','1','1',NULL,'1','officeselect',NULL),('1069431401255088128','test_data_child','test_area_code',140,'varchar(64)','区域选择','区域选择','testAreaCode|testAreaName','String',NULL,'1','1','1','1','1',NULL,'1','areaselect',NULL),('1069431401267671040','test_data_child','test_area_name',150,'varchar(100)','区域名称','区域名称','testAreaName','String',NULL,'1','1','1','1','0','LIKE','0','input',NULL),('1069431401347362816','test_tree','tree_code',10,'varchar(64)','节点编码','节点编码','treeCode','String','1','0','1',NULL,NULL,NULL,NULL,'1','input','{\"fieldValid\":\"abc\"}'),('1069431401359945728','test_tree','parent_code',20,'varchar(64)','父级编号','父级编号','parentCode|parentName','This',NULL,'0','1','1','1','1',NULL,'1','treeselect',NULL),('1069431401376722944','test_tree','parent_codes',30,'varchar(1000)','所有父级编号','所有父级编号','parentCodes','String',NULL,'0','1','1','1','1','LIKE','0','input',NULL),('1069431401393500160','test_tree','tree_sort',40,'decimal(10,0)','本级排序号','本级排序号（升序）','treeSort','Integer',NULL,'0','1','1','1','1',NULL,'1','input','{\"fieldValid\":\"digits\"}'),('1069431401410277376','test_tree','tree_sorts',50,'varchar(1000)','所有级别排序号','所有级别排序号','treeSorts','String',NULL,'0','1','1','0','1',NULL,'0','input',NULL),('1069431401427054592','test_tree','tree_leaf',60,'char(1)','是否最末级','是否最末级','treeLeaf','String',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input',NULL),('1069431401439637504','test_tree','tree_level',70,'decimal(4,0)','层次级别','层次级别','treeLevel','Integer',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input','{\"fieldValid\":\"digits\"}'),('1069431401456414720','test_tree','tree_names',80,'varchar(1000)','全节点名','全节点名','treeNames','String',NULL,'0','1','1','1','1','LIKE','1','input',NULL),('1069431401468997632','test_tree','tree_name',90,'varchar(200)','节点名称','节点名称','treeName','String',NULL,'0','1','1','1','1','LIKE','1','input',NULL),('1069431401481580544','test_tree','status',100,'char(1)','状态','状态（0正常 1删除 2停用）','status','String',NULL,'0','1',NULL,'1','1',NULL,NULL,'select','{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}'),('1069431401498357760','test_tree','create_by',110,'varchar(64)','创建者','创建者','createBy','String',NULL,'0','1',NULL,NULL,NULL,NULL,NULL,'input',NULL),('1069431401510940672','test_tree','create_date',120,'datetime','创建时间','创建时间','createDate','java.util.Date',NULL,'0','1',NULL,'1',NULL,NULL,NULL,'dateselect',NULL),('1069431401527717888','test_tree','update_by',130,'varchar(64)','更新者','更新者','updateBy','String',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input',NULL),('1069431401540300800','test_tree','update_date',140,'datetime','更新时间','更新时间','updateDate','java.util.Date',NULL,'0','1','1',NULL,NULL,NULL,NULL,'dateselect',NULL),('1069431401552883712','test_tree','remarks',150,'varchar(500)','备注信息','备注信息','remarks','String',NULL,'1','1','1','1','1',NULL,'1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
/*!40000 ALTER TABLE `js_gen_table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_blob_triggers`
--

DROP TABLE IF EXISTS `js_job_blob_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_blob_triggers`
--

LOCK TABLES `js_job_blob_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_blob_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_blob_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_calendars`
--

DROP TABLE IF EXISTS `js_job_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_calendars`
--

LOCK TABLES `js_job_calendars` WRITE;
/*!40000 ALTER TABLE `js_job_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_cron_triggers`
--

DROP TABLE IF EXISTS `js_job_cron_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_cron_triggers`
--

LOCK TABLES `js_job_cron_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_cron_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_cron_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_fired_triggers`
--

DROP TABLE IF EXISTS `js_job_fired_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_fired_triggers`
--

LOCK TABLES `js_job_fired_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_fired_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_fired_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_job_details`
--

DROP TABLE IF EXISTS `js_job_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_job_details`
--

LOCK TABLES `js_job_job_details` WRITE;
/*!40000 ALTER TABLE `js_job_job_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_job_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_locks`
--

DROP TABLE IF EXISTS `js_job_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_locks`
--

LOCK TABLES `js_job_locks` WRITE;
/*!40000 ALTER TABLE `js_job_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_paused_trigger_grps`
--

DROP TABLE IF EXISTS `js_job_paused_trigger_grps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_paused_trigger_grps`
--

LOCK TABLES `js_job_paused_trigger_grps` WRITE;
/*!40000 ALTER TABLE `js_job_paused_trigger_grps` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_paused_trigger_grps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_scheduler_state`
--

DROP TABLE IF EXISTS `js_job_scheduler_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_scheduler_state`
--

LOCK TABLES `js_job_scheduler_state` WRITE;
/*!40000 ALTER TABLE `js_job_scheduler_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_scheduler_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_simple_triggers`
--

DROP TABLE IF EXISTS `js_job_simple_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_simple_triggers`
--

LOCK TABLES `js_job_simple_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_simple_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_simple_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_simprop_triggers`
--

DROP TABLE IF EXISTS `js_job_simprop_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_simprop_triggers`
--

LOCK TABLES `js_job_simprop_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_simprop_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_simprop_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_triggers`
--

DROP TABLE IF EXISTS `js_job_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_job_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `js_job_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `js_job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_triggers`
--

LOCK TABLES `js_job_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_area`
--

DROP TABLE IF EXISTS `js_sys_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_area` (
  `area_code` varchar(100) NOT NULL COMMENT '区域编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `area_name` varchar(100) NOT NULL COMMENT '区域名称',
  `area_type` char(1) DEFAULT NULL COMMENT '区域类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`area_code`),
  KEY `idx_sys_area_pc` (`parent_code`),
  KEY `idx_sys_area_ts` (`tree_sort`),
  KEY `idx_sys_area_status` (`status`),
  KEY `idx_sys_area_pcs` (`parent_codes`(255)),
  KEY `idx_sys_area_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行政区划';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_area`
--

LOCK TABLES `js_sys_area` WRITE;
/*!40000 ALTER TABLE `js_sys_area` DISABLE KEYS */;
INSERT INTO `js_sys_area` VALUES ('370000','0','0,',370000,'0000370000,','0',0,'山东省','山东省','1','0','system','2018-12-03 11:20:28','system','2018-12-03 11:20:28',NULL),('370100','370000','0,370000,',370100,'0000370000,0000370100,','0',1,'山东省/济南市','济南市','2','0','system','2018-12-03 11:20:28','system','2018-12-03 11:20:28',NULL),('370102','370100','0,370000,370100,',370102,'0000370000,0000370100,0000370102,','1',2,'山东省/济南市/历下区','历下区','3','0','system','2018-12-03 11:20:28','system','2018-12-03 11:20:29',NULL),('370103','370100','0,370000,370100,',370103,'0000370000,0000370100,0000370103,','1',2,'山东省/济南市/市中区','市中区','3','0','system','2018-12-03 11:20:29','system','2018-12-03 11:20:29',NULL),('370104','370100','0,370000,370100,',370104,'0000370000,0000370100,0000370104,','1',2,'山东省/济南市/槐荫区','槐荫区','3','0','system','2018-12-03 11:20:29','system','2018-12-03 11:20:29',NULL),('370105','370100','0,370000,370100,',370105,'0000370000,0000370100,0000370105,','1',2,'山东省/济南市/天桥区','天桥区','3','0','system','2018-12-03 11:20:29','system','2018-12-03 11:20:29',NULL),('370112','370100','0,370000,370100,',370112,'0000370000,0000370100,0000370112,','1',2,'山东省/济南市/历城区','历城区','3','0','system','2018-12-03 11:20:29','system','2018-12-03 11:20:29',NULL),('370113','370100','0,370000,370100,',370113,'0000370000,0000370100,0000370113,','1',2,'山东省/济南市/长清区','长清区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370114','370100','0,370000,370100,',370114,'0000370000,0000370100,0000370114,','1',2,'山东省/济南市/章丘区','章丘区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370124','370100','0,370000,370100,',370124,'0000370000,0000370100,0000370124,','1',2,'山东省/济南市/平阴县','平阴县','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370125','370100','0,370000,370100,',370125,'0000370000,0000370100,0000370125,','1',2,'山东省/济南市/济阳县','济阳县','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370126','370100','0,370000,370100,',370126,'0000370000,0000370100,0000370126,','1',2,'山东省/济南市/商河县','商河县','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370200','370000','0,370000,',370200,'0000370000,0000370200,','0',1,'山东省/青岛市','青岛市','2','0','system','2018-12-03 11:20:28','system','2018-12-03 11:20:28',NULL),('370202','370200','0,370000,370200,',370202,'0000370000,0000370200,0000370202,','1',2,'山东省/青岛市/市南区','市南区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370203','370200','0,370000,370200,',370203,'0000370000,0000370200,0000370203,','1',2,'山东省/青岛市/市北区','市北区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370211','370200','0,370000,370200,',370211,'0000370000,0000370200,0000370211,','1',2,'山东省/青岛市/黄岛区','黄岛区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370212','370200','0,370000,370200,',370212,'0000370000,0000370200,0000370212,','1',2,'山东省/青岛市/崂山区','崂山区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370213','370200','0,370000,370200,',370213,'0000370000,0000370200,0000370213,','1',2,'山东省/青岛市/李沧区','李沧区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:30',NULL),('370214','370200','0,370000,370200,',370214,'0000370000,0000370200,0000370214,','1',2,'山东省/青岛市/城阳区','城阳区','3','0','system','2018-12-03 11:20:30','system','2018-12-03 11:20:31',NULL),('370281','370200','0,370000,370200,',370281,'0000370000,0000370200,0000370281,','1',2,'山东省/青岛市/胶州市','胶州市','3','0','system','2018-12-03 11:20:31','system','2018-12-03 11:20:31',NULL),('370282','370200','0,370000,370200,',370282,'0000370000,0000370200,0000370282,','1',2,'山东省/青岛市/即墨区','即墨区','3','0','system','2018-12-03 11:20:31','system','2018-12-03 11:20:31',NULL),('370283','370200','0,370000,370200,',370283,'0000370000,0000370200,0000370283,','1',2,'山东省/青岛市/平度市','平度市','3','0','system','2018-12-03 11:20:31','system','2018-12-03 11:20:31',NULL),('370285','370200','0,370000,370200,',370285,'0000370000,0000370200,0000370285,','1',2,'山东省/青岛市/莱西市','莱西市','3','0','system','2018-12-03 11:20:31','system','2018-12-03 11:20:31',NULL);
/*!40000 ALTER TABLE `js_sys_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_company`
--

DROP TABLE IF EXISTS `js_sys_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_company` (
  `company_code` varchar(64) NOT NULL COMMENT '公司编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) NOT NULL COMMENT '公司代码',
  `company_name` varchar(200) NOT NULL COMMENT '公司名称',
  `full_name` varchar(200) NOT NULL COMMENT '公司全称',
  `area_code` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`company_code`),
  KEY `idx_sys_company_cc` (`corp_code`),
  KEY `idx_sys_company_pc` (`parent_code`),
  KEY `idx_sys_company_ts` (`tree_sort`),
  KEY `idx_sys_company_status` (`status`),
  KEY `idx_sys_company_vc` (`view_code`),
  KEY `idx_sys_company_pcs` (`parent_codes`(255)),
  KEY `idx_sys_company_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_company`
--

LOCK TABLES `js_sys_company` WRITE;
/*!40000 ALTER TABLE `js_sys_company` DISABLE KEYS */;
INSERT INTO `js_sys_company` VALUES ('SD','0','0,',40,'0000000040,','0',0,'山东公司','SD','山东公司','山东公司',NULL,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN','SD','0,SD,',30,'0000000040,0000000030,','1',1,'山东公司/济南公司','SDJN','济南公司','山东济南公司',NULL,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD','SD','0,SD,',40,'0000000040,0000000040,','1',1,'山东公司/青岛公司','SDQD','青岛公司','山东青岛公司',NULL,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_company_office`
--

DROP TABLE IF EXISTS `js_sys_company_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_company_office` (
  `company_code` varchar(64) NOT NULL COMMENT '公司编码',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  PRIMARY KEY (`company_code`,`office_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司部门关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_company_office`
--

LOCK TABLES `js_sys_company_office` WRITE;
/*!40000 ALTER TABLE `js_sys_company_office` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_company_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_config`
--

DROP TABLE IF EXISTS `js_sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_config` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `config_name` varchar(100) NOT NULL COMMENT '名称',
  `config_key` varchar(100) NOT NULL COMMENT '参数键',
  `config_value` varchar(1000) DEFAULT NULL COMMENT '参数值',
  `is_sys` char(1) NOT NULL COMMENT '系统内置（1是 0否）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_sys_config_key` (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_config`
--

LOCK TABLES `js_sys_config` WRITE;
/*!40000 ALTER TABLE `js_sys_config` DISABLE KEYS */;
INSERT INTO `js_sys_config` VALUES ('1069431155850555392','研发工具-代码生成默认包名','gen.defaultPackageName','com.jeesite.modules','0','system','2018-12-03 11:20:31','system','2018-12-03 11:20:31','新建项目后，修改该键值，在生成代码的时候就不要再修改了'),('1069431156207071232','主框架页-桌面仪表盘首页地址','sys.index.desktopUrl','/desktop','0','system','2018-12-03 11:20:31','system','2018-12-03 11:20:31','主页面的第一个页签首页桌面地址'),('1069431156848799744','主框架页-侧边栏的默认显示样式','sys.index.sidebarStyle','1','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','1：默认显示侧边栏；2：默认折叠侧边栏'),('1069431157607968768','主框架页-默认皮肤样式名称','sys.index.skinName','skin-blue-light','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','skin-black-light、skin-black、skin-blue-light、skin-blue、skin-green-light、skin-green、skin-purple-light、skin-purple、skin-red-light、skin-red、skin-yellow-light、skin-yellow'),('1069431157884792832','用户登录-登录失败多少次数后显示验证码','sys.login.failedNumAfterValidCode','100','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','设置为0强制使用验证码登录'),('1069431158207754240','用户登录-登录失败多少次数后锁定账号','sys.login.failedNumAfterLockAccount','200','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','登录失败多少次数后锁定账号'),('1069431158480384000','用户登录-登录失败多少次数后锁定账号的时间','sys.login.failedNumAfterLockMinute','20','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','锁定账号的时间（单位：分钟）'),('1069431158643961856','账号自助-是否开启用户注册功能','sys.account.registerUser','true','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','是否开启注册用户功能'),('1069431160183271424','账号自助-允许自助注册的用户类型','sys.account.registerUser.userTypes','-1','0','system','2018-12-03 11:20:32','system','2018-12-03 11:20:32','允许注册的用户类型（多个用逗号隔开，如果注册时不选择用户类型，则第一个为默认类型）'),('1069431161038909440','账号自助-验证码有效时间（分钟）','sys.account.validCodeTimeout','10','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','找回密码时，短信/邮件验证码有效时间（单位：分钟，0表示不限制）'),('1069431161210875904','用户管理-账号默认角色-员工类型','sys.user.defaultRoleCodes.employee','default','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','所有员工用户都拥有的角色权限（适用于菜单授权查询）'),('1069431161395425280','用户管理-账号初始密码','sys.user.initPassword','123456','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','创建用户和重置密码的时候用户的密码'),('1069431161571586048','用户管理-初始密码修改策略','sys.user.initPasswordModify','1','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','0：初始密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作'),('1069431161739358208','用户管理-账号密码修改策略','sys.user.passwordModify','1','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','0：密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作。'),('1069431162204925952','用户管理-账号密码修改策略验证周期','sys.user.passwordModifyCycle','30','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','密码安全修改周期是指定时间内提醒或必须修改密码（例如设置30天）的验证时间（天），超过这个时间登录系统时需，提醒用户修改密码或强制修改密码才能继续使用系统。单位：天，如果设置30天，则第31天开始强制修改密码'),('1069431162376892416','用户管理-密码修改多少次内不允许重复','sys.user.passwordModifyNotRepeatNum','1','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','默认1次，表示不能与上次密码重复；若设置为3，表示并与前3次密码重复'),('1069431162641133568','用户管理-账号密码修改最低安全等级','sys.user.passwordModifySecurityLevel','0','0','system','2018-12-03 11:20:33','system','2018-12-03 11:20:33','0：不限制等级（用户在修改密码的时候不进行等级验证）\r；1：限制最低等级为很弱\r；2：限制最低等级为弱\r；3：限制最低等级为安全\r；4：限制最低等级为很安全'),('1092344803460943872','主框架页-主导航菜单显示风格','sys.index.menuStyle','1','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41','1：菜单全部在左侧；2：根菜单显示在顶部');
/*!40000 ALTER TABLE `js_sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_dict_data`
--

DROP TABLE IF EXISTS `js_sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_dict_data` (
  `dict_code` varchar(64) NOT NULL COMMENT '字典编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `dict_label` varchar(100) NOT NULL COMMENT '字典标签',
  `dict_value` varchar(100) NOT NULL COMMENT '字典键值',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `is_sys` char(1) NOT NULL COMMENT '系统内置（1是 0否）',
  `description` varchar(500) DEFAULT NULL COMMENT '字典描述',
  `css_style` varchar(500) DEFAULT NULL COMMENT 'css样式（如：color:red)',
  `css_class` varchar(500) DEFAULT NULL COMMENT 'css类名（如：red）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`dict_code`),
  KEY `idx_sys_dict_data_cc` (`corp_code`),
  KEY `idx_sys_dict_data_dt` (`dict_type`),
  KEY `idx_sys_dict_data_pc` (`parent_code`),
  KEY `idx_sys_dict_data_status` (`status`),
  KEY `idx_sys_dict_data_pcs` (`parent_codes`(255)),
  KEY `idx_sys_dict_data_ts` (`tree_sort`),
  KEY `idx_sys_dict_data_tss` (`tree_sorts`(255)),
  KEY `idx_sys_dict_data_dv` (`dict_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_dict_data`
--

LOCK TABLES `js_sys_dict_data` WRITE;
/*!40000 ALTER TABLE `js_sys_dict_data` DISABLE KEYS */;
INSERT INTO `js_sys_dict_data` VALUES ('1069431175337291776','0','0,',30,'0000000030,','1',0,'是','是','1','sys_yes_no','1','','','','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431176478142464','0','0,',40,'0000000040,','1',0,'否','否','0','sys_yes_no','1','','color:#aaa;','','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431177656741888','0','0,',20,'0000000020,','1',0,'正常','正常','0','sys_status','1','','','','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431178235555840','0','0,',30,'0000000030,','1',0,'删除','删除','1','sys_status','1','','color:#f00;','','0','system','2018-12-03 11:20:37','system','2018-12-03 11:20:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431181108654080','0','0,',40,'0000000040,','1',0,'停用','停用','2','sys_status','1','','color:#f00;','','0','system','2018-12-03 11:20:37','system','2018-12-03 11:20:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431181255454720','0','0,',50,'0000000050,','1',0,'冻结','冻结','3','sys_status','1','','color:#fa0;','','0','system','2018-12-03 11:20:37','system','2018-12-03 11:20:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431182945759232','0','0,',60,'0000000060,','1',0,'待审','待审','4','sys_status','1','','','','0','system','2018-12-03 11:20:38','system','2018-12-03 11:20:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431183151280128','0','0,',70,'0000000070,','1',0,'驳回','驳回','5','sys_status','1','','','','0','system','2018-12-03 11:20:38','system','2018-12-03 11:20:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431185470730240','0','0,',80,'0000000080,','1',0,'草稿','草稿','9','sys_status','1','','color:#aaa;','','0','system','2018-12-03 11:20:38','system','2018-12-03 11:20:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431185986629632','0','0,',30,'0000000030,','1',0,'显示','显示','1','sys_show_hide','1','','','','0','system','2018-12-03 11:20:38','system','2018-12-03 11:20:39',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431187806957568','0','0,',40,'0000000040,','1',0,'隐藏','隐藏','0','sys_show_hide','1','','color:#aaa;','','0','system','2018-12-03 11:20:39','system','2018-12-03 11:20:39',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431189811834880','0','0,',30,'0000000030,','1',0,'简体中文','简体中文','zh_CN','sys_lang_type','1','','','','0','system','2018-12-03 11:20:39','system','2018-12-03 11:20:39',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431189992189952','0','0,',40,'0000000040,','1',0,'英语','英语','en','sys_lang_type','1','','','','0','system','2018-12-03 11:20:39','system','2018-12-03 11:20:40',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431191900598272','0','0,',30,'0000000030,','1',0,'PC电脑','PC电脑','pc','sys_device_type','1','','','','0','system','2018-12-03 11:20:40','system','2018-12-03 11:20:40',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431192131284992','0','0,',40,'0000000040,','1',0,'手机APP','手机APP','mobileApp','sys_device_type','1','','','','0','system','2018-12-03 11:20:40','system','2018-12-03 11:20:40',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431194186493952','0','0,',50,'0000000050,','1',0,'手机Web','手机Web','mobileWeb','sys_device_type','1','','','','0','system','2018-12-03 11:20:40','system','2018-12-03 11:20:40',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431194324905984','0','0,',60,'0000000060,','1',0,'微信设备','微信设备','weixin','sys_device_type','1','','','','0','system','2018-12-03 11:20:40','system','2018-12-03 11:20:40',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431194480095232','0','0,',30,'0000000030,','1',0,'主导航菜单','主导航菜单','default','sys_menu_sys_code','1','','','','0','system','2018-12-03 11:20:40','system','2018-12-03 11:20:41',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431196778573824','0','0,',30,'0000000030,','1',0,'菜单','菜单','1','sys_menu_type','1','','','','0','system','2018-12-03 11:20:41','system','2018-12-03 11:20:41',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431196916985856','0','0,',40,'0000000040,','1',0,'权限','权限','2','sys_menu_type','1','','color:#c243d6;','','0','system','2018-12-03 11:20:41','system','2018-12-03 11:20:41',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431198393380864','0','0,',30,'0000000030,','1',0,'默认权重','默认权重','20','sys_menu_weight','1','','','','0','system','2018-12-03 11:20:41','system','2018-12-03 11:20:41',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431198594707456','0','0,',40,'0000000040,','1',0,'二级管理员','二级管理员','40','sys_menu_weight','1','','','','0','system','2018-12-03 11:20:41','system','2018-12-03 11:20:42',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431200297594880','0','0,',50,'0000000050,','1',0,'系统管理员','系统管理员','60','sys_menu_weight','1','','','','0','system','2018-12-03 11:20:42','system','2018-12-03 11:20:42',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431200461172736','0','0,',60,'0000000060,','1',0,'超级管理员','超级管理员','80','sys_menu_weight','1','','color:#c243d6;','','0','system','2018-12-03 11:20:42','system','2018-12-03 11:20:42',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431201664937984','0','0,',30,'0000000030,','1',0,'国家','国家','0','sys_area_type','1','','','','0','system','2018-12-03 11:20:42','system','2018-12-03 11:20:42',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431202042425344','0','0,',40,'0000000040,','1',0,'省份直辖市','省份直辖市','1','sys_area_type','1','','','','0','system','2018-12-03 11:20:42','system','2018-12-03 11:20:42',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431202235363328','0','0,',50,'0000000050,','1',0,'地市','地市','2','sys_area_type','1','','','','0','system','2018-12-03 11:20:42','system','2018-12-03 11:20:43',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431203975999488','0','0,',60,'0000000060,','1',0,'区县','区县','3','sys_area_type','1','','','','0','system','2018-12-03 11:20:43','system','2018-12-03 11:20:43',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431206282866688','0','0,',30,'0000000030,','1',0,'省级公司','省级公司','1','sys_office_type','1','','','','0','system','2018-12-03 11:20:43','system','2018-12-03 11:20:43',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431206396112896','0','0,',40,'0000000040,','1',0,'市级公司','市级公司','2','sys_office_type','1','','','','0','system','2018-12-03 11:20:43','system','2018-12-03 11:20:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431208061251584','0','0,',50,'0000000050,','1',0,'部门','部门','3','sys_office_type','1','','','','0','system','2018-12-03 11:20:44','system','2018-12-03 11:20:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431208203857920','0','0,',30,'0000000030,','1',0,'正常','正常','0','sys_search_status','1','','','','0','system','2018-12-03 11:20:44','system','2018-12-03 11:20:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431209806082048','0','0,',40,'0000000040,','1',0,'停用','停用','2','sys_search_status','1','','color:#f00;','','0','system','2018-12-03 11:20:44','system','2018-12-03 11:20:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431209936105472','0','0,',30,'0000000030,','1',0,'男','男','1','sys_user_sex','1','','','','0','system','2018-12-03 11:20:44','system','2018-12-03 11:20:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431210074517504','0','0,',40,'0000000040,','1',0,'女','女','2','sys_user_sex','1','','','','0','system','2018-12-03 11:20:44','system','2018-12-03 11:20:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431210460393472','0','0,',30,'0000000030,','1',0,'正常','正常','0','sys_user_status','1','','','','0','system','2018-12-03 11:20:45','system','2018-12-03 11:20:45',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431212784037888','0','0,',40,'0000000040,','1',0,'停用','停用','2','sys_user_status','1','','color:#f00;','','0','system','2018-12-03 11:20:45','system','2018-12-03 11:20:45',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431214373679104','0','0,',50,'0000000050,','1',0,'冻结','冻结','3','sys_user_status','1','','color:#fa0;','','0','system','2018-12-03 11:20:45','system','2018-12-03 11:20:45',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431214558228480','0','0,',30,'0000000030,','1',0,'员工','员工','employee','sys_user_type','1','','','','0','system','2018-12-03 11:20:45','system','2018-12-03 11:20:46',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431216013651968','0','0,',40,'0000000040,','1',0,'会员','会员','member','sys_user_type','1','','','','0','system','2018-12-03 11:20:46','system','2018-12-03 11:20:46',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431216139481088','0','0,',50,'0000000050,','1',0,'单位','单位','btype','sys_user_type','1','','','','0','system','2018-12-03 11:20:46','system','2018-12-03 11:20:46',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431217397772288','0','0,',60,'0000000060,','1',0,'个人','个人','persion','sys_user_type','1','','','','0','system','2018-12-03 11:20:46','system','2018-12-03 11:20:46',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431217557155840','0','0,',70,'0000000070,','1',0,'专家','专家','expert','sys_user_type','1','','','','0','system','2018-12-03 11:20:46','system','2018-12-03 11:20:46',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431218530234368','0','0,',30,'0000000030,','1',0,'高管','高管','1','sys_role_type','1','','','','0','system','2018-12-03 11:20:46','system','2018-12-03 11:20:46',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431218664452096','0','0,',40,'0000000040,','1',0,'中层','中层','2','sys_role_type','1','','','','0','system','2018-12-03 11:20:47','system','2018-12-03 11:20:47',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431219926937600','0','0,',50,'0000000050,','1',0,'基层','基层','3','sys_role_type','1','','','','0','system','2018-12-03 11:20:47','system','2018-12-03 11:20:47',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431221810180096','0','0,',60,'0000000060,','1',0,'其它','其它','4','sys_role_type','1','','','','0','system','2018-12-03 11:20:47','system','2018-12-03 11:20:47',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431222120558592','0','0,',30,'0000000030,','1',0,'未设置','未设置','0','sys_role_data_scope','1','','','','0','system','2018-12-03 11:20:47','system','2018-12-03 11:20:47',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431222569349120','0','0,',40,'0000000040,','1',0,'全部数据','全部数据','1','sys_role_data_scope','1','','','','0','system','2018-12-03 11:20:47','system','2018-12-03 11:20:48',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431226046427136','0','0,',50,'0000000050,','1',0,'自定义数据','自定义数据','2','sys_role_data_scope','1','','','','0','system','2018-12-03 11:20:48','system','2018-12-03 11:20:48',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431226210004992','0','0,',60,'0000000060,','1',0,'本部门数据','本部门数据','3','sys_role_data_scope','1','','','','0','system','2018-12-03 11:20:48','system','2018-12-03 11:20:49',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431229066326016','0','0,',70,'0000000070,','1',0,'本公司数据','本公司数据','4','sys_role_data_scope','1','','','','0','system','2018-12-03 11:20:49','system','2018-12-03 11:20:49',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431229229903872','0','0,',80,'0000000080,','1',0,'本部门和本公司数据','本部门和本公司数据','5','sys_role_data_scope','1','','','','0','system','2018-12-03 11:20:49','system','2018-12-03 11:20:49',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431229355732992','0','0,',30,'0000000030,','1',0,'高管','高管','1','sys_post_type','1','','','','0','system','2018-12-03 11:20:49','system','2018-12-03 11:20:49',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431231725514752','0','0,',40,'0000000040,','1',0,'中层','中层','2','sys_post_type','1','','','','0','system','2018-12-03 11:20:49','system','2018-12-03 11:20:49',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431232086224896','0','0,',50,'0000000050,','1',0,'基层','基层','3','sys_post_type','1','','','','0','system','2018-12-03 11:20:49','system','2018-12-03 11:20:50',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431233248047104','0','0,',60,'0000000060,','1',0,'其它','其它','4','sys_post_type','1','','','','0','system','2018-12-03 11:20:50','system','2018-12-03 11:20:50',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431233399042048','0','0,',30,'0000000030,','1',0,'接入日志','接入日志','access','sys_log_type','1','','','','0','system','2018-12-03 11:20:50','system','2018-12-03 11:20:50',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431234426646528','0','0,',40,'0000000040,','1',0,'修改日志','修改日志','update','sys_log_type','1','','','','0','system','2018-12-03 11:20:50','system','2018-12-03 11:20:50',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431235693326336','0','0,',50,'0000000050,','1',0,'查询日志','查询日志','select','sys_log_type','1','','','','0','system','2018-12-03 11:20:50','system','2018-12-03 11:20:50',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431235861098496','0','0,',60,'0000000060,','1',0,'登录登出','登录登出','loginLogout','sys_log_type','1','','','','0','system','2018-12-03 11:20:50','system','2018-12-03 11:20:51',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431237660454912','0','0,',30,'0000000030,','1',0,'默认','默认','DEFAULT','sys_job_group','1','','','','0','system','2018-12-03 11:20:51','system','2018-12-03 11:20:51',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431237811449856','0','0,',40,'0000000040,','1',0,'系统','系统','SYSTEM','sys_job_group','1','','','','0','system','2018-12-03 11:20:51','system','2018-12-03 11:20:51',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431239774384128','0','0,',30,'0000000030,','1',0,'错过计划等待本次计划完成后立即执行一次','错过计划等待本次计划完成后立即执行一次','1','sys_job_misfire_instruction','1','','','','0','system','2018-12-03 11:20:51','system','2018-12-03 11:20:51',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431239916990464','0','0,',40,'0000000040,','1',0,'本次执行时间根据上次结束时间重新计算（时间间隔方式）','本次执行时间根据上次结束时间重新计算（时间间隔方式）','2','sys_job_misfire_instruction','1','','','','0','system','2018-12-03 11:20:51','system','2018-12-03 11:20:51',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431240063791104','0','0,',30,'0000000030,','1',0,'正常','正常','0','sys_job_status','1','','','','0','system','2018-12-03 11:20:51','system','2018-12-03 11:20:52',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431242525847552','0','0,',40,'0000000040,','1',0,'删除','删除','1','sys_job_status','1','','','','0','system','2018-12-03 11:20:52','system','2018-12-03 11:20:52',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431242672648192','0','0,',50,'0000000050,','1',0,'暂停','暂停','2','sys_job_status','1','','color:#f00;','','0','system','2018-12-03 11:20:52','system','2018-12-03 11:20:52',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431242819448832','0','0,',30,'0000000030,','1',0,'完成','完成','3','sys_job_status','1','','','','0','system','2018-12-03 11:20:52','system','2018-12-03 11:20:53',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431245273116672','0','0,',40,'0000000040,','1',0,'错误','错误','4','sys_job_status','1','','color:#f00;','','0','system','2018-12-03 11:20:53','system','2018-12-03 11:20:53',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431245424111616','0','0,',50,'0000000050,','1',0,'运行','运行','5','sys_job_status','1','','','','0','system','2018-12-03 11:20:53','system','2018-12-03 11:20:53',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431248230100992','0','0,',30,'0000000030,','1',0,'计划日志','计划日志','scheduler','sys_job_type','1','','','','0','system','2018-12-03 11:20:53','system','2018-12-03 11:20:53',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431248406261760','0','0,',40,'0000000040,','1',0,'任务日志','任务日志','job','sys_job_type','1','','','','0','system','2018-12-03 11:20:53','system','2018-12-03 11:20:54',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431249614221312','0','0,',50,'0000000050,','1',0,'触发日志','触发日志','trigger','sys_job_type','1','','','','0','system','2018-12-03 11:20:54','system','2018-12-03 11:20:54',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431249798770688','0','0,',30,'0000000030,','1',0,'计划创建','计划创建','jobScheduled','sys_job_event','1','','','','0','system','2018-12-03 11:20:54','system','2018-12-03 11:20:54',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431251090616320','0','0,',40,'0000000040,','1',0,'计划移除','计划移除','jobUnscheduled','sys_job_event','1','','','','0','system','2018-12-03 11:20:54','system','2018-12-03 11:20:54',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431252843835392','0','0,',50,'0000000050,','1',0,'计划暂停','计划暂停','triggerPaused','sys_job_event','1','','','','0','system','2018-12-03 11:20:54','system','2018-12-03 11:20:54',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431253003218944','0','0,',60,'0000000060,','1',0,'计划恢复','计划恢复','triggerResumed','sys_job_event','1','','','','0','system','2018-12-03 11:20:54','system','2018-12-03 11:20:54',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431253154213888','0','0,',70,'0000000070,','1',0,'调度错误','调度错误','schedulerError','sys_job_event','1','','','','0','system','2018-12-03 11:20:54','system','2018-12-03 11:20:55',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431255511412736','0','0,',80,'0000000080,','1',0,'任务执行','任务执行','jobToBeExecuted','sys_job_event','1','','','','0','system','2018-12-03 11:20:55','system','2018-12-03 11:20:55',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431257872805888','0','0,',90,'0000000090,','1',0,'任务结束','任务结束','jobWasExecuted','sys_job_event','1','','','','0','system','2018-12-03 11:20:56','system','2018-12-03 11:20:56',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431258002829312','0','0,',100,'0000000100,','1',0,'任务停止','任务停止','jobExecutionVetoed','sys_job_event','1','','','','0','system','2018-12-03 11:20:56','system','2018-12-03 11:20:56',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431259902849024','0','0,',110,'0000000110,','1',0,'触发计划','触发计划','triggerFired','sys_job_event','1','','','','0','system','2018-12-03 11:20:56','system','2018-12-03 11:20:56',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431260024483840','0','0,',120,'0000000120,','1',0,'触发验证','触发验证','vetoJobExecution','sys_job_event','1','','','','0','system','2018-12-03 11:20:56','system','2018-12-03 11:20:57',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431262218104832','0','0,',130,'0000000130,','1',0,'触发完成','触发完成','triggerComplete','sys_job_event','1','','','','0','system','2018-12-03 11:20:57','system','2018-12-03 11:20:57',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431262578814976','0','0,',140,'0000000140,','1',0,'触发错过','触发错过','triggerMisfired','sys_job_event','1','','','','0','system','2018-12-03 11:20:57','system','2018-12-03 11:20:57',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431263904215040','0','0,',30,'0000000030,','1',0,'PC','PC','pc','sys_msg_type','1','消息类型','','','0','system','2018-12-03 11:20:57','system','2018-12-03 11:20:57',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431264113930240','0','0,',40,'0000000040,','1',0,'APP','APP','app','sys_msg_type','1','','','','0','system','2018-12-03 11:20:57','system','2018-12-03 11:20:57',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431265162506240','0','0,',50,'0000000050,','1',0,'短信','短信','sms','sys_msg_type','1','','','','0','system','2018-12-03 11:20:57','system','2018-12-03 11:20:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431266265608192','0','0,',60,'0000000060,','1',0,'邮件','邮件','email','sys_msg_type','1','','','','0','system','2018-12-03 11:20:58','system','2018-12-03 11:20:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431266412408832','0','0,',70,'0000000070,','1',0,'微信','微信','weixin','sys_msg_type','1','','','','0','system','2018-12-03 11:20:58','system','2018-12-03 11:20:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431267993661440','0','0,',30,'0000000030,','1',0,'待推送','待推送','0','sys_msg_push_status','1','推送状态','','','0','system','2018-12-03 11:20:58','system','2018-12-03 11:20:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431268140462080','0','0,',30,'0000000030,','1',0,'成功','成功','1','sys_msg_push_status','1','','','','0','system','2018-12-03 11:20:58','system','2018-12-03 11:20:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431269558136832','0','0,',40,'0000000040,','1',0,'失败','失败','2','sys_msg_push_status','1','','color:#f00;','','0','system','2018-12-03 11:20:58','system','2018-12-03 11:20:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431269679771648','0','0,',30,'0000000030,','1',0,'未送达','未送达','0','sys_msg_read_status','1','读取状态','','','0','system','2018-12-03 11:20:58','system','2018-12-03 11:20:59',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431271005171712','0','0,',40,'0000000040,','1',0,'已读','已读','1','sys_msg_read_status','1','','','','0','system','2018-12-03 11:20:59','system','2018-12-03 11:20:59',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431271118417920','0','0,',50,'0000000050,','1',0,'未读','未读','2','sys_msg_read_status','1','','','','0','system','2018-12-03 11:20:59','system','2018-12-03 11:20:59',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431272104079360','0','0,',30,'0000000030,','1',0,'普通','普通','1','msg_inner_content_level','1','内容级别','','','0','system','2018-12-03 11:20:59','system','2018-12-03 11:21:00',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431274813599744','0','0,',40,'0000000040,','1',0,'一般','一般','2','msg_inner_content_level','1','','','','0','system','2018-12-03 11:21:00','system','2018-12-03 11:21:00',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431274972983296','0','0,',50,'0000000050,','1',0,'紧急','紧急','3','msg_inner_content_level','1','','color:#f00;','','0','system','2018-12-03 11:21:00','system','2018-12-03 11:21:00',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431277019803648','0','0,',30,'0000000030,','1',0,'公告','公告','1','msg_inner_content_type','1','内容类型','','','0','system','2018-12-03 11:21:00','system','2018-12-03 11:21:00',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431277137244160','0','0,',40,'0000000040,','1',0,'新闻','新闻','2','msg_inner_content_type','1','','','','0','system','2018-12-03 11:21:00','system','2018-12-03 11:21:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431278680748032','0','0,',50,'0000000050,','1',0,'会议','会议','3','msg_inner_content_type','1','','','','0','system','2018-12-03 11:21:01','system','2018-12-03 11:21:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431278944989184','0','0,',60,'0000000060,','1',0,'其它','其它','4','msg_inner_content_type','1','','','','0','system','2018-12-03 11:21:01','system','2018-12-03 11:21:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431280341692416','0','0,',30,'0000000030,','1',0,'用户','用户','1','msg_inner_receiver_type','1','接受类型','','','0','system','2018-12-03 11:21:01','system','2018-12-03 11:21:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431280522047488','0','0,',40,'0000000040,','1',0,'部门','部门','2','msg_inner_receiver_type','1','','','','0','system','2018-12-03 11:21:01','system','2018-12-03 11:21:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431281809698816','0','0,',50,'0000000050,','1',0,'角色','角色','3','msg_inner_receiver_type','1','','','','0','system','2018-12-03 11:21:01','system','2018-12-03 11:21:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431282027802624','0','0,',60,'0000000060,','1',0,'岗位','岗位','4','msg_inner_receiver_type','1','','','','0','system','2018-12-03 11:21:02','system','2018-12-03 11:21:02',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431283508391936','0','0,',30,'0000000030,','1',0,'正常','正常','0','msg_inner_msg_status','1','消息状态','','','0','system','2018-12-03 11:21:02','system','2018-12-03 11:21:02',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431286138220544','0','0,',40,'0000000040,','1',0,'删除','删除','1','msg_inner_msg_status','1','','','','0','system','2018-12-03 11:21:02','system','2018-12-03 11:21:02',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431286268243968','0','0,',50,'0000000050,','1',0,'审核','审核','4','msg_inner_msg_status','1','','','','0','system','2018-12-03 11:21:02','system','2018-12-03 11:21:03',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431288113737728','0','0,',60,'0000000060,','1',0,'驳回','驳回','5','msg_inner_msg_status','1','','color:#f00;','','0','system','2018-12-03 11:21:03','system','2018-12-03 11:21:03',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431288495419392','0','0,',70,'0000000070,','1',0,'草稿','草稿','9','msg_inner_msg_status','1','','','','0','system','2018-12-03 11:21:03','system','2018-12-03 11:21:03',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1106135527342673920','0','0,',20,'0000000020,','1',0,'全部','全部','0','msg_inner_receiver_type','1','','','','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41','','0','JeeSite','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1126375062364020736','0','0,',60,'0000000060,','1',0,'日本語','日本語','ja_JP','sys_lang_type','1','','','','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41','','0','JeeSite','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1149344606834356224','0','0,',30,'0000000030,','1',0,'组织管理','组织管理','office_user','sys_role_biz_scope','1','','','','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41','','0','JeeSite','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_dict_type`
--

DROP TABLE IF EXISTS `js_sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_dict_type` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `dict_name` varchar(100) NOT NULL COMMENT '字典名称',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `is_sys` char(1) NOT NULL COMMENT '是否系统字典',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_dict_type_is` (`is_sys`),
  KEY `idx_sys_dict_type_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_dict_type`
--

LOCK TABLES `js_sys_dict_type` WRITE;
/*!40000 ALTER TABLE `js_sys_dict_type` DISABLE KEYS */;
INSERT INTO `js_sys_dict_type` VALUES ('1069431166361481216','是否','sys_yes_no','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431166562807808','状态','sys_status','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431166692831232','显示隐藏','sys_show_hide','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431166814466048','国际化语言类型','sys_lang_type','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431166894157824','客户端设备类型','sys_device_type','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431166965460992','菜单归属系统','sys_menu_sys_code','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431167040958464','菜单类型','sys_menu_type','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431167783350272','菜单权重','sys_menu_weight','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431167842070528','区域类型','sys_area_type','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431167909179392','机构类型','sys_office_type','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431168001454080','查询状态','sys_search_status','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431168072757248','用户性别','sys_user_sex','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('1069431168144060416','用户状态','sys_user_status','1','0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:35',NULL),('1069431169972776960','用户类型','sys_user_type','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431170245406720','角色分类','sys_role_type','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431170778083328','角色数据范围','sys_role_data_scope','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431171088461824','岗位分类','sys_post_type','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431171218485248','日志类型','sys_log_type','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431171314954240','作业分组','sys_job_group','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431171386257408','作业错过策略','sys_job_misfire_instruction','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431173412106240','作业状态','sys_job_status','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:35',NULL),('1069431173487603712','作业任务类型','sys_job_type','1','0','system','2018-12-03 11:20:35','system','2018-12-03 11:20:36',NULL),('1069431173634404352','作业任务事件','sys_job_event','1','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:36',NULL),('1069431173718290432','消息类型','sys_msg_type','1','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:36',NULL),('1069431173806370816','推送状态','sys_msg_push_status','1','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:36',NULL),('1069431173898645504','读取状态','sys_msg_read_status','1','0','system','2018-12-03 11:20:36','system','2018-12-03 11:20:36',NULL),('1105440848414543872','消息状态','msg_inner_msg_status','0','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41',''),('1149344200121085952','角色业务范围','sys_role_biz_scope','1','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41','');
/*!40000 ALTER TABLE `js_sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_employee`
--

DROP TABLE IF EXISTS `js_sys_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_employee` (
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `emp_name` varchar(100) NOT NULL COMMENT '员工姓名',
  `emp_name_en` varchar(100) DEFAULT NULL COMMENT '英文名',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `office_name` varchar(100) NOT NULL COMMENT '机构名称',
  `company_code` varchar(64) DEFAULT NULL COMMENT '公司编码',
  `company_name` varchar(200) DEFAULT NULL COMMENT '公司名称',
  `status` char(1) NOT NULL COMMENT '状态（0在职 1删除 2离职）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`emp_code`),
  KEY `idx_sys_employee_cco` (`company_code`),
  KEY `idx_sys_employee_cc` (`corp_code`),
  KEY `idx_sys_employee_ud` (`update_date`),
  KEY `idx_sys_employee_oc` (`office_code`),
  KEY `idx_sys_employee_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_employee`
--

LOCK TABLES `js_sys_employee` WRITE;
/*!40000 ALTER TABLE `js_sys_employee` DISABLE KEYS */;
INSERT INTO `js_sys_employee` VALUES ('user10_nw09','用户10',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user11_ibcm','用户11',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user12_8hyj','用户12',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user13_h3sk','用户13',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user14_deqy','用户14',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user15_c5sr','用户15',NULL,'SDQD01','企管部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user16_pkfs','用户16',NULL,'SDQD01','企管部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user17_ogfo','用户17',NULL,'SDQD01','企管部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user18_ycie','用户18',NULL,'SDQD02','财务部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user19_urge','用户19',NULL,'SDQD02','财务部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user1_cwme','用户01',NULL,'SDJN01','企管部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user20_e7n4','用户20',NULL,'SDQD02','财务部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user21_oxfg','用户21',NULL,'SDQD03','研发部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user22_o5j5','用户22',NULL,'SDQD03','研发部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user23_ymbm','用户23',NULL,'SDQD03','研发部','SDQD','青岛公司','0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite'),('user2_fuda','用户02',NULL,'SDJN01','企管部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user3_ll1n','用户03',NULL,'SDJN01','企管部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user4_m1vk','用户04',NULL,'SDJN02','财务部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user5_pwtv','用户05',NULL,'SDJN02','财务部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user6_hpw5','用户06',NULL,'SDJN02','财务部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user7_lxut','用户07',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user8_wp9j','用户08',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user9_twd4','用户09',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite');
/*!40000 ALTER TABLE `js_sys_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_employee_office`
--

DROP TABLE IF EXISTS `js_sys_employee_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_employee_office` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `post_code` varchar(64) DEFAULT NULL COMMENT '岗位编码',
  PRIMARY KEY (`emp_code`,`office_code`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='员工附属机构关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_employee_office`
--

LOCK TABLES `js_sys_employee_office` WRITE;
/*!40000 ALTER TABLE `js_sys_employee_office` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_employee_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_employee_post`
--

DROP TABLE IF EXISTS `js_sys_employee_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_employee_post` (
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  PRIMARY KEY (`emp_code`,`post_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工与岗位关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_employee_post`
--

LOCK TABLES `js_sys_employee_post` WRITE;
/*!40000 ALTER TABLE `js_sys_employee_post` DISABLE KEYS */;
INSERT INTO `js_sys_employee_post` VALUES ('user10_nw09','user'),('user11_ibcm','user'),('user12_8hyj','user'),('user13_h3sk','user'),('user14_deqy','dept'),('user15_c5sr','dept'),('user16_pkfs','user'),('user17_ogfo','user'),('user18_ycie','dept'),('user19_urge','user'),('user1_cwme','dept'),('user20_e7n4','user'),('user21_oxfg','dept'),('user22_o5j5','user'),('user23_ymbm','user'),('user2_fuda','user'),('user3_ll1n','user'),('user4_m1vk','dept'),('user5_pwtv','user'),('user6_hpw5','user'),('user7_lxut','dept'),('user8_wp9j','user'),('user9_twd4','user');
/*!40000 ALTER TABLE `js_sys_employee_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_file_entity`
--

DROP TABLE IF EXISTS `js_sys_file_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_file_entity` (
  `file_id` varchar(64) NOT NULL COMMENT '文件编号',
  `file_md5` varchar(64) NOT NULL COMMENT '文件MD5',
  `file_path` varchar(1000) NOT NULL COMMENT '文件相对路径',
  `file_content_type` varchar(200) NOT NULL COMMENT '文件内容类型',
  `file_extension` varchar(100) NOT NULL COMMENT '文件后缀扩展名',
  `file_size` decimal(31,0) NOT NULL COMMENT '文件大小(单位B)',
  `file_meta` varchar(255) DEFAULT NULL COMMENT '文件信息(JSON格式)',
  PRIMARY KEY (`file_id`),
  KEY `idx_sys_file_entity_md5` (`file_md5`),
  KEY `idx_sys_file_entity_size` (`file_size`),
  KEY `file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件实体表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_file_entity`
--

LOCK TABLES `js_sys_file_entity` WRITE;
/*!40000 ALTER TABLE `js_sys_file_entity` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_file_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_file_upload`
--

DROP TABLE IF EXISTS `js_sys_file_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_file_upload` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `file_id` varchar(64) NOT NULL COMMENT '文件编号',
  `file_name` varchar(500) NOT NULL COMMENT '文件名称',
  `file_type` varchar(20) NOT NULL COMMENT '文件分类（image、media、file）',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_file_biz_ft` (`file_type`),
  KEY `idx_sys_file_biz_fi` (`file_id`),
  KEY `idx_sys_file_biz_status` (`status`),
  KEY `idx_sys_file_biz_cb` (`create_by`),
  KEY `idx_sys_file_biz_ud` (`update_date`),
  KEY `idx_sys_file_biz_bt` (`biz_type`),
  KEY `idx_sys_file_biz_bk` (`biz_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_file_upload`
--

LOCK TABLES `js_sys_file_upload` WRITE;
/*!40000 ALTER TABLE `js_sys_file_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_file_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_job`
--

DROP TABLE IF EXISTS `js_sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_job` (
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `description` varchar(100) NOT NULL COMMENT '任务描述',
  `invoke_target` varchar(1000) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) NOT NULL COMMENT 'Cron执行表达式',
  `misfire_instruction` decimal(1,0) NOT NULL COMMENT '计划执行错误策略',
  `concurrent` char(1) NOT NULL COMMENT '是否并发执行',
  `instance_name` varchar(64) NOT NULL DEFAULT 'JeeSiteScheduler' COMMENT '集群的实例名字',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2暂停）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`job_name`,`job_group`),
  KEY `idx_sys_job_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业调度表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_job`
--

LOCK TABLES `js_sys_job` WRITE;
/*!40000 ALTER TABLE `js_sys_job` DISABLE KEYS */;
INSERT INTO `js_sys_job` VALUES ('MsgLocalMergePushTask','SYSTEM','消息推送服务 (延迟推送)','msgLocalMergePushTask.execute()','0 0/30 * * * ?',2,'0','JeeSiteScheduler','2','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL),('MsgLocalPushTask','SYSTEM','消息推送服务 (实时推送)','msgLocalPushTask.execute()','0/3 * * * * ?',2,'0','JeeSiteScheduler','2','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL);
/*!40000 ALTER TABLE `js_sys_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_job_log`
--

DROP TABLE IF EXISTS `js_sys_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_job_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `job_type` varchar(50) DEFAULT NULL COMMENT '日志类型',
  `job_event` varchar(200) DEFAULT NULL COMMENT '日志事件',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `is_exception` char(1) DEFAULT NULL COMMENT '是否异常',
  `exception_info` text COMMENT '异常信息',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_job_log_jn` (`job_name`),
  KEY `idx_sys_job_log_jg` (`job_group`),
  KEY `idx_sys_job_log_t` (`job_type`),
  KEY `idx_sys_job_log_e` (`job_event`),
  KEY `idx_sys_job_log_ie` (`is_exception`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业调度日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_job_log`
--

LOCK TABLES `js_sys_job_log` WRITE;
/*!40000 ALTER TABLE `js_sys_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_lang`
--

DROP TABLE IF EXISTS `js_sys_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_lang` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `module_code` varchar(64) NOT NULL COMMENT '归属模块',
  `lang_code` varchar(500) NOT NULL COMMENT '语言编码',
  `lang_text` varchar(500) NOT NULL COMMENT '语言译文',
  `lang_type` varchar(50) NOT NULL COMMENT '语言类型',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_lang_code` (`lang_code`(255)),
  KEY `idx_sys_lang_type` (`lang_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国际化语言';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_lang`
--

LOCK TABLES `js_sys_lang` WRITE;
/*!40000 ALTER TABLE `js_sys_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_log`
--

DROP TABLE IF EXISTS `js_sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `log_type` varchar(50) NOT NULL COMMENT '日志类型',
  `log_title` varchar(500) NOT NULL COMMENT '日志标题',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_by_name` varchar(100) NOT NULL COMMENT '用户名称',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `request_uri` varchar(500) DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(10) DEFAULT NULL COMMENT '操作方式',
  `request_params` longtext COMMENT '操作提交的数据',
  `diff_modify_data` text COMMENT '新旧数据比较结果',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `remote_addr` varchar(255) NOT NULL COMMENT '操作IP地址',
  `server_addr` varchar(255) NOT NULL COMMENT '请求服务器地址',
  `is_exception` char(1) DEFAULT NULL COMMENT '是否异常',
  `exception_info` text COMMENT '异常信息',
  `user_agent` varchar(500) DEFAULT NULL COMMENT '用户代理',
  `device_name` varchar(100) DEFAULT NULL COMMENT '设备名称/操作系统',
  `browser_name` varchar(100) DEFAULT NULL COMMENT '浏览器名称',
  `execute_time` decimal(19,0) DEFAULT NULL COMMENT '执行时间',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`id`),
  KEY `idx_sys_log_cb` (`create_by`),
  KEY `idx_sys_log_cc` (`corp_code`),
  KEY `idx_sys_log_lt` (`log_type`),
  KEY `idx_sys_log_bk` (`biz_key`),
  KEY `idx_sys_log_bt` (`biz_type`),
  KEY `idx_sys_log_ie` (`is_exception`),
  KEY `idx_sys_log_cd` (`create_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_log`
--

LOCK TABLES `js_sys_log` WRITE;
/*!40000 ALTER TABLE `js_sys_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_menu`
--

DROP TABLE IF EXISTS `js_sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_menu` (
  `menu_code` varchar(64) NOT NULL COMMENT '菜单编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `menu_name` varchar(100) NOT NULL COMMENT '菜单名称',
  `menu_type` char(1) NOT NULL COMMENT '菜单类型（1菜单 2权限 3开发）',
  `menu_href` varchar(1000) DEFAULT NULL COMMENT '链接',
  `menu_target` varchar(20) DEFAULT NULL COMMENT '目标',
  `menu_icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `menu_color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `menu_title` varchar(100) DEFAULT NULL COMMENT '菜单标题',
  `permission` varchar(1000) DEFAULT NULL COMMENT '权限标识',
  `weight` decimal(4,0) DEFAULT NULL COMMENT '菜单权重',
  `is_show` char(1) NOT NULL COMMENT '是否显示（1显示 0隐藏）',
  `sys_code` varchar(64) NOT NULL COMMENT '归属系统（default:主导航菜单、mobileApp:APP菜单）',
  `module_codes` varchar(500) NOT NULL COMMENT '归属模块（多个用逗号隔开）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`menu_code`),
  KEY `idx_sys_menu_pc` (`parent_code`),
  KEY `idx_sys_menu_ts` (`tree_sort`),
  KEY `idx_sys_menu_status` (`status`),
  KEY `idx_sys_menu_mt` (`menu_type`),
  KEY `idx_sys_menu_pss` (`parent_codes`(255)),
  KEY `idx_sys_menu_tss` (`tree_sorts`(255)),
  KEY `idx_sys_menu_sc` (`sys_code`),
  KEY `idx_sys_menu_is` (`is_show`),
  KEY `idx_sys_menu_mcs` (`module_codes`(255)),
  KEY `idx_sys_menu_wt` (`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_menu`
--

LOCK TABLES `js_sys_menu` WRITE;
/*!40000 ALTER TABLE `js_sys_menu` DISABLE KEYS */;
INSERT INTO `js_sys_menu` VALUES ('1028924699103330304','0','0,',9030,'0000009030,','0',0,'JFlow流程','JFlow流程','1','','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 16:42:19','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028924891047264256','1028924699103330304','0,1028924699103330304,',30,'0000009030,0000000030,','1',1,'JFlow流程/流程设计器','流程设计器','1','//WF/Admin/CCBPMDesigner/Default.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 16:43:05','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028925472004505600','1028924699103330304','0,1028924699103330304,',60,'0000009030,0000000060,','0',1,'JFlow流程/流程办理','流程办理','1','','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 16:45:24','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933723626536960','1028925472004505600','0,1028924699103330304,1028925472004505600,',30,'0000009030,0000000060,0000000030,','1',2,'JFlow流程/流程办理/发起','发起','1','//WF/Start.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:18:11','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933804492718080','1028925472004505600','0,1028924699103330304,1028925472004505600,',60,'0000009030,0000000060,0000000060,','1',2,'JFlow流程/流程办理/待办','待办','1','//WF/Todolist.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:18:30','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933895135821824','1028925472004505600','0,1028924699103330304,1028925472004505600,',90,'0000009030,0000000060,0000000090,','1',2,'JFlow流程/流程办理/会签','会签','1','//WF/HuiQianList.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:18:52','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933988475863040','1028925472004505600','0,1028924699103330304,1028925472004505600,',120,'0000009030,0000000060,0000000120,','1',2,'JFlow流程/流程办理/在途','在途','1','//WF/Runing.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:19:14','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934263999692800','1028924699103330304','0,1028924699103330304,',90,'0000009030,0000000090,','0',1,'JFlow流程/流程查询','流程查询','1','','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:20:20','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934340843536384','1028934263999692800','0,1028924699103330304,1028934263999692800,',30,'0000009030,0000000090,0000000030,','1',2,'JFlow流程/流程查询/我发起的','我发起的','1','//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyStartFlows','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:20:38','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934445013270528','1028934263999692800','0,1028924699103330304,1028934263999692800,',60,'0000009030,0000000090,0000000060,','1',2,'JFlow流程/流程查询/我审批的','我审批的','1','//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyJoinFlows','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:21:03','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934560448905216','1028934263999692800','0,1028924699103330304,1028934263999692800,',90,'0000009030,0000000090,0000000090,','1',2,'JFlow流程/流程查询/流程分布','流程分布','1','//WF/RptSearch/DistributedOfMy.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:21:31','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934755261743104','1028934263999692800','0,1028924699103330304,1028934263999692800,',120,'0000009030,0000000090,0000000120,','1',2,'JFlow流程/流程查询/我的流程','我的流程','1','//WF/Search.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:22:17','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934869279703040','1028934263999692800','0,1028924699103330304,1028934263999692800,',150,'0000009030,0000000090,0000000150,','1',2,'JFlow流程/流程查询/单流程查询','单流程查询','1','//WF/RptDfine/Flowlist.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:22:44','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935178097917952','1028934263999692800','0,1028924699103330304,1028934263999692800,',180,'0000009030,0000000090,0000000180,','1',2,'JFlow流程/流程查询/综合查询','综合查询','1','//WF/RptSearch/Default.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:23:58','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935301783748608','1028925472004505600','0,1028924699103330304,1028925472004505600,',150,'0000009030,0000000060,0000000150,','1',2,'JFlow流程/流程办理/共享任务','共享任务','1','//WF/TaskPoolSharing.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:24:27','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935378346573824','1028925472004505600','0,1028924699103330304,1028925472004505600,',180,'0000009030,0000000060,0000000180,','1',2,'JFlow流程/流程办理/申请下来的任务','申请下来的任务','1','//WF/TaskPoolApply.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:24:46','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935550501781504','1028924699103330304','0,1028924699103330304,',120,'0000009030,0000000120,','0',1,'JFlow流程/高级功能','高级功能','1','','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:25:27','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935644458385408','1028935550501781504','0,1028924699103330304,1028935550501781504,',30,'0000009030,0000000120,0000000030,','1',2,'JFlow流程/高级功能/我的草稿','我的草稿','1','//WF/Draft.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:25:49','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935717149868032','1028935550501781504','0,1028924699103330304,1028935550501781504,',60,'0000009030,0000000120,0000000060,','1',2,'JFlow流程/高级功能/抄送','抄送','1','//WF/CC.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:26:06','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935775526191104','1028935550501781504','0,1028924699103330304,1028935550501781504,',90,'0000009030,0000000120,0000000090,','1',2,'JFlow流程/高级功能/我的关注','我的关注','1','//WF/Focus.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:26:20','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935931373944832','1028935550501781504','0,1028924699103330304,1028935550501781504,',120,'0000009030,0000000120,0000000120,','1',2,'JFlow流程/高级功能/授权待办','授权待办','1','//WF/TodolistOfAuth.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:26:57','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028936085451702272','1028935550501781504','0,1028924699103330304,1028935550501781504,',150,'0000009030,0000000120,0000000150,','1',2,'JFlow流程/高级功能/挂起的工作','挂起的工作','1','//WF/HungUpList.htm','','','',NULL,'',40,'1','default','jflow','0','system','2018-08-13 20:27:34','system','2019-09-02 17:07:06','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431293998346240','0','0,',9000,'0000009000,','0',0,'系统管理','系统管理','1','','','icon-settings','',NULL,'',40,'1','default','core','0','system','2018-12-03 11:21:04','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431294329696256','1069431293998346240','0,1069431293998346240,',300,'0000009000,0000000300,','0',1,'系统管理/组织管理','组织管理','1','','','icon-grid','',NULL,'',40,'1','default','core','0','system','2018-12-03 11:21:04','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431296649146368','1069431294329696256','0,1069431293998346240,1069431294329696256,',40,'0000009000,0000000300,0000000040,','0',2,'系统管理/组织管理/用户管理','用户管理','1','/sys/empUser/index','','icon-user','',NULL,'',40,'1','default','core','0','system','2018-12-03 11:21:05','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431298603692032','1069431296649146368','0,1069431293998346240,1069431294329696256,1069431296649146368,',30,'0000009000,0000000300,0000000040,0000000030,','1',3,'系统管理/组织管理/用户管理/查看','查看','2','','','','',NULL,'sys:empUser:view',40,'1','default','core','0','system','2018-12-03 11:21:05','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431298796630016','1069431296649146368','0,1069431293998346240,1069431294329696256,1069431296649146368,',40,'0000009000,0000000300,0000000040,0000000040,','1',3,'系统管理/组织管理/用户管理/编辑','编辑','2','','','','',NULL,'sys:empUser:edit',40,'1','default','core','0','system','2018-12-03 11:21:05','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431300721815552','1069431296649146368','0,1069431293998346240,1069431294329696256,1069431296649146368,',60,'0000009000,0000000300,0000000040,0000000060,','1',3,'系统管理/组织管理/用户管理/分配角色','分配角色','2','','','','',NULL,'sys:empUser:authRole',40,'1','default','core','0','system','2018-12-03 11:21:06','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431302433091584','1069431296649146368','0,1069431293998346240,1069431294329696256,1069431296649146368,',50,'0000009000,0000000300,0000000040,0000000050,','1',3,'系统管理/组织管理/用户管理/分配数据','分配数据','2','','','','',NULL,'sys:empUser:authDataScope',40,'1','default','core','0','system','2018-12-03 11:21:06','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431302592475136','1069431296649146368','0,1069431293998346240,1069431294329696256,1069431296649146368,',60,'0000009000,0000000300,0000000040,0000000060,','1',3,'系统管理/组织管理/用户管理/停用启用','停用启用','2','','','','',NULL,'sys:empUser:updateStatus',40,'1','default','core','0','system','2018-12-03 11:21:06','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431304442163200','1069431296649146368','0,1069431293998346240,1069431294329696256,1069431296649146368,',70,'0000009000,0000000300,0000000040,0000000070,','1',3,'系统管理/组织管理/用户管理/重置密码','重置密码','2','','','','',NULL,'sys:empUser:resetpwd',40,'1','default','core','0','system','2018-12-03 11:21:07','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431306438651904','1069431294329696256','0,1069431293998346240,1069431294329696256,',50,'0000009000,0000000300,0000000050,','0',2,'系统管理/组织管理/机构管理','机构管理','1','/sys/office/list','','icon-grid','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:07','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431306631589888','1069431306438651904','0,1069431293998346240,1069431294329696256,1069431306438651904,',30,'0000009000,0000000300,0000000050,0000000030,','1',3,'系统管理/组织管理/机构管理/查看','查看','2','','','','',NULL,'sys:office:view',60,'1','default','core','0','system','2018-12-03 11:21:07','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431308800045056','1069431306438651904','0,1069431293998346240,1069431294329696256,1069431306438651904,',40,'0000009000,0000000300,0000000050,0000000040,','1',3,'系统管理/组织管理/机构管理/编辑','编辑','2','','','','',NULL,'sys:office:edit',60,'1','default','core','0','system','2018-12-03 11:21:08','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431310695870464','1069431294329696256','0,1069431293998346240,1069431294329696256,',70,'0000009000,0000000300,0000000070,','0',2,'系统管理/组织管理/公司管理','公司管理','1','/sys/company/list','','icon-fire','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:08','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431310913974272','1069431310695870464','0,1069431293998346240,1069431294329696256,1069431310695870464,',30,'0000009000,0000000300,0000000070,0000000030,','1',3,'系统管理/组织管理/公司管理/查看','查看','2','','','','',NULL,'sys:company:view',60,'1','default','core','0','system','2018-12-03 11:21:08','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431313849987072','1069431310695870464','0,1069431293998346240,1069431294329696256,1069431310695870464,',40,'0000009000,0000000300,0000000070,0000000040,','1',3,'系统管理/组织管理/公司管理/编辑','编辑','2','','','','',NULL,'sys:company:edit',60,'1','default','core','0','system','2018-12-03 11:21:09','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431315615789056','1069431294329696256','0,1069431293998346240,1069431294329696256,',70,'0000009000,0000000300,0000000070,','0',2,'系统管理/组织管理/岗位管理','岗位管理','1','/sys/post/list','','icon-trophy','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:09','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431315817115648','1069431315615789056','0,1069431293998346240,1069431294329696256,1069431315615789056,',30,'0000009000,0000000300,0000000070,0000000030,','1',3,'系统管理/组织管理/岗位管理/查看','查看','2','','','','',NULL,'sys:post:view',60,'1','default','core','0','system','2018-12-03 11:21:09','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431318912512000','1069431315615789056','0,1069431293998346240,1069431294329696256,1069431315615789056,',40,'0000009000,0000000300,0000000070,0000000040,','1',3,'系统管理/组织管理/岗位管理/编辑','编辑','2','','','','',NULL,'sys:post:edit',60,'1','default','core','0','system','2018-12-03 11:21:10','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431320846086144','1069431293998346240','0,1069431293998346240,',400,'0000009000,0000000400,','0',1,'系统管理/权限管理','权限管理','1','','','icon-social-dropbox','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:11','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431321043218432','1069431320846086144','0,1069431293998346240,1069431320846086144,',30,'0000009000,0000000400,0000000030,','1',2,'系统管理/权限管理/角色管理','角色管理','1','/sys/role/list','','icon-people','',NULL,'sys:role',60,'1','default','core','0','system','2018-12-03 11:21:11','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431324021174272','1069431320846086144','0,1069431293998346240,1069431320846086144,',40,'0000009000,0000000400,0000000040,','1',2,'系统管理/权限管理/二级管理员','二级管理员','1','/sys/secAdmin/list','','icon-user-female','',NULL,'sys:secAdmin',60,'1','default','core','0','system','2018-12-03 11:21:11','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431325766004736','1069431320846086144','0,1069431293998346240,1069431320846086144,',50,'0000009000,0000000400,0000000050,','1',2,'系统管理/权限管理/系统管理员','系统管理员','1','/sys/corpAdmin/list','','icon-badge','',NULL,'sys:corpAdmin',80,'1','default','core','0','system','2018-12-03 11:21:12','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431325963137024','1069431293998346240','0,1069431293998346240,',500,'0000009000,0000000500,','0',1,'系统管理/系统设置','系统设置','1','','','icon-settings','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:12','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431328970452992','1069431325963137024','0,1069431293998346240,1069431325963137024,',30,'0000009000,0000000500,0000000030,','1',2,'系统管理/系统设置/菜单管理','菜单管理','1','/sys/menu/list','','icon-book-open','',NULL,'sys:menu',80,'1','default','core','0','system','2018-12-03 11:21:13','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431330744643584','1069431325963137024','0,1069431293998346240,1069431325963137024,',40,'0000009000,0000000500,0000000040,','1',2,'系统管理/系统设置/模块管理','模块管理','1','/sys/module/list','','icon-grid','',NULL,'sys:module',80,'1','default','core','0','system','2018-12-03 11:21:13','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431330924998656','1069431325963137024','0,1069431293998346240,1069431325963137024,',50,'0000009000,0000000500,0000000050,','1',2,'系统管理/系统设置/参数设置','参数设置','1','/sys/config/list','','icon-wrench','',NULL,'sys:config',60,'1','default','core','0','system','2018-12-03 11:21:13','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431332514639872','1069431325963137024','0,1069431293998346240,1069431325963137024,',60,'0000009000,0000000500,0000000060,','0',2,'系统管理/系统设置/字典管理','字典管理','1','/sys/dictType/list','','icon-social-dropbox','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:13','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431334368522240','1069431332514639872','0,1069431293998346240,1069431325963137024,1069431332514639872,',30,'0000009000,0000000500,0000000060,0000000030,','1',3,'系统管理/系统设置/字典管理/类型查看','类型查看','2','','','','',NULL,'sys:dictType:view',60,'1','default','core','0','system','2018-12-03 11:21:14','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431334557265920','1069431332514639872','0,1069431293998346240,1069431325963137024,1069431332514639872,',40,'0000009000,0000000500,0000000060,0000000040,','1',3,'系统管理/系统设置/字典管理/类型编辑','类型编辑','2','','','','',NULL,'sys:dictType:edit',80,'1','default','core','0','system','2018-12-03 11:21:14','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431336167878656','1069431332514639872','0,1069431293998346240,1069431325963137024,1069431332514639872,',50,'0000009000,0000000500,0000000060,0000000050,','1',3,'系统管理/系统设置/字典管理/数据查看','数据查看','2','','','','',NULL,'sys:dictData:view',60,'1','default','core','0','system','2018-12-03 11:21:14','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431338181144576','1069431332514639872','0,1069431293998346240,1069431325963137024,1069431332514639872,',60,'0000009000,0000000500,0000000060,0000000060,','1',3,'系统管理/系统设置/字典管理/数据编辑','数据编辑','2','','','','',NULL,'sys:dictData:edit',60,'1','default','core','0','system','2018-12-03 11:21:15','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431340198604800','1069431325963137024','0,1069431293998346240,1069431325963137024,',70,'0000009000,0000000500,0000000070,','1',2,'系统管理/系统设置/行政区划','行政区划','1','/sys/area/list','','icon-map','',NULL,'sys:area',60,'1','default','core','0','system','2018-12-03 11:21:15','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431340357988352','1069431325963137024','0,1069431293998346240,1069431325963137024,',80,'0000009000,0000000500,0000000080,','1',2,'系统管理/系统设置/国际化管理','国际化管理','1','/sys/lang/list','','icon-globe','',NULL,'sys:lang',80,'1','default','core','0','system','2018-12-03 11:21:15','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431342304145408','1069431325963137024','0,1069431293998346240,1069431325963137024,',90,'0000009000,0000000500,0000000090,','1',2,'系统管理/系统设置/产品许可信息','产品许可信息','1','//licence','','icon-paper-plane','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:16','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431343885398016','1069431293998346240','0,1069431293998346240,',600,'0000009000,0000000600,','0',1,'系统管理/系统监控','系统监控','1','','','icon-ghost','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:16','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431344317411328','1069431343885398016','0,1069431293998346240,1069431343885398016,',40,'0000009000,0000000600,0000000040,','1',2,'系统管理/系统监控/访问日志','访问日志','1','/sys/log/list','','fa fa-bug','',NULL,'sys:log',60,'1','default','core','0','system','2018-12-03 11:21:16','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431345902858240','1069431343885398016','0,1069431293998346240,1069431343885398016,',50,'0000009000,0000000600,0000000050,','1',2,'系统管理/系统监控/数据监控','数据监控','1','//druid','','icon-disc','',NULL,'sys:state:druid',80,'1','default','core','0','system','2018-12-03 11:21:17','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431347442167808','1069431343885398016','0,1069431293998346240,1069431343885398016,',60,'0000009000,0000000600,0000000060,','1',2,'系统管理/系统监控/缓存监控','缓存监控','1','/state/cache/index','','icon-social-dribbble','',NULL,'sys:stste:cache',80,'1','default','core','0','system','2018-12-03 11:21:17','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431347626717184','1069431343885398016','0,1069431293998346240,1069431343885398016,',70,'0000009000,0000000600,0000000070,','1',2,'系统管理/系统监控/服务器监控','服务器监控','1','/state/server/index','','icon-speedometer','',NULL,'sys:state:server',80,'1','default','core','0','system','2018-12-03 11:21:17','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431349526736896','1069431343885398016','0,1069431293998346240,1069431343885398016,',80,'0000009000,0000000600,0000000080,','1',2,'系统管理/系统监控/作业监控','作业监控','1','/job/list','','icon-notebook','',NULL,'sys:job',80,'1','default','core','0','system','2018-12-03 11:21:17','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431351468699648','1069431343885398016','0,1069431293998346240,1069431343885398016,',90,'0000009000,0000000600,0000000090,','1',2,'系统管理/系统监控/在线用户','在线用户','1','/sys/online/list','','icon-social-twitter','',NULL,'sys:online',60,'1','default','core','0','system','2018-12-03 11:21:18','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431351686803456','1069431343885398016','0,1069431293998346240,1069431343885398016,',100,'0000009000,0000000600,0000000100,','1',2,'系统管理/系统监控/在线文档','在线文档','1','//swagger-ui.html','','icon-book-open','',NULL,'sys:swagger',80,'1','default','core','0','system','2018-12-03 11:21:18','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431353876230144','1069431293998346240','0,1069431293998346240,',700,'0000009000,0000000700,','0',1,'系统管理/消息推送','消息推送','1','','','icon-envelope-letter','',NULL,'',60,'1','default','core','0','system','2018-12-03 11:21:19','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431355646226432','1069431353876230144','0,1069431293998346240,1069431353876230144,',30,'0000009000,0000000700,0000000030,','1',2,'系统管理/消息推送/未完成消息','未完成消息','1','/msg/msgPush/list','','','',NULL,'msg:msgPush',60,'1','default','core','0','system','2018-12-03 11:21:19','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431355839164416','1069431353876230144','0,1069431293998346240,1069431353876230144,',40,'0000009000,0000000700,0000000040,','1',2,'系统管理/消息推送/已完成消息','已完成消息','1','/msg/msgPush/list?pushed=true','','','',NULL,'msg:msgPush',60,'1','default','core','0','system','2018-12-03 11:21:19','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431358032785408','1069431353876230144','0,1069431293998346240,1069431353876230144,',50,'0000009000,0000000700,0000000050,','1',2,'系统管理/消息推送/消息模板管理','消息模板管理','1','/msg/msgTemplate/list','','','',NULL,'msg:msgTemplate',60,'1','default','core','0','system','2018-12-03 11:21:19','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431359710507008','1069431293998346240','0,1069431293998346240,',900,'0000009000,0000000900,','0',1,'系统管理/研发工具','研发工具','1','','','fa fa-code','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:20','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431359953776640','1069431359710507008','0,1069431293998346240,1069431359710507008,',30,'0000009000,0000000900,0000000030,','1',2,'系统管理/研发工具/代码生成工具','代码生成工具','1','/gen/genTable/list','','fa fa-code','',NULL,'gen:genTable',80,'1','default','core','0','system','2018-12-03 11:21:20','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431362164174848','1069431359710507008','0,1069431293998346240,1069431359710507008,',100,'0000009000,0000000900,0000000100,','0',2,'系统管理/研发工具/代码生成实例','代码生成实例','1','','','icon-social-dropbox','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:20','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431363963531264','1069431362164174848','0,1069431293998346240,1069431359710507008,1069431362164174848,',30,'0000009000,0000000900,0000000100,0000000030,','1',3,'系统管理/研发工具/代码生成实例/单表_主子表','单表/主子表','1','/test/testData/list','','','',NULL,'test:testData',80,'1','default','core','0','system','2018-12-03 11:21:21','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431364265521152','1069431362164174848','0,1069431293998346240,1069431359710507008,1069431362164174848,',40,'0000009000,0000000900,0000000100,0000000040,','1',3,'系统管理/研发工具/代码生成实例/树表_树结构表','树表/树结构表','1','/test/testTree/list','','','',NULL,'test:testTree',80,'1','default','core','0','system','2018-12-03 11:21:21','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431367176368128','1069431359710507008','0,1069431293998346240,1069431359710507008,',200,'0000009000,0000000900,0000000200,','0',2,'系统管理/研发工具/数据表格实例','数据表格实例','1','','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:22','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431368921198592','1069431367176368128','0,1069431293998346240,1069431359710507008,1069431367176368128,',30,'0000009000,0000000900,0000000200,0000000030,','1',3,'系统管理/研发工具/数据表格实例/多表头分组小计合计','多表头分组小计合计','1','/demo/dataGrid/groupGrid','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:22','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431369105747968','1069431367176368128','0,1069431293998346240,1069431359710507008,1069431367176368128,',40,'0000009000,0000000900,0000000200,0000000040,','1',3,'系统管理/研发工具/数据表格实例/编辑表格多行编辑','编辑表格多行编辑','1','/demo/dataGrid/editGrid','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:22','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431372050149376','1069431359710507008','0,1069431293998346240,1069431359710507008,',300,'0000009000,0000000900,0000000300,','0',2,'系统管理/研发工具/表单组件实例','表单组件实例','1','','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:23','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431373895643136','1069431372050149376','0,1069431293998346240,1069431359710507008,1069431372050149376,',30,'0000009000,0000000900,0000000300,0000000030,','1',3,'系统管理/研发工具/表单组件实例/组件应用实例','组件应用实例','1','/demo/form/editForm','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:23','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431374126329856','1069431372050149376','0,1069431293998346240,1069431359710507008,1069431372050149376,',40,'0000009000,0000000900,0000000300,0000000040,','1',3,'系统管理/研发工具/表单组件实例/栅格布局实例','栅格布局实例','1','/demo/form/layoutForm','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:23','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431376357699584','1069431372050149376','0,1069431293998346240,1069431359710507008,1069431372050149376,',50,'0000009000,0000000900,0000000300,0000000050,','1',3,'系统管理/研发工具/表单组件实例/表格表单实例','表格表单实例','1','/demo/form/tableForm','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:24','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431377938952192','1069431359710507008','0,1069431293998346240,1069431359710507008,',400,'0000009000,0000000900,0000000400,','0',2,'系统管理/研发工具/前端界面实例','前端界面实例','1','','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:24','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431378144473088','1069431377938952192','0,1069431293998346240,1069431359710507008,1069431377938952192,',30,'0000009000,0000000900,0000000400,0000000030,','1',3,'系统管理/研发工具/前端界面实例/图标样式查找','图标样式查找','1','//tags/iconselect','','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:24','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431381374087168','1069431293998346240','0,1069431293998346240,',999,'0000009000,0000000999,','0',1,'系统管理/JeeSite社区','JeeSite社区','1','','','fa fa-code','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:25','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431383135694848','1069431381374087168','0,1069431293998346240,1069431381374087168,',30,'0000009000,0000000999,0000000030,','1',2,'系统管理/JeeSite社区/官方网站','官方网站','1','http://jeesite.com','_blank','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:25','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431383324438528','1069431381374087168','0,1069431293998346240,1069431381374087168,',50,'0000009000,0000000999,0000000050,','1',2,'系统管理/JeeSite社区/作者博客','作者博客','1','https://my.oschina.net/thinkgem','_blank','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:26','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431384863748096','1069431381374087168','0,1069431293998346240,1069431381374087168,',40,'0000009000,0000000999,0000000040,','1',2,'系统管理/JeeSite社区/问题反馈','问题反馈','1','https://gitee.com/thinkgem/jeesite4/issues','_blank','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:26','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069431386726019072','1069431381374087168','0,1069431293998346240,1069431381374087168,',60,'0000009000,0000000999,0000000060,','1',2,'系统管理/JeeSite社区/开源社区','开源社区','1','http://jeesite.net','_blank','','',NULL,'',80,'1','default','core','0','system','2018-12-03 11:21:26','system','2019-03-11 15:20:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1105443204287991808','0','0,',9030,'0000009030,','1',0,'站内消息','站内消息','1','/msg/msgInner/list','','icon-speech','',NULL,'msg:msgInner',40,'1','default','core','0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_module`
--

DROP TABLE IF EXISTS `js_sys_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_module` (
  `module_code` varchar(64) NOT NULL COMMENT '模块编码',
  `module_name` varchar(100) NOT NULL COMMENT '模块名称',
  `description` varchar(500) DEFAULT NULL COMMENT '模块描述',
  `main_class_name` varchar(500) DEFAULT NULL COMMENT '主类全名',
  `current_version` varchar(50) DEFAULT NULL COMMENT '当前版本',
  `upgrade_info` varchar(300) DEFAULT NULL COMMENT '升级信息',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`module_code`),
  KEY `idx_sys_module_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模块表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_module`
--

LOCK TABLES `js_sys_module` WRITE;
/*!40000 ALTER TABLE `js_sys_module` DISABLE KEYS */;
INSERT INTO `js_sys_module` VALUES ('cms','内容管理','网站、站点、栏目、文章、链接、评论、留言板','com.jeesite.modules.cms.web.CmsController','4.0.0',NULL,'0','system','2018-12-03 11:20:34','system','2018-12-03 11:20:34',NULL),('core','核心模块','用户、角色、组织、模块、菜单、字典、参数','com.jeesite.modules.sys.web.LoginController','4.1.6','upgrade 2019-08-30 11:44:40 (4.1.1 -> 4.1.6)','0','system','2018-12-03 11:20:33','system','2019-08-30 11:44:40',NULL),('filemanager','文件管理','公共文件柜、个人文件柜、文件分享','com.jeesite.modules.filemanager.web.FilemanagerController','4.1.4',NULL,'0','system','2019-08-30 11:44:41','system','2019-08-30 11:44:41',NULL),('jflow','JFlow工作流','适合OA、可视化的java工作流引擎、自定义表单引擎。','com.jeesite.modules.jflow.config.JflowConfig','4.0.6',NULL,'0','system','2018-08-13 21:47:17','system','2018-08-13 21:48:00',NULL);
/*!40000 ALTER TABLE `js_sys_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_inner`
--

DROP TABLE IF EXISTS `js_sys_msg_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_msg_inner` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `content_level` char(1) NOT NULL COMMENT '内容级别（1普通 2一般 3紧急）',
  `content_type` char(1) DEFAULT NULL COMMENT '内容类型（1公告 2新闻 3会议 4其它）',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `receive_type` char(1) NOT NULL COMMENT '接受者类型（1用户 2部门 3角色 4岗位）',
  `receive_codes` text NOT NULL COMMENT '接受者字符串',
  `receive_names` text NOT NULL COMMENT '接受者名称字符串',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_attac` char(1) DEFAULT NULL COMMENT '是否有附件',
  `notify_types` varchar(100) NOT NULL COMMENT '通知类型（PC APP 短信 邮件 微信）多选',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 4审核 5驳回 9草稿）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_inner_cb` (`create_by`),
  KEY `idx_sys_msg_inner_status` (`status`),
  KEY `idx_sys_msg_inner_cl` (`content_level`),
  KEY `idx_sys_msg_inner_sc` (`send_user_code`),
  KEY `idx_sys_msg_inner_sd` (`send_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内部消息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_inner`
--

LOCK TABLES `js_sys_msg_inner` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_inner` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_inner_record`
--

DROP TABLE IF EXISTS `js_sys_msg_inner_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_msg_inner_record` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_inner_id` varchar(64) NOT NULL COMMENT '所属消息',
  `receive_user_code` varchar(64) DEFAULT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `read_status` char(1) NOT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '阅读时间',
  `is_star` char(1) DEFAULT NULL COMMENT '是否标星',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_inner_r_mi` (`msg_inner_id`),
  KEY `idx_sys_msg_inner_r_rc` (`receive_user_code`),
  KEY `idx_sys_msg_inner_r_ruc` (`receive_user_code`),
  KEY `idx_sys_msg_inner_r_status` (`read_status`),
  KEY `idx_sys_msg_inner_r_star` (`is_star`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内部消息发送记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_inner_record`
--

LOCK TABLES `js_sys_msg_inner_record` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_inner_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_inner_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_push`
--

DROP TABLE IF EXISTS `js_sys_msg_push`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_msg_push` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_type` varchar(16) NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_code` varchar(200) DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) DEFAULT NULL COMMENT '推送返回消息编号',
  `push_return_content` text COMMENT '推送返回的内容信息',
  `push_status` char(1) DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) DEFAULT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_push_type` (`msg_type`),
  KEY `idx_sys_msg_push_rc` (`receive_code`),
  KEY `idx_sys_msg_push_uc` (`receive_user_code`),
  KEY `idx_sys_msg_push_suc` (`send_user_code`),
  KEY `idx_sys_msg_push_pd` (`plan_push_date`),
  KEY `idx_sys_msg_push_ps` (`push_status`),
  KEY `idx_sys_msg_push_rs` (`read_status`),
  KEY `idx_sys_msg_push_bk` (`biz_key`),
  KEY `idx_sys_msg_push_bt` (`biz_type`),
  KEY `idx_sys_msg_push_imp` (`is_merge_push`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_push`
--

LOCK TABLES `js_sys_msg_push` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_push` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_push` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_pushed`
--

DROP TABLE IF EXISTS `js_sys_msg_pushed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_msg_pushed` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_type` varchar(16) NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_content` text COMMENT '推送返回的内容信息',
  `push_return_code` varchar(200) DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) DEFAULT NULL COMMENT '推送返回消息编号',
  `push_status` char(1) DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) DEFAULT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_pushed_type` (`msg_type`),
  KEY `idx_sys_msg_pushed_rc` (`receive_code`),
  KEY `idx_sys_msg_pushed_uc` (`receive_user_code`),
  KEY `idx_sys_msg_pushed_suc` (`send_user_code`),
  KEY `idx_sys_msg_pushed_pd` (`plan_push_date`),
  KEY `idx_sys_msg_pushed_ps` (`push_status`),
  KEY `idx_sys_msg_pushed_rs` (`read_status`),
  KEY `idx_sys_msg_pushed_bk` (`biz_key`),
  KEY `idx_sys_msg_pushed_bt` (`biz_type`),
  KEY `idx_sys_msg_pushed_imp` (`is_merge_push`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息已推送表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_pushed`
--

LOCK TABLES `js_sys_msg_pushed` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_pushed` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_pushed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_template`
--

DROP TABLE IF EXISTS `js_sys_msg_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_msg_template` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `module_code` varchar(64) DEFAULT NULL COMMENT '归属模块',
  `tpl_key` varchar(100) NOT NULL COMMENT '模板键值',
  `tpl_name` varchar(100) NOT NULL COMMENT '模板名称',
  `tpl_type` varchar(16) NOT NULL COMMENT '模板类型',
  `tpl_content` text NOT NULL COMMENT '模板内容',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_tpl_key` (`tpl_key`),
  KEY `idx_sys_msg_tpl_type` (`tpl_type`),
  KEY `idx_sys_msg_tpl_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_template`
--

LOCK TABLES `js_sys_msg_template` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_office`
--

DROP TABLE IF EXISTS `js_sys_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_office` (
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) NOT NULL COMMENT '机构代码',
  `office_name` varchar(100) NOT NULL COMMENT '机构名称',
  `full_name` varchar(200) NOT NULL COMMENT '机构全称',
  `office_type` char(1) NOT NULL COMMENT '机构类型',
  `leader` varchar(100) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `zip_code` varchar(100) DEFAULT NULL COMMENT '邮政编码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`office_code`),
  KEY `idx_sys_office_cc` (`corp_code`),
  KEY `idx_sys_office_pc` (`parent_code`),
  KEY `idx_sys_office_pcs` (`parent_codes`(255)),
  KEY `idx_sys_office_status` (`status`),
  KEY `idx_sys_office_ot` (`office_type`),
  KEY `idx_sys_office_vc` (`view_code`),
  KEY `idx_sys_office_ts` (`tree_sort`),
  KEY `idx_sys_office_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_office`
--

LOCK TABLES `js_sys_office` WRITE;
/*!40000 ALTER TABLE `js_sys_office` DISABLE KEYS */;
INSERT INTO `js_sys_office` VALUES ('SD','0','0,',40,'0000000040,','0',0,'山东公司','SD','山东公司','山东公司','1',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN','SD','0,SD,',30,'0000000040,0000000030,','0',1,'山东公司/济南公司','SDJN','济南公司','山东济南公司','2',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN01','SDJN','0,SD,SDJN,',30,'0000000040,0000000030,0000000030,','1',2,'山东公司/济南公司/企管部','SDJN01','企管部','山东济南企管部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN02','SDJN','0,SD,SDJN,',40,'0000000040,0000000030,0000000040,','1',2,'山东公司/济南公司/财务部','SDJN02','财务部','山东济南财务部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN03','SDJN','0,SD,SDJN,',50,'0000000040,0000000030,0000000050,','1',2,'山东公司/济南公司/研发部','SDJN03','研发部','山东济南研发部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD','SD','0,SD,',40,'0000000040,0000000040,','0',1,'山东公司/青岛公司','SDQD','青岛公司','山东青岛公司','2',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD01','SDQD','0,SD,SDQD,',30,'0000000040,0000000040,0000000030,','1',2,'山东公司/青岛公司/企管部','SDQD01','企管部','山东青岛企管部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD02','SDQD','0,SD,SDQD,',40,'0000000040,0000000040,0000000040,','1',2,'山东公司/青岛公司/财务部','SDQD02','财务部','山东青岛财务部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD03','SDQD','0,SD,SDQD,',50,'0000000040,0000000040,0000000050,','1',2,'山东公司/青岛公司/研发部','SDQD03','研发部','山东青岛研发部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_post`
--

DROP TABLE IF EXISTS `js_sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_post` (
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(100) NOT NULL COMMENT '岗位名称',
  `post_type` varchar(100) DEFAULT NULL COMMENT '岗位分类（高管、中层、基层）',
  `post_sort` decimal(10,0) DEFAULT NULL COMMENT '岗位排序（升序）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`post_code`),
  KEY `idx_sys_post_cc` (`corp_code`),
  KEY `idx_sys_post_status` (`status`),
  KEY `idx_sys_post_ps` (`post_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工岗位表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_post`
--

LOCK TABLES `js_sys_post` WRITE;
/*!40000 ALTER TABLE `js_sys_post` DISABLE KEYS */;
INSERT INTO `js_sys_post` VALUES ('ceo','总经理',NULL,1,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('cfo','财务经理',NULL,2,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('dept','部门经理',NULL,2,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('hrm','人力经理',NULL,2,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite'),('user','普通员工',NULL,3,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite');
/*!40000 ALTER TABLE `js_sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_role`
--

DROP TABLE IF EXISTS `js_sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_role` (
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `role_type` varchar(100) DEFAULT NULL COMMENT '角色分类（高管、中层、基层、其它）',
  `role_sort` decimal(10,0) DEFAULT NULL COMMENT '角色排序（升序）',
  `is_sys` char(1) DEFAULT NULL COMMENT '系统内置（1是 0否）',
  `user_type` varchar(16) DEFAULT NULL COMMENT '用户类型（employee员工 member会员）',
  `data_scope` char(1) DEFAULT NULL COMMENT '数据范围设置（0未设置  1全部数据 2自定义数据）',
  `biz_scope` varchar(255) DEFAULT NULL COMMENT '适应业务范围（不同的功能，不同的数据权限支持）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`role_code`),
  KEY `idx_sys_role_cc` (`corp_code`),
  KEY `idx_sys_role_is` (`is_sys`),
  KEY `idx_sys_role_status` (`status`),
  KEY `idx_sys_role_rs` (`role_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_role`
--

LOCK TABLES `js_sys_role` WRITE;
/*!40000 ALTER TABLE `js_sys_role` DISABLE KEYS */;
INSERT INTO `js_sys_role` VALUES ('corpAdmin','系统管理员',NULL,200,'1','none','0',NULL,'0','system','2018-12-03 11:21:04','system','2018-12-03 11:21:04','客户方使用的管理员角色，客户方管理员，集团管理员','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('default','默认角色',NULL,100,'1','none','0',NULL,'0','system','2018-12-03 11:21:04','system','2018-12-03 11:21:04','非管理员用户，共有的默认角色，在参数配置里指定','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dept','部门经理',NULL,40,'0','employee','0',NULL,'0','system','2018-12-03 11:21:04','system','2018-12-03 11:21:04','部门经理','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user','普通员工',NULL,50,'0','employee','0',NULL,'0','system','2018-12-03 11:21:04','system','2018-12-03 11:21:04','普通员工','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_role_data_scope`
--

DROP TABLE IF EXISTS `js_sys_role_data_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_role_data_scope` (
  `role_code` varchar(64) NOT NULL COMMENT '控制角色编码',
  `ctrl_type` varchar(20) NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`role_code`,`ctrl_type`,`ctrl_data`,`ctrl_permi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色数据权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_role_data_scope`
--

LOCK TABLES `js_sys_role_data_scope` WRITE;
/*!40000 ALTER TABLE `js_sys_role_data_scope` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_role_data_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_role_menu`
--

DROP TABLE IF EXISTS `js_sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_role_menu` (
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  `menu_code` varchar(64) NOT NULL COMMENT '菜单编码',
  PRIMARY KEY (`role_code`,`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_role_menu`
--

LOCK TABLES `js_sys_role_menu` WRITE;
/*!40000 ALTER TABLE `js_sys_role_menu` DISABLE KEYS */;
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin','1069431293998346240'),('corpAdmin','1069431294329696256'),('corpAdmin','1069431296649146368'),('corpAdmin','1069431298603692032'),('corpAdmin','1069431298796630016'),('corpAdmin','1069431300721815552'),('corpAdmin','1069431302433091584'),('corpAdmin','1069431302592475136'),('corpAdmin','1069431304442163200'),('corpAdmin','1069431306438651904'),('corpAdmin','1069431306631589888'),('corpAdmin','1069431308800045056'),('corpAdmin','1069431310695870464'),('corpAdmin','1069431310913974272'),('corpAdmin','1069431313849987072'),('corpAdmin','1069431315615789056'),('corpAdmin','1069431315817115648'),('corpAdmin','1069431318912512000'),('corpAdmin','1069431320846086144'),('corpAdmin','1069431321043218432'),('corpAdmin','1069431324021174272'),('corpAdmin','1069431325766004736'),('corpAdmin','1069431325963137024'),('corpAdmin','1069431328970452992'),('corpAdmin','1069431330744643584'),('corpAdmin','1069431330924998656'),('corpAdmin','1069431332514639872'),('corpAdmin','1069431334368522240'),('corpAdmin','1069431334557265920'),('corpAdmin','1069431336167878656'),('corpAdmin','1069431338181144576'),('corpAdmin','1069431340198604800'),('corpAdmin','1069431340357988352'),('corpAdmin','1069431342304145408'),('corpAdmin','1069431343885398016'),('corpAdmin','1069431344317411328'),('corpAdmin','1069431345902858240'),('corpAdmin','1069431347442167808'),('corpAdmin','1069431347626717184'),('corpAdmin','1069431349526736896'),('corpAdmin','1069431351468699648'),('corpAdmin','1069431351686803456'),('corpAdmin','1069431353876230144'),('corpAdmin','1069431355646226432'),('corpAdmin','1069431355839164416'),('corpAdmin','1069431358032785408'),('corpAdmin','1069431359710507008'),('corpAdmin','1069431359953776640'),('corpAdmin','1069431362164174848'),('corpAdmin','1069431363963531264'),('corpAdmin','1069431364265521152'),('corpAdmin','1069431367176368128'),('corpAdmin','1069431368921198592'),('corpAdmin','1069431369105747968'),('corpAdmin','1069431372050149376'),('corpAdmin','1069431373895643136'),('corpAdmin','1069431374126329856'),('corpAdmin','1069431376357699584'),('corpAdmin','1069431377938952192'),('corpAdmin','1069431378144473088'),('corpAdmin','1069431381374087168'),('corpAdmin','1069431383135694848'),('corpAdmin','1069431383324438528'),('corpAdmin','1069431384863748096'),('corpAdmin','1069431386726019072');
/*!40000 ALTER TABLE `js_sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user`
--

DROP TABLE IF EXISTS `js_sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_user` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `login_code` varchar(100) NOT NULL COMMENT '登录账号',
  `user_name` varchar(100) NOT NULL COMMENT '用户昵称',
  `password` varchar(100) NOT NULL COMMENT '登录密码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `sex` char(1) DEFAULT NULL COMMENT '用户性别',
  `avatar` varchar(1000) DEFAULT NULL COMMENT '头像路径',
  `sign` varchar(200) DEFAULT NULL COMMENT '个性签名',
  `wx_openid` varchar(100) DEFAULT NULL COMMENT '绑定的微信号',
  `mobile_imei` varchar(100) DEFAULT NULL COMMENT '绑定的手机串号',
  `user_type` varchar(16) NOT NULL COMMENT '用户类型',
  `ref_code` varchar(64) DEFAULT NULL COMMENT '用户类型引用编号',
  `ref_name` varchar(100) DEFAULT NULL COMMENT '用户类型引用姓名',
  `mgr_type` char(1) NOT NULL COMMENT '管理员类型（0非管理员 1系统管理员  2二级管理员）',
  `pwd_security_level` decimal(1,0) DEFAULT NULL COMMENT '密码安全级别（0初始 1很弱 2弱 3安全 4很安全）',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `pwd_update_record` varchar(1000) DEFAULT NULL COMMENT '密码修改记录',
  `pwd_question` varchar(200) DEFAULT NULL COMMENT '密保问题',
  `pwd_question_answer` varchar(200) DEFAULT NULL COMMENT '密保问题答案',
  `pwd_question_2` varchar(200) DEFAULT NULL COMMENT '密保问题2',
  `pwd_question_answer_2` varchar(200) DEFAULT NULL COMMENT '密保问题答案2',
  `pwd_question_3` varchar(200) DEFAULT NULL COMMENT '密保问题3',
  `pwd_question_answer_3` varchar(200) DEFAULT NULL COMMENT '密保问题答案3',
  `pwd_quest_update_date` datetime DEFAULT NULL COMMENT '密码问题修改时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `freeze_date` datetime DEFAULT NULL COMMENT '冻结时间',
  `freeze_cause` varchar(200) DEFAULT NULL COMMENT '冻结原因',
  `user_weight` decimal(8,0) DEFAULT '0' COMMENT '用户权重（降序）',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2停用 3冻结）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`user_code`),
  KEY `idx_sys_user_lc` (`login_code`),
  KEY `idx_sys_user_email` (`email`(255)),
  KEY `idx_sys_user_mobile` (`mobile`),
  KEY `idx_sys_user_wo` (`wx_openid`),
  KEY `idx_sys_user_imei` (`mobile_imei`),
  KEY `idx_sys_user_rt` (`user_type`),
  KEY `idx_sys_user_rc` (`ref_code`),
  KEY `idx_sys_user_mt` (`mgr_type`),
  KEY `idx_sys_user_us` (`user_weight`),
  KEY `idx_sys_user_ud` (`update_date`),
  KEY `idx_sys_user_status` (`status`),
  KEY `idx_sys_user_cc` (`corp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user`
--

LOCK TABLES `js_sys_user` WRITE;
/*!40000 ALTER TABLE `js_sys_user` DISABLE KEYS */;
INSERT INTO `js_sys_user` VALUES ('admin','admin','系统管理员','199726ba67bd001fbeda63388198be639e3777ed221468a9893ee0ca',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,'1',1,'2018-12-03 11:21:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27','客户方使用的系统管理员，用于一些常用的基础数据配置。','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('system','system','超级管理员','e104cb736c710d308c02d06c0289a974eee5abea70971d46bc137ead',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,'0',1,'2018-12-03 11:21:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0:0:0:0:0:0:0:1','2019-09-02 17:07:57',NULL,NULL,0,'0','system','2018-12-03 11:21:27','system','2018-12-03 11:21:27','开发者使用的最高级别管理员，主要用于开发和调试。','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user10_nw09','user10','用户10','7e6f3b72c1c4fb9cc529a0dc75c7d284a4bbe2e8a70b5bc9d5d800cd','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user10_nw09','用户10','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user11_ibcm','user11','用户11','dc4b688fe329a8c377dddf0d94308a6f3432d31e0837145cbbd8f619','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user11_ibcm','用户11','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user12_8hyj','user12','用户12','a7316a47621e5986efbc278d3a099e5249a94b23737f43e3d0bc53fa','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user12_8hyj','用户12','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user13_h3sk','user13','用户13','ab7decb2136136dc787bf4e1bb0f55e68dc3db132765822615dd7034','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user13_h3sk','用户13','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user14_deqy','user14','用户14','ba6f7aa9e277f0b67b574dc0a1b3203f5e358ae2f7b30d04ffc9c7b3','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user14_deqy','用户14','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user15_c5sr','user15','用户15','04fdc6a73ac1d89f2ecf75fe5ea1f1e993d3a010cf0b6c2e25a53ffa','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user15_c5sr','用户15','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user16_pkfs','user16','用户16','c98db574fd3b0b6c2a5b06804226b96031a639911fa3d1b176fc31dd','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user16_pkfs','用户16','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user17_ogfo','user17','用户17','c6df40d3966126eb67323f7ef39063169e568869c53205d6d7e6624a','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user17_ogfo','用户17','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user18_ycie','user18','用户18','e69e9be629856467583fc8b2ef3a4df23fd612da0fa30dc5b4e3c3ba','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user18_ycie','用户18','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user19_urge','user19','用户19','ecd6003cb598bbe5bc40dcb5e0732a2120e19d1651c35db0682de3fc','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user19_urge','用户19','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user1_cwme','user1','用户01','e2657b2e8fad4a74f0f24aa66e5ab77cd44cc8bc08baf59076207a21','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user1_cwme','用户01','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user20_e7n4','user20','用户20','915c05116802a01f5cf21c7925b011affa66253206029cf49deb26e6','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user20_e7n4','用户20','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user21_oxfg','user21','用户21','60be9ee1245015a6aee8ce4e6a61a216ae1247ba24aae9fc3b48c693','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user21_oxfg','用户21','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user22_o5j5','user22','用户22','c3009de72d75429740b665b3c036878a08495ba8dc8b422e48eb2e15','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user22_o5j5','用户22','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user23_ymbm','user23','用户23','09eae1abcfdd138e5fa52033645bf01b6e87a8c5e3f99504a9dd59e5','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user23_ymbm','用户23','0',0,'2018-12-03 11:21:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:29','system','2018-12-03 11:21:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user2_fuda','user2','用户02','bdb480ae623d2124bbbb77b8ec21221e2b8af63710cc31cebc715e63','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user2_fuda','用户02','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user3_ll1n','user3','用户03','506f84703ca2b6ac183749e3dd2b179ab6b6e882d6bc4050a1feba69','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user3_ll1n','用户03','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user4_m1vk','user4','用户04','b7e8047082d12683a893e49b8b1c1f2e1d408893884c2338dd52748a','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user4_m1vk','用户04','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user5_pwtv','user5','用户05','950e0c5e6f2cdc2914a445d9316daaee1bae1aa29b168b36a173b7f0','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user5_pwtv','用户05','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user6_hpw5','user6','用户06','1101b9b5f65f95eced30785575387562f69f82006ca88b460ead7b56','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user6_hpw5','用户06','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user7_lxut','user7','用户07','bafa863d95576d8d7577ff84b1525a71307627e30e3d050ed9e99935','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user7_lxut','用户07','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user8_wp9j','user8','用户08','51bc525aa96c1cea707e4e137afd80e370ce94401117da4d3933c301','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user8_wp9j','用户08','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user9_twd4','user9','用户09','1c9614b3cc4ed886ad242721cbc764da85452270aa597e5eedf1d2bd','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user9_twd4','用户09','0',0,'2018-12-03 11:21:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-03 11:21:28','system','2018-12-03 11:21:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user_data_scope`
--

DROP TABLE IF EXISTS `js_sys_user_data_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_user_data_scope` (
  `user_code` varchar(100) NOT NULL COMMENT '控制用户编码',
  `ctrl_type` varchar(20) NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`user_code`,`ctrl_type`,`ctrl_data`,`ctrl_permi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户数据权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user_data_scope`
--

LOCK TABLES `js_sys_user_data_scope` WRITE;
/*!40000 ALTER TABLE `js_sys_user_data_scope` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_user_data_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user_role`
--

DROP TABLE IF EXISTS `js_sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `js_sys_user_role` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`user_code`,`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user_role`
--

LOCK TABLES `js_sys_user_role` WRITE;
/*!40000 ALTER TABLE `js_sys_user_role` DISABLE KEYS */;
INSERT INTO `js_sys_user_role` VALUES ('user10_nw09','user'),('user11_ibcm','user'),('user12_8hyj','user'),('user13_h3sk','user'),('user14_deqy','dept'),('user15_c5sr','dept'),('user16_pkfs','user'),('user17_ogfo','user'),('user18_ycie','dept'),('user19_urge','user'),('user1_cwme','dept'),('user20_e7n4','user'),('user21_oxfg','dept'),('user22_o5j5','user'),('user23_ymbm','user'),('user2_fuda','user'),('user3_ll1n','user'),('user4_m1vk','dept'),('user5_pwtv','user'),('user6_hpw5','user'),('user7_lxut','dept'),('user8_wp9j','user'),('user9_twd4','user');
/*!40000 ALTER TABLE `js_sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_dept`
--

DROP TABLE IF EXISTS `port_dept`;
/*!50001 DROP VIEW IF EXISTS `port_dept`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_dept` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `NameOfPath`,
 1 AS `ParentNo`,
 1 AS `TreeNo`,
 1 AS `Leader`,
 1 AS `Tel`,
 1 AS `Idx`,
 1 AS `IsDir`,
 1 AS `OrgNo`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_dept_bak`
--

DROP TABLE IF EXISTS `port_dept_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_dept_bak` (
  `No` varchar(50) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点编号',
  `NameOfPath` varchar(300) DEFAULT '' COMMENT '部门路径',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `OrgNo` varchar(50) DEFAULT '' COMMENT '隶属组织',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_dept_bak`
--

LOCK TABLES `port_dept_bak` WRITE;
/*!40000 ALTER TABLE `port_dept_bak` DISABLE KEYS */;
INSERT INTO `port_dept_bak` VALUES ('100','集团总部','0','',0,''),('1001','集团市场部','100','',0,''),('1002','集团研发部','100','',0,''),('1003','集团服务部','100','',0,''),('1004','集团财务部','100','',0,''),('1005','集团人力资源部','100','',0,''),('1060','南方分公司','100','',0,''),('1061','市场部','1060','',0,''),('1062','财务部','1060','',0,''),('1063','销售部','1060','',0,''),('1070','北方分公司','100','',0,''),('1071','市场部','1070','',0,''),('1072','财务部','1070','',0,''),('1073','销售部','1070','',0,''),('1099','外来单位','100','',0,'');
/*!40000 ALTER TABLE `port_dept_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_deptemp`
--

DROP TABLE IF EXISTS `port_deptemp`;
/*!50001 DROP VIEW IF EXISTS `port_deptemp`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_deptemp` AS SELECT 
 1 AS `MyPk`,
 1 AS `FK_Emp`,
 1 AS `FK_Dept`,
 1 AS `FK_Duty`,
 1 AS `DutyLevel`,
 1 AS `Leader`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_deptemp_bak`
--

DROP TABLE IF EXISTS `port_deptemp_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_deptemp_bak` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptemp_bak`
--

LOCK TABLES `port_deptemp_bak` WRITE;
/*!40000 ALTER TABLE `port_deptemp_bak` DISABLE KEYS */;
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhanghaicheng','1001','zhanghaicheng'),('1001_zhangyifan','1001','zhangyifan'),('1001_zhoushengyu','1001','zhoushengyu'),('1002_qifenglin','1002','qifenglin'),('1002_zhoutianjiao','1002','zhoutianjiao'),('1003_fuhui','1003','fuhui'),('1003_guoxiangbin','1003','guoxiangbin'),('1004_guobaogeng','1004','guobaogeng'),('1004_yangyilei','1004','yangyilei'),('1005_liping','1005','liping'),('1005_liyan','1005','liyan'),('100_zhoupeng','100','zhoupeng');
/*!40000 ALTER TABLE `port_deptemp_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_deptempstation`
--

DROP TABLE IF EXISTS `port_deptempstation`;
/*!50001 DROP VIEW IF EXISTS `port_deptempstation`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_deptempstation` AS SELECT 
 1 AS `MyPk`,
 1 AS `FK_Dept`,
 1 AS `FK_Station`,
 1 AS `FK_Emp`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_deptempstation_bak`
--

DROP TABLE IF EXISTS `port_deptempstation_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_deptempstation_bak` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Station` varchar(50) DEFAULT NULL COMMENT '岗位',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptempstation_bak`
--

LOCK TABLES `port_deptempstation_bak` WRITE;
/*!40000 ALTER TABLE `port_deptempstation_bak` DISABLE KEYS */;
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhanghaicheng_02','1001','02','zhanghaicheng'),('1001_zhangyifan_07','1001','07','zhangyifan'),('1001_zhoushengyu_07','1001','07','zhoushengyu'),('1002_qifenglin_03','1002','03','qifenglin'),('1002_zhoutianjiao_08','1002','08','zhoutianjiao'),('1003_fuhui_09','1003','09','fuhui'),('1003_guoxiangbin_04','1003','04','guoxiangbin'),('1004_guobaogeng_10','1004','10','guobaogeng'),('1004_yangyilei_05','1004','05','yangyilei'),('1005_liping_06','1005','06','liping'),('1005_liyan_11','1005','11','liyan'),('100_zhoupeng_01','100','01','zhoupeng'),('1099_Guest_12','1005','12','Guest');
/*!40000 ALTER TABLE `port_deptempstation_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptsearchscorp`
--

DROP TABLE IF EXISTS `port_deptsearchscorp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_deptsearchscorp` (
  `FK_Emp` varchar(50) NOT NULL COMMENT '操作员 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Emp`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门查询权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptsearchscorp`
--

LOCK TABLES `port_deptsearchscorp` WRITE;
/*!40000 ALTER TABLE `port_deptsearchscorp` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_deptsearchscorp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptstation`
--

DROP TABLE IF EXISTS `port_deptstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_deptstation` (
  `FK_Dept` varchar(15) NOT NULL COMMENT '部门',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位',
  PRIMARY KEY (`FK_Dept`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptstation`
--

LOCK TABLES `port_deptstation` WRITE;
/*!40000 ALTER TABLE `port_deptstation` DISABLE KEYS */;
INSERT INTO `port_deptstation` VALUES ('100','01'),('1001','07'),('1002','08'),('1003','09'),('1004','10'),('1005','11');
/*!40000 ALTER TABLE `port_deptstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_depttype`
--

DROP TABLE IF EXISTS `port_depttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_depttype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_depttype`
--

LOCK TABLES `port_depttype` WRITE;
/*!40000 ALTER TABLE `port_depttype` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_depttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_emp`
--

DROP TABLE IF EXISTS `port_emp`;
/*!50001 DROP VIEW IF EXISTS `port_emp`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_emp` AS SELECT 
 1 AS `No`,
 1 AS `EmpNo`,
 1 AS `Name`,
 1 AS `Pass`,
 1 AS `FK_Dept`,
 1 AS `FK_Duty`,
 1 AS `Leader`,
 1 AS `SID`,
 1 AS `Tel`,
 1 AS `Email`,
 1 AS `PinYin`,
 1 AS `SignType`,
 1 AS `NumOfDept`,
 1 AS `Idx`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_emp_bak`
--

DROP TABLE IF EXISTS `port_emp_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_emp_bak` (
  `No` varchar(50) NOT NULL DEFAULT '',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `Pass` varchar(100) DEFAULT NULL,
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `SID` varchar(36) DEFAULT NULL COMMENT '安全校验码',
  `Tel` varchar(20) DEFAULT '' COMMENT '电话',
  `Email` varchar(100) DEFAULT '' COMMENT '邮箱',
  `PinYin` varchar(500) DEFAULT '' COMMENT '拼音',
  `SignType` int(11) DEFAULT '0' COMMENT '签字类型',
  `Idx` int(11) DEFAULT '0' COMMENT '序号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_emp_bak`
--

LOCK TABLES `port_emp_bak` WRITE;
/*!40000 ALTER TABLE `port_emp_bak` DISABLE KEYS */;
INSERT INTO `port_emp_bak` VALUES ('admin','admin','123','100',NULL,'0531-82374939','zhoupeng@ccflow.org','',0,0),('fuhui','福惠','123','1003',NULL,'0531-82374939','fuhui@ccflow.org','',0,0),('guobaogeng','郭宝庚','123','1004',NULL,'0531-82374939','guobaogeng@ccflow.org','',0,0),('guoxiangbin','郭祥斌','123','1003',NULL,'0531-82374939','guoxiangbin@ccflow.org','',0,0),('liping','李萍','123','1005',NULL,'0531-82374939','liping@ccflow.org','',0,0),('liyan','李言','123','1005',NULL,'0531-82374939','liyan@ccflow.org','',0,0),('qifenglin','祁凤林','123','1002',NULL,'0531-82374939','qifenglin@ccflow.org','',0,0),('yangyilei','杨依雷','123','1004',NULL,'0531-82374939','yangyilei@ccflow.org','',0,0),('zhanghaicheng','张海成','123','1001',NULL,'0531-82374939','zhanghaicheng@ccflow.org','',0,0),('zhangyifan','张一帆','123','1001',NULL,'0531-82374939','zhangyifan@ccflow.org','',0,0),('zhoupeng','周朋','123','100',NULL,'0531-82374939','zhoupeng@ccflow.org','',0,0),('zhoushengyu','周升雨','123','1001',NULL,'0531-82374939','zhoushengyu@ccflow.org','',0,0),('zhoutianjiao','周天娇','123','1002',NULL,'0531-82374939','zhoutianjiao@ccflow.org','',0,0);
/*!40000 ALTER TABLE `port_emp_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_empstation`
--

DROP TABLE IF EXISTS `port_empstation`;
/*!50001 DROP VIEW IF EXISTS `port_empstation`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_empstation` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `FK_Station`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_helper`
--

DROP TABLE IF EXISTS `port_helper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_helper` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `Name` text COMMENT '描述',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '附件或图片',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_helper`
--

LOCK TABLES `port_helper` WRITE;
/*!40000 ALTER TABLE `port_helper` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_helper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_inc`
--

DROP TABLE IF EXISTS `port_inc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_inc` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(30) DEFAULT NULL COMMENT '父节点编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='独立组织';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_inc`
--

LOCK TABLES `port_inc` WRITE;
/*!40000 ALTER TABLE `port_inc` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_inc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_station`
--

DROP TABLE IF EXISTS `port_station`;
/*!50001 DROP VIEW IF EXISTS `port_station`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_station` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `FK_StationType`,
 1 AS `DutyReq`,
 1 AS `Makings`,
 1 AS `OrgNo`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_station_bak`
--

DROP TABLE IF EXISTS `port_station_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_station_bak` (
  `No` varchar(4) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `FK_StationType` varchar(100) DEFAULT NULL COMMENT '类型',
  `OrgNo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_station_bak`
--

LOCK TABLES `port_station_bak` WRITE;
/*!40000 ALTER TABLE `port_station_bak` DISABLE KEYS */;
INSERT INTO `port_station_bak` VALUES ('01','总经理','1','0'),('02','市场部经理','2','0'),('03','研发部经理','2','0'),('04','客服部经理','2','0'),('05','财务部经理','2','0'),('06','人力资源部经理','2','0'),('07','销售人员岗','3','0'),('08','程序员岗','3','0'),('09','技术服务岗','3','0'),('10','出纳岗','3','0'),('11','人力资源助理岗','3','0'),('12','外来人员岗','3','0');
/*!40000 ALTER TABLE `port_station_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `port_stationtype`
--

DROP TABLE IF EXISTS `port_stationtype`;
/*!50001 DROP VIEW IF EXISTS `port_stationtype`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `port_stationtype` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `Idx`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_stationtype_bak`
--

DROP TABLE IF EXISTS `port_stationtype_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `port_stationtype_bak` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序',
  `OrgNo` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_stationtype_bak`
--

LOCK TABLES `port_stationtype_bak` WRITE;
/*!40000 ALTER TABLE `port_stationtype_bak` DISABLE KEYS */;
INSERT INTO `port_stationtype_bak` VALUES ('1','高层',0,NULL),('2','中层',0,NULL),('3','基层',0,NULL);
/*!40000 ALTER TABLE `port_stationtype_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_day`
--

DROP TABLE IF EXISTS `pub_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_day` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_day`
--

LOCK TABLES `pub_day` WRITE;
/*!40000 ALTER TABLE `pub_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_nd`
--

DROP TABLE IF EXISTS `pub_nd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_nd` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_nd`
--

LOCK TABLES `pub_nd` WRITE;
/*!40000 ALTER TABLE `pub_nd` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_nd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_ny`
--

DROP TABLE IF EXISTS `pub_ny`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_ny` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_ny`
--

LOCK TABLES `pub_ny` WRITE;
/*!40000 ALTER TABLE `pub_ny` DISABLE KEYS */;
INSERT INTO `pub_ny` VALUES ('2019-09','2019-09');
/*!40000 ALTER TABLE `pub_ny` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_yf`
--

DROP TABLE IF EXISTS `pub_yf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_yf` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_yf`
--

LOCK TABLES `pub_yf` WRITE;
/*!40000 ALTER TABLE `pub_yf` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_yf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spgw`
--

DROP TABLE IF EXISTS `spgw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spgw` (
  `TXR` varchar(300) DEFAULT NULL COMMENT 'TXR',
  `BGSR` varchar(300) DEFAULT NULL COMMENT '审批人',
  `CWCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `GBCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `GHCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `PFRQ` varchar(50) DEFAULT NULL COMMENT '批复日期',
  `SQDW` varchar(300) DEFAULT NULL COMMENT '申请单位',
  `TZRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `TZYJ` varchar(300) DEFAULT NULL COMMENT '厅长审批',
  `BGSRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `BGSYJ` varchar(300) DEFAULT NULL COMMENT '办公室审核',
  `CWCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `CWCYJ` varchar(300) DEFAULT NULL COMMENT '财务处审核',
  `GBCRQ` varchar(50) DEFAULT NULL COMMENT '时间',
  `GHCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `GHCYJ` varchar(300) DEFAULT NULL COMMENT '规划处意见',
  `DJGLCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `Context` varchar(300) DEFAULT NULL COMMENT '批复内容',
  `DJGLCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `DJGLCYJ` varchar(300) DEFAULT NULL COMMENT '地籍管理处意见',
  `STGBCYJ` varchar(300) DEFAULT NULL COMMENT '省厅耕保处意见',
  `JBXX_XMMC` varchar(300) DEFAULT NULL COMMENT 'JBXX_XMMC',
  `PiFuWenHao` varchar(300) DEFAULT NULL COMMENT '批复文号',
  `OID` int(11) NOT NULL COMMENT 'OID',
  `RDT` varchar(50) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spgw`
--

LOCK TABLES `spgw` WRITE;
/*!40000 ALTER TABLE `spgw` DISABLE KEYS */;
/*!40000 ALTER TABLE `spgw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_cfield`
--

DROP TABLE IF EXISTS `sys_cfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_cfield` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '工作人员',
  `Attrs` text COMMENT '属性s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='列选择';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_cfield`
--

LOCK TABLES `sys_cfield` WRITE;
/*!40000 ALTER TABLE `sys_cfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_cfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_contrast`
--

DROP TABLE IF EXISTS `sys_contrast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_contrast` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ContrastKey` varchar(20) DEFAULT NULL COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT NULL COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT NULL COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT NULL COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT NULL COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT NULL COMMENT '求什么?SumAvg',
  `OrderWay` varchar(10) DEFAULT NULL COMMENT 'OrderWay',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='对比状态存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_contrast`
--

LOCK TABLES `sys_contrast` WRITE;
/*!40000 ALTER TABLE `sys_contrast` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_contrast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_datarpt`
--

DROP TABLE IF EXISTS `sys_datarpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_datarpt` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ColCount` varchar(50) DEFAULT NULL COMMENT '列',
  `RowCount` varchar(50) DEFAULT NULL COMMENT '行',
  `Val` float DEFAULT NULL COMMENT '值',
  `RefOID` float DEFAULT NULL COMMENT '关联的值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表数据存储模版';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_datarpt`
--

LOCK TABLES `sys_datarpt` WRITE;
/*!40000 ALTER TABLE `sys_datarpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_datarpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_defval`
--

DROP TABLE IF EXISTS `sys_defval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_defval` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT NULL COMMENT '节点对应字段',
  `LB` int(11) DEFAULT NULL COMMENT '类别',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '人员',
  `CurValue` text COMMENT '文本',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择词汇';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_defval`
--

LOCK TABLES `sys_defval` WRITE;
/*!40000 ALTER TABLE `sys_defval` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_defval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_docfile`
--

DROP TABLE IF EXISTS `sys_docfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_docfile` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FileName` varchar(200) DEFAULT NULL COMMENT '名称',
  `FileSize` int(11) DEFAULT '0' COMMENT '大小',
  `FileType` varchar(50) DEFAULT NULL COMMENT '文件类型',
  `D1` text COMMENT 'D1',
  `D2` text COMMENT 'D2',
  `D3` text COMMENT 'D3',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_docfile`
--

LOCK TABLES `sys_docfile` WRITE;
/*!40000 ALTER TABLE `sys_docfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_docfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_domain`
--

DROP TABLE IF EXISTS `sys_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_domain` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(30) DEFAULT NULL COMMENT 'Name',
  `DBLink` varchar(130) DEFAULT NULL COMMENT 'DBLink',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='域';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_domain`
--

LOCK TABLES `sys_domain` WRITE;
/*!40000 ALTER TABLE `sys_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_encfg`
--

DROP TABLE IF EXISTS `sys_encfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_encfg` (
  `No` varchar(100) NOT NULL COMMENT '实体名称',
  `GroupTitle` varchar(2000) DEFAULT NULL COMMENT '分组标签',
  `Url` varchar(500) DEFAULT NULL COMMENT '要打开的Url',
  `FJSavePath` varchar(100) DEFAULT NULL COMMENT '保存路径',
  `FJWebPath` varchar(100) DEFAULT NULL COMMENT '附件Web路径',
  `Datan` varchar(200) DEFAULT NULL COMMENT '字段数据分析方式',
  `UI` varchar(2000) DEFAULT NULL COMMENT 'UI设置',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_encfg`
--

LOCK TABLES `sys_encfg` WRITE;
/*!40000 ALTER TABLE `sys_encfg` DISABLE KEYS */;
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmBill','@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Frm.FrmDict','@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Sys.FrmUI.FrmAttachmentExt','@MyPK=基础信息,附件的基本配置.\n@DeleteWay=权限控制,控制附件的下载与上传权限.@IsRowLock=WebOffice属性,设置与公文有关系的属性配置.\n@IsToHeLiuHZ=流程相关,控制节点附件的分合流.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.FlowExt','@No=基础信息,基础信息权限信息.@IsBatchStart=数据&表单,数据导入导出.@DesignerNo=设计者,流程开发设计者信息',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.FlowSheet','@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@CFlowWay=延续流程@DTSWay=流程数据与业务数据同步@PStarter=轨迹查看权限',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.FrmNodeComponent','@NodeID=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFLab=父子流程组件,在该节点上配置与显示父子流程.@FrmThreadLab=子线程组件,对合流节点有效，用于配置与现实子线程运行的情况。@FrmTrackLab=轨迹组件,用于显示流程运行的轨迹图.@FTCLab=流转自定义,在每个节点上自己控制节点的处理人.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.MapDataExt','@No=基本属性@Designer=设计者信息',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.MapDtlExt','@No=基础信息,基础信息权限信息.@IsExp=数据导入导出,数据导入导出.@MTR=多表头,实现多表头.@IsEnableLink=超链接,显示在从表的右边.@IsCopyNDData=流程相关,与流程相关的配置非流程可以忽略.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.MapFrmFool','@No=基础属性,基础属性.@Designer=设计者信息,设计者的单位信息，人员信息，可以上传到表单云.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.NodeExt','@NodeID=基本配置@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.NodeSheet','@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFSta=父子流程,对启动，查看父子流程的控件设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@OfficeOpen=公文按钮,只有当该节点是公文流程时候有效',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_encfg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enum`
--

DROP TABLE IF EXISTS `sys_enum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_enum` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Lab` varchar(300) DEFAULT NULL COMMENT 'Lab',
  `EnumKey` varchar(100) DEFAULT NULL COMMENT 'EnumKey',
  `IntKey` int(11) DEFAULT '0' COMMENT 'Val',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enum`
--

LOCK TABLES `sys_enum` WRITE;
/*!40000 ALTER TABLE `sys_enum` DISABLE KEYS */;
INSERT INTO `sys_enum` VALUES ('ActionType_CH_0','GET','ActionType',0,'CH'),('ActionType_CH_1','POST','ActionType',1,'CH'),('AlertType_CH_0','短信','AlertType',0,'CH'),('AlertType_CH_1','邮件','AlertType',1,'CH'),('AlertType_CH_2','邮件与短信','AlertType',2,'CH'),('AlertType_CH_3','系统(内部)消息','AlertType',3,'CH'),('AlertWay_CH_0','不接收','AlertWay',0,'CH'),('AlertWay_CH_1','短信','AlertWay',1,'CH'),('AlertWay_CH_2','邮件','AlertWay',2,'CH'),('AlertWay_CH_3','内部消息','AlertWay',3,'CH'),('AlertWay_CH_4','QQ消息','AlertWay',4,'CH'),('AlertWay_CH_5','RTX消息','AlertWay',5,'CH'),('AlertWay_CH_6','MSN消息','AlertWay',6,'CH'),('AppModel_CH_0','BS系统','AppModel',0,'CH'),('AppModel_CH_1','CS系统','AppModel',1,'CH'),('AppType_CH_0','外部Url连接','AppType',0,'CH'),('AppType_CH_1','本地可执行文件','AppType',1,'CH'),('AthCtrlWay_CH_0','PK-主键','AthCtrlWay',0,'CH'),('AthCtrlWay_CH_1','FID-流程ID','AthCtrlWay',1,'CH'),('AthCtrlWay_CH_2','ParentID-父流程ID','AthCtrlWay',2,'CH'),('AthCtrlWay_CH_3','仅能查看自己上传的附件','AthCtrlWay',3,'CH'),('AthCtrlWay_CH_4','按照WorkID计算(对流程节点表单有效)','AthCtrlWay',4,'CH'),('AthSaveWay_CH_0','保存到IIS服务器','AthSaveWay',0,'CH'),('AthSaveWay_CH_1','保存到数据库','AthSaveWay',1,'CH'),('AthSaveWay_CH_2','ftp服务器','AthSaveWay',2,'CH'),('AthUploadWay_CH_0','继承模式','AthUploadWay',0,'CH'),('AthUploadWay_CH_1','协作模式','AthUploadWay',1,'CH'),('AuthorWay_CH_0','不授权','AuthorWay',0,'CH'),('AuthorWay_CH_1','全部流程授权','AuthorWay',1,'CH'),('AuthorWay_CH_2','指定流程授权','AuthorWay',2,'CH'),('BillFileType_CH_0','Word','BillFileType',0,'CH'),('BillFileType_CH_1','PDF','BillFileType',1,'CH'),('BillFileType_CH_2','Excel(未完成)','BillFileType',2,'CH'),('BillFileType_CH_3','Html(未完成)','BillFileType',3,'CH'),('BillFileType_CH_5','锐浪报表','BillFileType',5,'CH'),('BillOpenModel_CH_0','下载本地','BillOpenModel',0,'CH'),('BillOpenModel_CH_1','在线WebOffice打开','BillOpenModel',1,'CH'),('CancelRole_CH_0','上一步可以撤销','CancelRole',0,'CH'),('CancelRole_CH_1','不能撤销','CancelRole',1,'CH'),('CancelRole_CH_2','上一步与开始节点可以撤销','CancelRole',2,'CH'),('CancelRole_CH_3','指定的节点可以撤销','CancelRole',3,'CH'),('CCRole_CH_0','不能抄送','CCRole',0,'CH'),('CCRole_CH_1','手工抄送','CCRole',1,'CH'),('CCRole_CH_2','自动抄送','CCRole',2,'CH'),('CCRole_CH_3','手工与自动','CCRole',3,'CH'),('CCRole_CH_4','按表单SysCCEmps字段计算','CCRole',4,'CH'),('CCRole_CH_5','在发送前打开抄送窗口','CCRole',5,'CH'),('CCWriteTo_CH_0','写入抄送列表','CCWriteTo',0,'CH'),('CCWriteTo_CH_1','写入待办','CCWriteTo',1,'CH'),('CCWriteTo_CH_2','写入待办与抄送列表','CCWriteTo',2,'CH'),('CeShiMoShi_CH_0','强办模式','CeShiMoShi',0,'CH'),('CeShiMoShi_CH_1','协作模式','CeShiMoShi',1,'CH'),('CeShiMoShi_CH_2','队列模式','CeShiMoShi',2,'CH'),('CeShiMoShi_CH_3','共享模式','CeShiMoShi',3,'CH'),('CGDD_CH_0','本市','CGDD',0,'CH'),('CGDD_CH_1','外地','CGDD',1,'CH'),('CHAlertRole_CH_0','不提示','CHAlertRole',0,'CH'),('CHAlertRole_CH_1','每天1次','CHAlertRole',1,'CH'),('CHAlertRole_CH_2','每天2次','CHAlertRole',2,'CH'),('CHAlertWay_CH_0','邮件','CHAlertWay',0,'CH'),('CHAlertWay_CH_1','短信','CHAlertWay',1,'CH'),('CHAlertWay_CH_2','CCIM即时通讯','CHAlertWay',2,'CH'),('ChartType_CH_0','几何图形','ChartType',0,'CH'),('ChartType_CH_1','肖像图片','ChartType',1,'CH'),('CHSta_CH_0','及时完成','CHSta',0,'CH'),('CHSta_CH_1','按期完成','CHSta',1,'CH'),('CHSta_CH_2','逾期完成','CHSta',2,'CH'),('CHSta_CH_3','超期完成','CHSta',3,'CH'),('CodeStruct_CH_0','普通的编码表(具有No,Name)','CodeStruct',0,'CH'),('CodeStruct_CH_1','树结构(具有No,Name,ParentNo)','CodeStruct',1,'CH'),('CodeStruct_CH_2','行政机构编码表(编码以两位编号标识级次树形关系)','CodeStruct',2,'CH'),('ColSpanAttrDT_CH_0','跨0个单元格','ColSpanAttrDT',0,'CH'),('ColSpanAttrDT_CH_1','跨1个单元格','ColSpanAttrDT',1,'CH'),('ColSpanAttrDT_CH_2','跨2个单元格','ColSpanAttrDT',2,'CH'),('ColSpanAttrDT_CH_3','跨3个单元格','ColSpanAttrDT',3,'CH'),('ColSpanAttrDT_CH_4','跨4个单元格','ColSpanAttrDT',4,'CH'),('ColSpanAttrString_CH_1','跨1个单元格','ColSpanAttrString',1,'CH'),('ColSpanAttrString_CH_2','跨2个单元格','ColSpanAttrString',2,'CH'),('ColSpanAttrString_CH_3','跨3个单元格','ColSpanAttrString',3,'CH'),('ColSpanAttrString_CH_4','跨4个单元格','ColSpanAttrString',4,'CH'),('ColSpanAttrString_CH_5','跨4个单元格','ColSpanAttrString',5,'CH'),('ColSpanAttrString_CH_6','跨4个单元格','ColSpanAttrString',6,'CH'),('CondModel_CH_0','由连接线条件控制','CondModel',0,'CH'),('CondModel_CH_1','按照用户选择计算','CondModel',1,'CH'),('CondModel_CH_2','发送按钮旁下拉框选择','CondModel',2,'CH'),('ConfirmKind_CH_0','当前单元格','ConfirmKind',0,'CH'),('ConfirmKind_CH_1','左方单元格','ConfirmKind',1,'CH'),('ConfirmKind_CH_2','上方单元格','ConfirmKind',2,'CH'),('ConfirmKind_CH_3','右方单元格','ConfirmKind',3,'CH'),('ConfirmKind_CH_4','下方单元格','ConfirmKind',4,'CH'),('ConnJudgeWay_CH_0','or','ConnJudgeWay',0,'CH'),('ConnJudgeWay_CH_1','and','ConnJudgeWay',1,'CH'),('CtrlWay_CH_0','单个','CtrlWay',0,'CH'),('CtrlWay_CH_1','多个','CtrlWay',1,'CH'),('CtrlWay_CH_2','指定','CtrlWay',2,'CH'),('DataStoreModel_CH_0','数据轨迹模式','DataStoreModel',0,'CH'),('DataStoreModel_CH_1','数据合并模式','DataStoreModel',1,'CH'),('DataType_CH_0','字符串','DataType',0,'CH'),('DataType_CH_1','整数','DataType',1,'CH'),('DataType_CH_2','浮点数','DataType',2,'CH'),('DataType_CH_3','日期','DataType',3,'CH'),('DataType_CH_4','日期时间','DataType',4,'CH'),('DataType_CH_5','外键','DataType',5,'CH'),('DataType_CH_6','枚举','DataType',6,'CH'),('DBSrcType_CH_0','应用系统主数据库(默认)','DBSrcType',0,'CH'),('DBSrcType_CH_1','SQLServer数据库','DBSrcType',1,'CH'),('DBSrcType_CH_100','WebService数据源','DBSrcType',100,'CH'),('DBSrcType_CH_2','Oracle数据库','DBSrcType',2,'CH'),('DBSrcType_CH_3','MySQL数据库','DBSrcType',3,'CH'),('DBSrcType_CH_4','Informix数据库','DBSrcType',4,'CH'),('DBSrcType_CH_50','Dubbo服务','DBSrcType',50,'CH'),('DelEnable_CH_0','不能删除','DelEnable',0,'CH'),('DelEnable_CH_1','逻辑删除','DelEnable',1,'CH'),('DelEnable_CH_2','记录日志方式删除','DelEnable',2,'CH'),('DelEnable_CH_3','彻底删除','DelEnable',3,'CH'),('DelEnable_CH_4','让用户决定删除方式','DelEnable',4,'CH'),('DeleteWay_CH_0','不能删除','DeleteWay',0,'CH'),('DeleteWay_CH_1','删除所有','DeleteWay',1,'CH'),('DeleteWay_CH_2','只能删除自己上传的','DeleteWay',2,'CH'),('DocType_CH_0','正式公文','DocType',0,'CH'),('DocType_CH_1','便函','DocType',1,'CH'),('Draft_CH_0','无(不设草稿)','Draft',0,'CH'),('Draft_CH_1','保存到待办','Draft',1,'CH'),('Draft_CH_2','保存到草稿箱','Draft',2,'CH'),('DtlAddRecModel_CH_0','按设置的数量初始化空白行','DtlAddRecModel',0,'CH'),('DtlAddRecModel_CH_1','用按钮增加空白行','DtlAddRecModel',1,'CH'),('DtlOpenType_CH_0','操作员','DtlOpenType',0,'CH'),('DtlOpenType_CH_1','工作ID','DtlOpenType',1,'CH'),('DtlOpenType_CH_2','流程ID','DtlOpenType',2,'CH'),('DtlSaveModel_CH_0','自动存盘(失去焦点自动存盘)','DtlSaveModel',0,'CH'),('DtlSaveModel_CH_1','手动存盘(保存按钮触发存盘)','DtlSaveModel',1,'CH'),('DTSearchWay_CH_0','不启用','DTSearchWay',0,'CH'),('DTSearchWay_CH_1','按日期','DTSearchWay',1,'CH'),('DTSearchWay_CH_2','按日期时间','DTSearchWay',2,'CH'),('DTSWay_CH_0','不考核','DTSWay',0,'CH'),('DTSWay_CH_1','按照时效考核','DTSWay',1,'CH'),('DTSWay_CH_2','按照工作量考核','DTSWay',2,'CH'),('EditerType_CH_0','无编辑器','EditerType',0,'CH'),('EditerType_CH_1','Sina编辑器0','EditerType',1,'CH'),('EditerType_CH_2','FKEditer','EditerType',2,'CH'),('EditerType_CH_3','KindEditor','EditerType',3,'CH'),('EditerType_CH_4','百度的UEditor','EditerType',4,'CH'),('EnumUIContralType_CH_1','下拉框','EnumUIContralType',1,'CH'),('EnumUIContralType_CH_3','单选按钮','EnumUIContralType',3,'CH'),('EventType_CH_0','禁用','EventType',0,'CH'),('EventType_CH_1','执行URL','EventType',1,'CH'),('EventType_CH_2','执行CCFromRef.js','EventType',2,'CH'),('ExcelType_CH_0','普通文件数据提取','ExcelType',0,'CH'),('ExcelType_CH_1','流程附件数据提取','ExcelType',1,'CH'),('ExpType_CH_3','按照SQL计算','ExpType',3,'CH'),('ExpType_CH_4','按照参数计算','ExpType',4,'CH'),('FeiYongYuSuan_CH_0','1万元以下','FeiYongYuSuan',0,'CH'),('FeiYongYuSuan_CH_1','1-2万元','FeiYongYuSuan',1,'CH'),('FeiYongYuSuan_CH_2','2-5万元','FeiYongYuSuan',2,'CH'),('FeiYongYuSuan_CH_3','5-10万元','FeiYongYuSuan',3,'CH'),('FeiYongYuSuan_CH_4','10-20万元','FeiYongYuSuan',4,'CH'),('FeiYongYuSuan_CH_5','20-30万元','FeiYongYuSuan',5,'CH'),('FeiYongYuSuan_CH_6','30-50万元','FeiYongYuSuan',6,'CH'),('FeiYongYuSuan_CH_7','50-70万元','FeiYongYuSuan',7,'CH'),('FeiYongYuSuan_CH_8','70-100万元','FeiYongYuSuan',8,'CH'),('FeiYongYuSuan_CH_9','100万元','FeiYongYuSuan',9,'CH'),('FindLeader_CH_0','直接领导','FindLeader',0,'CH'),('FindLeader_CH_1','指定职务级别的领导','FindLeader',1,'CH'),('FindLeader_CH_2','指定职务的领导','FindLeader',2,'CH'),('FindLeader_CH_3','指定岗位的领导','FindLeader',3,'CH'),('FJOpen_CH_0','关闭附件','FJOpen',0,'CH'),('FJOpen_CH_1','操作员','FJOpen',1,'CH'),('FJOpen_CH_2','工作ID','FJOpen',2,'CH'),('FJOpen_CH_3','流程ID','FJOpen',3,'CH'),('FlowAppType_CH_0','业务流程','FlowAppType',0,'CH'),('FlowAppType_CH_1','工程类(项目组流程)','FlowAppType',1,'CH'),('FlowAppType_CH_2','公文流程(VSTO)','FlowAppType',2,'CH'),('FlowDeleteRole_CH_0','超级管理员可以删除','FlowDeleteRole',0,'CH'),('FlowDeleteRole_CH_1','分级管理员可以删除','FlowDeleteRole',1,'CH'),('FlowDeleteRole_CH_2','发起人可以删除','FlowDeleteRole',2,'CH'),('FlowDeleteRole_CH_3','节点启动删除按钮的操作员','FlowDeleteRole',3,'CH'),('FlowRunWay_CH_0','手工启动','FlowRunWay',0,'CH'),('FlowRunWay_CH_1','指定人员按时启动','FlowRunWay',1,'CH'),('FlowRunWay_CH_2','数据集按时启动','FlowRunWay',2,'CH'),('FlowRunWay_CH_3','触发式启动','FlowRunWay',3,'CH'),('FLRole_CH_0','按接受人','FLRole',0,'CH'),('FLRole_CH_1','按部门','FLRole',1,'CH'),('FLRole_CH_2','按岗位','FLRole',2,'CH'),('FrmSln_CH_0','默认方案','FrmSln',0,'CH'),('FrmSln_CH_1','只读方案','FrmSln',1,'CH'),('FrmSln_CH_2','自定义方案','FrmSln',2,'CH'),('FrmThreadSta_CH_0','禁用','FrmThreadSta',0,'CH'),('FrmThreadSta_CH_1','启用','FrmThreadSta',1,'CH'),('FrmTrackSta_CH_0','禁用','FrmTrackSta',0,'CH'),('FrmTrackSta_CH_1','标准风格','FrmTrackSta',1,'CH'),('FrmTrackSta_CH_2','华东院风格','FrmTrackSta',2,'CH'),('FrmTrackSta_CH_3','华夏银行风格','FrmTrackSta',3,'CH'),('FrmType_CH_0','傻瓜表单','FrmType',0,'CH'),('FrmType_CH_1','自由表单','FrmType',1,'CH'),('FrmType_CH_11','累加表单','FrmType',11,'CH'),('FrmType_CH_3','嵌入式表单','FrmType',3,'CH'),('FrmType_CH_4','Word表单','FrmType',4,'CH'),('FrmType_CH_5','在线编辑模式Excel表单','FrmType',5,'CH'),('FrmType_CH_6','VSTO模式Excel表单','FrmType',6,'CH'),('FrmType_CH_7','实体类组件','FrmType',7,'CH'),('FrmUrlShowWay_CH_0','不显示','FrmUrlShowWay',0,'CH'),('FrmUrlShowWay_CH_1','自动大小','FrmUrlShowWay',1,'CH'),('FrmUrlShowWay_CH_2','指定大小','FrmUrlShowWay',2,'CH'),('FrmUrlShowWay_CH_3','新窗口','FrmUrlShowWay',3,'CH'),('FTCSta_CH_0','禁用','FTCSta',0,'CH'),('FTCSta_CH_1','只读','FTCSta',1,'CH'),('FTCSta_CH_2','可设置人员','FTCSta',2,'CH'),('FTCWorkModel_CH_0','简洁模式','FTCWorkModel',0,'CH'),('FTCWorkModel_CH_1','高级模式','FTCWorkModel',1,'CH'),('FWCAth_CH_0','不启用','FWCAth',0,'CH'),('FWCAth_CH_1','多附件','FWCAth',1,'CH'),('FWCAth_CH_2','单附件(暂不支持)','FWCAth',2,'CH'),('FWCAth_CH_3','图片附件(暂不支持)','FWCAth',3,'CH'),('FWCMsgShow_CH_0','都显示','FWCMsgShow',0,'CH'),('FWCMsgShow_CH_1','仅显示自己的意见','FWCMsgShow',1,'CH'),('FWCOrderModel_CH_0','按审批时间先后排序','FWCOrderModel',0,'CH'),('FWCOrderModel_CH_1','按照接受人员列表先后顺序(官职大小)','FWCOrderModel',1,'CH'),('FWCShowModel_CH_0','表格方式','FWCShowModel',0,'CH'),('FWCShowModel_CH_1','自由模式','FWCShowModel',1,'CH'),('FWCSta_CH_0','禁用','FWCSta',0,'CH'),('FWCSta_CH_1','启用','FWCSta',1,'CH'),('FWCSta_CH_2','只读','FWCSta',2,'CH'),('FWCType_CH_0','审核组件','FWCType',0,'CH'),('FWCType_CH_1','日志组件','FWCType',1,'CH'),('FWCType_CH_2','周报组件','FWCType',2,'CH'),('FWCType_CH_3','月报组件','FWCType',3,'CH'),('FWCVer_CH_0','2018','FWCVer',0,'CH'),('FWCVer_CH_1','2019','FWCVer',1,'CH'),('FYLX_CH_0','汽车票','FYLX',0,'CH'),('FYLX_CH_1','打的票','FYLX',1,'CH'),('FYLX_CH_2','火车票','FYLX',2,'CH'),('FYLX_CH_3','飞机票','FYLX',3,'CH'),('FYLX_CH_4','其它','FYLX',4,'CH'),('GengDiJianSheJiBenTiaoJian_CH_0','条件1','GengDiJianSheJiBenTiaoJian',0,'CH'),('GengDiJianSheJiBenTiaoJian_CH_1','条件2','GengDiJianSheJiBenTiaoJian',1,'CH'),('GengDiJianSheJiBenTiaoJian_CH_2','条件3','GengDiJianSheJiBenTiaoJian',2,'CH'),('HuiQianRole_CH_0','不启用','HuiQianRole',0,'CH'),('HuiQianRole_CH_1','协作(同事)模式','HuiQianRole',1,'CH'),('HuiQianRole_CH_4','组长(领导)模式','HuiQianRole',4,'CH'),('HungUpWay_CH_0','无限挂起','HungUpWay',0,'CH'),('HungUpWay_CH_1','按指定的时间解除挂起并通知我自己','HungUpWay',1,'CH'),('HungUpWay_CH_2','按指定的时间解除挂起并通知所有人','HungUpWay',2,'CH'),('ImgSrcType_CH_0','本地','ImgSrcType',0,'CH'),('ImgSrcType_CH_1','URL','ImgSrcType',1,'CH'),('ImpModel_CH_0','不导入','ImpModel',0,'CH'),('ImpModel_CH_1','按配置模式导入','ImpModel',1,'CH'),('ImpModel_CH_2','按照xls文件模版导入','ImpModel',2,'CH'),('IsAutoSendSubFlowOver_CH_0','不处理','IsAutoSendSubFlowOver',0,'CH'),('IsAutoSendSubFlowOver_CH_1','让父流程自动运行下一步','IsAutoSendSubFlowOver',1,'CH'),('IsAutoSendSubFlowOver_CH_2','结束父流程','IsAutoSendSubFlowOver',2,'CH'),('IsSigan_CH_0','无','IsSigan',0,'CH'),('IsSigan_CH_1','图片签名','IsSigan',1,'CH'),('IsSigan_CH_2','山东CA','IsSigan',2,'CH'),('IsSigan_CH_3','广东CA','IsSigan',3,'CH'),('IsSigan_CH_4','图片盖章','IsSigan',4,'CH'),('IsSupperText_CH_0','yyyy-MM-dd','IsSupperText',0,'CH'),('IsSupperText_CH_1','yyyy-MM-dd HH:mm','IsSupperText',1,'CH'),('IsSupperText_CH_2','yyyy-MM-dd HH:mm:ss','IsSupperText',2,'CH'),('IsSupperText_CH_3','yyyy-MM','IsSupperText',3,'CH'),('IsSupperText_CH_4','HH:mm','IsSupperText',4,'CH'),('IsSupperText_CH_5','HH:mm:ss','IsSupperText',5,'CH'),('jd_CH_0','未定义','jd',0,'CH'),('JiMiChengDu_CH_0','公开','JiMiChengDu',0,'CH'),('JiMiChengDu_CH_1','保密','JiMiChengDu',1,'CH'),('JiMiChengDu_CH_2','秘密','JiMiChengDu',2,'CH'),('JiMiChengDu_CH_3','机密','JiMiChengDu',3,'CH'),('JingJiLeiXing_CH_0','内资','JingJiLeiXing',0,'CH'),('JingJiLeiXing_CH_1','外资','JingJiLeiXing',1,'CH'),('JinJiChengDu_CH_0','平件','JinJiChengDu',0,'CH'),('JinJiChengDu_CH_1','紧急','JinJiChengDu',1,'CH'),('jjlx1_CH_0','内资','jjlx1',0,'CH'),('JMCD_CH_0','一般','JMCD',0,'CH'),('JMCD_CH_1','保密','JMCD',1,'CH'),('JMCD_CH_2','秘密','JMCD',2,'CH'),('JMCD_CH_3','机密','JMCD',3,'CH'),('JumpWay_CH_0','不能跳转','JumpWay',0,'CH'),('JumpWay_CH_1','只能向后跳转','JumpWay',1,'CH'),('JumpWay_CH_2','只能向前跳转','JumpWay',2,'CH'),('JumpWay_CH_3','任意节点跳转','JumpWay',3,'CH'),('JumpWay_CH_4','按指定规则跳转','JumpWay',4,'CH'),('LGType_CH_0','普通','LGType',0,'CH'),('LGType_CH_1','枚举','LGType',1,'CH'),('LGType_CH_2','外键','LGType',2,'CH'),('LGType_CH_3','打开系统页面','LGType',3,'CH'),('ListShowModel_CH_0','表格','ListShowModel',0,'CH'),('ListShowModel_CH_1','卡片','ListShowModel',1,'CH'),('MenuCtrlWay_CH_0','按照设置的控制','MenuCtrlWay',0,'CH'),('MenuCtrlWay_CH_1','任何人都可以使用','MenuCtrlWay',1,'CH'),('MenuCtrlWay_CH_2','Admin用户可以使用','MenuCtrlWay',2,'CH'),('MenuType_CH_0','系统根目录','MenuType',0,'CH'),('MenuType_CH_1','系统类别','MenuType',1,'CH'),('MenuType_CH_2','系统','MenuType',2,'CH'),('MenuType_CH_3','目录','MenuType',3,'CH'),('MenuType_CH_4','功能','MenuType',4,'CH'),('MenuType_CH_5','功能控制点','MenuType',5,'CH'),('MiMiDengJi_CH_0','无','MiMiDengJi',0,'CH'),('MiMiDengJi_CH_1','普通','MiMiDengJi',1,'CH'),('MiMiDengJi_CH_2','秘密','MiMiDengJi',2,'CH'),('MiMiDengJi_CH_3','机密','MiMiDengJi',3,'CH'),('MiMiDengJi_CH_4','绝密','MiMiDengJi',4,'CH'),('Model_CH_0','普通','Model',0,'CH'),('Model_CH_1','固定行','Model',1,'CH'),('MoveToShowWay_CH_0','不显示','MoveToShowWay',0,'CH'),('MoveToShowWay_CH_1','下拉列表0','MoveToShowWay',1,'CH'),('MoveToShowWay_CH_2','平铺','MoveToShowWay',2,'CH'),('MsgCtrl_CH_0','不发送','MsgCtrl',0,'CH'),('MsgCtrl_CH_1','按设置的下一步接受人自动发送（默认）','MsgCtrl',1,'CH'),('MsgCtrl_CH_2','由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定','MsgCtrl',2,'CH'),('MsgCtrl_CH_3','由SDK开发者参数(IsSendEmail,IsSendSMS)来决定','MsgCtrl',3,'CH'),('MyDataType_CH_1','字符串String','MyDataType',1,'CH'),('MyDataType_CH_2','整数类型Int','MyDataType',2,'CH'),('MyDataType_CH_3','浮点类型AppFloat','MyDataType',3,'CH'),('MyDataType_CH_4','判断类型Boolean','MyDataType',4,'CH'),('MyDataType_CH_5','双精度类型Double','MyDataType',5,'CH'),('MyDataType_CH_6','日期型Date','MyDataType',6,'CH'),('MyDataType_CH_7','时间类型Datetime','MyDataType',7,'CH'),('MyDataType_CH_8','金额类型AppMoney','MyDataType',8,'CH'),('MyDeptRole_CH_0','仅部门领导可以查看','MyDeptRole',0,'CH'),('MyDeptRole_CH_1','部门下所有的人都可以查看','MyDeptRole',1,'CH'),('MyDeptRole_CH_2','本部门里指定岗位的人可以查看','MyDeptRole',2,'CH'),('OpenWay_CH_0','新窗口','OpenWay',0,'CH'),('OpenWay_CH_1','本窗口','OpenWay',1,'CH'),('OpenWay_CH_2','覆盖新窗口','OpenWay',2,'CH'),('PopValFormat_CH_0','No(仅编号)','PopValFormat',0,'CH'),('PopValFormat_CH_1','Name(仅名称)','PopValFormat',1,'CH'),('PopValFormat_CH_2','No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)','PopValFormat',2,'CH'),('PrintDocEnable_CH_0','不打印','PrintDocEnable',0,'CH'),('PrintDocEnable_CH_1','打印网页','PrintDocEnable',1,'CH'),('PrintDocEnable_CH_2','打印RTF模板','PrintDocEnable',2,'CH'),('PrintDocEnable_CH_3','打印Word模版','PrintDocEnable',3,'CH'),('PrintPDFModle_CH_0','全部打印','PrintPDFModle',0,'CH'),('PrintPDFModle_CH_1','单个表单打印(针对树形表单)','PrintPDFModle',1,'CH'),('PRI_CH_0','低','PRI',0,'CH'),('PRI_CH_1','中','PRI',1,'CH'),('PRI_CH_2','高','PRI',2,'CH'),('PushWay_CH_0','按照指定节点的工作人员','PushWay',0,'CH'),('PushWay_CH_1','按照指定的工作人员','PushWay',1,'CH'),('PushWay_CH_2','按照指定的工作岗位','PushWay',2,'CH'),('PushWay_CH_3','按照指定的部门','PushWay',3,'CH'),('PushWay_CH_4','按照指定的SQL','PushWay',4,'CH'),('PushWay_CH_5','按照系统指定的字段','PushWay',5,'CH'),('QingJiaLeiXing_CH_0','事假','QingJiaLeiXing',0,'CH'),('QingJiaLeiXing_CH_1','病假','QingJiaLeiXing',1,'CH'),('QingJiaLeiXing_CH_2','婚假','QingJiaLeiXing',2,'CH'),('QRModel_CH_0','不生成','QRModel',0,'CH'),('QRModel_CH_1','生成二维码','QRModel',1,'CH'),('RBShowModel_CH_0','竖向','RBShowModel',0,'CH'),('RBShowModel_CH_3','横向','RBShowModel',3,'CH'),('ReadReceipts_CH_0','不回执','ReadReceipts',0,'CH'),('ReadReceipts_CH_1','自动回执','ReadReceipts',1,'CH'),('ReadReceipts_CH_2','由上一节点表单字段决定','ReadReceipts',2,'CH'),('ReadReceipts_CH_3','由SDK开发者参数决定','ReadReceipts',3,'CH'),('ReadRole_CH_0','不控制','ReadRole',0,'CH'),('ReadRole_CH_1','未阅读阻止发送','ReadRole',1,'CH'),('ReadRole_CH_2','未阅读做记录','ReadRole',2,'CH'),('ReturnOneNodeRole_CH_0','不启用','ReturnOneNodeRole',0,'CH'),('ReturnOneNodeRole_CH_1','按照[退回信息填写字段]作为退回意见直接退回','ReturnOneNodeRole',1,'CH'),('ReturnOneNodeRole_CH_2','按照[审核组件]填写的信息作为退回意见直接退回','ReturnOneNodeRole',2,'CH'),('ReturnRole_CH_0','不能退回','ReturnRole',0,'CH'),('ReturnRole_CH_1','只能退回上一个节点','ReturnRole',1,'CH'),('ReturnRole_CH_2','可退回以前任意节点','ReturnRole',2,'CH'),('ReturnRole_CH_3','可退回指定的节点','ReturnRole',3,'CH'),('ReturnRole_CH_4','由流程图设计的退回路线决定','ReturnRole',4,'CH'),('ReturnSendModel_CH_0','从退回节点正常执行','ReturnSendModel',0,'CH'),('ReturnSendModel_CH_1','直接发送到当前节点','ReturnSendModel',1,'CH'),('ReturnSendModel_CH_2','直接发送到当前节点的下一个节点','ReturnSendModel',2,'CH'),('RowSpanAttrString_CH_1','跨1行','RowSpanAttrString',1,'CH'),('RowSpanAttrString_CH_2','跨2行','RowSpanAttrString',2,'CH'),('RowSpanAttrString_CH_3','跨3行','RowSpanAttrString',3,'CH'),('RunModel_CH_0','普通','RunModel',0,'CH'),('RunModel_CH_1','合流','RunModel',1,'CH'),('RunModel_CH_2','分流','RunModel',2,'CH'),('RunModel_CH_3','分合流','RunModel',3,'CH'),('RunModel_CH_4','子线程','RunModel',4,'CH'),('SaveModel_CH_0','仅节点表','SaveModel',0,'CH'),('SaveModel_CH_1','节点表与Rpt表','SaveModel',1,'CH'),('sbsy_CH_0','自有','sbsy',0,'CH'),('SearchUrlOpenType_CH_0','En.htm','SearchUrlOpenType',0,'CH'),('SearchUrlOpenType_CH_1','EnOnly.htm','SearchUrlOpenType',1,'CH'),('SearchUrlOpenType_CH_2','自定义url','SearchUrlOpenType',2,'CH'),('SelectAccepterEnable_CH_0','不启用','SelectAccepterEnable',0,'CH'),('SelectAccepterEnable_CH_1','单独启用','SelectAccepterEnable',1,'CH'),('SelectAccepterEnable_CH_2','在发送前打开','SelectAccepterEnable',2,'CH'),('SelectAccepterEnable_CH_3','转入新页面','SelectAccepterEnable',3,'CH'),('SelectorModel_CH_0','按岗位','SelectorModel',0,'CH'),('SelectorModel_CH_1','按部门','SelectorModel',1,'CH'),('SelectorModel_CH_2','按人员','SelectorModel',2,'CH'),('SelectorModel_CH_3','按SQL','SelectorModel',3,'CH'),('SelectorModel_CH_4','按SQL模版计算','SelectorModel',4,'CH'),('SelectorModel_CH_5','使用通用人员选择器','SelectorModel',5,'CH'),('SelectorModel_CH_6','部门与岗位的交集','SelectorModel',6,'CH'),('SelectorModel_CH_7','自定义Url','SelectorModel',7,'CH'),('SelectorModel_CH_8','使用通用部门岗位人员选择器','SelectorModel',8,'CH'),('SelectorModel_CH_9','按岗位智能计算(操作员所在部门)','SelectorModel',9,'CH'),('sex__CH_0','男','sex_',0,'CH'),('sex__CH_1','女','sex_',1,'CH'),('SFOpenType_CH_0','工作查看器','SFOpenType',0,'CH'),('SFOpenType_CH_1','傻瓜表单轨迹查看器','SFOpenType',1,'CH'),('SFShowCtrl_CH_0','可以看所有的子流程','SFShowCtrl',0,'CH'),('SFShowCtrl_CH_1','仅仅可以看自己发起的子流程','SFShowCtrl',1,'CH'),('SFShowModel_CH_0','表格方式','SFShowModel',0,'CH'),('SFShowModel_CH_1','自由模式','SFShowModel',1,'CH'),('SFSta_CH_0','禁用','SFSta',0,'CH'),('SFSta_CH_1','启用','SFSta',1,'CH'),('SFSta_CH_2','只读','SFSta',2,'CH'),('SharingType_CH_0','共享','SharingType',0,'CH'),('SharingType_CH_1','私有','SharingType',1,'CH'),('ShenFen_CH_0','工人','ShenFen',0,'CH'),('ShenFen_CH_1','农民','ShenFen',1,'CH'),('ShenFen_CH_2','干部','ShenFen',2,'CH'),('ShiFouYunXuZuoYe_CH_0','符合安全标准允许作业','ShiFouYunXuZuoYe',0,'CH'),('ShiFouYunXuZuoYe_CH_1','不符合安全标准需要整改','ShiFouYunXuZuoYe',1,'CH'),('ShowWhere_CH_0','树形表单','ShowWhere',0,'CH'),('ShowWhere_CH_1','工具栏','ShowWhere',1,'CH'),('SignType_CH_0','不签名','SignType',0,'CH'),('SignType_CH_1','图片签名','SignType',1,'CH'),('SignType_CH_2','电子签名','SignType',2,'CH'),('SQLType_CH_0','方向条件','SQLType',0,'CH'),('SQLType_CH_1','接受人规则','SQLType',1,'CH'),('SQLType_CH_2','下拉框数据过滤','SQLType',2,'CH'),('SQLType_CH_3','级联下拉框','SQLType',3,'CH'),('SQLType_CH_4','PopVal开窗返回值','SQLType',4,'CH'),('SQLType_CH_5','人员选择器人员选择范围','SQLType',5,'CH'),('SrcType_CH_0','本地的类','SrcType',0,'CH'),('SrcType_CH_1','创建表','SrcType',1,'CH'),('SrcType_CH_2','表或视图','SrcType',2,'CH'),('SrcType_CH_3','SQL查询表','SrcType',3,'CH'),('SrcType_CH_4','WebServices','SrcType',4,'CH'),('SrcType_CH_5','微服务Handler外部数据源','SrcType',5,'CH'),('SrcType_CH_6','JavaScript外部数据源','SrcType',6,'CH'),('SSOType_CH_0','SID验证','SSOType',0,'CH'),('SSOType_CH_1','连接','SSOType',1,'CH'),('SSOType_CH_2','表单提交','SSOType',2,'CH'),('SSOType_CH_3','不传值','SSOType',3,'CH'),('SubFlowStartWay_CH_0','不启动','SubFlowStartWay',0,'CH'),('SubFlowStartWay_CH_1','指定的字段启动','SubFlowStartWay',1,'CH'),('SubFlowStartWay_CH_2','按明细表启动','SubFlowStartWay',2,'CH'),('SubFlowType_CH_0','手动启动子流程','SubFlowType',0,'CH'),('SubFlowType_CH_1','触发启动子流程','SubFlowType',1,'CH'),('SubFlowType_CH_2','延续子流程','SubFlowType',2,'CH'),('SubThreadType_CH_0','同表单','SubThreadType',0,'CH'),('SubThreadType_CH_1','异表单','SubThreadType',1,'CH'),('TableCol_CH_0','4列','TableCol',0,'CH'),('TableCol_CH_1','6列','TableCol',1,'CH'),('TableCol_CH_2','3列','TableCol',2,'CH'),('TabType_CH_0','本地表或视图','TabType',0,'CH'),('TabType_CH_1','通过一个SQL确定的一个外部数据源','TabType',1,'CH'),('TabType_CH_2','通过WebServices获得的一个数据源','TabType',2,'CH'),('Target_CH_0','新窗口','Target',0,'CH'),('Target_CH_1','本窗口','Target',1,'CH'),('Target_CH_2','父窗口','Target',2,'CH'),('TaskSta_CH_0','未开始','TaskSta',0,'CH'),('TaskSta_CH_1','进行中','TaskSta',1,'CH'),('TaskSta_CH_2','完成','TaskSta',2,'CH'),('TaskSta_CH_3','推迟','TaskSta',3,'CH'),('TemplateFileModel_CH_0','rtf模版','TemplateFileModel',0,'CH'),('TemplateFileModel_CH_1','vsto模式的word模版','TemplateFileModel',1,'CH'),('TemplateFileModel_CH_2','vsto模式的excel模版','TemplateFileModel',2,'CH'),('ThreadKillRole_CH_0','不能删除','ThreadKillRole',0,'CH'),('ThreadKillRole_CH_1','手工删除','ThreadKillRole',1,'CH'),('ThreadKillRole_CH_2','自动删除','ThreadKillRole',2,'CH'),('TimelineRole_CH_0','按节点(由节点属性来定义)','TimelineRole',0,'CH'),('TimelineRole_CH_1','按发起人(开始节点SysSDTOfFlow字段计算)','TimelineRole',1,'CH'),('ToobarExcType_CH_0','超链接','ToobarExcType',0,'CH'),('ToobarExcType_CH_1','函数','ToobarExcType',1,'CH'),('TSpan_CH_0','本周','TSpan',0,'CH'),('TSpan_CH_1','上周','TSpan',1,'CH'),('TSpan_CH_2','两周以前','TSpan',2,'CH'),('TSpan_CH_3','三周以前','TSpan',3,'CH'),('TSpan_CH_4','更早','TSpan',4,'CH'),('UIRowStyleGlo_CH_0','无风格','UIRowStyleGlo',0,'CH'),('UIRowStyleGlo_CH_1','交替风格','UIRowStyleGlo',1,'CH'),('UIRowStyleGlo_CH_2','鼠标移动','UIRowStyleGlo',2,'CH'),('UIRowStyleGlo_CH_3','交替并鼠标移动','UIRowStyleGlo',3,'CH'),('UploadFileCheck_CH_0','不控制','UploadFileCheck',0,'CH'),('UploadFileCheck_CH_1','上传附件个数不能为0','UploadFileCheck',1,'CH'),('UploadFileCheck_CH_2','每个类别下面的个数不能为0','UploadFileCheck',2,'CH'),('UploadFileNumCheck_CH_0','不用校验','UploadFileNumCheck',0,'CH'),('UploadFileNumCheck_CH_1','不能为空','UploadFileNumCheck',1,'CH'),('UploadFileNumCheck_CH_2','每个类别下不能为空','UploadFileNumCheck',2,'CH'),('UrlSrcType_CH_0','自定义','UrlSrcType',0,'CH'),('UrlSrcType_CH_1','表单库','UrlSrcType',1,'CH'),('UserType_CH_0','普通用户','UserType',0,'CH'),('UserType_CH_1','管理员用户','UserType',1,'CH'),('UseSta_CH_0','禁用','UseSta',0,'CH'),('UseSta_CH_1','启用','UseSta',1,'CH'),('WebOfficeEnable_CH_0','不启用','WebOfficeEnable',0,'CH'),('WebOfficeEnable_CH_1','按钮方式','WebOfficeEnable',1,'CH'),('WebOfficeEnable_CH_2','标签页置后方式','WebOfficeEnable',2,'CH'),('WebOfficeEnable_CH_3','标签页置前方式','WebOfficeEnable',3,'CH'),('WFStateApp_CH_10','批处理','WFStateApp',10,'CH'),('WFStateApp_CH_2','运行中','WFStateApp',2,'CH'),('WFStateApp_CH_3','已完成','WFStateApp',3,'CH'),('WFStateApp_CH_4','挂起','WFStateApp',4,'CH'),('WFStateApp_CH_5','退回','WFStateApp',5,'CH'),('WFStateApp_CH_6','转发','WFStateApp',6,'CH'),('WFStateApp_CH_7','删除','WFStateApp',7,'CH'),('WFStateApp_CH_8','加签','WFStateApp',8,'CH'),('WFStateApp_CH_9','冻结','WFStateApp',9,'CH'),('WFState_CH_0','空白','WFState',0,'CH'),('WFState_CH_1','草稿','WFState',1,'CH'),('WFState_CH_10','批处理','WFState',10,'CH'),('WFState_CH_11','加签回复状态','WFState',11,'CH'),('WFState_CH_2','运行中','WFState',2,'CH'),('WFState_CH_3','已完成','WFState',3,'CH'),('WFState_CH_4','挂起','WFState',4,'CH'),('WFState_CH_5','退回','WFState',5,'CH'),('WFState_CH_6','转发','WFState',6,'CH'),('WFState_CH_7','删除','WFState',7,'CH'),('WFState_CH_8','加签','WFState',8,'CH'),('WFState_CH_9','冻结','WFState',9,'CH'),('WFSta_CH_0','运行中','WFSta',0,'CH'),('WFSta_CH_1','已完成','WFSta',1,'CH'),('WFSta_CH_2','其他','WFSta',2,'CH'),('WhenOverSize_CH_0','不处理','WhenOverSize',0,'CH'),('WhenOverSize_CH_1','向下顺增行','WhenOverSize',1,'CH'),('WhenOverSize_CH_2','次页显示','WhenOverSize',2,'CH'),('WhoExeIt_CH_0','操作员执行','WhoExeIt',0,'CH'),('WhoExeIt_CH_1','机器执行','WhoExeIt',1,'CH'),('WhoExeIt_CH_2','混合执行','WhoExeIt',2,'CH'),('WhoIsPK_CH_0','WorkID是主键','WhoIsPK',0,'CH'),('WhoIsPK_CH_1','FID是主键(干流程的WorkID)','WhoIsPK',1,'CH'),('WhoIsPK_CH_2','父流程ID是主键','WhoIsPK',2,'CH'),('WhoIsPK_CH_3','延续流程ID是主键','WhoIsPK',3,'CH'),('XB_CH_0','女','XB',0,'CH'),('XB_CH_1','男','XB',1,'CH'),('XingBie_CH_0','男','XingBie',0,'CH'),('XingBie_CH_1','女','XingBie',1,'CH'),('xqsj_CH_0','长期有效','xqsj',0,'CH'),('xqsj_CH_1','截止于','xqsj',1,'CH'),('xsq_CH_0','未绑定','xsq',0,'CH'),('YBFlowReturnRole_CH_0','不能退回','YBFlowReturnRole',0,'CH'),('YBFlowReturnRole_CH_1','退回到父流程的开始节点','YBFlowReturnRole',1,'CH'),('YBFlowReturnRole_CH_2','退回到父流程的任何节点','YBFlowReturnRole',2,'CH'),('YBFlowReturnRole_CH_3','退回父流程的启动节点','YBFlowReturnRole',3,'CH'),('YBFlowReturnRole_CH_4','可退回到指定的节点','YBFlowReturnRole',4,'CH'),('ZhuYaoShiChang_CH_0','国外','ZhuYaoShiChang',0,'CH'),('ZhuYaoShiChang_CH_1','国内','ZhuYaoShiChang',1,'CH'),('ZhuYaoShiChang_CH_2','内外并重','ZhuYaoShiChang',2,'CH'),('ZZMM_CH_0','少先队员','ZZMM',0,'CH'),('ZZMM_CH_1','团员','ZZMM',1,'CH'),('ZZMM_CH_2','党员','ZZMM',2,'CH');
/*!40000 ALTER TABLE `sys_enum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enummain`
--

DROP TABLE IF EXISTS `sys_enummain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_enummain` (
  `No` varchar(40) NOT NULL COMMENT '编号',
  `Name` varchar(40) DEFAULT NULL COMMENT '名称',
  `CfgVal` varchar(1500) DEFAULT NULL COMMENT '配置信息',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enummain`
--

LOCK TABLES `sys_enummain` WRITE;
/*!40000 ALTER TABLE `sys_enummain` DISABLE KEYS */;
INSERT INTO `sys_enummain` VALUES ('CeShiMoShi','测试模式','@0=强办模式@1=协作模式@2=队列模式@3=共享模式','CH'),('CGDD','采购地点','@0=本市@1=外地','CH'),('FYLX','费用类型','@0=汽车票@1=打的票@2=火车票','CH'),('QingJiaLeiXing','请假类型','@0=事假@1=病假@2=婚假','CH'),('ShiFouYunXuZuoYe','是否允许作业','@0=符合安全标准允许作业@1=不符合安全标准需要整改','CH');
/*!40000 ALTER TABLE `sys_enummain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enver`
--

DROP TABLE IF EXISTS `sys_enver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_enver` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `No` varchar(50) DEFAULT NULL COMMENT '实体类',
  `Name` varchar(100) DEFAULT NULL COMMENT '实体名',
  `PKValue` varchar(300) DEFAULT NULL COMMENT '主键值',
  `EVer` int(11) DEFAULT '1' COMMENT '版本号',
  `Rec` varchar(100) DEFAULT NULL COMMENT '修改人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enver`
--

LOCK TABLES `sys_enver` WRITE;
/*!40000 ALTER TABLE `sys_enver` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_enver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enverdtl`
--

DROP TABLE IF EXISTS `sys_enverdtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_enverdtl` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `EnName` varchar(200) DEFAULT NULL COMMENT '实体名',
  `EnVerPK` varchar(100) DEFAULT NULL COMMENT '版本主表PK',
  `AttrKey` varchar(100) DEFAULT NULL COMMENT '字段',
  `AttrName` varchar(200) DEFAULT NULL COMMENT '字段名',
  `OldVal` varchar(100) DEFAULT NULL COMMENT '旧值',
  `NewVal` varchar(100) DEFAULT NULL COMMENT '新值',
  `EnVer` int(11) DEFAULT '1' COMMENT '版本号(日期)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '日期',
  `Rec` varchar(100) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enverdtl`
--

LOCK TABLES `sys_enverdtl` WRITE;
/*!40000 ALTER TABLE `sys_enverdtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_enverdtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_excelfield`
--

DROP TABLE IF EXISTS `sys_excelfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_excelfield` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `CellName` varchar(50) DEFAULT NULL COMMENT '单元格名称',
  `CellRow` int(11) DEFAULT '0' COMMENT '行号',
  `CellColumn` int(11) DEFAULT '0' COMMENT '列号',
  `FK_ExcelSheet` varchar(100) DEFAULT NULL COMMENT '所属ExcelSheet表',
  `Field` varchar(50) DEFAULT NULL COMMENT '存储字段名',
  `FK_ExcelTable` varchar(100) DEFAULT NULL COMMENT '存储数据表',
  `DataType` int(11) DEFAULT '0' COMMENT '值类型',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '外键表/枚举',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '外键表No',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '外键表Name',
  `Validators` text COMMENT '校验器',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT '所属Excel模板',
  `AtPara` text COMMENT '参数',
  `ConfirmKind` int(11) DEFAULT '0' COMMENT '单元格确认方式',
  `ConfirmCellCount` int(11) DEFAULT '1' COMMENT '单元格确认方向移动量',
  `ConfirmCellValue` varchar(200) DEFAULT NULL COMMENT '对应单元格值',
  `ConfirmRepeatIndex` int(11) DEFAULT '0' COMMENT '对应单元格值重复选定次序',
  `SkipIsNull` int(11) DEFAULT '0' COMMENT '不计非空',
  `SyncToField` varchar(100) DEFAULT NULL COMMENT '同步到字段',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_excelfield`
--

LOCK TABLES `sys_excelfield` WRITE;
/*!40000 ALTER TABLE `sys_excelfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_excelfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_excelfile`
--

DROP TABLE IF EXISTS `sys_excelfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_excelfile` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Mark` varchar(50) DEFAULT NULL COMMENT '标识',
  `ExcelType` int(11) DEFAULT '0' COMMENT '类型',
  `Note` text COMMENT '上传说明',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '模板文件',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_excelfile`
--

LOCK TABLES `sys_excelfile` WRITE;
/*!40000 ALTER TABLE `sys_excelfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_excelfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_excelsheet`
--

DROP TABLE IF EXISTS `sys_excelsheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_excelsheet` (
  `No` varchar(36) NOT NULL COMMENT 'No',
  `Name` varchar(50) DEFAULT NULL COMMENT 'Sheet名称',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板',
  `SheetIndex` int(11) DEFAULT '0' COMMENT 'Sheet索引',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_excelsheet`
--

LOCK TABLES `sys_excelsheet` WRITE;
/*!40000 ALTER TABLE `sys_excelsheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_excelsheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_exceltable`
--

DROP TABLE IF EXISTS `sys_exceltable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_exceltable` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '数据表名',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板',
  `IsDtl` int(11) DEFAULT '0' COMMENT '是否明细表',
  `Note` text COMMENT '数据表说明',
  `SyncToTable` varchar(100) DEFAULT NULL COMMENT '同步到表',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_exceltable`
--

LOCK TABLES `sys_exceltable` WRITE;
/*!40000 ALTER TABLE `sys_exceltable` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_exceltable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_filemanager`
--

DROP TABLE IF EXISTS `sys_filemanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_filemanager` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `AttrFileName` varchar(50) DEFAULT NULL COMMENT '指定名称',
  `AttrFileNo` varchar(50) DEFAULT NULL COMMENT '指定编号',
  `EnName` varchar(50) DEFAULT NULL COMMENT '关联的表',
  `RefVal` varchar(50) DEFAULT NULL COMMENT '主键值',
  `WebPath` varchar(100) DEFAULT NULL COMMENT 'Web路径',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '文件名称',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  `RDT` varchar(50) DEFAULT NULL COMMENT '上传时间',
  `Rec` varchar(50) DEFAULT NULL COMMENT '上传人',
  `Doc` text COMMENT '内容',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_filemanager`
--

LOCK TABLES `sys_filemanager` WRITE;
/*!40000 ALTER TABLE `sys_filemanager` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_filemanager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_formtree`
--

DROP TABLE IF EXISTS `sys_formtree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_formtree` (
  `No` varchar(10) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织编号',
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `IsDir` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_formtree`
--

LOCK TABLES `sys_formtree` WRITE;
/*!40000 ALTER TABLE `sys_formtree` DISABLE KEYS */;
INSERT INTO `sys_formtree` VALUES ('01','表单元素','1',NULL,0,0),('1','表单库','0',NULL,0,0);
/*!40000 ALTER TABLE `sys_formtree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmattachment`
--

DROP TABLE IF EXISTS `sys_frmattachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmattachment` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件标识',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点控制(对sln有效)',
  `AthRunModel` int(11) DEFAULT '0' COMMENT '运行模式',
  `Name` varchar(50) DEFAULT NULL COMMENT '附件名称',
  `Exts` varchar(50) DEFAULT NULL COMMENT '文件格式',
  `NumOfUpload` int(11) DEFAULT '0' COMMENT '最低上传数量',
  `AthSaveWay` int(11) DEFAULT '0' COMMENT '保存方式',
  `SaveTo` varchar(150) DEFAULT NULL COMMENT '保存到',
  `Sort` varchar(500) DEFAULT NULL COMMENT '类别',
  `IsTurn2Html` int(11) DEFAULT '0' COMMENT '是否转换成html(方便手机浏览)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `W` float DEFAULT NULL COMMENT '宽度',
  `H` float DEFAULT NULL COMMENT '高度',
  `DeleteWay` int(11) DEFAULT '0' COMMENT '附件删除规则',
  `IsUpload` int(11) DEFAULT '1' COMMENT '是否可以上传',
  `IsDownload` int(11) DEFAULT '1' COMMENT '是否可以下载',
  `IsOrder` int(11) DEFAULT '0' COMMENT '是否可以排序',
  `IsAutoSize` int(11) DEFAULT '1' COMMENT '自动控制大小',
  `IsNote` int(11) DEFAULT '1' COMMENT '是否增加备注',
  `IsExpCol` int(11) DEFAULT '1' COMMENT '是否启用扩展列',
  `IsShowTitle` int(11) DEFAULT '1' COMMENT '是否显示标题列',
  `UploadType` int(11) DEFAULT '0' COMMENT '上传类型',
  `AthUploadWay` int(11) DEFAULT '0' COMMENT '控制上传控制方式',
  `CtrlWay` int(11) DEFAULT '0' COMMENT '控制呈现控制方式',
  `IsRowLock` int(11) DEFAULT '1' COMMENT '是否启用锁定行',
  `IsWoEnableWF` int(11) DEFAULT '1' COMMENT '是否启用weboffice',
  `IsWoEnableSave` int(11) DEFAULT '1' COMMENT '是否启用保存',
  `IsWoEnableReadonly` int(11) DEFAULT '1' COMMENT '是否只读',
  `IsWoEnableRevise` int(11) DEFAULT '1' COMMENT '是否启用修订',
  `IsWoEnableViewKeepMark` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `IsWoEnablePrint` int(11) DEFAULT '1' COMMENT '是否打印',
  `IsWoEnableSeal` int(11) DEFAULT '1' COMMENT '是否启用签章',
  `IsWoEnableOver` int(11) DEFAULT '1' COMMENT '是否启用套红',
  `IsWoEnableTemplete` int(11) DEFAULT '1' COMMENT '是否启用公文模板',
  `IsWoEnableCheck` int(11) DEFAULT '1' COMMENT '是否自动写入审核信息',
  `IsWoEnableInsertFlow` int(11) DEFAULT '1' COMMENT '是否插入流程',
  `IsWoEnableInsertFengXian` int(11) DEFAULT '1' COMMENT '是否插入风险点',
  `IsWoEnableMarks` int(11) DEFAULT '1' COMMENT '是否启用留痕模式',
  `IsWoEnableDown` int(11) DEFAULT '1' COMMENT '是否启用下载',
  `IsToHeLiuHZ` int(11) DEFAULT '1' COMMENT '该附件是否要汇总到合流节点上去？(对子线程节点有效)',
  `IsHeLiuHuiZong` int(11) DEFAULT '1' COMMENT '是否是合流节点的汇总附件组件？(对合流节点有效)',
  `DataRefNoOfObj` varchar(150) DEFAULT NULL COMMENT '对应附件标识',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  `GroupID` int(11) DEFAULT '0' COMMENT 'GroupID',
  `GUID` varchar(128) DEFAULT '' COMMENT 'GUID',
  `IsDelete` int(11) DEFAULT '1',
  `ReadRole` int(11) DEFAULT '0',
  `UploadFileNumCheck` int(11) DEFAULT '0',
  `TopNumOfUpload` int(11) DEFAULT '99',
  `FileMaxSize` int(11) DEFAULT '10240',
  `IsVisable` int(11) DEFAULT '1',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmattachment`
--

LOCK TABLES `sys_frmattachment` WRITE;
/*!40000 ALTER TABLE `sys_frmattachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmattachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmattachmentdb`
--

DROP TABLE IF EXISTS `sys_frmattachmentdb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmattachmentdb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `FK_FrmAttachment` varchar(500) DEFAULT NULL COMMENT '附件主键',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件标识',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `NodeID` varchar(50) DEFAULT NULL COMMENT '节点ID',
  `Sort` varchar(200) DEFAULT NULL COMMENT '类别',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  `IsRowLock` int(11) DEFAULT '0' COMMENT '是否锁定行',
  `Idx` int(11) DEFAULT '0' COMMENT '排序',
  `UploadGUID` varchar(500) DEFAULT NULL COMMENT '上传GUID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmattachmentdb`
--

LOCK TABLES `sys_frmattachmentdb` WRITE;
/*!40000 ALTER TABLE `sys_frmattachmentdb` DISABLE KEYS */;
INSERT INTO `sys_frmattachmentdb` VALUES ('23f37bb3608b4f39992cd3181da541e3','ND20801','ND20801_Ath1','Ath1','128',0,'20801','','D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20801/\\128\\23f37bb3608b4f39992cd3181da541e3.1cae3637a1d4f5dd8ec63fdf0bc687c3.png','1cae3637a1d4f5dd8ec63fdf0bc687c3.png','png',68.8477,'2019-03-11','admin','系统管理员','',0,0,'23f37bb3608b4f39992cd3181da541e3',''),('78ac849cf3154f778c25591d190c37b8','ND20701','ND20701_Ath1','Ath1','127',0,'20701','','D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\127\\78ac849cf3154f778c25591d190c37b8.1cae3637a1d4f5dd8ec63fdf0bc687c3.png','1cae3637a1d4f5dd8ec63fdf0bc687c3.png','png',68.8477,'2019-03-09','admin','系统管理员','',0,0,'78ac849cf3154f778c25591d190c37b8',''),('795fd02e67144baeb2cf544198037067','ND20701','ND20701_Ath1','Ath1','126',0,'20701','','D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\126\\795fd02e67144baeb2cf544198037067.1cae3637a1d4f5dd8ec63fdf0bc687c3.png','1cae3637a1d4f5dd8ec63fdf0bc687c3.png','png',68.8477,'2019-03-09','admin','系统管理员','',0,0,'795fd02e67144baeb2cf544198037067',''),('a5d64372f8944f03b49f907e7cc1f88e','ND20701','ND20701_Ath1','Ath1','126',0,'20701','','D:\\jeesite4-jflow3\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\126\\a5d64372f8944f03b49f907e7cc1f88e.1cae3637a1d4f5dd8ec63fdf0bc687c3.png','1cae3637a1d4f5dd8ec63fdf0bc687c3.png','png',68.8477,'2019-03-09','admin','系统管理员','',0,0,'a5d64372f8944f03b49f907e7cc1f88e',''),('c7f888b5c5a6425381558c2c29e1b3f9','ND20701','ND20701_Ath1','Ath1','127',0,'20701','','D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\127\\c7f888b5c5a6425381558c2c29e1b3f9.1fab5316dfd667e31e937259d484c992.png','1fab5316dfd667e31e937259d484c992.png','png',25.3721,'2019-03-09','admin','系统管理员','',0,0,'c7f888b5c5a6425381558c2c29e1b3f9',''),('e28108de2cb04282bd55fdf9eb64249d','ND20701','ND20701_Ath1','Ath1','126',0,'20701','','D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\126\\e28108de2cb04282bd55fdf9eb64249d.5cebdbbdc3088ba927a5745ef0feb55d.jpg','5cebdbbdc3088ba927a5745ef0feb55d.jpg','jpg',79.4951,'2019-03-09','admin','系统管理员','',0,0,'e28108de2cb04282bd55fdf9eb64249d','');
/*!40000 ALTER TABLE `sys_frmattachmentdb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmbtn`
--

DROP TABLE IF EXISTS `sys_frmbtn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmbtn` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Text` text COMMENT '标签',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `IsView` int(11) DEFAULT '0' COMMENT '是否可见',
  `IsEnable` int(11) DEFAULT '0' COMMENT '是否起用',
  `UAC` int(11) DEFAULT '0' COMMENT '控制类型',
  `UACContext` text COMMENT '控制内容',
  `EventType` int(11) DEFAULT '0' COMMENT '事件类型',
  `EventContext` text COMMENT '事件内容',
  `MsgOK` varchar(500) DEFAULT NULL COMMENT '运行成功提示',
  `MsgErr` varchar(500) DEFAULT NULL COMMENT '运行失败提示',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` int(11) DEFAULT '0' COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT '0' COMMENT '所在分组',
  `BtnType` int(11) DEFAULT '0',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmbtn`
--

LOCK TABLES `sys_frmbtn` WRITE;
/*!40000 ALTER TABLE `sys_frmbtn` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmbtn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmele`
--

DROP TABLE IF EXISTS `sys_frmele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmele` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `EleID` varchar(50) DEFAULT NULL COMMENT '控件ID值(对部分控件有效)',
  `EleName` varchar(200) DEFAULT NULL COMMENT '控件名称(对部分控件有效)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `Tag1` varchar(50) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(50) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(50) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(50) DEFAULT NULL COMMENT 'Tag4',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmele`
--

LOCK TABLES `sys_frmele` WRITE;
/*!40000 ALTER TABLE `sys_frmele` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmele` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmeledb`
--

DROP TABLE IF EXISTS `sys_frmeledb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmeledb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `EleID` varchar(50) DEFAULT NULL COMMENT 'EleID',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT 'RefPKVal',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `Tag1` varchar(1000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(1000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(1000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(1000) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(1000) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmeledb`
--

LOCK TABLES `sys_frmeledb` WRITE;
/*!40000 ALTER TABLE `sys_frmeledb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmeledb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmevent`
--

DROP TABLE IF EXISTS `sys_frmevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmevent` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Event` varchar(400) DEFAULT NULL COMMENT '事件名称',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `EventDoType` int(11) DEFAULT '0' COMMENT '事件类型',
  `DoDoc` varchar(400) DEFAULT NULL COMMENT '执行内容',
  `MsgOK` varchar(400) DEFAULT NULL COMMENT '成功执行提示',
  `MsgError` varchar(400) DEFAULT NULL COMMENT '异常信息提示',
  `MsgCtrl` int(11) DEFAULT '0' COMMENT '消息发送控制',
  `MailEnable` int(11) DEFAULT '1' COMMENT '是否启用邮件发送？(如果启用就要设置邮件模版，支持ccflow表达式。)',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `SMSEnable` int(11) DEFAULT '0' COMMENT '是否启用短信发送？(如果启用就要设置短信模版，支持ccflow表达式。)',
  `SMSDoc` text COMMENT '短信内容模版',
  `MobilePushEnable` int(11) DEFAULT '1' COMMENT '是否推送到手机、pad端。',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmevent`
--

LOCK TABLES `sys_frmevent` WRITE;
/*!40000 ALTER TABLE `sys_frmevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmimg`
--

DROP TABLE IF EXISTS `sys_frmimg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmimg` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `ImgAppType` int(11) DEFAULT '0' COMMENT '应用类型',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `ImgURL` varchar(200) DEFAULT NULL COMMENT 'ImgURL',
  `ImgPath` varchar(200) DEFAULT NULL COMMENT 'ImgPath',
  `LinkURL` varchar(200) DEFAULT NULL COMMENT 'LinkURL',
  `LinkTarget` varchar(200) DEFAULT NULL COMMENT 'LinkTarget',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag0` varchar(500) DEFAULT NULL COMMENT '参数',
  `ImgSrcType` int(11) DEFAULT '0' COMMENT '图片来源0=本地,1=URL',
  `IsEdit` int(11) DEFAULT '0' COMMENT '是否可以编辑',
  `Name` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `EnPK` varchar(500) DEFAULT NULL COMMENT '英文名称',
  `GroupID` varchar(50) DEFAULT '0' COMMENT '所在分组',
  `GroupIDText` varchar(500) DEFAULT NULL,
  `KeyOfEn` varchar(100) DEFAULT '',
  `ColSpan` int(11) DEFAULT '0',
  `TextColSpan` int(11) DEFAULT '1',
  `RowSpan` int(11) DEFAULT '1',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmimg`
--

LOCK TABLES `sys_frmimg` WRITE;
/*!40000 ALTER TABLE `sys_frmimg` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmimg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmimgath`
--

DROP TABLE IF EXISTS `sys_frmimgath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmimgath` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `CtrlID` varchar(200) DEFAULT NULL COMMENT '控件ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '中文名称',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `IsEdit` int(11) DEFAULT '1' COMMENT '是否可编辑',
  `IsRequired` int(11) DEFAULT '0' COMMENT '是否必填项',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmimgath`
--

LOCK TABLES `sys_frmimgath` WRITE;
/*!40000 ALTER TABLE `sys_frmimgath` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmimgath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmimgathdb`
--

DROP TABLE IF EXISTS `sys_frmimgathdb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmimgathdb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '附件ID',
  `FK_FrmImgAth` varchar(50) DEFAULT NULL COMMENT '图片附件编号',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件全路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展名',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmimgathdb`
--

LOCK TABLES `sys_frmimgathdb` WRITE;
/*!40000 ALTER TABLE `sys_frmimgathdb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmimgathdb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmlab`
--

DROP TABLE IF EXISTS `sys_frmlab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmlab` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `Text` text COMMENT 'Label',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT '12' COMMENT '字体大小',
  `FontColor` varchar(50) DEFAULT NULL COMMENT '颜色',
  `FontName` varchar(50) DEFAULT NULL COMMENT '字体名称',
  `FontStyle` varchar(200) DEFAULT NULL COMMENT '字体风格',
  `FontWeight` varchar(50) DEFAULT NULL COMMENT '字体宽度',
  `IsBold` int(11) DEFAULT '0' COMMENT '是否粗体',
  `IsItalic` int(11) DEFAULT '0' COMMENT '是否斜体',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmlab`
--

LOCK TABLES `sys_frmlab` WRITE;
/*!40000 ALTER TABLE `sys_frmlab` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmlab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmline`
--

DROP TABLE IF EXISTS `sys_frmline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmline` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `X1` float DEFAULT NULL COMMENT 'X1',
  `Y1` float DEFAULT NULL COMMENT 'Y1',
  `X2` float DEFAULT NULL COMMENT 'X2',
  `Y2` float DEFAULT NULL COMMENT 'Y2',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `BorderWidth` float DEFAULT NULL COMMENT '宽度',
  `BorderColor` varchar(30) DEFAULT NULL COMMENT '颜色',
  `GUID` varchar(128) DEFAULT NULL COMMENT '初始的GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmline`
--

LOCK TABLES `sys_frmline` WRITE;
/*!40000 ALTER TABLE `sys_frmline` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmlink`
--

DROP TABLE IF EXISTS `sys_frmlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmlink` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `Text` varchar(500) DEFAULT NULL COMMENT 'Label',
  `URL` varchar(500) DEFAULT NULL COMMENT 'URL',
  `Target` varchar(20) DEFAULT NULL COMMENT 'Target',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT '12' COMMENT 'FontSize',
  `FontColor` varchar(50) DEFAULT NULL COMMENT 'FontColor',
  `FontName` varchar(50) DEFAULT NULL COMMENT 'FontName',
  `FontStyle` varchar(50) DEFAULT NULL COMMENT 'FontStyle',
  `IsBold` int(11) DEFAULT '0' COMMENT 'IsBold',
  `IsItalic` int(11) DEFAULT '0' COMMENT 'IsItalic',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` varchar(50) DEFAULT '0' COMMENT '显示的分组',
  `GroupIDText` varchar(50) DEFAULT '0' COMMENT '显示的分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmlink`
--

LOCK TABLES `sys_frmlink` WRITE;
/*!40000 ALTER TABLE `sys_frmlink` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmrb`
--

DROP TABLE IF EXISTS `sys_frmrb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmrb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(30) DEFAULT NULL COMMENT '字段',
  `EnumKey` varchar(30) DEFAULT NULL COMMENT '枚举值',
  `Lab` varchar(90) DEFAULT NULL COMMENT '标签',
  `IntKey` int(11) DEFAULT '0' COMMENT 'IntKey',
  `UIIsEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `Script` text COMMENT '要执行的脚本',
  `FieldsCfg` text COMMENT '配置信息@FieldName=Sta',
  `SetVal` varchar(200) DEFAULT NULL COMMENT '设置的值',
  `Tip` varchar(1000) DEFAULT NULL COMMENT '选择后提示的信息',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(1000) DEFAULT '',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmrb`
--

LOCK TABLES `sys_frmrb` WRITE;
/*!40000 ALTER TABLE `sys_frmrb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmrb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmreportfield`
--

DROP TABLE IF EXISTS `sys_frmreportfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmreportfield` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单编号',
  `KeyOfEn` varchar(100) DEFAULT NULL COMMENT '字段名',
  `Name` varchar(200) DEFAULT NULL COMMENT '显示中文名',
  `UIWidth` varchar(100) DEFAULT NULL COMMENT '宽度',
  `UIVisible` int(11) DEFAULT '1' COMMENT '是否显示',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmreportfield`
--

LOCK TABLES `sys_frmreportfield` WRITE;
/*!40000 ALTER TABLE `sys_frmreportfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmreportfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmrpt`
--

DROP TABLE IF EXISTS `sys_frmrpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmrpt` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '描述',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `PTable` varchar(30) DEFAULT NULL COMMENT '物理表',
  `SQLOfColumn` varchar(300) DEFAULT NULL COMMENT '列的数据源',
  `SQLOfRow` varchar(300) DEFAULT NULL COMMENT '行数据源',
  `RowIdx` int(11) DEFAULT '99' COMMENT '位置',
  `GroupID` int(11) DEFAULT '0' COMMENT 'GroupID',
  `IsShowSum` int(11) DEFAULT '1' COMMENT 'IsShowSum',
  `IsShowIdx` int(11) DEFAULT '1' COMMENT 'IsShowIdx',
  `IsCopyNDData` int(11) DEFAULT '1' COMMENT 'IsCopyNDData',
  `IsHLDtl` int(11) DEFAULT '0' COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT '0' COMMENT 'IsReadonly',
  `IsShowTitle` int(11) DEFAULT '1' COMMENT 'IsShowTitle',
  `IsView` int(11) DEFAULT '1' COMMENT '是否可见',
  `IsExp` int(11) DEFAULT '1' COMMENT 'IsExp',
  `IsImp` int(11) DEFAULT '1' COMMENT 'IsImp',
  `IsInsert` int(11) DEFAULT '1' COMMENT 'IsInsert',
  `IsDelete` int(11) DEFAULT '1' COMMENT 'IsDelete',
  `IsUpdate` int(11) DEFAULT '1' COMMENT 'IsUpdate',
  `IsEnablePass` int(11) DEFAULT '0' COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT '0' COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT '0' COMMENT 'WhenOverSize',
  `DtlOpenType` int(11) DEFAULT '1' COMMENT '数据开放类型',
  `EditModel` int(11) DEFAULT '0' COMMENT '显示格式',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `FrmW` float DEFAULT NULL COMMENT 'FrmW',
  `FrmH` float DEFAULT NULL COMMENT 'FrmH',
  `MTR` varchar(3000) DEFAULT NULL COMMENT '多表头列',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmrpt`
--

LOCK TABLES `sys_frmrpt` WRITE;
/*!40000 ALTER TABLE `sys_frmrpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmrpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmsln`
--

DROP TABLE IF EXISTS `sys_frmsln`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_frmsln` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) DEFAULT NULL COMMENT '字段名',
  `EleType` varchar(20) DEFAULT NULL COMMENT '类型',
  `UIIsEnable` int(11) DEFAULT '1' COMMENT '是否可用',
  `UIVisible` int(11) DEFAULT '1' COMMENT '是否可见',
  `IsSigan` int(11) DEFAULT '0' COMMENT '是否签名',
  `IsNotNull` int(11) DEFAULT '0' COMMENT '是否为空',
  `RegularExp` varchar(500) DEFAULT NULL COMMENT '正则表达式',
  `IsWriteToFlowTable` int(11) DEFAULT '0' COMMENT '是否写入流程表',
  `IsWriteToGenerWorkFlow` int(11) DEFAULT '0' COMMENT '是否写入流程注册表',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmsln`
--

LOCK TABLES `sys_frmsln` WRITE;
/*!40000 ALTER TABLE `sys_frmsln` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmsln` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_glovar`
--

DROP TABLE IF EXISTS `sys_glovar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_glovar` (
  `No` varchar(50) NOT NULL COMMENT '键',
  `Name` varchar(120) DEFAULT NULL COMMENT '名称',
  `Val` text COMMENT '值',
  `GroupKey` varchar(120) DEFAULT NULL COMMENT '分组值',
  `Note` text COMMENT '说明',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_glovar`
--

LOCK TABLES `sys_glovar` WRITE;
/*!40000 ALTER TABLE `sys_glovar` DISABLE KEYS */;
INSERT INTO `sys_glovar` VALUES ('0','选择系统约定默认值',NULL,'DefVal',NULL),('@CurrWorker','当前工作可处理人员',NULL,'DefVal',NULL),('@FK_ND','当前年度',NULL,'DefVal',NULL),('@FK_YF','当前月份',NULL,'DefVal',NULL),('@WebUser.FK_Dept','登陆人员部门编号',NULL,'DefVal',NULL),('@WebUser.FK_DeptFullName','登陆人员部门全称',NULL,'DefVal',NULL),('@WebUser.FK_DeptName','登陆人员部门名称',NULL,'DefVal',NULL),('@WebUser.Name','登陆人员名称',NULL,'DefVal',NULL),('@WebUser.No','登陆人员账号',NULL,'DefVal',NULL),('@yyyy年MM月dd日','当前日期(yyyy年MM月dd日)',NULL,'DefVal',NULL),('@yyyy年MM月dd日HH时mm分','当前日期(yyyy年MM月dd日HH时mm分)',NULL,'DefVal',NULL),('@yy年MM月dd日','当前日期(yy年MM月dd日)',NULL,'DefVal',NULL),('@yy年MM月dd日HH时mm分','当前日期(yy年MM月dd日HH时mm分)',NULL,'DefVal',NULL),('DTSTodoStaPM2018-12-03','时效调度 WFTodoSta PM 调度','DTSTodoStaPM2018-12-03','WF','时效调度PMDTSTodoStaPM2018-12-03'),('DTSTodoStaPM2019-08-31','时效调度 WFTodoSta PM 调度','DTSTodoStaPM2019-08-31','WF','时效调度PMDTSTodoStaPM2019-08-31');
/*!40000 ALTER TABLE `sys_glovar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_groupenstemplate`
--

DROP TABLE IF EXISTS `sys_groupenstemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_groupenstemplate` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `EnName` varchar(500) DEFAULT NULL COMMENT '表称',
  `Name` varchar(500) DEFAULT NULL COMMENT '报表名',
  `EnsName` varchar(90) DEFAULT NULL COMMENT '报表类名',
  `OperateCol` varchar(90) DEFAULT NULL COMMENT '操作属性',
  `Attrs` varchar(90) DEFAULT NULL COMMENT '运算属性',
  `Rec` varchar(90) DEFAULT NULL COMMENT '记录人',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_groupenstemplate`
--

LOCK TABLES `sys_groupenstemplate` WRITE;
/*!40000 ALTER TABLE `sys_groupenstemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_groupenstemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_groupfield`
--

DROP TABLE IF EXISTS `sys_groupfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_groupfield` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Lab` varchar(500) DEFAULT NULL COMMENT '标签',
  `FrmID` varchar(200) DEFAULT NULL COMMENT '表单ID',
  `CtrlType` varchar(50) DEFAULT NULL COMMENT '控件类型',
  `CtrlID` varchar(500) DEFAULT NULL COMMENT '控件ID',
  `Idx` int(11) DEFAULT '99' COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_groupfield`
--

LOCK TABLES `sys_groupfield` WRITE;
/*!40000 ALTER TABLE `sys_groupfield` DISABLE KEYS */;
INSERT INTO `sys_groupfield` VALUES (1228,'请假流程-经典模式报表','ND1MyRpt','','',1,'',''),(1258,'请假流程报表','ND2MyRpt','','',1,'',''),(1287,'请假流程报表','ND4MyRpt','','',1,'','');
/*!40000 ALTER TABLE `sys_groupfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_m2m`
--

DROP TABLE IF EXISTS `sys_m2m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_m2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `M2MNo` varchar(20) DEFAULT NULL COMMENT 'M2MNo',
  `EnOID` int(11) DEFAULT NULL COMMENT '实体OID',
  `DtlObj` varchar(20) DEFAULT NULL COMMENT 'DtlObj(对于m2mm有效)',
  `Doc` text COMMENT '内容',
  `ValsName` text COMMENT 'ValsName',
  `ValsSQL` text COMMENT 'ValsSQL',
  `NumSelected` int(11) DEFAULT NULL COMMENT '选择数',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='M2M数据存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_m2m`
--

LOCK TABLES `sys_m2m` WRITE;
/*!40000 ALTER TABLE `sys_m2m` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_m2m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapattr`
--

DROP TABLE IF EXISTS `sys_mapattr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_mapattr` (
  `MyPK` varchar(200) NOT NULL DEFAULT '',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体标识',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '属性',
  `Name` varchar(200) DEFAULT NULL COMMENT '描述',
  `DefVal` varchar(4000) DEFAULT NULL,
  `UIContralType` int(11) DEFAULT '0' COMMENT '控件',
  `MyDataType` int(11) DEFAULT '1' COMMENT '数据类型',
  `LGType` int(11) DEFAULT '0' COMMENT '逻辑类型',
  `UIWidth` float DEFAULT NULL COMMENT '宽度',
  `UIHeight` float DEFAULT NULL COMMENT '高度',
  `MinLen` int(11) DEFAULT '0' COMMENT '最小长度',
  `MaxLen` int(11) DEFAULT '300' COMMENT '最大长度',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '绑定的信息',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '绑定的Key',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '绑定的Text',
  `UIVisible` int(11) DEFAULT '1' COMMENT '是否可见',
  `UIIsEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `UIIsLine` int(11) DEFAULT '0' COMMENT '是否单独栏显示',
  `UIIsInput` int(11) DEFAULT '0' COMMENT '是否必填字段',
  `IsRichText` int(11) DEFAULT '0' COMMENT '富文本',
  `IsSupperText` int(11) DEFAULT '0' COMMENT '富文本',
  `FontSize` int(11) DEFAULT '0' COMMENT '富文本',
  `IsSigan` int(11) DEFAULT '0' COMMENT '签字？',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag` varchar(100) DEFAULT NULL COMMENT '标识（存放临时数据）',
  `EditType` int(11) DEFAULT '0' COMMENT '编辑类型',
  `Tip` varchar(800) DEFAULT NULL,
  `ColSpan` int(11) DEFAULT '1' COMMENT '单元格数量',
  `GroupID` varchar(100) DEFAULT NULL COMMENT '显示的分组',
  `IsEnableInAPP` int(11) DEFAULT '1' COMMENT '是否在移动端中显示',
  `Idx` int(11) DEFAULT '0' COMMENT '序号',
  `AtPara` text COMMENT 'AtPara',
  `DefValText` varchar(50) DEFAULT '0' COMMENT '默认值（选中）',
  `RBShowModel` int(11) DEFAULT '0' COMMENT '单选按钮的展现方式',
  `IsEnableJS` int(11) DEFAULT '0' COMMENT '是否启用JS高级设置？',
  `GroupIDText` varchar(500) DEFAULT NULL,
  `ExtDefVal` varchar(50) DEFAULT '0' COMMENT '系统默认值',
  `ExtDefValText` varchar(500) DEFAULT NULL,
  `ExtIsSum` int(11) DEFAULT '0' COMMENT '是否显示合计(对从表有效)',
  `Tag1` varchar(100) DEFAULT '',
  `Tag2` varchar(100) DEFAULT '',
  `Tag3` varchar(100) DEFAULT '',
  `TextColSpan` int(11) DEFAULT '1',
  `RowSpan` int(11) DEFAULT '1',
  `ExtRows` float(50,2) DEFAULT '1.00',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapattr`
--

LOCK TABLES `sys_mapattr` WRITE;
/*!40000 ALTER TABLE `sys_mapattr` DISABLE KEYS */;
INSERT INTO `sys_mapattr` VALUES ('MyDeptRole_OID','MyDeptRole','OID','OID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'1234',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('MyDeptRole_RDT','MyDeptRole','RDT','更新时间','@RDT',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'1234',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_CDT','ND105','CDT','完成时间','@RDT',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_Emps','ND105','Emps','Emps','',0,1,0,100,23,0,400,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_FID','ND105','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_FK_Dept','ND105','FK_Dept','隶属部门','',0,1,0,100,23,0,100,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_OID','ND105','OID','WorkID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_RDT','ND105','RDT','接受时间','',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND105_Rec','ND105','Rec','记录人','@WebUser.No',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_CDT','ND12204','CDT','完成时间','@RDT',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_Emps','ND12204','Emps','Emps','',0,1,0,100,23,0,400,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_FID','ND12204','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_FK_Dept','ND12204','FK_Dept','操作员部门','',0,1,0,100,23,0,100,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_OID','ND12204','OID','WorkID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_RDT','ND12204','RDT','接受时间','',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND12204_Rec','ND12204','Rec','记录人','@WebUser.No',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_CDT','ND13099','CDT','完成时间','@RDT',0,7,0,145,23,0,500,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_Emps','ND13099','Emps','Emps','',0,1,0,100,23,0,400,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_FID','ND13099','FID','FID','0',0,2,0,100,23,0,500,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_FK_Dept','ND13099','FK_Dept','隶属部门','',0,1,0,100,23,0,100,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_OID','ND13099','OID','WorkID','0',0,2,0,100,23,0,500,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_RDT','ND13099','RDT','接受时间','',0,7,0,145,23,0,500,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND13099_Rec','ND13099','Rec','记录人','@WebUser.No',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FK_Dept','ND1MyRpt','FK_Dept','操作员部门','',1,1,2,100,23,0,100,'BP.Port.Depts','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1228',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FK_NY','ND1MyRpt','FK_NY','年月','',1,1,2,100,23,0,7,'BP.Pub.NYs','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1228',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowDaySpan','ND1MyRpt','FlowDaySpan','跨度(天)','',0,8,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',1,'',1,'1226',1,-101,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowEmps','ND1MyRpt','FlowEmps','参与人','',0,1,0,100,23,0,1000,'','','',1,0,1,0,0,0,0,0,5,5,'','',1,'',1,'1226',1,-100,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowEnder','ND1MyRpt','FlowEnder','结束人','',1,1,2,100,23,0,20,'BP.Port.Emps','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1226',1,-1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowEnderRDT','ND1MyRpt','FlowEnderRDT','结束时间','',0,7,0,145,23,0,300,'','','',1,0,0,0,0,1,0,0,5,5,'','',1,'',1,'1226',1,-101,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowEndNode','ND1MyRpt','FlowEndNode','结束节点','0',0,2,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1226',1,-101,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowStarter','ND1MyRpt','FlowStarter','发起人','',1,1,2,100,23,0,20,'BP.Port.Emps','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1226',1,-1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_FlowStartRDT','ND1MyRpt','FlowStartRDT','发起时间','',0,7,0,145,23,0,300,'','','',1,0,0,0,0,1,0,0,5,5,'','',1,'',1,'1226',1,-101,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_OID','ND1MyRpt','OID','WorkID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'1224',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_Title','ND1MyRpt','Title','标题','',0,1,0,251,23,0,200,'','','',0,0,1,0,0,0,0,0,174.83,54.4,'','',0,'',1,'1220',1,-100,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_WFSta','ND1MyRpt','WFSta','状态','',1,2,1,100,23,0,1000,'WFSta','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1224',1,-1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND1MyRpt_WFState','ND1MyRpt','WFState','流程状态','',1,2,1,100,23,0,1000,'WFState','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1226',1,-1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_CDT','ND20505','CDT','完成时间','@RDT',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_Emps','ND20505','Emps','Emps','',0,1,0,100,23,0,400,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_FID','ND20505','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_FK_Dept','ND20505','FK_Dept','隶属部门','',0,1,0,100,23,0,100,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_OID','ND20505','OID','WorkID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_RDT','ND20505','RDT','接受时间','',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND20505_Rec','ND20505','Rec','记录人','@WebUser.No',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND2MyRpt_FK_Dept','ND2MyRpt','FK_Dept','操作员部门','',1,1,2,100,23,0,100,'BP.Port.Depts','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1258',1,999,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FK_NY','ND2MyRpt','FK_NY','年月','',1,1,2,100,23,0,7,'BP.Pub.NYs','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1258',1,999,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowDaySpan','ND2MyRpt','FlowDaySpan','跨度(天)','',0,8,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowEmps','ND2MyRpt','FlowEmps','参与人','',0,1,0,100,23,0,1000,'','','',1,0,1,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-100,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowEnder','ND2MyRpt','FlowEnder','结束人','',1,1,2,100,23,0,20,'BP.Port.Emps','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowEnderRDT','ND2MyRpt','FlowEnderRDT','结束时间','',0,7,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowEndNode','ND2MyRpt','FlowEndNode','结束节点','0',0,2,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowStarter','ND2MyRpt','FlowStarter','发起人','',1,1,2,100,23,0,20,'BP.Port.Emps','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_FlowStartRDT','ND2MyRpt','FlowStartRDT','发起时间','',0,7,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_OID','ND2MyRpt','OID','OID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'1254',1,999,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_Title','ND2MyRpt','Title','标题','',0,1,0,251,23,0,200,'','','',0,0,1,0,0,0,0,0,174.83,54.4,'','',0,'',1,'1246',1,-100,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_WFSta','ND2MyRpt','WFSta','状态','',1,2,1,100,23,0,1000,'WFSta','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1254',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND2MyRpt_WFState','ND2MyRpt','WFState','流程状态','',1,2,1,100,23,0,1000,'WFState','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1256',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4502Dtl1_ChanPinMingCheng','ND4502Dtl1','ChanPinMingCheng','产品名称','',0,1,0,100,23,0,300,'0','','',1,1,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,6,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_FID','ND4502Dtl1','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_OID','ND4502Dtl1','OID','主键','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,2,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_RDT','ND4502Dtl1','RDT','记录时间','',0,7,0,145,23,0,20,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,3,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_Rec','ND4502Dtl1','Rec','记录人','@WebUser.No',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','@WebUser.No',2,'',1,'0',1,4,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_RefPK','ND4502Dtl1','RefPK','关联ID','0',0,1,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,5,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_TianXieRen','ND4502Dtl1','TianXieRen','填写人','@WebUser.Name',0,1,0,100,23,0,300,'0','','',0,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_XiaoJi','ND4502Dtl1','XiaoJi','小计','0.00',0,8,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,9,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_XiaoShouJiaGe','ND4502Dtl1','XiaoShouJiaGe','销售价格','0.00',0,8,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,7,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4502Dtl1_XiaoShouShuLiang','ND4502Dtl1','XiaoShouShuLiang','销售数量','0',0,2,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,8,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_ChanPinMingCheng','ND4503Dtl1','ChanPinMingCheng','产品名称','',0,1,0,100,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,6,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_FID','ND4503Dtl1','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_OID','ND4503Dtl1','OID','主键','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,2,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_RDT','ND4503Dtl1','RDT','记录时间','',0,7,0,145,23,0,20,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,3,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_Rec','ND4503Dtl1','Rec','记录人','',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','@WebUser.No',2,'',1,'0',1,4,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_RefPK','ND4503Dtl1','RefPK','关联ID','0',0,1,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,5,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_TianXieRen','ND4503Dtl1','TianXieRen','填写人','',0,1,0,80,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'224',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_XiaoJi','ND4503Dtl1','XiaoJi','小计','0.00',0,8,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,9,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_XiaoShouJiaGe','ND4503Dtl1','XiaoShouJiaGe','销售价格','0.00',0,8,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,7,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_XiaoShouJingLiBeiZhu','ND4503Dtl1','XiaoShouJingLiBeiZhu','销售经理备注','',0,1,0,100,23,0,300,'0','','',1,1,0,0,0,0,0,0,5,5,'','',0,'',1,'224',1,10,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4503Dtl1_XiaoShouShuLiang','ND4503Dtl1','XiaoShouShuLiang','销售数量','0',0,2,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,8,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_ChanPinMingCheng','ND4504Dtl1','ChanPinMingCheng','产品名称','',0,1,0,100,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,6,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_FID','ND4504Dtl1','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_OID','ND4504Dtl1','OID','主键','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,2,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_RDT','ND4504Dtl1','RDT','记录时间','',0,7,0,145,23,0,20,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,3,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_Rec','ND4504Dtl1','Rec','记录人','',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','@WebUser.No',2,'',1,'0',1,4,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_RefPK','ND4504Dtl1','RefPK','关联ID','0',0,1,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,5,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_TianXieRen','ND4504Dtl1','TianXieRen','填写人','',0,1,0,80,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'227',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_XiaoJi','ND4504Dtl1','XiaoJi','小计','0.00',0,8,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,9,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_XiaoShouJiaGe','ND4504Dtl1','XiaoShouJiaGe','销售价格','0.00',0,8,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',0,'',1,'227',1,7,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_XiaoShouJingLiBeiZhu','ND4504Dtl1','XiaoShouJingLiBeiZhu','销售经理备注','',0,1,0,100,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'224',1,10,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4504Dtl1_XiaoShouShuLiang','ND4504Dtl1','XiaoShouShuLiang','销售数量','0',0,2,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',0,'',1,'227',1,8,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_ChanPinMingCheng','ND4599Dtl1','ChanPinMingCheng','产品名称','',0,1,0,100,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,6,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_FID','ND4599Dtl1','FID','FID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,1,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_OID','ND4599Dtl1','OID','主键','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,2,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_RDT','ND4599Dtl1','RDT','记录时间','',0,7,0,145,23,0,20,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'0',1,3,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_Rec','ND4599Dtl1','Rec','记录人','',0,1,0,100,23,0,20,'','','',0,0,0,0,0,0,0,0,5,5,'','@WebUser.No',2,'',1,'0',1,4,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_RefPK','ND4599Dtl1','RefPK','关联ID','0',0,1,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'0',1,5,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_TianXieRen','ND4599Dtl1','TianXieRen','填写人','',0,1,0,100,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'230',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_XiaoJi','ND4599Dtl1','XiaoJi','小计','0.00',0,8,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,9,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_XiaoShouJiaGe','ND4599Dtl1','XiaoShouJiaGe','销售价格','0.00',0,8,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,7,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_XiaoShouJingLiBeiZhu','ND4599Dtl1','XiaoShouJingLiBeiZhu','销售经理备注','',0,1,0,100,23,0,300,'0','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'224',1,10,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4599Dtl1_XiaoShouShuLiang','ND4599Dtl1','XiaoShouShuLiang','销售数量','0',0,2,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',0,'',1,'221',1,8,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('ND4MyRpt_FK_Dept','ND4MyRpt','FK_Dept','操作员部门','',1,1,2,100,23,0,100,'BP.Port.Depts','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1287',1,999,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FK_NY','ND4MyRpt','FK_NY','年月','',1,1,2,100,23,0,7,'BP.Pub.NYs','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'0',1,999,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowDaySpan','ND4MyRpt','FlowDaySpan','跨度(天)','',0,8,0,100,23,0,300,'','','',1,1,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowEmps','ND4MyRpt','FlowEmps','参与人','',0,1,0,100,23,0,1000,'','','',1,0,1,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-100,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowEnder','ND4MyRpt','FlowEnder','结束人','',1,1,2,100,23,0,20,'BP.Port.Emps','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowEnderRDT','ND4MyRpt','FlowEnderRDT','结束时间','',0,7,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowEndNode','ND4MyRpt','FlowEndNode','结束节点','0',0,2,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowStarter','ND4MyRpt','FlowStarter','发起人','',1,1,2,100,23,0,20,'BP.Port.Emps','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_FlowStartRDT','ND4MyRpt','FlowStartRDT','发起时间','',0,7,0,100,23,0,300,'','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-101,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_OID','ND4MyRpt','OID','OID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'1281',1,999,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_Title','ND4MyRpt','Title','标题','',0,1,0,251,23,0,200,'','','',0,0,1,0,0,0,0,0,174.83,54.4,'','',0,'',1,'1274',1,-100,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_WFSta','ND4MyRpt','WFSta','状态','',1,2,1,100,23,0,1000,'WFSta','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1281',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('ND4MyRpt_WFState','ND4MyRpt','WFState','流程状态','',1,2,1,100,23,0,1000,'WFState','','',1,0,0,0,0,0,0,0,5,5,'','',1,'',1,'1282',1,-1,'','0',0,0,NULL,'0',NULL,0,'','','',1,1,1.00),('_OID','','OID','OID','0',0,2,0,100,23,0,300,'','','',0,0,0,0,0,0,0,0,5,5,'','',2,'',1,'1218',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00),('_RDT','','RDT','更新时间','@RDT',0,7,0,145,23,0,300,'','','',0,0,0,0,0,1,0,0,5,5,'','1',1,'',1,'1218',1,999,'','0',0,0,'0','0','0',0,'','','',1,1,1.00);
/*!40000 ALTER TABLE `sys_mapattr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapdata`
--

DROP TABLE IF EXISTS `sys_mapdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_mapdata` (
  `No` varchar(200) NOT NULL COMMENT '表单编号',
  `Name` varchar(500) DEFAULT NULL COMMENT '表单名称',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `PTable` varchar(500) DEFAULT NULL,
  `IsTemplate` int(11) DEFAULT '0' COMMENT '是否是表单模版',
  `AtPara` text COMMENT 'AtPara',
  `FormEventEntity` varchar(100) DEFAULT '' COMMENT '事件实体',
  `EnPK` varchar(200) DEFAULT '' COMMENT '实体主键',
  `PTableModel` int(11) DEFAULT '0' COMMENT '表存储模式',
  `Url` varchar(500) DEFAULT '' COMMENT '连接(对嵌入式表单有效)',
  `Dtls` varchar(500) DEFAULT '' COMMENT '从表',
  `FrmW` int(11) DEFAULT '900' COMMENT 'FrmW',
  `FrmH` int(11) DEFAULT '1200' COMMENT 'FrmH',
  `TableCol` int(11) DEFAULT '4' COMMENT '傻瓜表单显示的列',
  `Tag` varchar(500) DEFAULT '' COMMENT 'Tag',
  `FK_FrmSort` varchar(500) DEFAULT '' COMMENT '表单类别',
  `FK_FormTree` varchar(500) DEFAULT '' COMMENT '表单树类别',
  `FrmType` int(11) DEFAULT '1' COMMENT '表单类型',
  `AppType` int(11) DEFAULT '0' COMMENT '应用类型',
  `DBSrc` varchar(100) DEFAULT 'local' COMMENT '数据源',
  `BodyAttr` varchar(100) DEFAULT '' COMMENT '表单Body属性',
  `Note` varchar(4000) DEFAULT NULL,
  `Designer` varchar(500) DEFAULT '' COMMENT '设计者',
  `DesignerUnit` varchar(500) DEFAULT '' COMMENT '单位',
  `DesignerContact` varchar(500) DEFAULT '' COMMENT '联系方式',
  `Idx` int(11) DEFAULT '100' COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT '' COMMENT 'GUID',
  `Ver` varchar(30) DEFAULT '' COMMENT '版本号',
  `FlowCtrls` varchar(200) DEFAULT '' COMMENT '流程控件',
  `FK_Flow` varchar(50) DEFAULT '' COMMENT '独立表单属性:FK_Flow',
  `DBURL` int(11) DEFAULT '0' COMMENT 'DBURL',
  `MyFileName` varchar(300) DEFAULT '' COMMENT '表单模版',
  `MyFilePath` varchar(300) DEFAULT '' COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT '' COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT '' COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float(11,2) DEFAULT '0.00' COMMENT 'MyFileSize',
  `RightViewWay` int(11) DEFAULT '0' COMMENT '报表查看权限控制方式',
  `RightViewTag` text COMMENT '报表查看权限控制Tag',
  `RightDeptWay` int(11) DEFAULT '0' COMMENT '部门数据查看控制方式',
  `RightDeptTag` text COMMENT '部门数据查看控制Tag',
  `TemplaterVer` varchar(30) DEFAULT '' COMMENT '模版编号',
  `DBSave` varchar(50) DEFAULT '' COMMENT 'Excel数据文件存储',
  `TableWidth` int(11) DEFAULT '800',
  `TableHeight` int(11) DEFAULT '900',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapdata`
--

LOCK TABLES `sys_mapdata` WRITE;
/*!40000 ALTER TABLE `sys_mapdata` DISABLE KEYS */;
INSERT INTO `sys_mapdata` VALUES ('ND1MyRpt','请假流程-经典模式报表',NULL,0,NULL,0,NULL,1,NULL,0,NULL,0,NULL,0,1,NULL,0,NULL,0,NULL,0,0,0,NULL,0,1,NULL,1,0,NULL,'ND1Rpt',0,'@IsHaveCA=0','','',0,'','',900,1200,0,'','','',0,0,'local','','默认.','','','',100,'','2019-08-31 11:50:11','','001',0,'','','','',0,0,0.00,0,'',0,'','','',800,900),('ND2MyRpt','请假流程报表',NULL,0,NULL,0,NULL,1,NULL,0,NULL,0,NULL,0,1,NULL,0,NULL,0,NULL,0,0,0,NULL,0,1,NULL,1,0,NULL,'ND2Rpt',0,'@IsHaveCA=0','','',0,'','',900,1200,0,'','','',0,0,'local','','默认.','','','',100,'','2019-08-31 19:00:55','','002',0,'','','','',0,0,0.00,0,'',0,'','','',800,900),('ND4MyRpt','请假流程报表',NULL,0,NULL,0,NULL,1,NULL,0,NULL,0,NULL,0,1,NULL,0,NULL,0,NULL,0,0,0,NULL,0,1,NULL,1,0,NULL,'ND4Rpt',0,'@IsHaveCA=0','','',0,'','',900,1200,0,'','','',0,0,'local','','默认.','','','',100,'','2019-09-02 16:21:41','','004',0,'','','','',0,0,0.00,0,'',0,'','','',800,900);
/*!40000 ALTER TABLE `sys_mapdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapdtl`
--

DROP TABLE IF EXISTS `sys_mapdtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_mapdtl` (
  `No` varchar(100) NOT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '描述',
  `Alias` varchar(200) DEFAULT NULL COMMENT '别名',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `PTable` varchar(200) DEFAULT NULL COMMENT '物理表',
  `GroupField` varchar(300) DEFAULT NULL COMMENT '分组字段',
  `RefPK` varchar(100) DEFAULT NULL COMMENT '关联的主键',
  `FEBD` varchar(100) DEFAULT NULL COMMENT '映射的事件实体类',
  `Model` int(11) DEFAULT '0' COMMENT '工作模式',
  `RowsOfList` int(11) DEFAULT '6' COMMENT '初始化行数',
  `IsEnableGroupField` int(11) DEFAULT '0' COMMENT '是否启用分组字段',
  `IsShowSum` int(11) DEFAULT '1' COMMENT '是否显示合计？',
  `IsShowIdx` int(11) DEFAULT '1' COMMENT '是否显示序号？',
  `IsCopyNDData` int(11) DEFAULT '1' COMMENT '是否允许Copy数据',
  `IsHLDtl` int(11) DEFAULT '0' COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT '0' COMMENT '是否只读？',
  `IsShowTitle` int(11) DEFAULT '1' COMMENT '是否显示标题？',
  `IsView` int(11) DEFAULT '1' COMMENT '是否可见',
  `IsInsert` int(11) DEFAULT '1' COMMENT '是否可以插入行？',
  `IsDelete` int(11) DEFAULT '1' COMMENT '是否可以删除行',
  `IsUpdate` int(11) DEFAULT '1' COMMENT '是否可以更新？',
  `IsEnablePass` int(11) DEFAULT '0' COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT '0' COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT '0' COMMENT '列表数据显示格式',
  `DtlOpenType` int(11) DEFAULT '1' COMMENT '数据开放类型',
  `ListShowModel` int(11) DEFAULT '0' COMMENT '列表数据显示格式',
  `EditModel` int(11) DEFAULT '0' COMMENT '行数据显示格式',
  `X` float DEFAULT NULL COMMENT '距左',
  `Y` float DEFAULT NULL COMMENT '距上',
  `H` float DEFAULT NULL COMMENT '高度',
  `W` float DEFAULT NULL COMMENT '宽度',
  `FrmW` float DEFAULT NULL COMMENT '表单宽度',
  `FrmH` float DEFAULT NULL COMMENT '表单高度',
  `MTR` varchar(4000) DEFAULT NULL,
  `FilterSQLExp` varchar(200) DEFAULT NULL COMMENT '过滤SQL表达式',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点(用户独立表单权限控制)',
  `ShowCols` varchar(500) DEFAULT NULL COMMENT '显示的列',
  `IsExp` int(11) DEFAULT '1' COMMENT 'IsExp',
  `ImpModel` int(11) DEFAULT '0' COMMENT '导入规则',
  `ImpSQLSearch` varchar(4000) DEFAULT NULL,
  `ImpSQLInit` varchar(4000) DEFAULT NULL,
  `ImpSQLFullOneRow` varchar(4000) DEFAULT NULL,
  `ImpSQLNames` varchar(900) DEFAULT NULL COMMENT '字段中文名',
  `ColAutoExp` varchar(200) DEFAULT NULL COMMENT '列自动计算表达式',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(300) DEFAULT NULL COMMENT 'AtPara',
  `IsEnableLink` int(11) DEFAULT '0' COMMENT '是否启用超链接',
  `LinkLabel` varchar(50) DEFAULT '' COMMENT '超连接标签',
  `LinkTarget` varchar(10) DEFAULT '' COMMENT '连接目标',
  `LinkUrl` varchar(200) DEFAULT '' COMMENT '连接URL',
  `SubThreadWorker` varchar(50) DEFAULT '' COMMENT '子线程处理人字段',
  `SubThreadWorkerText` varchar(50) DEFAULT '' COMMENT '子线程处理人字段',
  `ImpSQLFull` varchar(500) DEFAULT '',
  `PTableModel` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapdtl`
--

LOCK TABLES `sys_mapdtl` WRITE;
/*!40000 ALTER TABLE `sys_mapdtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapdtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapext`
--

DROP TABLE IF EXISTS `sys_mapext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_mapext` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `ExtType` varchar(30) DEFAULT NULL COMMENT '类型',
  `DoWay` int(11) DEFAULT '0' COMMENT '执行方式',
  `AttrOfOper` varchar(30) DEFAULT NULL COMMENT '操作的Attr',
  `AttrsOfActive` varchar(900) DEFAULT NULL COMMENT '激活的字段',
  `Doc` text COMMENT '内容',
  `Tag` varchar(2000) DEFAULT NULL COMMENT 'Tag',
  `Tag1` varchar(2000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) DEFAULT NULL COMMENT 'Tag4',
  `H` int(11) DEFAULT '500' COMMENT '高度',
  `W` int(11) DEFAULT '400' COMMENT '宽度',
  `DBType` int(11) DEFAULT '0' COMMENT '数据类型',
  `FK_DBSrc` varchar(100) DEFAULT NULL COMMENT '数据源',
  `PRI` int(11) DEFAULT '0' COMMENT 'PRI',
  `AtPara` text COMMENT '参数',
  `DBSrc` varchar(20) DEFAULT '',
  `Tag5` varchar(2000) DEFAULT '',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapext`
--

LOCK TABLES `sys_mapext` WRITE;
/*!40000 ALTER TABLE `sys_mapext` DISABLE KEYS */;
INSERT INTO `sys_mapext` VALUES ('AutoFull_ND4502Dtl1_XiaoJi','ND4502Dtl1','AutoFull',0,'XiaoJi','','@销售价格*@销售数量','1','','','','',500,400,0,'',0,'','',''),('AutoFull_ND4503Dtl1_XiaoJi','ND4503Dtl1','AutoFull',0,'XiaoJi','','@销售价格*@销售数量','1','','','','',500,400,0,'',0,'','',''),('AutoFull_ND4504Dtl1_XiaoJi','ND4504Dtl1','AutoFull',0,'XiaoJi','','@销售价格*@销售数量','1','','','','',500,400,0,'',0,'','',''),('AutoFull_ND4599Dtl1_XiaoJi','ND4599Dtl1','AutoFull',0,'XiaoJi','','@销售价格*@销售数量','1','','','','',500,400,0,'',0,'','','');
/*!40000 ALTER TABLE `sys_mapext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapframe`
--

DROP TABLE IF EXISTS `sys_mapframe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_mapframe` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `URL` varchar(3000) DEFAULT NULL COMMENT 'URL',
  `UrlSrcType` int(11) DEFAULT '0' COMMENT 'URL来源',
  `FrmID` varchar(50) DEFAULT NULL COMMENT '表单表单',
  `FrmIDText` varchar(50) DEFAULT NULL COMMENT '表单表单',
  `Y` varchar(20) DEFAULT NULL COMMENT 'Y',
  `X` varchar(20) DEFAULT NULL COMMENT 'x',
  `W` varchar(20) DEFAULT NULL COMMENT '宽度',
  `H` varchar(20) DEFAULT NULL COMMENT '高度',
  `IsAutoSize` int(11) DEFAULT '1' COMMENT '是否自动设置大小',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapframe`
--

LOCK TABLES `sys_mapframe` WRITE;
/*!40000 ALTER TABLE `sys_mapframe` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapframe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapm2m`
--

DROP TABLE IF EXISTS `sys_mapm2m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_mapm2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `NoOfObj` varchar(20) DEFAULT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `DBOfLists` text COMMENT '列表数据源(对一对多对多模式有效）',
  `DBOfObjs` text COMMENT 'DBOfObjs',
  `DBOfGroups` text COMMENT 'DBOfGroups',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `ShowWay` int(11) DEFAULT NULL COMMENT '显示方式',
  `M2MType` int(11) DEFAULT NULL COMMENT '类型',
  `RowIdx` int(11) DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) DEFAULT NULL COMMENT '分组ID',
  `Cols` int(11) DEFAULT NULL COMMENT '记录呈现列数',
  `IsDelete` int(11) DEFAULT NULL COMMENT '可删除否',
  `IsInsert` int(11) DEFAULT NULL COMMENT '可插入否',
  `IsCheckAll` int(11) DEFAULT NULL COMMENT '是否显示选择全部',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多选';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapm2m`
--

LOCK TABLES `sys_mapm2m` WRITE;
/*!40000 ALTER TABLE `sys_mapm2m` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapm2m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rptdept`
--

DROP TABLE IF EXISTS `sys_rptdept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_rptdept` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  PRIMARY KEY (`FK_Rpt`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rptdept`
--

LOCK TABLES `sys_rptdept` WRITE;
/*!40000 ALTER TABLE `sys_rptdept` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rptdept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rptemp`
--

DROP TABLE IF EXISTS `sys_rptemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_rptemp` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员',
  PRIMARY KEY (`FK_Rpt`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rptemp`
--

LOCK TABLES `sys_rptemp` WRITE;
/*!40000 ALTER TABLE `sys_rptemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rptemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rptstation`
--

DROP TABLE IF EXISTS `sys_rptstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_rptstation` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位',
  PRIMARY KEY (`FK_Rpt`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rptstation`
--

LOCK TABLES `sys_rptstation` WRITE;
/*!40000 ALTER TABLE `sys_rptstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rptstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rpttemplate`
--

DROP TABLE IF EXISTS `sys_rpttemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_rpttemplate` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `EnsName` varchar(500) DEFAULT NULL COMMENT '类名',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT '操作员',
  `D1` varchar(90) DEFAULT NULL COMMENT 'D1',
  `D2` varchar(90) DEFAULT NULL COMMENT 'D2',
  `D3` varchar(90) DEFAULT NULL COMMENT 'D3',
  `AlObjs` varchar(90) DEFAULT NULL COMMENT '要分析的对象',
  `Height` int(11) DEFAULT '600' COMMENT 'Height',
  `Width` int(11) DEFAULT '800' COMMENT 'Width',
  `IsSumBig` int(11) DEFAULT '0' COMMENT '是否显示大合计',
  `IsSumLittle` int(11) DEFAULT '0' COMMENT '是否显示小合计',
  `IsSumRight` int(11) DEFAULT '0' COMMENT '是否显示右合计',
  `PercentModel` int(11) DEFAULT '0' COMMENT '比率显示方式',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rpttemplate`
--

LOCK TABLES `sys_rpttemplate` WRITE;
/*!40000 ALTER TABLE `sys_rpttemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rpttemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_serial`
--

DROP TABLE IF EXISTS `sys_serial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_serial` (
  `CfgKey` varchar(100) NOT NULL COMMENT 'CfgKey',
  `IntVal` int(11) DEFAULT '0' COMMENT '属性',
  PRIMARY KEY (`CfgKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_serial`
--

LOCK TABLES `sys_serial` WRITE;
/*!40000 ALTER TABLE `sys_serial` DISABLE KEYS */;
INSERT INTO `sys_serial` VALUES ('BP.WF.Template.FlowSort',105),('Demo_Resume',279),('OID',1287),('Template',100),('UpdataCCFlowVer',831115239),('Ver',20190621),('WorkID',148);
/*!40000 ALTER TABLE `sys_serial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sfdbsrc`
--

DROP TABLE IF EXISTS `sys_sfdbsrc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_sfdbsrc` (
  `No` varchar(20) NOT NULL COMMENT '数据源编号(必须是英文)',
  `Name` varchar(30) DEFAULT NULL COMMENT '数据源名称',
  `DBSrcType` int(11) DEFAULT '0' COMMENT '数据源类型',
  `UserID` varchar(30) DEFAULT NULL COMMENT '数据库登录用户ID',
  `Password` varchar(30) DEFAULT NULL COMMENT '数据库登录用户密码',
  `IP` varchar(500) DEFAULT NULL COMMENT 'IP地址/数据库实例名',
  `DBName` varchar(30) DEFAULT NULL COMMENT '数据库名称/Oracle保持为空',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sfdbsrc`
--

LOCK TABLES `sys_sfdbsrc` WRITE;
/*!40000 ALTER TABLE `sys_sfdbsrc` DISABLE KEYS */;
INSERT INTO `sys_sfdbsrc` VALUES ('local','本机数据源(默认)',0,'','','','');
/*!40000 ALTER TABLE `sys_sfdbsrc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sftable`
--

DROP TABLE IF EXISTS `sys_sftable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_sftable` (
  `No` varchar(200) NOT NULL COMMENT '表英文名称',
  `Name` varchar(200) DEFAULT NULL COMMENT '表中文名称',
  `SrcType` int(11) DEFAULT '0' COMMENT '数据表类型',
  `CodeStruct` int(11) DEFAULT '0' COMMENT '字典表类型',
  `RootVal` varchar(200) DEFAULT NULL COMMENT '根节点值',
  `FK_Val` varchar(200) DEFAULT NULL COMMENT '默认创建的字段名',
  `TableDesc` varchar(200) DEFAULT NULL COMMENT '表描述',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  `FK_SFDBSrc` varchar(100) DEFAULT NULL COMMENT '数据源',
  `SrcTable` varchar(200) DEFAULT NULL COMMENT '数据源表',
  `ColumnValue` varchar(200) DEFAULT NULL COMMENT '显示的值(编号列)',
  `ColumnText` varchar(200) DEFAULT NULL COMMENT '显示的文字(名称列)',
  `ParentValue` varchar(200) DEFAULT NULL COMMENT '父级值(父级列)',
  `SelectStatement` varchar(1000) DEFAULT NULL COMMENT '查询语句',
  `RDT` varchar(50) DEFAULT NULL COMMENT '加入日期',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sftable`
--

LOCK TABLES `sys_sftable` WRITE;
/*!40000 ALTER TABLE `sys_sftable` DISABLE KEYS */;
INSERT INTO `sys_sftable` VALUES ('BP.Port.Depts','部门',0,0,NULL,'FK_Dept','部门','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Port.Emps','人员',0,0,NULL,'FK_Emp','系统中的操作员','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Port.Stations','岗位',0,0,NULL,'FK_Station','工作岗位','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Pub.Days','日',0,0,NULL,'FK_Day','1-31日','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Pub.NYs','年月',0,0,NULL,'FK_NY','年度与月份','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Pub.YFs','月',0,0,NULL,'FK_YF','1-12月','','local',NULL,NULL,NULL,NULL,NULL,NULL),('CN_City','城市',1,0,NULL,'FK_City','中国的市级城市','','local',NULL,NULL,NULL,NULL,NULL,NULL),('CN_PQ','地区',1,0,NULL,'FK_DQ','华北、西北、西南。。。','','local',NULL,NULL,NULL,NULL,NULL,NULL),('CN_SF','省份',1,0,NULL,'FK_SF','中国的省份。','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Demo_BanJi','班级',1,0,NULL,'FK_BJ','班级','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Demo_Student','学生',1,0,NULL,'FK_Student','学生','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Port_Dept','部门',1,0,NULL,'FK_Dept','部门','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Port_Emp','操作员',1,0,NULL,'FK_Emp','操作员','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Port_Station','岗位',1,0,NULL,'FK_Station','岗位','','local',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_sftable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sms`
--

DROP TABLE IF EXISTS `sys_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_sms` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人(可以为空)',
  `SendTo` varchar(200) DEFAULT NULL COMMENT '发送给(可以为空)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '写入时间',
  `Mobile` varchar(30) DEFAULT NULL COMMENT '手机号(可以为空)',
  `MobileSta` int(11) DEFAULT '0' COMMENT '消息状态',
  `MobileInfo` varchar(1000) DEFAULT NULL COMMENT '短信信息',
  `Email` varchar(200) DEFAULT NULL COMMENT 'Email(可以为空)',
  `EmailSta` int(11) DEFAULT '0' COMMENT 'EmaiSta消息状态',
  `EmailTitle` varchar(3000) DEFAULT NULL COMMENT '标题',
  `EmailDoc` text COMMENT '内容',
  `SendDT` varchar(50) DEFAULT NULL COMMENT '发送时间',
  `IsRead` int(11) DEFAULT '0' COMMENT '是否读取?',
  `IsAlert` int(11) DEFAULT '0' COMMENT '是否提示?',
  `MsgFlag` varchar(200) DEFAULT NULL COMMENT '消息标记(用于防止发送重复)',
  `MsgType` varchar(200) DEFAULT NULL COMMENT '消息类型(CC抄送,Todolist待办,Return退回,Etc其他消息...)',
  `AtPara` varchar(500) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sms`
--

LOCK TABLES `sys_sms` WRITE;
/*!40000 ALTER TABLE `sys_sms` DISABLE KEYS */;
INSERT INTO `sys_sms` VALUES ('789ee172c3674a60b9072f5e29ca696f','admin','system','2019-09-02 17:08','',0,'有新工作{山东公司-admin,系统管理员在2019-09-02 17:08发起.}需要您处理, 发送人:admin, 系统管理员,打开http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=system_148_201_2019-09-02 .','',3,'','','',0,0,'WKAlt202_148','SendSuccess','@FK_Flow=002@WorkID=148@FK_Node=201@OpenUrl=http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=system_148_201_2019-09-02'),('ab17a36421c241a983d63624af381d1e','admin','admin','2019-09-02 16:22','',0,'有新工作{山东公司-admin,系统管理员在2019-09-02 16:21发起.}需要您处理, 发送人:admin, 系统管理员,打开http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_147_401_2019-09-02 .','',3,'','','',0,0,'WKAlt402_147','SendSuccess','@FK_Flow=004@WorkID=147@FK_Node=401@OpenUrl=http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_147_401_2019-09-02'),('e569a5036a234a36ad1f29bbff9f3236','admin','admin','2019-09-02 16:20','',0,'有新工作{山东公司-admin,系统管理员在2019-09-02 16:20发起.}需要您处理, 发送人:admin, 系统管理员,打开http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_145_201_2019-09-02 .','',3,'','','',0,0,'WKAlt202_145','SendSuccess','@FK_Flow=002@WorkID=145@FK_Node=201@OpenUrl=http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_145_201_2019-09-02');
/*!40000 ALTER TABLE `sys_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_userlogt`
--

DROP TABLE IF EXISTS `sys_userlogt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_userlogt` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '用户',
  `IP` varchar(200) DEFAULT NULL COMMENT 'IP',
  `LogFlag` varchar(300) DEFAULT NULL COMMENT '标识',
  `Docs` varchar(300) DEFAULT NULL COMMENT '说明',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_userlogt`
--

LOCK TABLES `sys_userlogt` WRITE;
/*!40000 ALTER TABLE `sys_userlogt` DISABLE KEYS */;
INSERT INTO `sys_userlogt` VALUES ('01f0903801e544fc8650f8a646bffe65','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:46:10'),('03c3eb4d6b6141d4bd59f35928a66721','admin','127.0.0.1','SignIn','登录','2019-03-09 15:06:40'),('04524ba37d2f4473baf7a7bf9f2051c8','admin','127.0.0.1','SignIn','登录','2019-03-09 15:01:41'),('04c1a561faa54b388123f35f300ce978','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:53:11'),('06f592c31fdb48df8179b9f10c4f88d2','admin','127.0.0.1','SignIn','登录','2019-03-09 11:41:19'),('0c26445f12674d24951417a33efd4c07','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:28:34'),('11b0b37a903042f19e94acba1086797f','admin','127.0.0.1','SignIn','登录','2019-03-09 11:34:21'),('150c7d779ebb4e398efa006d192f2b67','admin','127.0.0.1','SignIn','登录','2019-03-09 14:58:50'),('17dd3802a13e4bed97615ff0f842a8db','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 10:04:45'),('1d64d05ba3e64b8b8460f407d978df4c','admin','127.0.0.1','SignIn','登录','2019-03-09 14:58:45'),('1ee926bf669d48ab95cba5c6f6882122','admin','127.0.0.1','SignIn','登录','2019-03-08 17:12:56'),('23758487e19c4a51ad3252cde46eff4e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:58:54'),('36f275d1b6714a028af3b8aaca1d5577','admin','127.0.0.1','SignIn','登录','2019-03-09 14:59:20'),('3882d8f7d30a49debc2a170468724e5a','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:58:50'),('3b2cdb725e3e4f00ab3a71528686e97a','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:14:45'),('444abe42db0a439d90dd06d2d614e2bc','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:45:58'),('44e3a62dc11348f29aed3fdd95776efa','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:24:23'),('4b8195327ff14f14bc511b8676a9e5e5','admin','127.0.0.1','SignIn','登录','2019-03-09 15:07:10'),('4c7a342188b9489c8f877c9b8d86ec25','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:37:48'),('54314c0454da404f9a6bdac360a10c70','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:03:05'),('595698e9f6a549c9b44815ab5b01532d','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:37:40'),('5c0654d913884927869015d6c11c500e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:18:07'),('5c46a54655384464a6a8ca9a3e576d1b','admin','127.0.0.1','SignIn','登录','2019-03-09 11:35:30'),('674d908f356744aaa6cd82ccb9fda053','admin','127.0.0.1','SignIn','登录','2019-03-09 14:59:09'),('6b1afbd17d474f93a2d94d95a0a5d02a','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 10:25:46'),('6eada14d4e1f4b93a9772f02bc1fe61e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:53:04'),('7256c7cff28e412ab053fd0a671cbfb7','admin','127.0.0.1','SignIn','登录','2019-03-09 11:40:38'),('72d1aa282f0d4f00aa8a2578e5df0ab5','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:24:19'),('7ae0bd7c23f94fdba9830e272921bb9e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 10:02:17'),('7cd9c503e8384166b21d01733dea34cf','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 12:08:01'),('7d7a35c8a0ef4c3baa27a5a710c93dc9','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:52:59'),('805db20566344b4fafef0e460686d00d','admin','127.0.0.1','SignIn','登录','2019-03-09 15:05:28'),('80757d84036c4d7892bb34d4aadb7890','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:28:27'),('81608043e48144819227e9681dd9fa3b','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:19:20'),('83fc9ad75e144f648fdc4df314ff499a','admin','127.0.0.1','SignIn','登录','2019-03-09 11:34:11'),('85014537ddba4945b1d79d9945924ee8','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:19:25'),('87ee680279d34d1b8c29ce3e66922cc7','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 10:01:18'),('8aa14819fb1845adb13a4be648a82366','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 10:03:30'),('8dd332978e564f4591e96d14c511232e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:18:48'),('8f2f8f7ae3664d508b5174f378d59cf3','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:46:04'),('913352d11a5f43cca6aa972b76b3e2c8','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:19:51'),('9405d6771e434476b408802682359267','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:37:36'),('9ed527fecb9647b0bad2c4e9911e19b8','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:28:55'),('a06f7c5550d844c0a31f90f997557a76','admin','127.0.0.1','SignIn','登录','2019-03-08 17:12:46'),('a151631be2e945a089917b9267b2219d','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 10:01:30'),('a40ac683ce394e358bdd5cac077dfa60','admin','127.0.0.1','SignIn','登录','2019-03-09 15:05:24'),('a80e37a9252a4ff4b6db183345ceae10','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 12:07:52'),('b403a0504e124394a693ce421e9713a9','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:02:48'),('b7c05d605c4b4bda89b099eaa91acaac','admin','127.0.0.1','SignIn','登录','2019-03-09 15:01:44'),('b817ed1c2cec4450843bb4483e78e5ff','admin','127.0.0.1','SignIn','登录','2019-03-09 15:07:01'),('c4ffed0c83fd49e59fabb3fbb96a3f5f','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:55:46'),('c87f04cb1160486c85038151d279d68a','admin','127.0.0.1','SignIn','登录','2019-03-09 11:40:31'),('cb2f6b6223de424fb494c3629f56ced9','admin','127.0.0.1','SignIn','登录','2019-03-08 17:15:55'),('eae6d5023b5147dfb3580f5609935403','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:33:23'),('edcdad9112064125a0141cb9b7b16429','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:56:04'),('ef4d9bd3c957485b8ec7a73632829e01','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:18:11'),('f1778414c419482682a5a6581f713acc','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:33:19'),('f57c3cf698f74d01a06b1e8c9fd76987','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:59:00'),('fabcd1863e2044dfba8eceb16649386e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:02:43'),('faf06dad3dff4166a3ee8763892d7279','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:55:42'),('fb04a19e6c9149a29cbdaa490f1f9c05','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 14:03:36'),('fedf6a50b1674495bf130bf1fb118d59','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:33:29'),('ff31d84b51874480b51540ba9f70940e','admin','0:0:0:0:0:0:0:1','SignIn','登录','2019-03-09 13:14:58');
/*!40000 ALTER TABLE `sys_userlogt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_userregedit`
--

DROP TABLE IF EXISTS `sys_userregedit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_userregedit` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '工作人员',
  `Attrs` text COMMENT '属性s',
  `CfgKey` varchar(200) DEFAULT '' COMMENT '键',
  `Vals` varchar(2000) DEFAULT '' COMMENT '值',
  `GenerSQL` varchar(2000) DEFAULT '' COMMENT 'GenerSQL',
  `Paras` varchar(2000) DEFAULT '' COMMENT 'Paras',
  `NumKey` varchar(300) DEFAULT '' COMMENT '分析的Key',
  `OrderBy` varchar(300) DEFAULT '' COMMENT 'OrderBy',
  `OrderWay` varchar(300) DEFAULT '' COMMENT 'OrderWay',
  `SearchKey` varchar(300) DEFAULT '' COMMENT 'SearchKey',
  `MVals` varchar(2000) DEFAULT '' COMMENT 'MVals',
  `IsPic` int(11) DEFAULT '0' COMMENT '是否图片',
  `DTFrom` varchar(20) DEFAULT '' COMMENT '查询时间从',
  `DTTo` varchar(20) DEFAULT '' COMMENT '到',
  `AtPara` text COMMENT 'AtPara',
  `FK_MapData` varchar(100) DEFAULT '' COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT '' COMMENT '节点对应字段',
  `LB` int(11) DEFAULT '0' COMMENT '类别',
  `CurValue` text COMMENT '文本',
  `ContrastKey` varchar(20) DEFAULT '' COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT '' COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT '' COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT '' COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT '' COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT '1' COMMENT '求什么?SumAvg',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_userregedit`
--

LOCK TABLES `sys_userregedit` WRITE;
/*!40000 ALTER TABLE `sys_userregedit` DISABLE KEYS */;
INSERT INTO `sys_userregedit` VALUES ('adminND1RptAdminer_SearchAttrs',NULL,'admin',NULL,'ND1RptAdminer_SearchAttrs','','','','','','','','',0,'','','','','',0,NULL,'','','','','',1),('adminND1RptMyDept_SearchAttrs',NULL,'admin',NULL,'ND1RptMyDept_SearchAttrs','','','','','','','','',0,'','','','','',0,NULL,'','','','','',1),('adminND1RptMyJoin_SearchAttrs',NULL,'admin',NULL,'ND1RptMyJoin_SearchAttrs','','','','','','','','',0,'','','','','',0,NULL,'','','','','',1),('admin_BP.GPM.Emps_SearchAttrs',NULL,'',NULL,'','','','','','','','','',0,'','','@RecCount=25','','',0,NULL,'','','','','',1),('admin_BP.GPM.StationTypes_SearchAttrs',NULL,'',NULL,'','','','','','','','','',0,'','','@RecCount=4','','',0,NULL,'','','','','',1),('admin_BP.WF.Data.Delays_SearchAttrs',NULL,'',NULL,'','','','','','','','','',0,'','','@RecCount=0','','',0,NULL,'','','','','',1),('admin_BP.WF.Data.MyJoinFlows_SearchAttrs',NULL,'',NULL,'','','','','','','','','',0,'','','@RecCount=0','','',0,NULL,'','','','','',1),('admin_BP.WF.Data.MyStartFlows_SearchAttrs',NULL,'',NULL,'','','','','','','','','',0,'','','@RecCount=0','','',0,NULL,'','','','','',1);
/*!40000 ALTER TABLE `sys_userregedit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_wfsealdata`
--

DROP TABLE IF EXISTS `sys_wfsealdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_wfsealdata` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `OID` varchar(200) DEFAULT NULL COMMENT 'OID',
  `FK_Node` varchar(200) DEFAULT NULL COMMENT 'FK_Node',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `SealData` text COMMENT 'SealData',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_wfsealdata`
--

LOCK TABLES `sys_wfsealdata` WRITE;
/*!40000 ALTER TABLE `sys_wfsealdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_wfsealdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_flowstarter`
--

DROP TABLE IF EXISTS `v_flowstarter`;
/*!50001 DROP VIEW IF EXISTS `v_flowstarter`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flowstarter` AS SELECT 
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `FK_Emp`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flowstarterbpm`
--

DROP TABLE IF EXISTS `v_flowstarterbpm`;
/*!50001 DROP VIEW IF EXISTS `v_flowstarterbpm`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flowstarterbpm` AS SELECT 
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `FK_Emp`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_totalch`
--

DROP TABLE IF EXISTS `v_totalch`;
/*!50001 DROP VIEW IF EXISTS `v_totalch`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_totalch` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `AllNum`,
 1 AS `ASNum`,
 1 AS `CSNum`,
 1 AS `JiShi`,
 1 AS `ANQI`,
 1 AS `YuQi`,
 1 AS `ChaoQi`,
 1 AS `WCRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_totalchweek`
--

DROP TABLE IF EXISTS `v_totalchweek`;
/*!50001 DROP VIEW IF EXISTS `v_totalchweek`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_totalchweek` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `WeekNum`,
 1 AS `FK_NY`,
 1 AS `AllNum`,
 1 AS `ASNum`,
 1 AS `CSNum`,
 1 AS `JiShi`,
 1 AS `AnQi`,
 1 AS `YuQi`,
 1 AS `ChaoQi`,
 1 AS `WCRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_totalchyf`
--

DROP TABLE IF EXISTS `v_totalchyf`;
/*!50001 DROP VIEW IF EXISTS `v_totalchyf`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_totalchyf` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `FK_NY`,
 1 AS `AllNum`,
 1 AS `ASNum`,
 1 AS `CSNum`,
 1 AS `JiShi`,
 1 AS `AnQi`,
 1 AS `YuQi`,
 1 AS `ChaoQi`,
 1 AS `WCRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_wf_delay`
--

DROP TABLE IF EXISTS `v_wf_delay`;
/*!50001 DROP VIEW IF EXISTS `v_wf_delay`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_wf_delay` AS SELECT 
 1 AS `MyPK`,
 1 AS `PRI`,
 1 AS `WorkID`,
 1 AS `IsRead`,
 1 AS `Starter`,
 1 AS `StarterName`,
 1 AS `WFState`,
 1 AS `FK_Dept`,
 1 AS `DeptName`,
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `PWorkID`,
 1 AS `PFlowNo`,
 1 AS `FK_Node`,
 1 AS `NodeName`,
 1 AS `WorkerDept`,
 1 AS `Title`,
 1 AS `RDT`,
 1 AS `ADT`,
 1 AS `SDT`,
 1 AS `FK_Emp`,
 1 AS `FID`,
 1 AS `FK_FlowSort`,
 1 AS `SysType`,
 1 AS `SDTOfNode`,
 1 AS `PressTimes`,
 1 AS `GuestNo`,
 1 AS `GuestName`,
 1 AS `BillNo`,
 1 AS `FlowNote`,
 1 AS `TodoEmps`,
 1 AS `TodoEmpsNum`,
 1 AS `TodoSta`,
 1 AS `TaskSta`,
 1 AS `ListType`,
 1 AS `Sender`,
 1 AS `AtPara`,
 1 AS `MyNum`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wf_accepterrole`
--

DROP TABLE IF EXISTS `wf_accepterrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_accepterrole` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Name` varchar(200) DEFAULT NULL,
  `FK_Node` varchar(100) DEFAULT NULL COMMENT '节点',
  `FK_Mode` int(11) DEFAULT '0' COMMENT '模式类型',
  `Tag0` varchar(999) DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(999) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(999) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(999) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(999) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(999) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_accepterrole`
--

LOCK TABLES `wf_accepterrole` WRITE;
/*!40000 ALTER TABLE `wf_accepterrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_accepterrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_bill`
--

DROP TABLE IF EXISTS `wf_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_bill` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程',
  `FK_BillType` varchar(300) DEFAULT NULL COMMENT '单据类型',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Starter` varchar(50) DEFAULT NULL COMMENT '发起人',
  `StartDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `Url` varchar(2000) DEFAULT NULL COMMENT 'Url',
  `FullPath` varchar(2000) DEFAULT NULL COMMENT 'FullPath',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '打印人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '打印时间',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '隶属部门',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '隶属年月',
  `Emps` text COMMENT 'Emps',
  `FK_Node` varchar(30) DEFAULT NULL COMMENT '节点',
  `FK_Bill` varchar(500) DEFAULT NULL COMMENT 'FK_Bill',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_bill`
--

LOCK TABLES `wf_bill` WRITE;
/*!40000 ALTER TABLE `wf_bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_billtemplate`
--

DROP TABLE IF EXISTS `wf_billtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_billtemplate` (
  `No` varchar(190) NOT NULL COMMENT 'No',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `TempFilePath` varchar(200) DEFAULT NULL COMMENT '模板路径',
  `NodeID` int(11) DEFAULT '0' COMMENT 'NodeID',
  `BillFileType` int(11) DEFAULT '0' COMMENT '生成的文件类型',
  `BillOpenModel` int(11) DEFAULT '0' COMMENT '生成的文件打开方式',
  `QRModel` int(11) DEFAULT '0' COMMENT '二维码生成方式',
  `FK_BillType` varchar(4) DEFAULT NULL COMMENT '单据类型',
  `IDX` varchar(200) DEFAULT NULL COMMENT 'IDX',
  `ExpField` varchar(800) DEFAULT NULL COMMENT '要排除的字段',
  `ReplaceVal` varchar(3000) DEFAULT NULL COMMENT '要替换的值',
  `FK_MapData` varchar(300) DEFAULT '',
  `TemplateFileModel` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_billtemplate`
--

LOCK TABLES `wf_billtemplate` WRITE;
/*!40000 ALTER TABLE `wf_billtemplate` DISABLE KEYS */;
INSERT INTO `wf_billtemplate` VALUES ('100','Jflow (5)','Jflow (5).doc',301,0,0,0,'','','','','ND301',1);
/*!40000 ALTER TABLE `wf_billtemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_billtype`
--

DROP TABLE IF EXISTS `wf_billtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_billtype` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程',
  `IDX` int(11) DEFAULT '0' COMMENT 'IDX',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_billtype`
--

LOCK TABLES `wf_billtype` WRITE;
/*!40000 ALTER TABLE `wf_billtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_billtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ccdept`
--

DROP TABLE IF EXISTS `wf_ccdept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_ccdept` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ccdept`
--

LOCK TABLES `wf_ccdept` WRITE;
/*!40000 ALTER TABLE `wf_ccdept` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ccdept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ccemp`
--

DROP TABLE IF EXISTS `wf_ccemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_ccemp` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ccemp`
--

LOCK TABLES `wf_ccemp` WRITE;
/*!40000 ALTER TABLE `wf_ccemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ccemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_cclist`
--

DROP TABLE IF EXISTS `wf_cclist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_cclist` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `Sta` int(11) DEFAULT '0' COMMENT '状态',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `NodeName` varchar(500) DEFAULT NULL COMMENT '节点名称',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `Doc` text COMMENT '内容',
  `Rec` varchar(50) DEFAULT NULL COMMENT '抄送人员',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `CCTo` varchar(50) DEFAULT NULL COMMENT '抄送给',
  `CCToName` varchar(50) DEFAULT NULL COMMENT '抄送给(人员名称)',
  `CCToDept` varchar(50) DEFAULT NULL COMMENT '抄送到部门',
  `CCToDeptName` varchar(600) DEFAULT NULL COMMENT '抄送给部门名称',
  `CDT` varchar(50) DEFAULT NULL COMMENT '打开时间',
  `PFlowNo` varchar(100) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT '0' COMMENT '父流程WorkID',
  `InEmpWorks` int(11) DEFAULT '0' COMMENT '是否加入待办列表',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_cclist`
--

LOCK TABLES `wf_cclist` WRITE;
/*!40000 ALTER TABLE `wf_cclist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_cclist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ccstation`
--

DROP TABLE IF EXISTS `wf_ccstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_ccstation` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ccstation`
--

LOCK TABLES `wf_ccstation` WRITE;
/*!40000 ALTER TABLE `wf_ccstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ccstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ch`
--

DROP TABLE IF EXISTS `wf_ch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_ch` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(100) DEFAULT NULL,
  `FK_FlowT` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_NodeT` varchar(200) DEFAULT NULL COMMENT '节点名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `SenderT` varchar(200) DEFAULT NULL COMMENT '发送人名称',
  `FK_Emp` varchar(100) DEFAULT NULL,
  `FK_EmpT` varchar(200) DEFAULT NULL COMMENT '当事人名称',
  `GroupEmps` varchar(400) DEFAULT NULL COMMENT '相关当事人',
  `GroupEmpsNames` varchar(900) DEFAULT NULL COMMENT '相关当事人名称',
  `GroupEmpsNum` int(11) DEFAULT '1' COMMENT '相关当事人数量',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '任务下达时间',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '任务处理时间',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `FK_Dept` varchar(100) DEFAULT NULL,
  `FK_DeptT` varchar(500) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(100) DEFAULT NULL,
  `DTSWay` int(11) DEFAULT '0' COMMENT '考核方式',
  `TimeLimit` varchar(50) DEFAULT NULL COMMENT '规定限期',
  `OverMinutes` float DEFAULT NULL COMMENT '逾期分钟',
  `UseDays` float DEFAULT NULL COMMENT '实际使用天',
  `OverDays` float DEFAULT NULL COMMENT '逾期天',
  `CHSta` int(11) DEFAULT '0' COMMENT '状态',
  `WeekNum` int(11) DEFAULT '0' COMMENT '第几周',
  `Points` float DEFAULT NULL COMMENT '总扣分',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `UseMinutes` float(50,2) DEFAULT '0.00',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ch`
--

LOCK TABLES `wf_ch` WRITE;
/*!40000 ALTER TABLE `wf_ch` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_cheval`
--

DROP TABLE IF EXISTS `wf_cheval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_cheval` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(7) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT '0' COMMENT '评价节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `Rec` varchar(50) DEFAULT NULL COMMENT '评价人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '评价人名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '评价日期',
  `EvalEmpNo` varchar(50) DEFAULT NULL COMMENT '被考核的人员编号',
  `EvalEmpName` varchar(50) DEFAULT NULL COMMENT '被考核的人员名称',
  `EvalCent` varchar(20) DEFAULT NULL COMMENT '评价分值',
  `EvalNote` varchar(20) DEFAULT NULL COMMENT '评价内容',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(7) DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_cheval`
--

LOCK TABLES `wf_cheval` WRITE;
/*!40000 ALTER TABLE `wf_cheval` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_cheval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_cond`
--

DROP TABLE IF EXISTS `wf_cond`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_cond` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `CondType` int(11) DEFAULT '0' COMMENT '条件类型',
  `DataFrom` int(11) DEFAULT '0' COMMENT '条件数据来源0表单,1岗位(对方向条件有效)',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `NodeID` int(11) DEFAULT '0' COMMENT '发生的事件MainNode',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性',
  `AttrKey` varchar(60) DEFAULT NULL COMMENT '属性键',
  `AttrName` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` text COMMENT '要运算的值',
  `OperatorValueT` text COMMENT '要运算的值T',
  `ToNodeID` int(11) DEFAULT '0' COMMENT 'ToNodeID（对方向条件有效）',
  `ConnJudgeWay` int(11) DEFAULT '0' COMMENT '条件关系',
  `MyPOID` int(11) DEFAULT '0' COMMENT 'MyPOID',
  `PRI` int(11) DEFAULT '0' COMMENT '计算优先级',
  `CondOrAnd` int(11) DEFAULT '0' COMMENT '方向条件类型',
  `Note` varchar(500) DEFAULT NULL COMMENT '备注',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_cond`
--

LOCK TABLES `wf_cond` WRITE;
/*!40000 ALTER TABLE `wf_cond` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_cond` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_dataapply`
--

DROP TABLE IF EXISTS `wf_dataapply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_dataapply` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `NodeId` int(11) DEFAULT NULL COMMENT 'NodeId',
  `RunState` int(11) DEFAULT NULL COMMENT '运行状态0,没有提交，1，提交申请执行审批中，2，审核完毕。',
  `ApplyDays` int(11) DEFAULT NULL COMMENT '申请天数',
  `ApplyData` varchar(50) DEFAULT NULL COMMENT '申请日期',
  `Applyer` varchar(100) DEFAULT NULL COMMENT '申请人,外键:对应物理表:Port_Emp,表描述:用户',
  `ApplyNote1` text COMMENT '申请原因',
  `ApplyNote2` text COMMENT '申请备注',
  `Checker` varchar(100) DEFAULT NULL COMMENT '审批人,外键:对应物理表:Port_Emp,表描述:用户',
  `CheckerData` varchar(50) DEFAULT NULL COMMENT '审批日期',
  `CheckerDays` int(11) DEFAULT NULL COMMENT '批准天数',
  `CheckerNote1` text COMMENT '审批意见',
  `CheckerNote2` text COMMENT '审批备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='追加时间申请';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_dataapply`
--

LOCK TABLES `wf_dataapply` WRITE;
/*!40000 ALTER TABLE `wf_dataapply` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_dataapply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_deptflowsearch`
--

DROP TABLE IF EXISTS `wf_deptflowsearch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_deptflowsearch` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程编号',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_deptflowsearch`
--

LOCK TABLES `wf_deptflowsearch` WRITE;
/*!40000 ALTER TABLE `wf_deptflowsearch` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_deptflowsearch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_direction`
--

DROP TABLE IF EXISTS `wf_direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_direction` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(10) DEFAULT NULL COMMENT '流程',
  `Node` int(11) DEFAULT '0' COMMENT '从节点',
  `ToNode` int(11) DEFAULT '0' COMMENT '到节点',
  `IsCanBack` int(11) DEFAULT '0' COMMENT '是否可以原路返回(对后退线有效)',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_direction`
--

LOCK TABLES `wf_direction` WRITE;
/*!40000 ALTER TABLE `wf_direction` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_direction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_directionstation`
--

DROP TABLE IF EXISTS `wf_directionstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_directionstation` (
  `FK_Direction` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Direction`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_directionstation`
--

LOCK TABLES `wf_directionstation` WRITE;
/*!40000 ALTER TABLE `wf_directionstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_directionstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_emp`
--

DROP TABLE IF EXISTS `wf_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_emp` (
  `No` varchar(50) NOT NULL COMMENT '帐号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '主部门',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织',
  `UseSta` int(11) DEFAULT '3' COMMENT '用户状态',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `UserType` int(11) DEFAULT '3' COMMENT '用户状态',
  `RootOfFlow` varchar(100) DEFAULT '' COMMENT '流程权限节点',
  `RootOfForm` varchar(100) DEFAULT '' COMMENT '表单权限节点',
  `RootOfDept` varchar(100) DEFAULT '' COMMENT '组织结构权限节点',
  `Tel` varchar(50) DEFAULT '' COMMENT 'Tel',
  `Email` varchar(50) DEFAULT '' COMMENT 'Email',
  `TM` varchar(50) DEFAULT '' COMMENT '即时通讯号',
  `AlertWay` int(11) DEFAULT '3' COMMENT '收听方式',
  `Author` varchar(50) DEFAULT '' COMMENT '授权人',
  `AuthorDate` varchar(50) DEFAULT '' COMMENT '授权日期',
  `AuthorWay` int(11) DEFAULT '0' COMMENT '授权方式',
  `AuthorToDate` varchar(50) DEFAULT NULL,
  `AuthorFlows` text COMMENT '可以执行的授权流程',
  `Stas` varchar(3000) DEFAULT '' COMMENT '岗位s',
  `Depts` varchar(100) DEFAULT '' COMMENT 'Deptss',
  `FtpUrl` varchar(50) DEFAULT '' COMMENT 'FtpUrl',
  `Msg` text COMMENT 'Msg',
  `Style` text COMMENT 'Style',
  `StartFlows` longtext,
  `SPass` varchar(200) DEFAULT '' COMMENT '图片签名密码',
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `AtPara` text,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_emp`
--

LOCK TABLES `wf_emp` WRITE;
/*!40000 ALTER TABLE `wf_emp` DISABLE KEYS */;
INSERT INTO `wf_emp` VALUES ('admin','系统管理员','SD',NULL,1,1,3,'','','','82374939-601','admin@ccflow.org','',3,'','',0,'','','','','','','','','',0,NULL);
/*!40000 ALTER TABLE `wf_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `wf_empworks`
--

DROP TABLE IF EXISTS `wf_empworks`;
/*!50001 DROP VIEW IF EXISTS `wf_empworks`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `wf_empworks` AS SELECT 
 1 AS `PRI`,
 1 AS `WorkID`,
 1 AS `IsRead`,
 1 AS `Starter`,
 1 AS `StarterName`,
 1 AS `WFState`,
 1 AS `FK_Dept`,
 1 AS `DeptName`,
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `PWorkID`,
 1 AS `PFlowNo`,
 1 AS `FK_Node`,
 1 AS `NodeName`,
 1 AS `WorkerDept`,
 1 AS `Title`,
 1 AS `RDT`,
 1 AS `ADT`,
 1 AS `SDT`,
 1 AS `FK_Emp`,
 1 AS `FID`,
 1 AS `FK_FlowSort`,
 1 AS `SysType`,
 1 AS `SDTOfNode`,
 1 AS `PressTimes`,
 1 AS `GuestNo`,
 1 AS `GuestName`,
 1 AS `BillNo`,
 1 AS `FlowNote`,
 1 AS `TodoEmps`,
 1 AS `TodoEmpsNum`,
 1 AS `TodoSta`,
 1 AS `TaskSta`,
 1 AS `ListType`,
 1 AS `Sender`,
 1 AS `AtPara`,
 1 AS `MyNum`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wf_findworkerrole`
--

DROP TABLE IF EXISTS `wf_findworkerrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_findworkerrole` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `SortVal0` varchar(200) DEFAULT NULL COMMENT 'SortVal0',
  `SortText0` varchar(200) DEFAULT NULL COMMENT 'SortText0',
  `SortVal1` varchar(200) DEFAULT NULL COMMENT 'SortVal1',
  `SortText1` varchar(200) DEFAULT NULL COMMENT 'SortText1',
  `SortVal2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortText2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortVal3` varchar(200) DEFAULT NULL COMMENT 'SortVal3',
  `SortText3` varchar(200) DEFAULT NULL COMMENT 'SortText3',
  `TagVal0` varchar(1000) DEFAULT NULL COMMENT 'TagVal0',
  `TagVal1` varchar(1000) DEFAULT NULL COMMENT 'TagVal1',
  `TagVal2` varchar(1000) DEFAULT NULL COMMENT 'TagVal2',
  `TagVal3` varchar(1000) DEFAULT NULL COMMENT 'TagVal3',
  `TagText0` varchar(1000) DEFAULT NULL COMMENT 'TagText0',
  `TagText1` varchar(1000) DEFAULT NULL COMMENT 'TagText1',
  `TagText2` varchar(1000) DEFAULT NULL COMMENT 'TagText2',
  `TagText3` varchar(1000) DEFAULT NULL COMMENT 'TagText3',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否可用',
  `Idx` int(11) DEFAULT '0' COMMENT 'IDX',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_findworkerrole`
--

LOCK TABLES `wf_findworkerrole` WRITE;
/*!40000 ALTER TABLE `wf_findworkerrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_findworkerrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flow`
--

DROP TABLE IF EXISTS `wf_flow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_flow` (
  `No` varchar(200) NOT NULL DEFAULT '',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '流程类别',
  `Name` varchar(500) DEFAULT NULL,
  `FlowMark` varchar(150) DEFAULT NULL COMMENT '流程标记',
  `FlowEventEntity` varchar(150) DEFAULT NULL COMMENT '流程事件实体',
  `TitleRole` varchar(150) DEFAULT NULL COMMENT '标题生成规则',
  `IsCanStart` int(11) DEFAULT '1' COMMENT '可以独立启动否？(独立启动的流程可以显示在发起流程列表里)',
  `IsFullSA` int(11) DEFAULT '0' COMMENT '是否自动计算未来的处理人？',
  `IsAutoSendSubFlowOver` int(11) DEFAULT '0' COMMENT '为子流程时结束规则',
  `IsGuestFlow` int(11) DEFAULT '0' COMMENT '是否外部用户参与流程(非组织结构人员参与的流程)',
  `FlowAppType` int(11) DEFAULT '0' COMMENT '流程应用类型',
  `TimelineRole` int(11) DEFAULT '0' COMMENT '时效性规则',
  `Draft` int(11) DEFAULT '0' COMMENT '草稿规则',
  `FlowDeleteRole` int(11) DEFAULT '0' COMMENT '流程实例删除规则',
  `HelpUrl` varchar(300) DEFAULT NULL COMMENT '帮助文档',
  `SysType` varchar(100) DEFAULT NULL COMMENT '系统类型',
  `Tester` varchar(300) DEFAULT NULL COMMENT '发起测试人',
  `NodeAppType` varchar(50) DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `NodeAppTypeText` varchar(50) DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `ChartType` int(11) DEFAULT '1' COMMENT '节点图形类型',
  `IsBatchStart` int(11) DEFAULT '0' COMMENT '是否可以批量发起流程？(如果是就要设置发起的需要填写的字段,多个用逗号分开)',
  `BatchStartFields` varchar(500) DEFAULT NULL COMMENT '发起字段s',
  `HistoryFields` varchar(500) DEFAULT NULL COMMENT '历史查看字段',
  `IsResetData` int(11) DEFAULT '0' COMMENT '是否启用开始节点数据重置按钮？',
  `IsLoadPriData` int(11) DEFAULT '0' COMMENT '是否自动装载上一笔数据？',
  `IsDBTemplate` int(11) DEFAULT '1' COMMENT '是否启用数据模版？',
  `IsStartInMobile` int(11) DEFAULT '1' COMMENT '是否可以在手机里启用？(如果发起表单特别复杂就不要在手机里启用了)',
  `IsMD5` int(11) DEFAULT '0' COMMENT '是否是数据加密流程(MD5数据加密防篡改)',
  `DataStoreModel` int(11) DEFAULT '0' COMMENT '数据存储',
  `PTable` varchar(30) DEFAULT NULL COMMENT '流程数据存储表',
  `FlowNoteExp` varchar(500) DEFAULT NULL COMMENT '备注的表达式',
  `BillNoFormat` varchar(50) DEFAULT NULL COMMENT '单据编号格式',
  `DesignerNo` varchar(50) DEFAULT NULL COMMENT '设计者编号',
  `DesignerName` varchar(50) DEFAULT NULL COMMENT '设计者名称',
  `Note` text COMMENT '流程描述',
  `FlowRunWay` int(11) DEFAULT '0' COMMENT '运行方式',
  `RunObj` varchar(3000) DEFAULT NULL,
  `RunSQL` varchar(2000) DEFAULT '' COMMENT '流程结束执行后执行的SQL',
  `NumOfBill` int(11) DEFAULT '0' COMMENT '是否有单据',
  `NumOfDtl` int(11) DEFAULT '0' COMMENT 'NumOfDtl',
  `AvgDay` float(11,2) DEFAULT '0.00' COMMENT '平均运行用天',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序号(在发起列表中)',
  `Paras` varchar(2000) DEFAULT '' COMMENT '参数',
  `DRCtrlType` int(11) DEFAULT '0' COMMENT '部门查询权限控制方式',
  `StartLimitRole` int(11) DEFAULT '0' COMMENT '启动限制规则',
  `StartLimitPara` varchar(500) DEFAULT '' COMMENT '规则内容',
  `StartLimitAlert` varchar(500) DEFAULT '' COMMENT '限制提示',
  `StartLimitWhen` int(11) DEFAULT '0' COMMENT '提示时间',
  `StartGuideWay` int(11) DEFAULT '0' COMMENT '前置导航方式',
  `StartGuideLink` varchar(200) DEFAULT '' COMMENT '右侧的连接',
  `StartGuideLab` varchar(200) DEFAULT '' COMMENT '连接标签',
  `StartGuidePara1` varchar(500) DEFAULT '' COMMENT '参数1',
  `StartGuidePara2` varchar(500) DEFAULT '' COMMENT '参数2',
  `StartGuidePara3` varchar(500) DEFAULT '' COMMENT '参数3',
  `Ver` varchar(20) DEFAULT '' COMMENT '版本号',
  `DType` int(11) DEFAULT '0' COMMENT '设计类型0=ccbpm,1=bpmn',
  `AtPara` varchar(1000) DEFAULT '' COMMENT 'AtPara',
  `DTSWay` int(11) DEFAULT '0' COMMENT '同步方式',
  `DTSDBSrc` varchar(200) DEFAULT '' COMMENT '数据源',
  `DTSBTable` varchar(200) DEFAULT '' COMMENT '业务表名',
  `DTSBTablePK` varchar(32) DEFAULT '' COMMENT '业务表主键',
  `DTSTime` int(11) DEFAULT '0' COMMENT '执行同步时间点',
  `DTSSpecNodes` varchar(200) DEFAULT '' COMMENT '指定的节点ID',
  `DTSField` int(11) DEFAULT '0' COMMENT '要同步的字段计算方式',
  `DTSFields` varchar(2000) DEFAULT '' COMMENT '要同步的字段s,中间用逗号分开.',
  `MyDeptRole` int(11) DEFAULT '0' COMMENT '本部门发起的流程',
  `PStarter` int(11) DEFAULT '1' COMMENT '发起人可看(必选)',
  `PWorker` int(11) DEFAULT '1' COMMENT '参与人可看(必选)',
  `PCCer` int(11) DEFAULT '1' COMMENT '被抄送人可看(必选)',
  `PMyDept` int(11) DEFAULT '1' COMMENT '本部门人可看',
  `PPMyDept` int(11) DEFAULT '1' COMMENT '直属上级部门可看(比如:我是)',
  `PPDept` int(11) DEFAULT '1' COMMENT '上级部门可看',
  `PSameDept` int(11) DEFAULT '1' COMMENT '平级部门可看',
  `PSpecDept` int(11) DEFAULT '1' COMMENT '指定部门可看',
  `PSpecDeptExt` varchar(200) DEFAULT '' COMMENT '部门编号',
  `PSpecSta` int(11) DEFAULT '1' COMMENT '指定的岗位可看',
  `PSpecStaExt` varchar(200) DEFAULT '' COMMENT '岗位编号',
  `PSpecGroup` int(11) DEFAULT '1' COMMENT '指定的权限组可看',
  `PSpecGroupExt` varchar(200) DEFAULT '' COMMENT '权限组',
  `PSpecEmp` int(11) DEFAULT '1' COMMENT '指定的人员可看',
  `PSpecEmpExt` varchar(200) DEFAULT '' COMMENT '指定的人员编号',
  `HostRun` varchar(40) DEFAULT '',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flow`
--

LOCK TABLES `wf_flow` WRITE;
/*!40000 ALTER TABLE `wf_flow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flowemp`
--

DROP TABLE IF EXISTS `wf_flowemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_flowemp` (
  `FK_Flow` varchar(100) NOT NULL COMMENT 'FK_Flow,主外键:对应物理表:WF_Flow,表描述:流程',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Flow`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程岗位属性信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flowemp`
--

LOCK TABLES `wf_flowemp` WRITE;
/*!40000 ALTER TABLE `wf_flowemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flowemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flowformtree`
--

DROP TABLE IF EXISTS `wf_flowformtree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_flowformtree` (
  `No` varchar(10) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flowformtree`
--

LOCK TABLES `wf_flowformtree` WRITE;
/*!40000 ALTER TABLE `wf_flowformtree` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flowformtree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flownode`
--

DROP TABLE IF EXISTS `wf_flownode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_flownode` (
  `FK_Flow` varchar(20) NOT NULL COMMENT '流程编号 - 主键',
  `FK_Node` varchar(20) NOT NULL COMMENT '节点 - 主键',
  PRIMARY KEY (`FK_Flow`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程抄送节点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flownode`
--

LOCK TABLES `wf_flownode` WRITE;
/*!40000 ALTER TABLE `wf_flownode` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flownode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flowsort`
--

DROP TABLE IF EXISTS `wf_flowsort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_flowsort` (
  `No` varchar(100) NOT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL,
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(150) DEFAULT NULL,
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `Domain` varchar(100) DEFAULT '',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flowsort`
--

LOCK TABLES `wf_flowsort` WRITE;
/*!40000 ALTER TABLE `wf_flowsort` DISABLE KEYS */;
INSERT INTO `wf_flowsort` VALUES ('01.','线性流程','99','0',0,''),('02.','同表单分合流','99','0',0,''),('03.','异表单分合流','99','0',0,''),('04.','父子流程','99','0',0,''),('99','流程树','0','0',0,'');
/*!40000 ALTER TABLE `wf_flowsort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_frmnode`
--

DROP TABLE IF EXISTS `wf_frmnode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_frmnode` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Frm` varchar(200) DEFAULT NULL,
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点编号',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `FrmSln` int(11) DEFAULT '0' COMMENT '表单控制方案',
  `FrmType` varchar(20) DEFAULT '0' COMMENT '表单类型',
  `IsPrint` int(11) DEFAULT '0' COMMENT '是否可以打印',
  `IsEnableLoadData` int(11) DEFAULT '0' COMMENT '是否启用装载填充事件',
  `IsDefaultOpen` int(11) DEFAULT '0' COMMENT '是否默认打开',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `WhoIsPK` int(11) DEFAULT '0' COMMENT '谁是主键？',
  `Is1ToN` int(11) DEFAULT '0' COMMENT '是否1变N？',
  `HuiZong` varchar(300) DEFAULT '' COMMENT '子线程要汇总的数据表',
  `FrmEnableRole` int(11) DEFAULT '0' COMMENT '表单启用规则',
  `FrmEnableExp` varchar(4000) DEFAULT NULL,
  `TempleteFile` varchar(500) DEFAULT '' COMMENT '模版文件',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否显示',
  `GuanJianZiDuan` varchar(20) DEFAULT '' COMMENT '关键字段',
  `IsEnableFWC` int(11) DEFAULT '0',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_frmnode`
--

LOCK TABLES `wf_frmnode` WRITE;
/*!40000 ALTER TABLE `wf_frmnode` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_frmnode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_generworkerlist`
--

DROP TABLE IF EXISTS `wf_generworkerlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_generworkerlist` (
  `WorkID` int(11) NOT NULL DEFAULT '0' COMMENT '工作ID',
  `FK_Emp` varchar(20) NOT NULL COMMENT '人员',
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `FID` int(11) DEFAULT '0' COMMENT '流程ID',
  `FK_EmpText` varchar(30) DEFAULT NULL COMMENT '人员名称',
  `FK_NodeText` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '使用部门',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `DTOfWarning` varchar(50) DEFAULT NULL COMMENT '警告日期',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录时间',
  `CDT` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否可用',
  `IsRead` int(11) DEFAULT '0' COMMENT '是否读取',
  `IsPass` int(11) DEFAULT '0' COMMENT '是否通过(对合流节点有效)',
  `WhoExeIt` int(11) DEFAULT '0' COMMENT '谁执行它',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `PRI` int(11) DEFAULT '1' COMMENT '优先级',
  `PressTimes` int(11) DEFAULT '0' COMMENT '催办次数',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  `HungUpTimes` int(11) DEFAULT '0' COMMENT '挂起次数',
  `GuestNo` varchar(30) DEFAULT NULL COMMENT '外部用户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '外部用户名称',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`WorkID`,`FK_Emp`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_generworkerlist`
--

LOCK TABLES `wf_generworkerlist` WRITE;
/*!40000 ALTER TABLE `wf_generworkerlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_generworkerlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_generworkflow`
--

DROP TABLE IF EXISTS `wf_generworkflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_generworkflow` (
  `WorkID` int(11) NOT NULL DEFAULT '0' COMMENT 'WorkID',
  `FID` int(11) DEFAULT '0' COMMENT '流程ID',
  `FK_FlowSort` varchar(100) DEFAULT NULL,
  `SysType` varchar(10) DEFAULT NULL COMMENT '系统类别',
  `FK_Flow` varchar(100) DEFAULT NULL,
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `Title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `WFSta` int(11) DEFAULT '0' COMMENT '状态',
  `WFState` int(11) DEFAULT '0' COMMENT '流程状态',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `StarterName` varchar(200) DEFAULT NULL COMMENT '发起人名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) DEFAULT '1' COMMENT '优先级',
  `SDTOfNode` varchar(50) DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) DEFAULT NULL COMMENT '流程应完成时间',
  `PFlowNo` varchar(3) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT '0' COMMENT '父流程ID',
  `PNodeID` int(11) DEFAULT '0' COMMENT '父流程调用节点',
  `PFID` int(11) DEFAULT '0' COMMENT '父流程调用的PFID',
  `PEmp` varchar(32) DEFAULT NULL COMMENT '子流程的调用人',
  `GuestNo` varchar(100) DEFAULT NULL COMMENT '客户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `BillNo` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `FlowNote` text COMMENT '备注',
  `TodoEmps` text COMMENT '待办人员',
  `TodoEmpsNum` int(11) DEFAULT '0' COMMENT '待办人员数量',
  `TaskSta` int(11) DEFAULT '0' COMMENT '共享状态',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT '参数(流程运行设置临时存储的参数)',
  `Emps` text COMMENT '参与人',
  `GUID` varchar(36) DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(100) DEFAULT NULL,
  `WeekNum` int(11) DEFAULT '0' COMMENT '周次',
  `TSpan` int(11) DEFAULT '0' COMMENT '时间间隔',
  `TodoSta` int(11) DEFAULT '0' COMMENT '待办状态',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `SendDT` varchar(50) DEFAULT NULL,
  `Domain` varchar(100) DEFAULT '',
  `PrjNo` varchar(100) DEFAULT '',
  `PrjName` varchar(100) DEFAULT '',
  PRIMARY KEY (`WorkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_generworkflow`
--

LOCK TABLES `wf_generworkflow` WRITE;
/*!40000 ALTER TABLE `wf_generworkflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_generworkflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_hungup`
--

DROP TABLE IF EXISTS `wf_hungup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_hungup` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `HungUpWay` int(11) DEFAULT '0' COMMENT '挂起方式',
  `Note` text COMMENT '挂起原因(标题与内容支持变量)',
  `Rec` varchar(50) DEFAULT NULL COMMENT '挂起人',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '实际解除挂起时间',
  `DTOfUnHungUpPlan` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_hungup`
--

LOCK TABLES `wf_hungup` WRITE;
/*!40000 ALTER TABLE `wf_hungup` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_hungup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_labnote`
--

DROP TABLE IF EXISTS `wf_labnote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_labnote` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Name` varchar(3000) DEFAULT NULL,
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `X` int(11) DEFAULT '0' COMMENT 'X坐标',
  `Y` int(11) DEFAULT '0' COMMENT 'Y坐标',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_labnote`
--

LOCK TABLES `wf_labnote` WRITE;
/*!40000 ALTER TABLE `wf_labnote` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_labnote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_node`
--

DROP TABLE IF EXISTS `wf_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_node` (
  `NodeID` int(11) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `Step` int(11) DEFAULT '0' COMMENT '步骤(无计算意义)',
  `FK_Flow` varchar(150) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Tip` varchar(100) DEFAULT NULL COMMENT '操作提示',
  `WhoExeIt` int(11) DEFAULT '0' COMMENT '谁执行它',
  `ReadReceipts` int(11) DEFAULT '0' COMMENT '已读回执',
  `CondModel` int(11) DEFAULT '0' COMMENT '方向条件控制规则',
  `CancelRole` int(11) DEFAULT '0' COMMENT '撤销规则',
  `CancelDisWhenRead` int(11) DEFAULT '0' COMMENT '对方已经打开就不能撤销',
  `IsTask` int(11) DEFAULT '1' COMMENT '允许分配工作否?',
  `IsRM` int(11) DEFAULT '1' COMMENT '是否启用投递路径自动记忆功能?',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '生命周期从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '生命周期到',
  `IsBUnit` int(11) DEFAULT '0' COMMENT '是否是节点模版（业务单元）?',
  `FocusField` varchar(50) DEFAULT NULL COMMENT '焦点字段',
  `SaveModel` int(11) DEFAULT '0' COMMENT '保存方式',
  `IsGuestNode` int(11) DEFAULT '0' COMMENT '是否是外部用户执行的节点(非组织结构人员参与处理工作的节点)?',
  `NodeAppType` int(11) DEFAULT '0' COMMENT '节点业务类型',
  `FWCSta` int(11) DEFAULT '0' COMMENT '节点状态',
  `SelfParas` varchar(500) DEFAULT NULL COMMENT '自定义参数',
  `RunModel` int(11) DEFAULT '0' COMMENT '节点类型',
  `SubThreadType` int(11) DEFAULT '0' COMMENT '子线程类型',
  `PassRate` float DEFAULT NULL COMMENT '完成通过率',
  `SubFlowStartWay` int(11) DEFAULT '0' COMMENT '子线程启动方式',
  `SubFlowStartParas` varchar(100) DEFAULT NULL COMMENT '启动参数',
  `ThreadIsCanDel` int(11) DEFAULT '0' COMMENT '是否可以删除子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `ThreadIsCanShift` int(11) DEFAULT '0' COMMENT '是否可以移交子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `IsAllowRepeatEmps` int(11) DEFAULT '0' COMMENT '是否允许子线程接受人员重复(仅当分流点向子线程发送时有效)?',
  `AutoRunEnable` int(11) DEFAULT '0' COMMENT '是否启用自动运行？(仅当分流点向子线程发送时有效)',
  `AutoRunParas` varchar(100) DEFAULT NULL COMMENT '自动运行SQL',
  `AutoJumpRole0` int(11) DEFAULT '0' COMMENT '处理人就是发起人',
  `AutoJumpRole1` int(11) DEFAULT '0' COMMENT '处理人已经出现过',
  `AutoJumpRole2` int(11) DEFAULT '0' COMMENT '处理人与上一步相同',
  `WhenNoWorker` int(11) DEFAULT '0' COMMENT '(是)找不到人就跳转,(否)提示错误.',
  `SendLab` varchar(50) DEFAULT NULL COMMENT '发送按钮标签',
  `SendJS` varchar(999) DEFAULT NULL COMMENT '按钮JS函数',
  `SaveLab` varchar(50) DEFAULT NULL COMMENT '保存按钮标签',
  `SaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `ThreadLab` varchar(50) DEFAULT NULL COMMENT '子线程按钮标签',
  `ThreadEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `ThreadKillRole` int(11) DEFAULT '0' COMMENT '子线程删除方式',
  `JumpWayLab` varchar(50) DEFAULT NULL COMMENT '跳转按钮标签',
  `JumpWay` int(11) DEFAULT '0' COMMENT '跳转规则',
  `JumpToNodes` varchar(200) DEFAULT NULL COMMENT '可跳转的节点',
  `ReturnLab` varchar(50) DEFAULT NULL COMMENT '退回按钮标签',
  `ReturnRole` int(11) DEFAULT '0' COMMENT '退回规则',
  `ReturnAlert` varchar(999) DEFAULT NULL COMMENT '被退回后信息提示',
  `IsBackTracking` int(11) DEFAULT '1' COMMENT '是否可以原路返回(启用退回功能才有效)',
  `ReturnField` varchar(50) DEFAULT NULL COMMENT '退回信息填写字段',
  `ReturnReasonsItems` varchar(999) DEFAULT NULL COMMENT '退回原因',
  `CCLab` varchar(50) DEFAULT NULL COMMENT '抄送按钮标签',
  `CCRole` int(11) DEFAULT '0' COMMENT '抄送规则',
  `CCWriteTo` int(11) DEFAULT '0' COMMENT '抄送写入规则',
  `ShiftLab` varchar(50) DEFAULT NULL COMMENT '移交按钮标签',
  `ShiftEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `DelLab` varchar(50) DEFAULT NULL COMMENT '删除按钮标签',
  `DelEnable` int(11) DEFAULT '0' COMMENT '删除规则',
  `EndFlowLab` varchar(50) DEFAULT NULL COMMENT '结束流程按钮标签',
  `EndFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintHtmlLab` varchar(50) DEFAULT NULL COMMENT '打印Html标签',
  `PrintHtmlEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintPDFLab` varchar(50) DEFAULT NULL COMMENT '打印pdf标签',
  `PrintPDFEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintZipLab` varchar(50) DEFAULT NULL COMMENT '打包下载zip按钮标签',
  `PrintZipEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintDocLab` varchar(50) DEFAULT NULL COMMENT '打印单据按钮标签',
  `PrintDocEnable` int(11) DEFAULT '0' COMMENT '打印方式',
  `TrackLab` varchar(50) DEFAULT NULL COMMENT '轨迹按钮标签',
  `TrackEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `HungLab` varchar(50) DEFAULT NULL COMMENT '挂起按钮标签',
  `HungEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `SearchLab` varchar(50) DEFAULT NULL COMMENT '查询按钮标签',
  `SearchEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `WorkCheckLab` varchar(50) DEFAULT NULL COMMENT '审核按钮标签',
  `WorkCheckEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `AskforLab` varchar(50) DEFAULT NULL COMMENT '加签按钮标签',
  `AskforEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `HuiQianLab` varchar(50) DEFAULT NULL COMMENT '会签标签',
  `HuiQianRole` int(11) DEFAULT '0' COMMENT '会签模式',
  `TCLab` varchar(50) DEFAULT NULL COMMENT '流转自定义',
  `TCEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `WebOffice` varchar(50) DEFAULT NULL COMMENT '文档按钮标签',
  `WebOfficeEnable` int(11) DEFAULT '0' COMMENT '文档启用方式',
  `PRILab` varchar(50) DEFAULT NULL COMMENT '重要性',
  `PRIEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `CHLab` varchar(50) DEFAULT NULL COMMENT '节点时限',
  `CHEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `AllotLab` varchar(50) DEFAULT NULL COMMENT '分配按钮标签',
  `AllotEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `FocusLab` varchar(50) DEFAULT NULL COMMENT '关注',
  `FocusEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `ConfirmLab` varchar(50) DEFAULT NULL COMMENT '确认按钮标签',
  `ConfirmEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `CCIsAttr` int(11) DEFAULT '0' COMMENT '按表单字段抄送',
  `CCFormAttr` varchar(100) DEFAULT '' COMMENT '抄送人员字段',
  `CCIsStations` int(11) DEFAULT '0' COMMENT '按照岗位抄送',
  `CCStaWay` int(11) DEFAULT '0' COMMENT '抄送岗位计算方式',
  `CCIsDepts` int(11) DEFAULT '0' COMMENT '按照部门抄送',
  `CCIsEmps` int(11) DEFAULT '0' COMMENT '按照人员抄送',
  `CCIsSQLs` int(11) DEFAULT '0' COMMENT '按照SQL抄送',
  `CCSQL` varchar(200) DEFAULT '' COMMENT 'SQL表达式',
  `CCTitle` varchar(100) DEFAULT '' COMMENT '抄送标题',
  `CCDoc` text COMMENT '抄送内容(标题与内容支持变量)',
  `FWCLab` varchar(200) DEFAULT NULL,
  `FWCShowModel` int(11) DEFAULT '1' COMMENT '显示方式',
  `FWCType` int(11) DEFAULT '0' COMMENT '审核组件',
  `FWCNodeName` varchar(100) DEFAULT '' COMMENT '节点意见名称',
  `FWCAth` int(11) DEFAULT '0' COMMENT '附件上传',
  `FWCTrackEnable` int(11) DEFAULT '1' COMMENT '轨迹图是否显示？',
  `FWCListEnable` int(11) DEFAULT '1' COMMENT '历史审核信息是否显示？(否,仅出现意见框)',
  `FWCIsShowAllStep` int(11) DEFAULT '0' COMMENT '在轨迹表里是否显示所有的步骤？',
  `FWCOpLabel` varchar(50) DEFAULT '审核' COMMENT '操作名词(审核/审阅/批示)',
  `FWCDefInfo` varchar(50) DEFAULT '同意' COMMENT '默认审核信息',
  `SigantureEnabel` int(11) DEFAULT '0' COMMENT '操作人是否显示为图片签名？',
  `FWCIsFullInfo` int(11) DEFAULT '1' COMMENT '如果用户未审核是否按照默认意见填充？',
  `FWC_X` float(11,2) DEFAULT '300.00' COMMENT '位置X',
  `FWC_Y` float(11,2) DEFAULT '500.00' COMMENT '位置Y',
  `FWC_H` float(11,2) DEFAULT '300.00' COMMENT '高度(0=100%)',
  `FWC_W` float(11,2) DEFAULT '400.00' COMMENT '宽度(0=100%)',
  `FWCFields` varchar(50) DEFAULT '' COMMENT '审批格式字段',
  `FWCIsShowTruck` int(11) DEFAULT '0' COMMENT '是否显示未审核的轨迹？',
  `FWCIsShowReturnMsg` int(11) DEFAULT '0' COMMENT '是否显示退回信息？',
  `FWCOrderModel` int(11) DEFAULT '0' COMMENT '协作模式下操作员显示顺序',
  `FWCMsgShow` int(11) DEFAULT '0' COMMENT '审核意见显示方式',
  `SFLab` varchar(200) DEFAULT '子流程' COMMENT '显示标签',
  `SFSta` int(11) DEFAULT '0' COMMENT '父子流程状态',
  `SFShowModel` int(11) DEFAULT '1' COMMENT '显示方式',
  `SFCaption` varchar(100) DEFAULT '启动子流程' COMMENT '连接标题',
  `SFDefInfo` varchar(50) DEFAULT '' COMMENT '可启动的子流程编号(多个用逗号分开)',
  `SFActiveFlows` varchar(100) DEFAULT NULL,
  `SF_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `SF_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `SF_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `SF_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `SFFields` varchar(50) DEFAULT '' COMMENT '审批格式字段',
  `SFShowCtrl` int(11) DEFAULT '0' COMMENT '显示控制方式',
  `SFOpenType` int(11) DEFAULT '0' COMMENT '打开子流程显示',
  `FrmThreadLab` varchar(200) DEFAULT '子线程' COMMENT '显示标签',
  `FrmThreadSta` int(11) DEFAULT '0' COMMENT '组件状态',
  `FrmThread_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `FrmThread_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `FrmThread_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `FrmThread_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `FrmTrackLab` varchar(200) DEFAULT '轨迹' COMMENT '显示标签',
  `FrmTrackSta` int(11) DEFAULT '0' COMMENT '组件状态',
  `FrmTrack_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `FrmTrack_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `FrmTrack_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `FrmTrack_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `CheckNodes` varchar(50) DEFAULT '' COMMENT '工作节点s',
  `DeliveryWay` int(11) DEFAULT '0' COMMENT '访问规则',
  `FTCLab` varchar(50) DEFAULT '流转自定义' COMMENT '显示标签',
  `FTCSta` int(11) DEFAULT '0' COMMENT '组件状态',
  `FTCWorkModel` int(11) DEFAULT '0' COMMENT '工作模式',
  `FTC_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `FTC_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `FTC_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `FTC_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `SelectAccepterLab` varchar(50) DEFAULT '接受人' COMMENT '接受人按钮标签',
  `SelectAccepterEnable` int(11) DEFAULT '0' COMMENT '方式',
  `BatchLab` varchar(50) DEFAULT '批量审核' COMMENT '批量审核标签',
  `BatchEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenLab` varchar(50) DEFAULT '打开本地' COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT '打开模板' COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT '保存' COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT '接受修订' COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT '拒绝修订' COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT '套红' COMMENT '套红标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT '打印' COMMENT '打印标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT '签章' COMMENT '签章标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT '插入流程' COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT '下载' COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT '' COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT '' COMMENT '自动套红模板',
  `SelectorModel` int(11) DEFAULT '5' COMMENT '显示方式',
  `FK_SQLTemplate` varchar(50) DEFAULT '' COMMENT 'SQL模版',
  `FK_SQLTemplateText` varchar(50) DEFAULT '' COMMENT 'SQL模版',
  `IsAutoLoadEmps` int(11) DEFAULT '1' COMMENT '是否自动加载上一次选择的人员？',
  `IsSimpleSelector` int(11) DEFAULT '0' COMMENT '是否单项选择(只能选择一个人)？',
  `IsEnableDeptRange` int(11) DEFAULT '0' COMMENT '是否启用部门搜索范围限定(对使用通用人员选择器有效)？',
  `IsEnableStaRange` int(11) DEFAULT '0' COMMENT '是否启用岗位搜索范围限定(对使用通用人员选择器有效)？',
  `SelectorP1` text COMMENT '分组参数:可以为空,比如:SELECT No,Name,ParentNo FROM  Port_Dept',
  `SelectorP2` text COMMENT '操作员数据源:比如:SELECT No,Name,FK_Dept FROM  Port_Emp',
  `SelectorP3` text COMMENT '默认选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `SelectorP4` text COMMENT '强制选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `X` int(11) DEFAULT '0' COMMENT 'X坐标',
  `Y` int(11) DEFAULT '0' COMMENT 'Y坐标',
  `OfficeOpen` varchar(50) DEFAULT '打开本地' COMMENT '打开本地标签',
  `OfficeOpenTemplate` varchar(50) DEFAULT '打开模板' COMMENT '打开模板标签',
  `OfficeSave` varchar(50) DEFAULT '保存' COMMENT '保存标签',
  `OfficeAccept` varchar(50) DEFAULT '接受修订' COMMENT '接受修订标签',
  `OfficeRefuse` varchar(50) DEFAULT '拒绝修订' COMMENT '拒绝修订标签',
  `OfficeOver` varchar(50) DEFAULT '套红按钮' COMMENT '套红按钮标签',
  `OfficeMarks` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficeReadOnly` int(11) DEFAULT '0' COMMENT '是否只读',
  `OfficePrint` varchar(50) DEFAULT '打印按钮' COMMENT '打印按钮标签',
  `OfficeSeal` varchar(50) DEFAULT '签章按钮' COMMENT '签章按钮标签',
  `OfficeInsertFlow` varchar(50) DEFAULT '插入流程' COMMENT '插入流程标签',
  `OfficeIsTrueTH` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `WebOfficeFrmModel` int(11) DEFAULT '0' COMMENT '表单工作方式',
  `ICON` varchar(70) DEFAULT '' COMMENT '节点ICON图片路径',
  `NodeWorkType` int(11) DEFAULT '0' COMMENT '节点类型',
  `FlowName` varchar(200) DEFAULT '' COMMENT '流程名',
  `FrmAttr` varchar(300) DEFAULT '' COMMENT 'FrmAttr',
  `TimeLimit` float(11,2) DEFAULT '2.00' COMMENT '限期(天)',
  `TWay` int(11) DEFAULT '0' COMMENT '时间计算方式',
  `TAlertRole` int(11) DEFAULT '0' COMMENT '逾期提醒规则',
  `TAlertWay` int(11) DEFAULT '0' COMMENT '逾期提醒方式',
  `WarningDay` float(11,2) DEFAULT '1.00' COMMENT '工作预警(天)',
  `WAlertRole` int(11) DEFAULT '0' COMMENT '预警提醒规则',
  `WAlertWay` int(11) DEFAULT '0' COMMENT '预警提醒方式',
  `TCent` float(11,2) DEFAULT '2.00' COMMENT '扣分(每延期1小时)',
  `CHWay` int(11) DEFAULT '0' COMMENT '考核方式',
  `IsEval` int(11) DEFAULT '0' COMMENT '是否工作质量考核',
  `OutTimeDeal` int(11) DEFAULT '0' COMMENT '超时处理方式',
  `DoOutTime` varchar(300) DEFAULT '' COMMENT '超时处理内容',
  `Doc` varchar(100) DEFAULT '' COMMENT '描述',
  `IsExpSender` int(11) DEFAULT '1' COMMENT '本节点接收人不允许包含上一步发送人',
  `DeliveryParas` varchar(600) DEFAULT NULL,
  `NodeFrmID` varchar(50) DEFAULT '' COMMENT '节点表单ID',
  `IsCanDelFlow` int(11) DEFAULT '0' COMMENT '是否可以删除流程',
  `TodolistModel` int(11) DEFAULT '0' COMMENT '多人处理规则',
  `TeamLeaderConfirmRole` int(11) DEFAULT '0' COMMENT '组长确认规则',
  `TeamLeaderConfirmDoc` varchar(100) DEFAULT '' COMMENT '组长确认设置内容',
  `IsHandOver` int(11) DEFAULT '0' COMMENT '是否可以移交',
  `BlockModel` int(11) DEFAULT '0' COMMENT '阻塞模式',
  `BlockExp` varchar(200) DEFAULT '' COMMENT '阻塞表达式',
  `BlockAlert` varchar(100) DEFAULT '' COMMENT '被阻塞提示信息',
  `BatchRole` int(11) DEFAULT '0' COMMENT '批处理',
  `BatchListCount` int(11) DEFAULT '12' COMMENT '批处理数量',
  `BatchParas` varchar(500) DEFAULT '' COMMENT '参数',
  `FormType` int(11) DEFAULT '1' COMMENT '表单类型',
  `FormUrl` varchar(300) DEFAULT 'http://' COMMENT '表单URL',
  `TurnToDeal` int(11) DEFAULT '0' COMMENT '转向处理',
  `TurnToDealDoc` varchar(200) DEFAULT '' COMMENT '发送后提示信息',
  `NodePosType` int(11) DEFAULT '0' COMMENT '位置',
  `IsCCFlow` int(11) DEFAULT '0' COMMENT '是否有流程完成条件',
  `HisStas` varchar(3000) DEFAULT '' COMMENT '岗位',
  `HisDeptStrs` varchar(3000) DEFAULT '' COMMENT '部门',
  `HisToNDs` varchar(50) DEFAULT '' COMMENT '转到的节点',
  `HisBillIDs` varchar(50) DEFAULT '' COMMENT '单据IDs',
  `HisSubFlows` varchar(30) DEFAULT '' COMMENT 'HisSubFlows',
  `PTable` varchar(100) DEFAULT '' COMMENT '物理表',
  `ShowSheets` varchar(100) DEFAULT '' COMMENT '显示的表单',
  `GroupStaNDs` varchar(500) DEFAULT NULL,
  `RefOneFrmTreeType` varchar(100) DEFAULT '' COMMENT '独立表单类型',
  `AtPara` varchar(500) DEFAULT '' COMMENT 'AtPara',
  `TSpanHour` float(50,2) DEFAULT '0.00',
  `SubFlowLab` varchar(50) DEFAULT '子流程',
  `SubFlowEnable` int(11) DEFAULT '0',
  `FWCVer` int(11) DEFAULT '0',
  `ReturnOneNodeRole` int(11) DEFAULT '0',
  `PrintPDFModle` int(11) DEFAULT '0',
  `ShuiYinModle` varchar(100) DEFAULT '',
  `NoteLab` varchar(50) DEFAULT '备注',
  `NoteEnable` int(11) DEFAULT '0',
  `IsYouLiTai` int(11) DEFAULT '0',
  `OfficeBtnLab` varchar(50) DEFAULT '打开公文',
  `OfficeBtnEnable` int(11) DEFAULT '0',
  `ListLab` varchar(50) DEFAULT '列表',
  `ListEnable` int(11) DEFAULT '1',
  PRIMARY KEY (`NodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_node`
--

LOCK TABLES `wf_node` WRITE;
/*!40000 ALTER TABLE `wf_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodecancel`
--

DROP TABLE IF EXISTS `wf_nodecancel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodecancel` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `CancelTo` int(11) NOT NULL DEFAULT '0' COMMENT '撤销到',
  PRIMARY KEY (`FK_Node`,`CancelTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodecancel`
--

LOCK TABLES `wf_nodecancel` WRITE;
/*!40000 ALTER TABLE `wf_nodecancel` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodecancel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodedept`
--

DROP TABLE IF EXISTS `wf_nodedept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodedept` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodedept`
--

LOCK TABLES `wf_nodedept` WRITE;
/*!40000 ALTER TABLE `wf_nodedept` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodedept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodeemp`
--

DROP TABLE IF EXISTS `wf_nodeemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodeemp` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT 'Node',
  `FK_Emp` varchar(100) NOT NULL COMMENT '到人员',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodeemp`
--

LOCK TABLES `wf_nodeemp` WRITE;
/*!40000 ALTER TABLE `wf_nodeemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodeemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodeflow`
--

DROP TABLE IF EXISTS `wf_nodeflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodeflow` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Flow` varchar(100) NOT NULL COMMENT '子流程,主外键:对应物理表:WF_Flow,表描述:流程',
  PRIMARY KEY (`FK_Node`,`FK_Flow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点调用子流程';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodeflow`
--

LOCK TABLES `wf_nodeflow` WRITE;
/*!40000 ALTER TABLE `wf_nodeflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodeflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodereturn`
--

DROP TABLE IF EXISTS `wf_nodereturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodereturn` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `ReturnTo` int(11) NOT NULL DEFAULT '0' COMMENT '退回到',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`FK_Node`,`ReturnTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodereturn`
--

LOCK TABLES `wf_nodereturn` WRITE;
/*!40000 ALTER TABLE `wf_nodereturn` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodereturn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodestation`
--

DROP TABLE IF EXISTS `wf_nodestation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodestation` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodestation`
--

LOCK TABLES `wf_nodestation` WRITE;
/*!40000 ALTER TABLE `wf_nodestation` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodestation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodesubflow`
--

DROP TABLE IF EXISTS `wf_nodesubflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodesubflow` (
  `MyPK` varchar(100) CHARACTER SET utf8 NOT NULL,
  `FK_Node` int(11) DEFAULT NULL,
  `SubFlowType` int(11) DEFAULT NULL,
  `FK_Flow` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `FlowName` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ExpType` int(11) DEFAULT NULL,
  `CondExp` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `YBFlowReturnRole` int(11) DEFAULT NULL,
  `ReturnToNode` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ReturnToNodeText` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodesubflow`
--

LOCK TABLES `wf_nodesubflow` WRITE;
/*!40000 ALTER TABLE `wf_nodesubflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodesubflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodetoolbar`
--

DROP TABLE IF EXISTS `wf_nodetoolbar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_nodetoolbar` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `Target` varchar(100) DEFAULT NULL COMMENT '目标',
  `Url` varchar(500) DEFAULT NULL COMMENT '连接',
  `ShowWhere` int(11) DEFAULT '1' COMMENT '显示位置',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  `ExcType` int(11) DEFAULT '0',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodetoolbar`
--

LOCK TABLES `wf_nodetoolbar` WRITE;
/*!40000 ALTER TABLE `wf_nodetoolbar` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodetoolbar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_pushmsg`
--

DROP TABLE IF EXISTS `wf_pushmsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_pushmsg` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_Event` varchar(15) DEFAULT NULL COMMENT '事件类型',
  `PushWay` int(11) DEFAULT '0' COMMENT '推送方式',
  `PushDoc` text COMMENT '推送保存内容',
  `Tag` varchar(500) DEFAULT NULL COMMENT 'Tag',
  `SMSPushWay` int(11) DEFAULT '0' COMMENT '短信发送方式',
  `SMSField` varchar(100) DEFAULT NULL COMMENT '短信字段',
  `SMSDoc` text COMMENT '短信内容模版',
  `SMSNodes` varchar(100) DEFAULT NULL COMMENT 'SMS节点s',
  `MailPushWay` int(11) DEFAULT '0' COMMENT '邮件发送方式',
  `MailAddress` varchar(100) DEFAULT NULL COMMENT '邮件字段',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `MailNodes` varchar(100) DEFAULT NULL COMMENT 'Mail节点s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_pushmsg`
--

LOCK TABLES `wf_pushmsg` WRITE;
/*!40000 ALTER TABLE `wf_pushmsg` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_pushmsg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_rememberme`
--

DROP TABLE IF EXISTS `wf_rememberme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_rememberme` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '当前操作人员',
  `Objs` text COMMENT '分配人员',
  `ObjsExt` text COMMENT '分配人员Ext',
  `Emps` text COMMENT '所有的工作人员',
  `EmpsExt` text COMMENT '工作人员Ext',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_rememberme`
--

LOCK TABLES `wf_rememberme` WRITE;
/*!40000 ALTER TABLE `wf_rememberme` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_rememberme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_returnwork`
--

DROP TABLE IF EXISTS `wf_returnwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_returnwork` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `ReturnNode` int(11) DEFAULT '0' COMMENT '退回节点',
  `ReturnNodeName` varchar(100) DEFAULT NULL COMMENT '退回节点名称',
  `Returner` varchar(20) DEFAULT NULL COMMENT '退回人',
  `ReturnerName` varchar(100) DEFAULT NULL COMMENT '退回人名称',
  `ReturnToNode` int(11) DEFAULT '0' COMMENT 'ReturnToNode',
  `ReturnToEmp` text COMMENT '退回给',
  `BeiZhu` text COMMENT '退回原因',
  `RDT` varchar(50) DEFAULT NULL COMMENT '退回日期',
  `IsBackTracking` int(11) DEFAULT '0' COMMENT '是否要原路返回?',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_returnwork`
--

LOCK TABLES `wf_returnwork` WRITE;
/*!40000 ALTER TABLE `wf_returnwork` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_returnwork` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_selectaccper`
--

DROP TABLE IF EXISTS `wf_selectaccper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_selectaccper` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Node` int(11) DEFAULT '0' COMMENT '接受人节点',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT 'FK_Emp',
  `EmpName` varchar(20) DEFAULT NULL COMMENT 'EmpName',
  `DeptName` varchar(400) DEFAULT NULL COMMENT '部门名称',
  `AccType` int(11) DEFAULT '0' COMMENT '类型(@0=接受人@1=抄送人)',
  `Rec` varchar(20) DEFAULT NULL COMMENT '记录人',
  `Info` varchar(200) DEFAULT NULL COMMENT '办理意见信息',
  `IsRemember` int(11) DEFAULT '0' COMMENT '以后发送是否按本次计算',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号(可以用于流程队列审核模式)',
  `Tag` varchar(200) DEFAULT NULL COMMENT '维度信息Tag',
  `TimeLimit` int(11) DEFAULT '0' COMMENT '时限-天',
  `TSpanHour` float DEFAULT NULL COMMENT '时限-小时',
  `ADT` varchar(50) DEFAULT NULL COMMENT '到达日期(计划)',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期(计划)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_selectaccper`
--

LOCK TABLES `wf_selectaccper` WRITE;
/*!40000 ALTER TABLE `wf_selectaccper` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_selectaccper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_selectinfo`
--

DROP TABLE IF EXISTS `wf_selectinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_selectinfo` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `AcceptNodeID` int(11) DEFAULT NULL COMMENT '接受节点',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `InfoLeft` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `InfoCenter` varchar(200) DEFAULT NULL COMMENT 'InfoCenter',
  `InfoRight` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `AccType` int(11) DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择接受/抄送人节点信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_selectinfo`
--

LOCK TABLES `wf_selectinfo` WRITE;
/*!40000 ALTER TABLE `wf_selectinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_selectinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_shiftwork`
--

DROP TABLE IF EXISTS `wf_shiftwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_shiftwork` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT '0' COMMENT 'FK_Node',
  `FK_Emp` varchar(40) DEFAULT NULL COMMENT '移交人',
  `FK_EmpName` varchar(40) DEFAULT NULL COMMENT '移交人名称',
  `ToEmp` varchar(40) DEFAULT NULL COMMENT '移交给',
  `ToEmpName` varchar(40) DEFAULT NULL COMMENT '移交给名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '移交时间',
  `Note` varchar(2000) DEFAULT NULL COMMENT '移交原因',
  `IsRead` int(11) DEFAULT '0' COMMENT '是否读取？',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_shiftwork`
--

LOCK TABLES `wf_shiftwork` WRITE;
/*!40000 ALTER TABLE `wf_shiftwork` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_shiftwork` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_sqltemplate`
--

DROP TABLE IF EXISTS `wf_sqltemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_sqltemplate` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `SQLType` int(11) DEFAULT '0' COMMENT '模版SQL类型',
  `Name` varchar(200) DEFAULT NULL COMMENT 'SQL说明',
  `Docs` text COMMENT 'SQL模版',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_sqltemplate`
--

LOCK TABLES `wf_sqltemplate` WRITE;
/*!40000 ALTER TABLE `wf_sqltemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_sqltemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_task`
--

DROP TABLE IF EXISTS `wf_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_task` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(200) DEFAULT NULL COMMENT '流程编号',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `Paras` text COMMENT '参数',
  `TaskSta` int(11) DEFAULT '0' COMMENT '任务状态',
  `Msg` text COMMENT '消息',
  `StartDT` varchar(20) DEFAULT NULL COMMENT '发起时间',
  `RDT` varchar(20) DEFAULT NULL COMMENT '插入数据时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_task`
--

LOCK TABLES `wf_task` WRITE;
/*!40000 ALTER TABLE `wf_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testapi`
--

DROP TABLE IF EXISTS `wf_testapi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_testapi` (
  `No` varchar(92) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testapi`
--

LOCK TABLES `wf_testapi` WRITE;
/*!40000 ALTER TABLE `wf_testapi` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testapi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testcase`
--

DROP TABLE IF EXISTS `wf_testcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_testcase` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程编号',
  `ParaType` varchar(100) DEFAULT NULL COMMENT '参数类型',
  `Vals` varchar(500) DEFAULT NULL COMMENT '值s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testcase`
--

LOCK TABLES `wf_testcase` WRITE;
/*!40000 ALTER TABLE `wf_testcase` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testcase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testsample`
--

DROP TABLE IF EXISTS `wf_testsample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_testsample` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Name` varchar(50) DEFAULT NULL COMMENT '测试名称',
  `FK_API` varchar(100) DEFAULT NULL COMMENT '测试的API',
  `FK_Ver` varchar(100) DEFAULT NULL COMMENT '测试的版本',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '到',
  `TimeUse` float DEFAULT NULL COMMENT '用时(毫秒)',
  `TimesPerSecond` float DEFAULT NULL COMMENT '每秒跑多少个?',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testsample`
--

LOCK TABLES `wf_testsample` WRITE;
/*!40000 ALTER TABLE `wf_testsample` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testsample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testver`
--

DROP TABLE IF EXISTS `wf_testver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_testver` (
  `No` varchar(92) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testver`
--

LOCK TABLES `wf_testver` WRITE;
/*!40000 ALTER TABLE `wf_testver` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_transfercustom`
--

DROP TABLE IF EXISTS `wf_transfercustom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_transfercustom` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `Worker` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `WorkerName` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `SubFlowNo` varchar(3) DEFAULT NULL COMMENT '要经过的子流程编号',
  `PlanDT` varchar(50) DEFAULT NULL COMMENT '计划完成日期',
  `TodolistModel` int(11) DEFAULT '0' COMMENT '多人工作处理模式',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `NodeName` varchar(200) DEFAULT '',
  `IsEnable` int(11) DEFAULT '0',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_transfercustom`
--

LOCK TABLES `wf_transfercustom` WRITE;
/*!40000 ALTER TABLE `wf_transfercustom` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_transfercustom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_turnto`
--

DROP TABLE IF EXISTS `wf_turnto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_turnto` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `TurnToType` int(11) DEFAULT NULL COMMENT '条件类型',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性外键Sys_MapAttr',
  `AttrKey` varchar(80) DEFAULT NULL COMMENT '键值',
  `AttrT` varchar(80) DEFAULT NULL COMMENT '属性名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` varchar(60) DEFAULT NULL COMMENT '要运算的值',
  `OperatorValueT` varchar(60) DEFAULT NULL COMMENT '要运算的值T',
  `TurnToURL` varchar(700) DEFAULT NULL COMMENT '要转向的URL',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='转向条件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_turnto`
--

LOCK TABLES `wf_turnto` WRITE;
/*!40000 ALTER TABLE `wf_turnto` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_turnto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_workflowdeletelog`
--

DROP TABLE IF EXISTS `wf_workflowdeletelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wf_workflowdeletelog` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `FlowStarter` varchar(100) DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '年月',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `FlowEnderRDT` varchar(50) DEFAULT NULL COMMENT '最后处理时间',
  `FlowEndNode` int(11) DEFAULT '0' COMMENT '停留节点',
  `FlowDaySpan` float DEFAULT NULL COMMENT '跨度(天)',
  `FlowEmps` varchar(100) DEFAULT NULL COMMENT '参与人',
  `Oper` varchar(20) DEFAULT NULL COMMENT '删除人员',
  `OperDept` varchar(20) DEFAULT NULL COMMENT '删除人员部门',
  `OperDeptName` varchar(200) DEFAULT NULL COMMENT '删除人员名称',
  `DeleteNote` text COMMENT '删除原因',
  `DeleteDT` varchar(50) DEFAULT NULL COMMENT '删除日期',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_workflowdeletelog`
--

LOCK TABLES `wf_workflowdeletelog` WRITE;
/*!40000 ALTER TABLE `wf_workflowdeletelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_workflowdeletelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `port_dept`
--

/*!50001 DROP VIEW IF EXISTS `port_dept`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_dept` AS select `o`.`office_code` AS `No`,`o`.`office_name` AS `Name`,'' AS `NameOfPath`,(case when (`o`.`parent_code` = '0') then '0' else `o`.`parent_code` end) AS `ParentNo`,'' AS `TreeNo`,'admin' AS `Leader`,'' AS `Tel`,`o`.`tree_sort` AS `Idx`,(case when (`o`.`tree_leaf` = '0') then 1 else 0 end) AS `IsDir`,'' AS `OrgNo` from `js_sys_office` `o` where (`o`.`status` = '0') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_deptemp`
--

/*!50001 DROP VIEW IF EXISTS `port_deptemp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_deptemp` AS select concat(`e`.`FK_Dept`,'_',`e`.`No`) AS `MyPk`,`e`.`No` AS `FK_Emp`,`e`.`FK_Dept` AS `FK_Dept`,'' AS `FK_Duty`,0 AS `DutyLevel`,'admin' AS `Leader` from `port_emp` `e` where (`e`.`FK_Dept` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_deptempstation`
--

/*!50001 DROP VIEW IF EXISTS `port_deptempstation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_deptempstation` AS select concat(`e`.`FK_Dept`,'_',`e`.`No`,'_',`s`.`FK_Station`) AS `MyPk`,`e`.`FK_Dept` AS `FK_Dept`,`s`.`FK_Station` AS `FK_Station`,`e`.`No` AS `FK_Emp` from (`port_emp` `e` join `port_empstation` `s`) where ((`e`.`No` = `s`.`FK_Emp`) and (`e`.`FK_Dept` is not null)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_emp`
--

/*!50001 DROP VIEW IF EXISTS `port_emp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_emp` AS select `u`.`user_code` AS `No`,`e`.`emp_code` AS `EmpNo`,`u`.`user_name` AS `Name`,'123' AS `Pass`,(case when (`e`.`office_code` <> '') then `e`.`office_code` else (select `oo`.`office_code` from `js_sys_office` `oo` where (`oo`.`parent_code` = '0') limit 1) end) AS `FK_Dept`,'' AS `FK_Duty`,'admin' AS `Leader`,`u`.`extend_s1` AS `SID`,`u`.`mobile` AS `Tel`,`u`.`email` AS `Email`,'' AS `PinYin`,'' AS `SignType`,1 AS `NumOfDept`,0 AS `Idx` from (`js_sys_user` `u` left join `js_sys_employee` `e` on((`e`.`emp_code` = `u`.`ref_code`))) where (((`u`.`status` = '0') and (`u`.`user_type` = 'employee') and (`e`.`status` = '0')) or (`u`.`login_code` = 'system') or (`u`.`login_code` = 'admin')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_empstation`
--

/*!50001 DROP VIEW IF EXISTS `port_empstation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_empstation` AS select `ur`.`user_code` AS `FK_Emp`,`ur`.`role_code` AS `FK_Station` from `js_sys_user_role` `ur` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_station`
--

/*!50001 DROP VIEW IF EXISTS `port_station`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_station` AS select `r`.`role_code` AS `No`,`r`.`role_name` AS `Name`,`r`.`role_type` AS `FK_StationType`,'' AS `DutyReq`,'' AS `Makings`,'' AS `OrgNo` from `js_sys_role` `r` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_stationtype`
--

/*!50001 DROP VIEW IF EXISTS `port_stationtype`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `port_stationtype` AS select `d`.`dict_value` AS `No`,`d`.`dict_label` AS `Name`,'' AS `Idx` from `js_sys_dict_data` `d` where (`d`.`dict_type` = 'sys_role_type') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flowstarter`
--

/*!50001 DROP VIEW IF EXISTS `v_flowstarter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flowstarter` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`No` AS `No` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_emp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`e`.`FK_Emp` AS `FK_Emp` from ((((`wf_node` `a` join `wf_nodedept` `b`) join `wf_nodestation` `c`) join `port_emp` `d`) join `port_deptempstation` `e`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`NodeID` = `c`.`FK_Node`) and (`b`.`FK_Dept` = `d`.`FK_Dept`) and (`c`.`FK_Station` = `e`.`FK_Station`) and (`a`.`DeliveryWay` = 9)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flowstarterbpm`
--

/*!50001 DROP VIEW IF EXISTS `v_flowstarterbpm`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flowstarterbpm` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_deptemp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`e`.`FK_Emp` AS `FK_Emp` from (((`wf_node` `a` join `wf_nodedept` `b`) join `wf_nodestation` `c`) join `port_deptempstation` `e`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`NodeID` = `c`.`FK_Node`) and (`b`.`FK_Dept` = `e`.`FK_Dept`) and (`c`.`FK_Station` = `e`.`FK_Station`) and (`a`.`DeliveryWay` = 9)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalch`
--

/*!50001 DROP VIEW IF EXISTS `v_totalch`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalch` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`)) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ANQI`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalchweek`
--

/*!50001 DROP VIEW IF EXISTS `v_totalchweek`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalchweek` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`WeekNum` AS `WeekNum`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`WeekNum`,`wf_ch`.`FK_NY` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalchyf`
--

/*!50001 DROP VIEW IF EXISTS `v_totalchyf`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalchyf` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`FK_NY` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_wf_delay`
--

/*!50001 DROP VIEW IF EXISTS `v_wf_delay`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_wf_delay` AS select concat(`wf_empworks`.`WorkID`,'_',`wf_empworks`.`FK_Emp`,'_',`wf_empworks`.`FK_Node`) AS `MyPK`,`wf_empworks`.`PRI` AS `PRI`,`wf_empworks`.`WorkID` AS `WorkID`,`wf_empworks`.`IsRead` AS `IsRead`,`wf_empworks`.`Starter` AS `Starter`,`wf_empworks`.`StarterName` AS `StarterName`,`wf_empworks`.`WFState` AS `WFState`,`wf_empworks`.`FK_Dept` AS `FK_Dept`,`wf_empworks`.`DeptName` AS `DeptName`,`wf_empworks`.`FK_Flow` AS `FK_Flow`,`wf_empworks`.`FlowName` AS `FlowName`,`wf_empworks`.`PWorkID` AS `PWorkID`,`wf_empworks`.`PFlowNo` AS `PFlowNo`,`wf_empworks`.`FK_Node` AS `FK_Node`,`wf_empworks`.`NodeName` AS `NodeName`,`wf_empworks`.`WorkerDept` AS `WorkerDept`,`wf_empworks`.`Title` AS `Title`,`wf_empworks`.`RDT` AS `RDT`,`wf_empworks`.`ADT` AS `ADT`,`wf_empworks`.`SDT` AS `SDT`,`wf_empworks`.`FK_Emp` AS `FK_Emp`,`wf_empworks`.`FID` AS `FID`,`wf_empworks`.`FK_FlowSort` AS `FK_FlowSort`,`wf_empworks`.`SysType` AS `SysType`,`wf_empworks`.`SDTOfNode` AS `SDTOfNode`,`wf_empworks`.`PressTimes` AS `PressTimes`,`wf_empworks`.`GuestNo` AS `GuestNo`,`wf_empworks`.`GuestName` AS `GuestName`,`wf_empworks`.`BillNo` AS `BillNo`,`wf_empworks`.`FlowNote` AS `FlowNote`,`wf_empworks`.`TodoEmps` AS `TodoEmps`,`wf_empworks`.`TodoEmpsNum` AS `TodoEmpsNum`,`wf_empworks`.`TodoSta` AS `TodoSta`,`wf_empworks`.`TaskSta` AS `TaskSta`,`wf_empworks`.`ListType` AS `ListType`,`wf_empworks`.`Sender` AS `Sender`,`wf_empworks`.`AtPara` AS `AtPara`,`wf_empworks`.`MyNum` AS `MyNum` from `wf_empworks` where (`wf_empworks`.`SDT` > now()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `wf_empworks`
--

/*!50001 DROP VIEW IF EXISTS `wf_empworks`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`admin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `wf_empworks` AS select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`IsRead` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`WFState` AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`FK_NodeText` AS `NodeName`,`b`.`FK_Dept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`SDT` AS `SDT`,`b`.`FK_Emp` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,`b`.`PressTimes` AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TodoSta` AS `TodoSta`,`a`.`TaskSta` AS `TaskSta`,0 AS `ListType`,`a`.`Sender` AS `Sender`,`a`.`AtPara` AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_generworkerlist` `b`) where ((`b`.`IsEnable` = 1) and (`b`.`IsPass` = 0) and (`a`.`WorkID` = `b`.`WorkID`) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` <> 0) and (`b`.`WhoExeIt` <> 1)) union select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`Sta` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,2 AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`NodeName` AS `NodeName`,`b`.`CCToDept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`RDT` AS `SDT`,`b`.`CCTo` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,0 AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,0 AS `TodoSta`,0 AS `TaskSta`,1 AS `ListType`,`b`.`Rec` AS `Sender`,('@IsCC=1' or `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_cclist` `b`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`b`.`Sta` <= 1) and (`b`.`InEmpWorks` = 1) and (`a`.`WFState` <> 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-02 17:44:47
