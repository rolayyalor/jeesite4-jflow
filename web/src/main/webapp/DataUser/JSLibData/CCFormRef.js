﻿
/*
1. 该页面,是被引用到 /WF/MyFlowGener.htm, /WF/CCForm/FrmGener.htm 里面的.
2. 这里方法大多是执行后，返回json ,可以被页面控件调用. 
*/
function funDemo() {
    alert("我被执行了。");
}

function setShuZhi3Value(){
	alert("我被执行了11。");
}
/**从表附件导入的固定方法，不可人为删除**/
//FK_MapData,附件属性，RefPK,FK_Node
function afterDtlImp(FK_MapData, frmAth, newOID, FK_Node, oldOID,oldFK_MapData) {
    //处理从表附件导入的事件
}

function ChangeFormValue(form1,form2){
	//根据WorkID 获取form1表单的数据
	var handler = new HttpHandler("B")
	var workID = GetQueryString("WorkID");
	var en = new Entity("BP.Sys.GEEntity");
	
	en.OID = workID;
}

/*
新增的公共代码START*/
// function checkPhoneNumber() {
//     // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
//     var phone_number = $("#TB_DianHuaHaoMa").val();
//     var notification_number = $("#TB_TongZhiDanHao").val(); //旧的通知单号
//     var task_type = $("#TB_YWLX").val();
//
//     if(phone_number==""){
//         alert("电话号码为空，请先获取电话信息");
//         return false;
//     }
//     if(task_type==""){
//         alert("业务类型为空，请先获取电话信息");
//         return false;
//     }
//     var phoneNumberSQL = DBAccess.RunSQLReturnVal("SELECT COUNT(ppn.phone_number) \n" +
//         "FROM pm_phone_number ppn \n" +
//         "LEFT JOIN pm_install_information pii ON pii.number_id = ppn.id \n" +
//         "WHERE pii.business_status = 0 AND ppn.phone_number = " + phone_number);
//     alert("phoneNumberSQL"+phoneNumberSQL);
//     if(phoneNumberSQL < 1){
//         alert("该号码存在未完成的业务");
//         return false;
//     }
//
//     //生成通知单号
//     $.ajax({
//         url: "../restful/setNoticeOrderNumber",
//         type: "post",
//         dataType: "text",
//         data: {
//             // notification_number: notification_number,
//             task_type: task_type
//         },
//         success: function (res) {
//             if (res.length == 0) {
//             } else {
//                 $("#TB_NEWTongZhiDanHao").val(res);
//             }
//         },
//         error: function (err) {
//             alert("生成通知单号失败，数据异常，请联系管理员 "+err);
//             return false;
//         }
//     });
//
//     //设置该号码已经有业务在进行中
//     var count = DBAccess.RunSQL("\n" +
//         "UPDATE pm_install_information \n" +
//         "SET business_status = 1 \n" +
//         "WHERE notification_number = '" + notification_number+"' ");
//     if(count < 1){
//         return false;
//     }
//     return true;
// }

function validataPhoneNumberAndPoint(){
    var TB_JuDian=$("#TB_JuDian").val();
    if(TB_JuDian!=undefined&&TB_JuDian!=""){
        var TB_DianHuaHaoMa=$("#TB_DianHuaHaoMa").val();
        if(TB_DianHuaHaoMa!=undefined&&TB_DianHuaHaoMa!=""){
            //根据号码查询局点名称
            var pointName=DBAccess.RunSQLReturnVal("select pd.Name from pm_phone_number ppn left join port_dept pd on ppn.area_id=pd.No where ppn.phone_number='"+TB_DianHuaHaoMa+"'");
            if(pointName!=undefined&&pointName!=""){
                if(pointName!=TB_JuDian){
                    alert("输入的号码不属于选择的局点");
                    return false;
                }
            }
        }
    }
    return true;
}

function updateView(){
    //隐藏姓名、职别、单位名称
    var official_rank = DBAccess.RunSQLReturnVal("SELECT official_rank\n" +
        "FROM pm_official_rank\n" +
        "WHERE official_rank_name='"+$("#TB_ZhiBie").val()+"'");
    var val = DBAccess.RunSQLReturnVal("SELECT val\n" +
        "FROM sys_config\n" +
        "WHERE property='view'");
    if(parseInt(official_rank)>parseInt(val)){
        $("#TB_XingMing").prop("type","password");
        $("#TB_ZhiBie").prop("type","password");
        $("#TB_DanWeiMingCheng").prop("type","password");
    }else {
        $("#TB_XingMing").prop("type","text");
        $("#TB_ZhiBie").prop("type","text");
        $("#TB_DanWeiMingCheng").prop("type","text");
    }

    var timer=setInterval(function(){
        var TB_JunWangZhangTu=$("#TB_JunWangZhangTu").val();
        var TB_GongWangZhangTu=$("#TB_GongWangZhangTu").val();
        var TB_ChengKongGongNeng=$("#TB_ChengKongGongNeng").val();
        if(TB_JunWangZhangTu!=undefined&&TB_JunWangZhangTu!=""){
            if(TB_JunWangZhangTu=="true"){
                $("#TB_JunWangZhangTu").val("是");
            }else{
                $("#TB_JunWangZhangTu").val("否");
            }
        }
        if(TB_GongWangZhangTu!=undefined&&TB_GongWangZhangTu!=""){
            if(TB_GongWangZhangTu=="true"){
                $("#TB_GongWangZhangTu").val("是");
            }else{
                $("#TB_GongWangZhangTu").val("否");
            }
        }
        var ckgnNameStr="";
        if(TB_ChengKongGongNeng!=undefined&&TB_ChengKongGongNeng!=""){
            var ckgnArry=TB_ChengKongGongNeng.split(",");
            if(ckgnArry!=undefined&&ckgnArry.length>0){
                for(var i=0;i<ckgnArry.length;i++){
                    var singleCkgn=ckgnArry[i];
                    if(singleCkgn!=undefined&&singleCkgn!=""){
                        var ckgnName=DBAccess.RunSQLReturnVal("select function_name from pm_control_function where function_code='"+singleCkgn+"'");
                        if(ckgnName!=undefined&&ckgnName!=""){
                            if(ckgnNameStr==""){
                                ckgnNameStr=ckgnName;
                            }else{
                                ckgnNameStr+=","+ckgnName;
                            }
                        }
                    }
                }
            }
        }
        if(ckgnNameStr!=""){
            $("#TB_ChengKongGongNeng").val(ckgnNameStr);
        }
        clearInterval(timer);
    },100);
}
function updateGLView(){
    var timer=setInterval(function(){
        var TB_JWZT=$("#TB_JWZT").val();
        var TB_GWZT=$("#TB_GWZT").val();
        var TB_ChengKongGongNeng=$("#TB_ChengKongGongNeng").val();
        if(TB_JWZT!=undefined&&TB_JWZT!=""){
            if(TB_JWZT=="true"){
                $("#TB_JWZT").val("是");
            }else{
                $("#TB_JWZT").val("否");
            }
        }
        if(TB_GWZT!=undefined&&TB_GWZT!=""){
            if(TB_GWZT=="true"){
                $("#TB_GWZT").val("是");
            }else{
                $("#TB_GWZT").val("否");
            }
        }
        clearInterval(timer);
    },100);
}
function updateHHView(){
    var timer=setInterval(function(){
        var TB_JWZTX=$("#TB_JWZTX").val();
        var TB_GWZTX=$("#TB_GWZTX").val();
        if(TB_JWZTX!=undefined&&TB_JWZTX!=""){
            if(TB_JWZTX=="true"){
                $("#TB_JWZTX").val("是");
            }else{
                $("#TB_JWZTX").val("否");
            }
        }
        if(TB_GWZTX!=undefined&&TB_GWZTX!=""){
            if(TB_GWZTX=="true"){
                $("#TB_GWZTX").val("是");
            }else{
                $("#TB_GWZTX").val("否");
            }
        }
        clearInterval(timer);
    },100);
}

function downloadOption(){
    var DDL_DHXZ=$("#DDL_DHXZ").val();
    if(DDL_DHXZ!=undefined&&DDL_DHXZ!=""){
        $("#DDL_DHYT").empty();
        var optionHtml="";
        if(DDL_DHXZ==0){
            //地方号
            optionHtml="<option value=\"0\">宿舍</option><option value=\"1\">办公</option>";
        }else if(DDL_DHXZ==1){
            //专线号
            optionHtml="<option value=\"0\">宿舍</option><option value=\"1\">办公</option>";
        }else if(DDL_DHXZ==2){
            //自动号
            optionHtml="<option value=\"0\">宿舍</option><option value=\"1\">办公</option><option value=\"2\">测试</option><option value=\"3\">中继</option>";
        }else if(DDL_DHXZ==3){
            //密话

        }
        $("#DDL_DHYT").append(optionHtml);
    }
}

function dynemicTongZhi(){
    $("td[id='Td_TongZhi'][class='FDesc']").empty();
    var TB_JuDianp=$("#TB_JuDian").val();
    var TB_JuDian;
    if(TB_JuDianp!=undefined&&TB_JuDianp!=""){
        var TB_JuDianparry=TB_JuDianp.split("：");
        if(TB_JuDianparry!=undefined&&TB_JuDianparry.length==2){
            TB_JuDian=TB_JuDianparry[1];
        }
    }
    var TB_TaJuJuDianE=$("#TaJuJuDian_mtags span");
    var DDL_ShiFouYuTaJuXiangGua=$("#DDL_ShiFouYuTaJuXiangGua").val();
    var TB_TaJuJuDian="";
    if(TB_TaJuJuDianE!=undefined){
        TB_TaJuJuDianE.text(function(i,n){
            if(TB_TaJuJuDian==""){
                TB_TaJuJuDian=n;
            }else{
                TB_TaJuJuDian+=","+n;
            }
        });
    }
    var TongZhiHtml="";
    if(TB_JuDian!=undefined&&TB_JuDian!=""){
        //根据局名称取pm_switch_flow中的flow_val
        var tval=DBAccess.RunSQLReturnVal("select flow_val from pm_switch_flow where point_name='"+TB_JuDian+"'");
        if(tval!=undefined&&tval!=""){
            var tvalArry=tval.split(",");
            if(tvalArry!=undefined&&tvalArry.length>0){
                for(var i=0;i<tvalArry.length;i++){
                    var v=tvalArry[i];
                    if(i==0){
                        TongZhiHtml+=' <input type="checkbox" id="CB_TongZhi_TZDW_"'+v+' name="CB_TongZhi_TZDW" value="'+v+'" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">\n' +
                            '    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_'+v+'">&nbsp;'+TB_JuDian+'程控&nbsp;&nbsp;</label>';
                    }else if(i==1){
                        TongZhiHtml+=' <input type="checkbox" id="CB_TongZhi_TZDW_"'+v+' name="CB_TongZhi_TZDW" value="'+v+'" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">\n' +
                            '    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_'+v+'">&nbsp;'+TB_JuDian+'配线&nbsp;&nbsp;</label>';
                    }else if(i==2){
                        TongZhiHtml+=' <input type="checkbox" id="CB_TongZhi_TZDW_"'+v+' name="CB_TongZhi_TZDW" value="'+v+'" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">\n' +
                            '    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_'+v+'">&nbsp;'+TB_JuDian+'外线&nbsp;&nbsp;</label>';
                    }
                }
            }
        }
    }

    /*<td class="FDesc" id="Td_TongZhi" style="width:69%;" colspan="3" rowspan="1">
    <input type="checkbox" id="CB_TongZhi_TZDW_0" name="CB_TongZhi_TZDW" value="0" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">
    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_0">&nbsp;汇五程控&nbsp;&nbsp;</label>
    <input maxlength="50" id="TB_TongZhi" class="form-control" type="text" placeholder="" name="TB_TongZhi" style="display: none;">
    </td>*/

    if(DDL_ShiFouYuTaJuXiangGua!=undefined&&DDL_ShiFouYuTaJuXiangGua!=""&&DDL_ShiFouYuTaJuXiangGua=="1"){
        //选择了关联它局
        if(TB_TaJuJuDian!=""){
            //根据它局局点取对应的flow_val
            var TB_TaJuJuDianArry=TB_TaJuJuDian.split(",");
            if(TB_TaJuJuDianArry!=undefined&&TB_TaJuJuDianArry.length>0){
                for(var i=0;i<TB_TaJuJuDianArry.length;i++){
                    var stjp=TB_TaJuJuDianArry[i];
                    if(stjp!=undefined){
                        var stjparry=stjp.split("：");
                        if(stjparry!=undefined&&stjparry.length==2){
                            var stj=stjparry[1];
                            if(stj!=undefined){
                                var tval=DBAccess.RunSQLReturnVal("select flow_val from pm_switch_flow where point_name='"+stj+"'");
                                if(tval!=undefined&&tval!=""){
                                    var tvalArry=tval.split(",");
                                    if(tvalArry!=undefined&&tvalArry.length>0){
                                        for(var j=0;j<tvalArry.length;j++){
                                            var v=tvalArry[j];
                                            if(j==0){
                                                TongZhiHtml+=' <input type="checkbox" id="CB_TongZhi_TZDW_"'+v+' name="CB_TongZhi_TZDW" value="'+v+'" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">\n' +
                                                    '    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_'+v+'">&nbsp;'+stj+'程控&nbsp;&nbsp;</label>';
                                            }else if(j==1){
                                                TongZhiHtml+=' <input type="checkbox" id="CB_TongZhi_TZDW_"'+v+' name="CB_TongZhi_TZDW" value="'+v+'" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">\n' +
                                                    '    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_'+v+'">&nbsp;'+stj+'配线&nbsp;&nbsp;</label>';
                                            }else if(j==2){
                                                TongZhiHtml+=' <input type="checkbox" id="CB_TongZhi_TZDW_"'+v+' name="CB_TongZhi_TZDW" value="'+v+'" onclick="changeValue(&quot;TB_TongZhi&quot;,&quot;CB_TongZhi_TZDW&quot;)">\n' +
                                                    '    <label class="labRb align_cbl" for="CB_TongZhi_TZDW_'+v+'">&nbsp;'+stj+'外线&nbsp;&nbsp;</label>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    TongZhiHtml+='<input maxlength="50" id="TB_TongZhi" class="form-control" type="text" placeholder="" name="TB_TongZhi" style="display: none;">';
    if(TongZhiHtml!=""){
        $("td[id='Td_TongZhi'][class='FDesc']").append(TongZhiHtml);
    }
}



function getJuDianVal(){
    var TB_GuiShuJuZhan=$("#TB_GuiShuJuZhan").val();
    if(TB_GuiShuJuZhan!=undefined&&TB_GuiShuJuZhan!=""){
        //根据局名称取pm_switch_flow中的flow_val
        var tval=DBAccess.RunSQLReturnVal("select No from port_dept where Name='"+TB_GuiShuJuZhan+"'");
        if(tval!=undefined&&tval!=""){
            $("#TB_JuDian").val(tval);
        }
    }
}

function getTaJuJuDianVal(){
    alert(11);
    var TB_TaJuE=$("#TaJu_mtags span");
    var TB_TaJu="";
    if(TB_TaJuE!=undefined){
        TB_TaJuE.text(function(i,n){
            if(TB_TaJu==""){
                TB_TaJu=n;
            }else{
                TB_TaJu+=","+n;
            }
        });
    }
    alert(TB_TaJu);
    var tajujudian="";
    if(TB_TaJu!=""){
        //根据它局局点取对应的flow_val
        var TB_TaJuArry=TB_TaJu.split(",");
        if(TB_TaJuArry!=undefined&&TB_TaJuArry.length>0){
            for(var i=0;i<TB_TaJuArry.length;i++){
                var stj=TB_TaJuArry[i];
                if(stj!=undefined){
                    var tval=DBAccess.RunSQLReturnVal("select No from port_dept where Name='"+stj+"'");
                    if(tval!=undefined&&tval!=""){
                        if(tajujudian==""){
                            tajujudian=tval;
                        }else{
                            tajujudian+=","+tval;
                        }
                    }
                }
            }
        }
    }
    if(tajujudian!=""){
        $("#TB_TaJuJuDian").val(tajujudian);
    }
}


// function checkPXDPhoneNumber() {
//     // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
//     var phone_number = $("#TB_DianHuaHaoMa").val();
//     var notification_number = $("#TB_TongZhiDanHao").val(); //旧的通知单号
//     var task_type = $("#TB_YWLX").val();
//     var number_use = $("#DDL_DHYT").val();//0：办公；1：宿舍；2：测试；3：中继；
//     var number_nature = $("#DDL_DHXZ").val();
//
//     var specialPhoneLineNumber = $("#TB_ZhuanXianTaiHao").val();
//     var specialPhoneLineCodeName = $("#TB_ZhuanXianDaiHao").val();
//
//     if(task_type=="" || undefined == task_type){
//         alert("业务类型为空，请先获取电话信息");
//         return false;
//     }
//     if(number_nature=="" || undefined == number_nature){
//         alert("电话性质为空，请先获取电话信息");
//         return false;
//     }
//     if(notification_number=="" || undefined == notification_number){
//         alert("通知单号为空，请先获取电话信息");
//         return false;
//     }
//     if(number_nature != 3){//3:密话不用判断电话用途
//         if(undefined == number_use || "" == number_use){
//             alert("电话用途为空，请先获取电话信息");
//             return false;
//         }
//     }
//     if(number_nature == 1){
//         if (undefined == specialPhoneLineNumber || specialPhoneLineNumber == "" || undefined == specialPhoneLineCodeName || specialPhoneLineCodeName == "") {
//             alert("专线台号和专线代号不能为空");
//             return false;
//         }
//     }else {
//         if(undefined == phone_number || phone_number==""){
//             alert("电话号码为空，请先获取电话信息");
//             return false;
//         }
//     }
//
//     //判断是否存在未完成的业务
//     var phoneNumberSQL = DBAccess.RunSQLReturnVal("SELECT COUNT(ppn.phone_number) \n" +
//         "FROM pm_phone_number ppn \n" +
//         "LEFT JOIN pm_install_information pii ON pii.number_id = ppn.id \n" +
//         "WHERE pii.business_status = 0 AND ppn.phone_number = '"+ phone_number +"' ");
//
//     if(phoneNumberSQL < 1){
//         alert("该号码存在未完成的业务");
//         return false;
//     }
//     //生成派修单通知单号
//     var flag=true;
//     $.ajax({
//         url: "../restful/setNoticeOrderNumber",
//         type: "post",
//         dataType: "text",
//         async:false,
//         data: {
//             task_type: '20'
//         },
//         success: function (res) {
//             if (res.length == 0) {
//                 flag= false;
//             } else {
//                 $("#TB_NEWTongZhiDanHao").val(res);//新的通知单号
//             }
//         },
//         error: function (err) {
//             flag= false;
//         }
//     });
//
//     if(!flag){
//         alert("生成通知单号失败，数据异常，请联系管理员");
//         return false;
//     }
//
//     if(!flag){
//         alert("生成通知单号失败，数据异常，请联系管理员");
//         return false;
//     }
//     //设置该号码已经有业务在进行中
//     var count = DBAccess.RunSQL("\n" +
//         "UPDATE pm_install_information \n" +
//         "SET business_status = 1 \n" +
//         "WHERE notification_number = '" + notification_number+"' ");
//     if(count < 1){
//         alert("数据异常，请联系管理员");
//         return false;
//     }
//     return true;
// }

// function checkPhoneNumber(SPSF) {
//     var phone_number = $("#TB_DianHuaHaoMa").val();
//     var notification_number = $("#TB_TongZhiDanHao").val(); //旧的通知单号
//     // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
//     var task_type = $("#TB_YWLX").val();
//     var number_use = $("#DDL_DHYT").val();//0：办公；1：宿舍；2：测试；3：中继；
//     var number_nature = $("#DDL_DHXZ").val();
//
//     var specialPhoneLineNumber = $("#TB_ZhuanXianTaiHao").val();
//     var specialPhoneLineCodeName = $("#TB_ZhuanXianDaiHao").val();
//
//     var flag=true;
//     if(task_type=="" || undefined == task_type){
//         alert("业务类型为空，请先获取电话信息");
//         return false;
//     }
//     if(number_nature=="" || undefined == number_nature){
//         alert("电话性质为空，请先获取电话信息");
//         return false;
//     }
//     if(number_nature != 3){//3:密话不用判断电话用途
//         if(undefined == number_use || "" == number_use){
//             alert("电话用途为空，请先获取电话信息");
//             return false;
//         }
//     }
//     if(number_nature == 1){
//         if (undefined == specialPhoneLineNumber || specialPhoneLineNumber == "" || undefined == specialPhoneLineCodeName || specialPhoneLineCodeName == "") {
//             alert("专线台号和专线代号不能为空");
//             return false;
//         }
//     }else {
//         if(undefined == phone_number || phone_number==""){
//             alert("电话号码为空，请先获取电话信息");
//             return false;
//         }
//     }
//     if(task_type == 4 || task_type == 10){//宿舍号码才能过户
//         if(number_use != 1){
//             alert("宿舍号码才能过户");
//             return false;
//         }
//
//     }
//     if(task_type == 6 || task_type == 11 || task_type == 12){//办公号码才能改名
//         if(number_use != 0){
//             alert("办公号码才能改名");
//             return false;
//         }
//     }
//     if(task_type == 5){//换号
//         var newPhoneNumber = $("#TB_XDHHM").val();
//         if(undefined == newPhoneNumber || ""==newPhoneNumber){
//             alert("新的号码不能为空");
//             return false;
//         }
//         if(number_nature==2 || number_nature==3){//自动号或者密话
//             var checkNewPhoneNumber = 0;
//             $.ajax({
//                 url: "../restful/getIsUsingPhoneNumberTable",
//                 type: "post",
//                 dataType: "JSON",
//                 async:false,
//                 data: {},
//                 success: function (res) {
//                     if (res.length == 0) {
//                         flag=false;
//                     } else {
//                         checkNewPhoneNumber = DBAccess.RunSQLReturnVal("SELECT phone_number\n" +
//                             "      FROM pm_phone_number\n" +
//                             "      WHERE is_using = 0 AND phone_number= '"+newPhoneNumber+"' \n");
//                         if(checkNewPhoneNumber == null){
//                             alert("新号码不存在或已经被装机，无法重新装机");
//                             flag= false;
//                             return;
//                         }
//                         checkNewPhoneNumber = DBAccess.RunSQLReturnVal("SELECT DianHuaHaoMa\n" +
//                             "        FROM "+res.ZJ+" nd13\n" +
//                             "        LEFT JOIN wf_generworkflow wf ON nd13.OID = wf.WorkID\n" +
//                             "        WHERE nd13.DianHuaHaoMa = '"+newPhoneNumber+"' AND wf.WFSta = 0\n" +
//                             "\n" +
//                             "        UNION ALL\n" +
//                             "\n" +
//                             "        SELECT DianHuaHaoMa\n" +
//                             "        FROM "+res.SPZJ+" nd11\n" +
//                             "        LEFT JOIN wf_generworkflow wf ON nd11.OID = wf.WorkID\n" +
//                             "        WHERE nd11.DianHuaHaoMa = '"+newPhoneNumber+"' AND wf.WFSta = 0" +
//                             "        UNION ALL\n" +
//                             "\n" +
//                             "        SELECT DianHuaHaoMa\n" +
//                             "        FROM "+res.HH+" nd41\n" +
//                             "        LEFT JOIN wf_generworkflow wf ON nd41.OID = wf.WorkID\n" +
//                             "        WHERE nd41.XDHHM = '"+newPhoneNumber+"' AND wf.WFSta = 0\n" +
//                             "\n" +
//                             "        UNION ALL\n" +
//                             "\n" +
//                             "        SELECT DianHuaHaoMa\n" +
//                             "        FROM "+res.SPHH+" nd28\n" +
//                             "        LEFT JOIN wf_generworkflow wf ON nd28.OID = wf.WorkID\n" +
//                             "        WHERE nd28.XDHHM = '"+newPhoneNumber+"' AND wf.WFSta = 0");
//                         if(checkNewPhoneNumber != null){
//                             alert("该号码正在被装机或正在被换号，无法重新装机和换号");
//                             flag= false;
//                             return;
//                         }
//                     }
//                 },
//                 error: function (err) {
//                     flag= false;
//                 }
//             });
//         }
//     }
//     if(!flag) {
//         // alert("该号码已经被装机或正在被装机，无法重新装机");
//         return false;
//     }
//
//     //移副机前先判断是否有副机
//     if(task_type == 1 || task_type == 8 || task_type == 12){
//         var YongHuDiZhi2=$("input[type='checkbox'][name='CB_YongHuDiZhi2']").is(':checked');
//         if(YongHuDiZhi2 == true){//选了移副机
//             var isAuxiliaryEngine = DBAccess.RunSQLReturnVal("SELECT is_auxiliary_engine\n" +
//                 "FROM pm_install_information\n" +
//                 "WHERE notification_number = '"+ notification_number +"' ");
//             if(isAuxiliaryEngine == 0){
//                 alert("该号码没有副机，无法移副机");
//                 return false;
//             }
//         }
//     }
//     //撤副机前先判断是否有副机
//     if(task_type == 3){
//         var RB_XuanZeCheJiFangShi=$('input[name="RB_XuanZeCheJiFangShi"]:checked').val();
//         if(RB_XuanZeCheJiFangShi == "2" || RB_XuanZeCheJiFangShi == "0"){
//             var isAuxiliaryEngine = DBAccess.RunSQLReturnVal("SELECT is_auxiliary_engine\n" +
//                 "FROM pm_install_information\n" +
//                 "WHERE notification_number = '"+ notification_number +"' ");
//             if(isAuxiliaryEngine == 0){
//                 alert("该号码没有副机，无法撤除副机");
//                 return false;
//             }
//         }
//     }
//
//     //审批的业务处理节点，不需要重新生成通知单号和该号码存在未完成的业务
//     if(SPSF!=1){
//         //判断是否存在未完成的业务
//         var phoneNumberSQL = DBAccess.RunSQLReturnVal("SELECT COUNT(ppn.phone_number) \n" +
//             "FROM pm_phone_number ppn \n" +
//             "LEFT JOIN pm_install_information pii ON pii.number_id = ppn.id \n" +
//             "WHERE pii.business_status = 0 AND ppn.phone_number = '"+ phone_number +"' ");
//
//         if(phoneNumberSQL < 1){
//             alert("该号码存在未完成的业务");
//             return false;
//         }
//         //生成通知单号
//         $.ajax({
//             url: "../restful/setNoticeOrderNumber",
//             type: "post",
//             dataType: "text",
//             async:false,
//             data: {
//                 task_type: task_type
//             },
//             success: function (res) {
//                 if (res.length == 0) {
//                     flag= false;
//                 } else {
//                     $("#TB_NEWTongZhiDanHao").val(res);//新的通知单号
//                 }
//             },
//             error: function (err) {
//                 flag= false;
//             }
//         });
//         if(!flag){
//             alert("生成通知单号失败，数据异常，请联系管理员");
//             return false;
//         }
//     }
//
//
//
//     //设置该号码已经有业务在进行中
//     var count = DBAccess.RunSQL("\n" +
//         "UPDATE pm_install_information \n" +
//         "SET business_status = 1 \n" +
//         "WHERE notification_number = '" + notification_number+"' ");
//     if(count < 1){
//         alert("数据异常，请联系管理员");
//         return false;
//     }
//     return true;
// }

// function checkSPPhoneNumber() {
//     // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
//     // var phone_number = $("#TB_DianHuaHaoMa").val();
//     var task_type = $("#TB_YWLX").val();
//     var number_use = $("#DDL_DHYT").val();
//     var number_nature = $("#DDL_DHXZ").val();
//     var point_name = $("#JuDian_mtags span").text();
//     var cost_group_name = $("#TB_JiFeiFenZu").val();
//     var checkZJPhoneNumber = 0;
//     var notification_number = $("#TB_TongZhiDanHao").val(); //旧的通知单号
//     if(null == task_type || task_type=="" || task_type== undefined){
//         alert("业务类型不能为空");
//         return false;
//     }
//     if(null == number_nature ||number_nature=="" || number_nature== undefined){
//         alert("电话性质不能为空");
//         return false;
//     }
//     //审批装机
//     if(task_type==0){
//         if(number_nature == 0 || number_nature == 3) {  //0：地方号；3：密话
//             if (point_name == ""|| point_name == undefined) {
//                 alert("局点不能为空");
//                 return false;
//             }
//         }
//         if(number_nature == 2){
//             if(undefined == number_use ||number_use==""){
//                 alert("电话用途不能为空");
//                 return false;
//             }
//             if(undefined == point_name ||point_name==""){
//                 alert("局点不能为空");
//                 return false;
//             }
//             if(undefined == cost_group_name   ||cost_group_name==""){
//                 alert("计费分组不能为空");
//                 return false;
//             }
//             var point_name_list = point_name.toString().split("：");
//             if(point_name_list==undefined || point_name_list.length<2){
//                 alert("局点不能为空");
//                 return false;
//             }
//             var point_name_value = point_name_list[1];
//             if(point_name_value==undefined){
//                 alert("局点不能为空");
//                 return false;
//             }
//
//             var point_id = DBAccess.RunSQLReturnVal("\tSELECT No AS point_id\n" +
//                 "\t\tFROM port_dept\n" +
//                 "\t\tWHERE Name  ='" + point_name_value + "' ");
//
//             var cost_group = DBAccess.RunSQLReturnVal("\tSELECT id AS cost_group\n" +
//                 "\t\tFROM com_company_dept\n" +
//                 "\t\tWHERE dept_name = '" +cost_group_name + "'");
//
//             if(undefined == point_id || point_id==""){
//                 alert("局点不能为空");
//                 return false;
//             }
//             if(undefined == cost_group || cost_group==""){
//                 alert("计费分组不能为空");
//                 return false;
//             }
//         }
//     }else{
//         var phone_number = $("#TB_DianHuaHaoMa").val();
//         if(task_type == 4){//宿舍号码才能过户
//             if(number_use != 1){
//                 alert("宿舍号码才能过户");
//                 return false;
//             }
//         }
//         if(task_type == 6){//办公号码才能改名
//             if(number_use != 0){
//                 alert("办公号码才能改名");
//                 return false;
//             }
//         }
//         //移副机前先判断是否有副机
//         if(task_type == 1 || task_type == 8 || task_type == 12){
//             var YongHuDiZhi2=$("input[type='checkbox'][name='CB_YongHuDiZhi2']").is(':checked');
//             if(YongHuDiZhi2 == true){//选了移副机
//                 var isAuxiliaryEngine = DBAccess.RunSQLReturnVal("SELECT is_auxiliary_engine\n" +
//                     "FROM pm_install_information\n" +
//                     "WHERE notification_number = '"+ notification_number +"' ");
//                 if(isAuxiliaryEngine == 0){
//                     alert("该号码没有副机，无法移副机");
//                     return false;
//                 }
//             }
//         }
//         //撤副机前先判断是否有副机
//         if(task_type == 3){
//             var RB_XuanZeCheJiFangShi=$('input[name="RB_XuanZeCheJiFangShi"]:checked').val();
//             if(RB_XuanZeCheJiFangShi == "2" || RB_XuanZeCheJiFangShi == "0"){
//                 var isAuxiliaryEngine = DBAccess.RunSQLReturnVal("SELECT is_auxiliary_engine\n" +
//                     "FROM pm_install_information\n" +
//                     "WHERE notification_number = '"+ notification_number +"' ");
//                 if(isAuxiliaryEngine == 0){
//                     alert("该号码没有副机，无法撤除副机");
//                     return false;
//                 }
//             }
//         }
//         //0：地方号；2：自动号；3：密话
//         if(number_nature != 1) {
//             if(undefined == phone_number || ""==phone_number){
//                 alert("电话号码不能为空，请先获取号码信息");
//                 return false;
//             }
//             //判断是否存在未完成的业务
//             var phoneNumberSQL = DBAccess.RunSQLReturnVal("SELECT COUNT(ppn.phone_number) \n" +
//                 "FROM pm_phone_number ppn \n" +
//                 "LEFT JOIN pm_install_information pii ON pii.number_id = ppn.id \n" +
//                 "WHERE pii.business_status = 0 AND ppn.phone_number = '"+ phone_number +"' ");
//
//             if(phoneNumberSQL < 1){
//                 alert("该号码存在未完成的业务");
//                 return false;
//             }
//         }
//
//         if(task_type == 5){//换号
//             var newPhoneNumber = $("#TB_XDHHM").val();
//             if(undefined == newPhoneNumber || ""==newPhoneNumber){
//                 alert("新的号码不能为空");
//                 return false;
//             }
//             if(number_nature==2 || number_nature==3){//自动号或者密话
//                 var checkNewPhoneNumber = 0;
//                 var flag = true;
//                 $.ajax({
//                     url: "../restful/getIsUsingPhoneNumberTable",
//                     type: "post",
//                     dataType: "JSON",
//                     async:false,
//                     data: {},
//                     success: function (res) {
//                         if (res.length == 0) {
//                             alert("获取装机表和换号表失败");
//                             flag = false;
//                         } else {
//                             checkNewPhoneNumber = DBAccess.RunSQLReturnVal("SELECT phone_number\n" +
//                                 "      FROM pm_phone_number\n" +
//                                 "      WHERE is_using = 0 AND phone_number= '"+newPhoneNumber+"' \n");
//                             if(checkNewPhoneNumber == null){
//                                 alert("新号码不存在或已经被装机，无法重新装机");
//                                 flag = false;
//                                 return;
//                             }
//                             checkNewPhoneNumber = DBAccess.RunSQLReturnVal("SELECT DianHuaHaoMa\n" +
//                                 "        FROM "+res.ZJ+" nd13\n" +
//                                 "        LEFT JOIN wf_generworkflow wf ON nd13.OID = wf.WorkID\n" +
//                                 "        WHERE nd13.DianHuaHaoMa = '"+newPhoneNumber+"' AND wf.WFSta = 0\n" +
//                                 "\n" +
//                                 "        UNION ALL\n" +
//                                 "\n" +
//                                 "        SELECT DianHuaHaoMa\n" +
//                                 "        FROM "+res.SPZJ+" nd11\n" +
//                                 "        LEFT JOIN wf_generworkflow wf ON nd11.OID = wf.WorkID\n" +
//                                 "        WHERE nd11.DianHuaHaoMa = '"+newPhoneNumber+"' AND wf.WFSta = 0" +
//                                 "        UNION ALL\n" +
//                                 "\n" +
//                                 "        SELECT DianHuaHaoMa\n" +
//                                 "        FROM "+res.HH+" nd41\n" +
//                                 "        LEFT JOIN wf_generworkflow wf ON nd41.OID = wf.WorkID\n" +
//                                 "        WHERE nd41.XDHHM = '"+newPhoneNumber+"' AND wf.WFSta = 0\n" +
//                                 "\n" +
//                                 "        UNION ALL\n" +
//                                 "\n" +
//                                 "        SELECT DianHuaHaoMa\n" +
//                                 "        FROM "+res.SPHH+" nd28\n" +
//                                 "        LEFT JOIN wf_generworkflow wf ON nd28.OID = wf.WorkID\n" +
//                                 "        WHERE nd28.XDHHM = '"+newPhoneNumber+"' AND wf.WFSta = 0");
//                             if(checkNewPhoneNumber != null){
//                                 alert("该号码正在被装机或正在被换号，无法重新装机和换号");
//                                 flag = false;
//                                 return;
//                             }
//                         }
//                         alert(flag);
//                     },
//                     error: function (err) {
//                         flag = false;
//                     }
//                 });
//             }
//             if(!flag) {
//                 alert("flag："+flag);
//                 // alert("该号码已经被装机或正在被装机，无法重新装机");
//                 return false;
//             }
//         }
//     }
//     //生成审批通知单号
//     $.ajax({
//         url: "../restful/setNoticeOrderNumber",
//         type: "post",
//         dataType: "text",
//         async:false,
//         data: {
//             task_type: "21"//代表审批
//         },
//         success: function (res) {
//             if (res.length == 0) {
//                 alert("生成审批通知单号失败，数据异常，请联系管理员");
//                 checkZJPhoneNumber = 1;
//             }else if(res=="error"){
//                 alert("服务器异常，请联系管理员");
//                 checkZJPhoneNumber = 1;
//             } else{
//                 $("#TB_NEWSPTongZhiDanHao").val(res);//新的审批通知单号
//             }
//         },
//         error: function (err) {
//             alert("生成通知单号失败，数据异常，请联系管理员 "+err);
//             checkZJPhoneNumber = 1;
//         }
//     });
//     if(checkZJPhoneNumber == 1){
//         return false;
//     }
//     //生成通知单号
//     $.ajax({
//         url: "../restful/setNoticeOrderNumber",
//         type: "post",
//         dataType: "text",
//         async:false,
//         data: {
//             task_type: task_type
//         },
//         success: function (res) {
//             if (res.length == 0) {
//                 alert("生成通知单号失败");
//                 checkZJPhoneNumber= 1;
//             } else {
//                 $("#TB_NEWTongZhiDanHao").val(res);//新的通知单号
//             }
//         },
//         error: function (err) {
//             checkZJPhoneNumber= 1;
//         }
//     });
//
//     if(checkZJPhoneNumber == 1){
//         return false;
//     }
//
//     //不是审批装机的话，刚开始没有号码
//     if(task_type!=0){
//         //设置该号码已经有业务在进行中
//         var count = DBAccess.RunSQL("\n" +
//             "UPDATE pm_install_information \n" +
//             "SET business_status = 1 \n" +
//             "WHERE notification_number = '" + notification_number+"' ");
//         if(count < 1){
//             alert("数据异常，请联系管理员");
//             return false;
//         }
//     }
//
//     return true;
// }

function checkZJPhoneNumber1(SFSP) {
    // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
    var phone_number = $("#TB_DianHuaHaoMa").val();
    var task_type = $("#TB_YWLX").val();
    var number_use = $("#DDL_DHYT").val();
    var number_nature = $("#DDL_DHXZ").val();
    var point_name = $("#JuDian_mtags span").text();
    var cost_group_name = $("#TB_JiFeiFenZu").val();
    var specialPhoneLineNumber = $("#TB_ZhuanXianTaiHao").val();
    var specialPhoneLineCodeName = $("#TB_ZhuanXianDaiHao").val();
    var checkZJPhoneNumber = 0;
    if(null == task_type || task_type=="" || task_type== undefined){
        alert("业务类型不能为空");
        return false;
    }
    if(null == number_nature ||number_nature=="" || number_nature== undefined){
        alert("电话性质不能为空");
        return false;
    }

    //0：地方号；3：密话（自动号在后面验证）
    if(number_nature == 0 || number_nature == 3) {
        if (null == point_name || point_name == ""|| point_name == undefined) {
            alert("局点不能为空");
            return false;
        }
        if (null == phone_number || phone_number == "" || phone_number==undefined) {
            alert("电话号码不能为空");
            return false;
        }
    }
    //专线号
    if(number_nature == 1){
        if (undefined == specialPhoneLineNumber || specialPhoneLineNumber == "" || null == specialPhoneLineCodeName || specialPhoneLineCodeName == "" || specialPhoneLineCodeName == undefined) {
            alert("专线台号和专线代号不能为空");
            return false;
        }
    }
    //2：自动号
    if(number_nature == 2){
        if(undefined == phone_number || phone_number==""){
            alert("电话号码不能为空");
            return false;
        }
        if(undefined == number_use ||number_use==""){
            alert("电话用途不能为空");
            return false;
        }
        if(undefined == point_name ||point_name==""){
            alert("局点不能为空");
            return false;
        }
        if(undefined == cost_group_name   ||cost_group_name==""){
            alert("计费分组不能为空");
            return false;
        }

        var point_name_list = point_name.toString().split("：");
        if(point_name_list==undefined || point_name_list.length<2){
            alert("局点不能为空");
            return false;
        }
        var point_name_value = point_name_list[1];
        if(point_name_value==undefined){
            alert("局点不能为空");
            return false;
        }

        var point_id = DBAccess.RunSQLReturnVal("\tSELECT No AS point_id\n" +
            "\t\tFROM port_dept\n" +
            "\t\tWHERE Name  ='" + point_name_value + "' ");

        var cost_group = DBAccess.RunSQLReturnVal("\tSELECT id AS cost_group\n" +
            "\t\tFROM com_company_dept\n" +
            "\t\tWHERE dept_name = '" +cost_group_name + "'");

        if(undefined == point_id || point_id==""){
            alert("局点不能为空");
            return false;
        }
        if(undefined == cost_group || cost_group==""){
            alert("计费分组不能为空");
            return false;
        }

        //判断该电话号码是否合法
        $.ajax({
            url: "../restful/checkZJPhoneNumber",
            type: "post",
            dataType: "text",
            async:false,
            data: {
                task_type: task_type,
                phone_number:phone_number,
                number_use:number_use,
                point_id:point_id,
                cost_group:cost_group
            },
            success: function (res) {
                if (res.length == 0) {
                    alert("数据异常，请联系管理员");
                    checkZJPhoneNumber = 1;
                } else if (res == "success"){

                } else if (res == "nullPhoneNumber"){
                    alert("电话号码不能为空");
                    checkZJPhoneNumber = 1;
                } else if (res == "nullPointID"){
                    alert("局点不能为空");
                    checkZJPhoneNumber = 1;
                } else if (res == "nullCostGroup"){
                    alert("计费分组不能为空");
                    checkZJPhoneNumber = 1;
                } else if (res == "phoneNumberNotExsite"){
                    alert("号码不存在");
                    checkZJPhoneNumber = 1;
                } else if (res == "phoneNumberIsUsing"){
                    alert("号码被使用");
                    checkZJPhoneNumber = 1;
                } else if (res == "phoneIsNotInPoint"){
                    alert("号码不在局点内");
                    checkZJPhoneNumber = 1;
                } else if(res=="phoneNumberIsInstalling"){
                    alert("电话号码正在被装机或换号");
                    checkZJPhoneNumber = 1;
                }
                else{
                    alert("数据异常，请联系管理员");
                    checkZJPhoneNumber = 1;
                }
            },
            error: function (err) {
                alert("数据连接异常，请联系管理员 "+err);
                checkZJPhoneNumber = 1;
            }
        });
    }

    if(checkZJPhoneNumber == 1){
        return false;
    }

    //审批装机就不用重复生成通知单号
    if(SFSP!=1){
        //生成通知单号
        $.ajax({
            url: "../restful/setNoticeOrderNumber",
            type: "post",
            dataType: "text",
            async:false,
            data: {
                task_type: task_type
            },
            success: function (res) {
                if (res.length == 0) {
                    alert("生成通知单号失败，数据异常，请联系管理员");
                    checkZJPhoneNumber = 1;
                } else {
                    $("#TB_NEWTongZhiDanHao").val(res);//新的通知单号
                }
            },
            error: function (err) {
                alert("生成通知单号失败，数据异常，请联系管理员 "+err);
                checkZJPhoneNumber = 1;
            }
        });

        if(checkZJPhoneNumber == 1){
            return false;
        }
    }
    return true;
}


// function checkPhoneNumber() {
//     // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
//     var phone_number = $("#TB_DianHuaHaoMa").val();
//     var notification_number = $("#TB_TongZhiDanHao").val(); //旧的通知单号
//     var task_type = $("#TB_YWLX").val();
//     var number_use = $("#DDL_DHYT").val();//0：办公；1：宿舍；2：测试；3：中继；
//     var number_nature = $("#DDL_DHXZ").val();
//
//     var specialPhoneLineNumber = $("#TB_ZhuanXianTaiHao").val();
//     var specialPhoneLineCodeName = $("#TB_ZhuanXianDaiHao").val();
//
//     if(task_type=="" || undefined == task_type){
//         alert("业务类型为空，请先获取电话信息");
//         return false;
//     }
//     if(number_nature=="" || undefined == number_nature){
//         alert("电话性质为空，请先获取电话信息");
//         return false;
//     }
//     if(number_nature != 3){//3:密话不用判断电话用途
//         if(undefined == number_use || "" == number_use){
//             alert("电话用途为空，请先获取电话信息");
//             return false;
//         }
//     }
//     if(number_nature == 1){
//         if (undefined == specialPhoneLineNumber || specialPhoneLineNumber == "" || undefined == specialPhoneLineCodeName || specialPhoneLineCodeName == "") {
//             alert("专线台号和专线代号不能为空");
//             return false;
//         }
//     }else {
//         if(undefined == phone_number || phone_number==""){
//             alert("电话号码为空，请先获取电话信息");
//             return false;
//         }
//     }
//     if(number_use == 4){//宿舍号码才能过户
//         if(number_use != 1){
//             alert("宿舍号码才能过户");
//             return false;
//         }
//     }
//     if(number_use == 6){//办公号码才能改名
//         if(number_use != 0){
//             alert("办公号码才能改名");
//             return false;
//         }
//     }
//
//     //移副机前先判断是否有副机
//     if(task_type == 1 || task_type == 8 || task_type == 12){
//         var YongHuDiZhi2=$("input[type='checkbox'][name='CB_YongHuDiZhi2']").is(':checked');
//         if(YongHuDiZhi2 == true){//选了移副机
//             var isAuxiliaryEngine = DBAccess.RunSQLReturnVal("SELECT is_auxiliary_engine\n" +
//                 "FROM pm_install_information\n" +
//                 "WHERE notification_number = '"+ notification_number +"' ");
//             if(isAuxiliaryEngine == 0){
//                 alert("该号码没有副机，无法移副机");
//                 return false;
//             }
//         }
//     }
//     //撤副机前先判断是否有副机
//     if(task_type == 3){
//         var RB_XuanZeCheJiFangShi=$('input[name="RB_XuanZeCheJiFangShi"]:checked').val();
//         if(RB_XuanZeCheJiFangShi == "2" || RB_XuanZeCheJiFangShi == "0"){
//             var isAuxiliaryEngine = DBAccess.RunSQLReturnVal("SELECT is_auxiliary_engine\n" +
//                 "FROM pm_install_information\n" +
//                 "WHERE notification_number = '"+ notification_number +"' ");
//             if(isAuxiliaryEngine == 0){
//                 alert("该号码没有副机，无法撤除副机");
//                 return false;
//             }
//         }
//     }
//
//     //判断是否存在未完成的业务
//     var phoneNumberSQL = DBAccess.RunSQLReturnVal("SELECT COUNT(ppn.phone_number) \n" +
//         "FROM pm_phone_number ppn \n" +
//         "LEFT JOIN pm_install_information pii ON pii.number_id = ppn.id \n" +
//         "WHERE pii.business_status = 0 AND ppn.phone_number = '"+ phone_number +"' ");
//
//     if(phoneNumberSQL < 1){
//         alert("该号码存在未完成的业务");
//         return false;
//     }
//     //生成通知单号
//     var flag=true;
//     $.ajax({
//         url: "../restful/setNoticeOrderNumber",
//         type: "post",
//         dataType: "text",
//         async:false,
//         data: {
//             task_type: task_type
//         },
//         success: function (res) {
//             if (res.length == 0) {
//                 flag= false;
//             } else {
//                 $("#TB_NEWTongZhiDanHao").val(res);//新的通知单号
//             }
//         },
//         error: function (err) {
//             flag= false;
//         }
//     });
//     if(!flag){
//         alert("生成通知单号失败，数据异常，请联系管理员");
//         return false;
//     }
//     //设置该号码已经有业务在进行中
//     var count = DBAccess.RunSQL("\n" +
//         "UPDATE pm_install_information \n" +
//         "SET business_status = 1 \n" +
//         "WHERE notification_number = '" + notification_number+"' ");
//     if(count < 1){
//         alert("数据异常，请联系管理员");
//         return false;
//     }
//     return true;
// }
//
// function checkZJPhoneNumber() {
//     // task_type 业务类型（0：装机；1：移机；2：并机；3：撤机；4：过户；5：换号；6：改名；7：改类；8：移机+改类；9：并机+改类；10：过户+改类；11：改名+改类；12：改名+移机）
//     var phone_number = $("#TB_DianHuaHaoMa").val();
//     var task_type = $("#TB_YWLX").val();
//     var number_use = $("#DDL_DHYT").val();
//     var number_nature = $("#DDL_DHXZ").val();
//     var point_name = $("#JuDian_mtags span").text();
//     var cost_group_name = $("#TB_JiFeiFenZu").val();
//     var specialPhoneLineNumber = $("#TB_ZhuanXianTaiHao").val();
//     var specialPhoneLineCodeName = $("#TB_ZhuanXianDaiHao").val();
//     var checkZJPhoneNumber = 0;
//     if(null == task_type || task_type=="" || task_type== undefined){
//         alert("业务类型不能为空");
//         return false;
//     }
//     if(null == number_nature ||number_nature=="" || number_nature== undefined){
//         alert("电话性质不能为空");
//         return false;
//     }
//
//     //0：地方号；3：密话
//     if(number_nature == 0 || number_nature == 3) {
//         if (null == point_name || point_name == ""|| point_name == undefined) {
//             alert("局点不能为空");
//             return false;
//         }
//         if (null == phone_number || phone_number == "" || phone_number==undefined) {
//             alert("电话号码不能为空");
//             return false;
//         }
//     }
//     //专线号
//     if(number_nature == 1){
//         if (undefined == specialPhoneLineNumber || specialPhoneLineNumber == "" || null == specialPhoneLineCodeName || specialPhoneLineCodeName == "" || specialPhoneLineCodeName == undefined) {
//             alert("专线台号和专线代号不能为空");
//             return false;
//         }
//     }
//     //2：自动号
//     if(number_nature == 2){
//         if(undefined == phone_number || phone_number==""){
//             alert("电话号码不能为空");
//             return false;
//         }
//         if(undefined == number_use ||number_use==""){
//             alert("电话用途不能为空");
//             return false;
//         }
//         if(undefined == point_name ||point_name==""){
//             alert("局点不能为空");
//             return false;
//         }
//         if(undefined == cost_group_name   ||cost_group_name==""){
//             alert("计费分组不能为空");
//             return false;
//         }
//
//         var point_name_list = point_name.toString().split("：");
//         if(point_name_list==undefined || point_name_list.length<2){
//             alert("局点不能为空");
//             return false;
//         }
//         var point_name_value = point_name_list[1];
//         if(point_name_value==undefined){
//             alert("局点不能为空");
//             return false;
//         }
//
//         var point_id = DBAccess.RunSQLReturnVal("\tSELECT No AS point_id\n" +
//             "\t\tFROM port_dept\n" +
//             "\t\tWHERE Name  ='" + point_name_value + "' ");
//
//         var cost_group = DBAccess.RunSQLReturnVal("\tSELECT id AS cost_group\n" +
//             "\t\tFROM com_company_dept\n" +
//             "\t\tWHERE dept_name = '" +cost_group_name + "'");
//
//         if(undefined == point_id || point_id==""){
//             alert("局点不能为空");
//             return false;
//         }
//         if(undefined == cost_group || cost_group==""){
//             alert("计费分组不能为空");
//             return false;
//         }
//
//         //判断该电话号码是否合法
//         $.ajax({
//             url: "../restful/checkZJPhoneNumber",
//             type: "post",
//             dataType: "text",
//             async:false,
//             data: {
//                 task_type: task_type,
//                 phone_number:phone_number,
//                 number_use:number_use,
//                 point_id:point_id,
//                 cost_group:cost_group
//             },
//             success: function (res) {
//                 if (res.length == 0) {
//                     alert("判断电话号码是合法失败，数据异常，请联系管理员");
//                     checkZJPhoneNumber = 1;
//                 } else if (res == "success"){
//
//                 } else if (res == "nullPhoneNumber"){
//                     alert("电话号码不能为空");
//                     checkZJPhoneNumber = 1;
//                 } else if (res == "nullPointID"){
//                     alert("局点不能为空");
//                     checkZJPhoneNumber = 1;
//                 } else if (res == "nullCostGroup"){
//                     alert("计费分组不能为空");
//                     checkZJPhoneNumber = 1;
//                 } else if (res == "phoneNumberNotExsite"){
//                     alert("号码不存在或已被使用");
//                     checkZJPhoneNumber = 1;
//                 } else if (res == "phoneIsNotInPoint"){
//                     alert("号码不在局点内");
//                     checkZJPhoneNumber = 1;
//                 } else if(res=="phoneNumberIsInstalling"){
//                     alert("电话号码已经被装机");
//                     checkZJPhoneNumber = 1;
//                 } else{
//                     alert("数据异常，请联系管理员");
//                     checkZJPhoneNumber = 1;
//                 }
//             },
//             error: function (err) {
//                 alert("数据连接异常，请联系管理员 "+err);
//                 checkZJPhoneNumber = 1;
//             }
//         });
//     }
//
//     if(checkZJPhoneNumber == 1){
//         return false;
//     }
//
//     //生成通知单号
//     $.ajax({
//         url: "../restful/setNoticeOrderNumber",
//         type: "post",
//         dataType: "text",
//         async:false,
//         data: {
//             task_type: task_type
//         },
//         success: function (res) {
//             if (res.length == 0) {
//                 alert("生成通知单号失败，数据异常，请联系管理员");
//                 checkZJPhoneNumber = 1;
//             } else {
//                 $("#TB_NEWTongZhiDanHao").val(res);//新的通知单号
//             }
//         },
//         error: function (err) {
//             alert("生成通知单号失败，数据异常，请联系管理员 "+err);
//             checkZJPhoneNumber = 1;
//         }
//     });
//
//     if(checkZJPhoneNumber == 1){
//         return false;
//     }
//     return true;
// }

/*新增的公共代码END*/


function test(){
    alert("test");
}
function test1(){
    alert("test1");
}

$(function(){
    var timer=setInterval(function(){
        //dynemicTongZhi();
        clearInterval(timer);
    },2000);
});
