package BP.Frm;

import BP.En.EntityMyPKAttr;

public class FrmStationDeptAttr extends EntityMyPKAttr
{

    /// <summary>
    /// 表单
    /// </summary>
    public static final String FK_Frm = "FK_Frm";
    /// <summary>
    /// 工作岗位
    /// </summary>
    public static final String FK_Station = "FK_Station";
    /// <summary>
    /// 部门
    /// </summary>
    public static final String FK_Dept = "FK_Dept";

}
