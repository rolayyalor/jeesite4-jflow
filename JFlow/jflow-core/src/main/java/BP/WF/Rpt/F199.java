package BP.WF.Rpt;

import BP.DA.*;
import BP.DTS.*;
import BP.En.*;
import BP.Web.*;
import BP.Sys.*;
import BP.WF.*;

/**
 * 报销流程001 此类库必须放入到 BP.*.dll 才能被解析发射出来。
 * 
 */
public class F199 extends BP.WF.FlowEventBase {
	// C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
	/// #region 属性.
	/**
	 * 重写流程标记
	 * 
	 */
	@Override
	public String getFlowMark() {
		return "199";
	}

	/**
	 * 报销流程事件
	 * 
	 */
	public F199() {
		
	}
	

}